<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Authentication routes
Route::group(['namespace' => 'Auth'], function(){
// Auth routes..
    Route::get('/auth/register',  ['as' => 'getregister', 'uses' => 'AuthenticationController@getRegister'])->middleware('guest');
    Route::post('/auth/register', ['as' => 'postregister', 'uses' => 'AuthenticationController@postRegister'])->middleware('guest');
    Route::get('/auth/login',  ['as' => 'getlogin', 'uses' => 'AuthenticationController@getLogin'])->middleware('guest');
    Route::post('/auth/login', ['as' => 'postlogin', 'uses' =>'AuthenticationController@postLogin'])->middleware('guest');
    Route::get('/auth/logout', ['as' => 'getlogout', 'uses' =>'AuthenticationController@getLogout'])->middleware('auth');
// Username name request
    Route::get('/auth/username', ['as' => 'getusername', 'uses' =>'AuthenticationController@getUsername'])->middleware(['username', 'auth']);
    Route::post('/auth/username', ['as' => 'postusername', 'uses' =>'AuthenticationController@postUsername'])->middleware(['username', 'auth']);

// Session control (Ajax)
    Route::post('/auth/sessioncheck',  ['as' => 'sessioncheck', 'uses' => 'AuthenticationController@sessionCheck']);

// Social auth routes..
    Route::get('/{provider}/auth', ['as' => 'providerauth', 'uses' =>'SocialAuthController@providerAuthorize'])->middleware('guest');
    Route::get('/{provider}/callback', ['as' => 'providercallback', 'uses' =>'SocialAuthController@providerCallback'])->middleware('guest');

// Activation routes...
    Route::get('/auth/activate/{id}/{code}',  ['as' => 'getactivate', 'uses' => 'ActivationController@getActivate'])->middleware('guest');
    Route::get('/auth/reactivate',  ['as' => 'getreactivate', 'uses' => 'ActivationController@getReactivate'])->middleware('guest');
    Route::post('/auth/reactivate',  ['as' => 'postreactivate', 'uses' => 'ActivationController@postReactivate'])->middleware('guest');

// Password reset link request routes...
    Route::get('/auth/password/email', ['as' => 'getreset', 'uses' =>  'PasswordController@getResetMail'])->middleware('guest');
    Route::post('/auth/password/email', ['as' => 'postreset', 'uses' =>  'PasswordController@postResetMail'])->middleware('guest');

// Password reset routes...
    Route::get('/auth/password/reset/{id}/{code}',  ['as' => 'getpassreset', 'uses' => 'PasswordController@getReset'])->middleware('guest');
    Route::post('/auth/password/reset/{id}/{code}',  ['as' => 'postpassreset', 'uses' => 'PasswordController@postReset'])->middleware('guest');
});

// Admin Dashboard routes
Route::group(['namespace' => 'Backend', 'middleware' => ['admin', 'username', 'ban']], function(){
    Route::get('/dashboard',  ['as' => 'getdashboard', 'uses' => 'DashboardController@getDashboard']);

    // Settings
    Route::get('/dashboard/settings',  ['as' => 'getsettings', 'uses' => 'SettingsController@getSettings']);
    Route::post('/dashboard/settings/general',  ['as' => 'postgeneralsettings', 'uses' => 'SettingsController@postGeneralSettings']);
    Route::post('/dashboard/settings/gallery',  ['as' => 'postgallerysettings', 'uses' => 'SettingsController@postGallerySettings']);
    Route::post('/dashboard/settings/seo',  ['as' => 'postseosettings', 'uses' => 'SettingsController@postSeoSettings']);
    Route::post('/dashboard/settings/sitemap',  ['as' => 'postsitemapsettings', 'uses' => 'SettingsController@postSitemapSettings']);
    Route::post('/dashboard/settings/analytics',  ['as' => 'postanalyticssettings', 'uses' => 'SettingsController@postAnalyticsSettings']);

    // User operations
    Route::get('/dashboard/users',  ['as' => 'getusers', 'uses' => 'UsersController@getUsers']);
    //Route::post('/user/create',  ['as' => 'postcreateuser', 'uses' => 'UsersController@postCreateUser']);
    Route::post('/user/edit',  ['as' => 'postedituser', 'uses' => 'UsersController@postEditUser']);
    Route::post('/user/delete',  ['as' => 'postdeleteusers', 'uses' => 'UsersController@postDeleteUsers']);

    // Galleries
    Route::get('/dashboard/galleries',  ['as' => 'getgalleries', 'uses' => 'GalleriesController@getGalleries']);
    Route::post('/dashboard/galleries/create',  ['as' => 'postcreategallery', 'uses' => 'GalleriesController@postCreateGallery']);
    Route::post('/dashboard/galleries/reorder',  ['as' => 'postreordergalleries', 'uses' => 'GalleriesController@postReorderGalleries']);
    Route::post('/dashboard/galleries/edit',  ['as' => 'posteditgallery', 'uses' => 'GalleriesController@postEditGallery']);
    Route::post('/dashboard/galleries/delete',  ['as' => 'postdeletegallery', 'uses' => 'GalleriesController@postDeleteGallery']);

    // Wallpapers
    Route::get('/dashboard/wallpapers',  ['as' => 'getwallpapers', 'uses' => 'WallpapersController@getWallpapers']);
    Route::post('/dashboard/wallpapers/scan',  ['as' => 'postgalleryscan', 'uses' => 'WallpapersController@postScanGallery']);
    Route::post('/dashboard/wallpapers/process',  ['as' => 'postwallpaperprocess', 'uses' => 'WallpapersController@postProcessWallpaper']);
    Route::post('/dashboard/wallpapers/edit',  ['as' => 'postwallpaperedit', 'uses' => 'WallpapersController@postEditWallpaper']);
    Route::post('/dashboard/wallpapers/move',  ['as' => 'postwallpapermove', 'uses' => 'WallpapersController@postMoveWallpaper']);
    Route::post('/dashboard/wallpapers/publish',  ['as' => 'postwallpaperpublish', 'uses' => 'WallpapersController@postPublishWallpaper']);
    Route::post('/dashboard/wallpapers/delete',  ['as' => 'postwallpaperdelete', 'uses' => 'WallpapersController@postDeleteWallpaper']);

    // Tags
    Route::get('/dashboard/tags',  ['as' => 'gettags', 'uses' => 'TagsController@getTags']);
    Route::post('/dashboard/tags/edit',  ['as' => 'posttagedit', 'uses' => 'TagsController@postEditTag']);
    Route::post('/dashboard/tags/delete',  ['as' => 'posttagdelete', 'uses' => 'TagsController@postDeleteTag']);


    // Site devices
    Route::get('/dashboard/devices',  ['as' => 'getdevices', 'uses' => 'DeviceController@getDevices']);
    Route::post('/device/create',  ['as' => 'postcreatedevice', 'uses' => 'DeviceController@postCreateDevice']);
    Route::post('/device/edit',  ['as' => 'posteditdevice', 'uses' => 'DeviceController@postEditDevice']);
    Route::post('/device/delete',  ['as' => 'postdeletedevices', 'uses' => 'DeviceController@postDeleteDevices']);

    // Moderation
    Route::get('/dashboard/uploads',  ['as' => 'getuploads', 'uses' => 'ModerationController@getUploads']);
    Route::post('/dashboard/uploads/edit',  ['as' => 'postuploadedit', 'uses' => 'ModerationController@postEditUpload']);
    Route::post('/dashboard/uploads/discard',  ['as' => 'postuploaddiscard', 'uses' => 'ModerationController@postDiscardUpload']);
    
});

// Frontend routes
Route::get('/{order?}/{pageSlug?}', ['as' => 'gethome', 'uses' => function ($order = false, $pageSlug = false){
    $params = array ($order, $pageSlug);
    $order = $pageSlug = false;
    foreach ( $params as $p )
    {
        if ( $p && preg_match('('.GALLERY_ORDER_REGEX.')', $p)) { 
            $order = $p; 
        } elseif ( $p && preg_match('(^page-[1-9][0-9]*)', $p)) { 
            $pageSlug = $p; 
        }
    }
    return App::make('App\Http\Controllers\Frontend\HomeController')->getMain($order, $pageSlug);
}])
->where(['order' => '^(page-[1-9][0-9]*|'.GALLERY_ORDER_REGEX.')']) // match page regex in case order is missing (/pageSlug)
->where(['pageSlug' => '^page-[1-9][0-9]*'])
->middleware(['username', 'ban', 'serverpush']);

Route::group(['namespace' => 'Frontend', 'middleware' =>['username', 'ban', 'serverpush']], function(){

  // User Settings
  Route::get('/settings', ['as' => 'getusersettings', 'uses' => 'UserSettingsController@getUserSettings'])->middleware('auth');
  Route::post('/settings', ['as' => 'postusersettings', 'uses' => 'UserSettingsController@postUserSettings'])->middleware('auth');

  // Avatar Upload
  Route::post('/avatarupload', ['as' => 'postuseravatarupload', 'uses' => 'UserSettingsController@postAvatarUpload'])->middleware('auth');
  Route::post('/avatardelete', ['as' => 'postuseravatardelete', 'uses' => 'UserSettingsController@postAvatarDelete'])->middleware('auth');

  // Cover Upload
  Route::post('/coverupload', ['as' => 'postusercoverupload', 'uses' => 'UserSettingsController@postCoverUpload'])->middleware('auth');
  Route::post('/coverdelete', ['as' => 'postusercoverdelete', 'uses' => 'UserSettingsController@postCoverDelete'])->middleware('auth');

  // Upload
  Route::get('/upload', ['as' => 'getimageupload', 'uses' => 'UploadController@getImageUpload'])->middleware('auth');
  Route::post('/upload', ['as' => 'postimageupload', 'uses' => 'UploadController@postImageUpload'])->middleware('auth');
  Route::get('/suggest/tags',  ['as' => 'getsuggesttags', 'uses' => 'UploadController@getSuggestedTags'])->middleware('auth');
  
  // Device Picker
  Route::get('/devicepicker', ['as' => 'getdevicepicker', 'uses' => 'DeviceController@getDevicePicker']);
  Route::get('/devicepicker/set/{id}', ['as' => 'postdevicepicker', 'uses' => 'DeviceController@pickDevice']);
  Route::get('/devicepicker/clear', ['as' => 'postdevicepickerclear', 'uses' => 'DeviceController@clearDevice']);
  Route::post('/devicepicker/search', ['as' => 'postdevicepickersearch', 'uses' => 'DeviceController@searchDevices']);

  // Devices
  Route::get('/{deviceSlug}-wallpapers/{order?}/{pageSlug?}',  ['as' => 'getdevice', 'uses' => function ($deviceSlug, $order = false, $pageSlug = false){
        $params = array ($order, $pageSlug);
        $order = $pageSlug = false;
        foreach ( $params as $p )
        {
            if ( $p && preg_match('('.GALLERY_ORDER_REGEX.')', $p)) { 
                $order = $p; 
            } elseif ( $p && preg_match('(^page-[1-9][0-9]*)', $p)) { 
                $pageSlug = $p; 
            }
        }
        return App::make('App\Http\Controllers\Frontend\DeviceController')->getDevice($deviceSlug, $order, $pageSlug);
    }])
    ->where(['order' => '^(page-[1-9][0-9]*|'.GALLERY_ORDER_REGEX.')']) // match page regex in case order is missing (device/pageSlug)
    ->where(['pageSlug' => '^page-[1-9][0-9]*'])
    ->where(['deviceSlug' => '^[a-z0-9]+(?:-[a-z0-9]+)*$']);

  // Wallpaper + Download
  Route::post('/download/{wallpaper}/{device}', ['as' => 'postdownload', 'uses' => 'WallpapersController@postDownload']);
  Route::post('/favorite/{wallpaper}', ['as' => 'postfavorite', 'uses' => 'WallpapersController@postFavorite'])->middleware('auth');
  Route::post('/view/{wallpaper}', ['as' => 'postView', 'uses' => 'WallpapersController@postView']);
  Route::get('/{wallpaper}', ['as' => 'getwallpaper', 'uses' => 'WallpapersController@getWallpaper'])
    ->where(['wallpaper' => '^.*htm$']);
  
  // Tags
  Route::get('/tags/{tag?}/{pageSlug?}', ['as' => 'gettag', 'uses' => 'TagsController@getTag'])
    ->where(['pageSlug' => '^page-[1-9][0-9]*']);
    
  // Misc
  Route::get('/terms', ['as' => 'getterms', 'uses' => 'HomeController@getTerms']);
  Route::get('/privacy', ['as' => 'getprivacy', 'uses' => 'HomeController@getPrivacy']);
  Route::get('/copyright', ['as' => 'getcopyright', 'uses' => 'HomeController@getCopyright']);
  Route::get('/faq', ['as' => 'getfaq', 'uses' => 'HomeController@getFaq']);
  Route::get('/contact', ['as' => 'getcontact', 'uses' => 'HomeController@getContact']);
  Route::post('/contact', ['as' => 'postcontact', 'uses' => 'HomeController@postContact']);
  
  // Gallery
  // Todo: rule out misc pages routes
  Route::get('/{gallery}/{order?}/{pageSlug?}', ['as' => 'getgallery', 'uses' => function ($gallery, $order = false, $pageSlug = false){
        $params = array ($order, $pageSlug);
        $order = $pageSlug = false;
        foreach ( $params as $p )
        {
            if ( $p && preg_match('('.GALLERY_ORDER_REGEX.')', $p)) { 
                $order = $p; 
            } elseif ( $p && preg_match('(^page-[1-9][0-9]*)', $p)) { 
                $pageSlug = $p; 
            }
        }
        return App::make('App\Http\Controllers\Frontend\GalleriesController')->getGallery($gallery, $order, $pageSlug);
    }])
    ->where(['order' => '^(page-[1-9][0-9]*|'.GALLERY_ORDER_REGEX.')']) // match page regex in case order is missing (gallery/pageSlug)
    ->where(['pageSlug' => '^page-[1-9][0-9]*']);


  // Notifications
  Route::get('/user/notifications', ['as' => 'getusernotifications', 'uses' => 'NotificationsController@getUserNotifications'])->middleware('auth');
  Route::post('/user/notification/mark/{id}', ['as' => 'postmarknotification', 'uses' => 'NotificationsController@postNotificationRead'])->middleware('auth');
  
  // User (Main, Uploads, Favorites)
  Route::get('/user/{username}', ['as' => 'getuser', 'uses' => 'CommunityController@getUser']);
  Route::get('/user/{username}/favorites/{pageSlug?}', ['as' => 'getuserfavorites', 'uses' => 'CommunityController@getUserFavorites'])
  ->where(['pageSlug' => '^page-[1-9][0-9]*']);
  Route::get('/user/{username}/uploads/{pageSlug?}', ['as' => 'getuseruploads', 'uses' => 'CommunityController@getUserUploads'])
  ->where(['pageSlug' => '^page-[1-9][0-9]*']);

  // Search
  Route::get('/search-suggest/{term}', ['as' => 'getsearchsuggestions', 'uses' => 'SearchController@getSuggestions']);
  Route::get('/search/{term}/{pageSlug?}', ['as' => 'getsearchresults', 'uses' => 'SearchController@getResults'])
  ->where(['pageSlug' => '^page-[1-9][0-9]*']);
  
});


if (App::environment('local')) {
    // Subdomains assets
    $wallpaperRoutes = function() {

        Route::get('/'.THUMBS_PATH.'{gallery}/{file}.jpg', function($gallery, $file){
            return response()->file('storage/'.THUMBS_PATH.$gallery.'/'.$file.'.jpg');
        });
        Route::get('/'.PREVIEWS_PATH.'{gallery}/{file}.jpg', function($gallery, $file){
            return response()->file('storage/'.PREVIEWS_PATH.$gallery.'/'.$file.'.jpg');
        });

    };

    Route::group(array('domain' => 'static1.'.env('APP_DOMAIN'), 'middleware' => 'resources'), $wallpaperRoutes);
    Route::group(array('domain' => 'static2.'.env('APP_DOMAIN'), 'middleware' => 'resources'), $wallpaperRoutes);
    Route::group(array('domain' => 'static3.'.env('APP_DOMAIN'), 'middleware' => 'resources'), function() {

        Route::get('/gallery/{file}', function($file){
            return response()->file('storage/'.GALLERY_FEATURES_FOLDER.$file);
        });
        Route::get('/device/{file}', function($file){
            return response()->file('storage/'.DEVICE_FEATURE_IMAGE_PATH.$file);
        });
        Route::get('/avatar/{file}', function($file){
            return response()->file('storage/'.PROFILE_IMAGE_PATH.$file);
        });
        Route::get('/cover/{file}', function($file){
            return response()->file('storage/'.COVER_IMAGE_PATH.$file);
        });
        Route::get('/image/{p1}/{p2?}', function($p1, $p2 = null){
            $path = ($p2)? 'storage/image/'.$p1.'/'.$p2 : 'storage/image/'.$p1;
            return response()->file($path);
        });
        Route::get('/sc/{file}', function($file){ // social covers
            return response()->file('storage/sc/'.$file);
        });
        Route::get('/download/{file}', function($file){
            return response()->file('storage/'.DOWNLOADS_PATH.$file);
        });
        Route::get('/js/{file}', function($file){
            $file = \File::get('storage/js/'.$file);
            $response = \Response::make($file);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        });

        Route::get('/css/{file}', function($file){
            $file = \File::get('storage/css/'.$file);
            $response = \Response::make($file);
            $response->header('Content-Type', 'text/css');
            return $response;
        });

        Route::get('/fonts/{folder}/{file}', function($folder, $file){
            $file = \File::get('storage/fonts/'.$folder.'/'.$file);
            $response = \Response::make($file);
            $response->header('Access-Control-Allow-Origin', '*');
            return $response;
        });
    });
}