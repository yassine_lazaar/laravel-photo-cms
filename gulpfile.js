// --- INIT
var url = "http://laravel.localhost";
var elixir = require('laravel-elixir');
var gulp = require('gulp'),
    debug = require('gulp-debug'),
    less = require('gulp-less'), // compiles less to CSS
    autoprefix = require('gulp-autoprefixer'), // CSS browser compatibility
    cssnano = require('gulp-cssnano'), // minifies CSS
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'), // JS uglify
    rename = require('gulp-rename'),
    cssimport = require('gulp-cssimport'),
    critical = require('critical'),
    // optional
    livereload = require('gulp-livereload'),
    notifier = require('node-notifier');

// Paths variables
var paths = {
    'resources': {
        'css': 'resources/assets/css/',
        'js': 'resources/assets/js/',
        'pages_f': 'resources/assets/_frontend/',
        'pages_b': 'resources/assets/_backend/',
    },
    'dependencies': 'resources/assets/dependencies/',
    'build': {
        'css': 'storage/app/public/css/',
        'js': 'storage/app/public/js/'
    }
};

// --- TASKS

// LESS
gulp.task('less:front', function() {
    return gulp.src(paths.resources.pages_f + 'pages/less/pages.less')
        .pipe(less({
            paths: [paths.resources.pages_f + 'pages/less']
        }))
        .pipe(gulp.dest(paths.resources.pages_f + 'pages/css'));
});

gulp.task('less:back', function() {
    return gulp.src(paths.resources.pages_b + 'pages/less/pages.less')
        .pipe(less({
            paths: [paths.resources.pages_b + 'pages/less']
        }))
        .pipe(gulp.dest(paths.resources.pages_b + 'pages/css'));
});


// CSS
gulp.task('css:front', ['less:front'], function() {
    return gulp.src([
            paths.dependencies + 'css/bootstrap.min.css',
            paths.resources.pages_f + 'libs/css/priority/*.css',
            paths.resources.pages_f + 'libs/css/*.css',
            paths.resources.pages_f + 'pages/css/pages.css',
            paths.resources.css + 'frontend.css'
        ]) // get file
        .pipe(debug())
        .pipe(concat('frontend.css'))
        .pipe(cssimport())
        .pipe(autoprefix({ browsers: ['last 2 versions'] }))
        .pipe(gulp.dest(paths.build.css)) // output: frontend.css
        .pipe(cssnano({ discardComments: { removeAll: true } }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.build.css)); // output: frontend.min.css

});

gulp.task('css:back', ['less:back'], function() {
    return gulp.src([
            paths.dependencies + 'css/bootstrap.min.css',
            paths.resources.pages_b + 'libs/css/priority/*.css',
            paths.resources.pages_b + 'libs/css/*.css',
            paths.resources.pages_b + 'pages/css/pages.css',
            paths.resources.css + 'backend.css'
        ]) // get file
        .pipe(debug())
        .pipe(concat('backend.css'))
        .pipe(cssimport())
        .pipe(autoprefix({ browsers: ['last 2 versions'] }))
        .pipe(gulp.dest(paths.build.css)) // output: backend.css
        .pipe(cssnano({ discardComments: { removeAll: true } }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.build.css)); // output: backend.min.css

});

// JS
gulp.task('js:front', function() {
    return gulp.src([
            paths.dependencies + 'js/jquery/jquery-1.11.1.min.js',
            paths.dependencies + 'js/bootstrap.min.js',
            paths.dependencies + 'js/session-control.js',
            paths.resources.pages_f + 'libs/js/priority/*.js',
            paths.resources.pages_f + 'libs/js/*.js',
            paths.resources.pages_f + 'libs/js/widgets/**/*.js',
            paths.resources.pages_f + 'pages/js/pages.js',
            paths.resources.pages_f + 'widgets/*.js',
            paths.resources.js + 'frontend.js'
        ])
        .pipe(debug())
        .pipe(uglify())
        .pipe(concat('frontend.js'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.build.js)); // destination: /public/assets/js/frontend.min.js

});

gulp.task('js:back', function() {
    return gulp.src([
            paths.dependencies + 'js/jquery/jquery-1.11.1.min.js',
            paths.dependencies + 'js/bootstrap.min.js',
            paths.dependencies + 'js/session-control.js',
            paths.resources.pages_b + 'libs/js/priority/*.js',
            paths.resources.pages_b + 'libs/js/*.js',
            paths.resources.pages_b + 'libs/js/widgets/**/*.js',
            paths.resources.pages_b + 'pages/js/pages.js',
            paths.resources.pages_b + 'widgets/**/*.js',
            paths.resources.js + 'backend.js'
        ])
        .pipe(debug())
        .pipe(uglify({ mangle: { except: ['$super'] } }))
        .pipe(concat('backend.js'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.build.js)); // destination: /public/assets/js/backend.min.js
});

// --- WATCH
// Rerun the task when a file changes
gulp.task('watchChanges', function() {
    gulp.watch(
        [paths.resources.css + 'frontend.css',
            paths.resources.js + 'frontend.js'
        ], ['front']);

    gulp.watch(
        [paths.resources.css + 'backend.css',
            paths.resources.js + 'backend.js'
        ], ['back']);

});

gulp.task('watch', ['watchChanges', 'watchBuilds']);

// --- COMBINED TASKS
gulp.task('front', function() {
    gulp.start('js:front', 'css:front');
    notifier.notify({
        message: 'Frontend CSS+JS compiled'
    });
});
gulp.task('back', function() {
    gulp.start('js:back', 'css:back');
    notifier.notify({
        message: 'Backend CSS+JS compiled'
    });
});
gulp.task('less', function() {
    gulp.start('less:front', 'less:back');
    notifier.notify({
        message: 'LESS compiled'
    });
});
gulp.task('css', function() {
    gulp.start('css:front', 'css:back');
    notifier.notify({
        message: 'CSS compiled'
    });
});
gulp.task('js', function() {
    gulp.start('js:front', 'js:back');
    notifier.notify({
        message: 'JS compiled'
    });
});

gulp.task('version', function() {
    return elixir(function(mix) {
        mix.version(paths.build.css + '*.css');
        mix.version(paths.build.js + '*.js');
    });
});

require('events').EventEmitter.prototype._maxListeners = 100;
//process.stdin.setMaxListeners(0);
var dimensions = [{
    width: 320,
    height: 480
}, {
    width: 768,
    height: 1024
}, {
    width: 1280,
    height: 960
}];

gulp.task('critical', function(cb) {

    // Auth
    /*
    critical.generate({
        src: url+'/auth/login',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontendauthlogin.blade.php',
        minify: true,
        extract: false,
    });*/
    critical.generate({
        src: url+'/auth/register',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontendauth.blade.php',
        minify: true,
        extract: false,
    });
    /*
    // Home
    critical.generate({
        src: url+'',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontend.blade.php',
        minify: true,
        extract: false,
    });

    // Contact
    critical.generate({
        src: url+'/contact',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontendcontact.blade.php',
        minify: true,
        extract: false,
    });

    // Device Picker
    critical.generate({
        src: url+'/devicepicker',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontenddevicepicker.blade.php',
        minify: true,
        extract: false,
    });

    // Misc (tos, privacy ..)
    critical.generate({
        src: url+'/terms',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontendmisc.blade.php',
        minify: true,
        extract: false,
    });

    // Notifications
    critical.generate({
        src: url+'/user/notifications',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontendnotifications.blade.php',
        minify: true,
        extract: false,
    });

    // Settings
    critical.generate({
        src: url+'/settings',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontendsettings.blade.php',
        minify: true,
        extract: false,
    });

    // Upload
    critical.generate({
        src: url+'/upload',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontendupload.blade.php',
        minify: true,
        extract: false,
    });

    // User
    critical.generate({
        src: url+'/user/Wllppr',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontenduser.blade.php',
        minify: true,
        extract: false,
    });

    // User Uploads
    critical.generate({
        src: url+'/user/Wllppr/uploads',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontenduseruploads.blade.php',
        minify: true,
        extract: false,
    });

    // User Favorites
    critical.generate({
        src: url+'/user/Wllppr/favorites',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontenduserfavorites.blade.php',
        minify: true,
        extract: false,
    });

    // Wallpaper
    critical.generate({
        src: url+'/tesqucog5f.htm',
        css: ['public/storage/css/frontend.min.css'],
        dimensions: dimensions,
        dest: 'public/storage/css/critical/criticalfrontendwallpaper.blade.php',
        minify: true,
        extract: false,
    });*/

});

// --- DEFAULT
// When you run only with: `gulp`
gulp.task('default', ['css', 'js']);