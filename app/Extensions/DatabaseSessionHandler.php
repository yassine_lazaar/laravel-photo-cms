<?php

namespace App\Extensions;


use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Log;

class DatabaseSessionHandler extends \Illuminate\Session\DatabaseSessionHandler
{
    
    /**
     * {@inheritdoc}
     */
    public function read($sessionId)
    {
        $session = (object) $this->getQuery()->find($sessionId);
            if (isset($session->last_activity)) {
            if ($session->last_activity < Carbon::now()->subMinutes($this->minutes)->getTimestamp() * 1000) {
                $this->exists = false;
                return;
            }
        }

        if (isset($session->payload)) {
            $this->exists = true;

            return base64_decode($session->payload);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function write($sessionId, $data)
    {
        $request = request();
        $payload = $this->getDefaultPayload($data);

        //\Log::debug('session id '. $sessionId);
        //\Log::debug('write session for url '.$request->fullUrl());
        //\Log::debug('payload '. json_encode($payload));
        
        if (! $this->exists) {
            $this->read($sessionId);
        }

        if ($this->exists) {
            // Avoid altering session when making timeout-check requests via Ajax.
            if(!($request->expectsJson() && $request->get('request') === 'sessioncheck'))
                $this->getQuery()->where('id', $sessionId)->update($payload);
        } else {

            // Avoid altering session when making timeout-check requests via Ajax.
            if(!($request->expectsJson() && $request->get('request') === 'sessioncheck')){
                $payload['id'] = $sessionId;
                $this->getQuery()->insert($payload);
            }
        }

        $this->exists = true;
    }

    /**
     * {@inheritDoc}
     */
    protected function getDefaultPayload($data)
    {
        $timestamp = round(microtime(true) * 1000);
        $payload = ['payload' => base64_encode($data), 'last_activity' => $timestamp];

        if (! $container = $this->container) {
            return $payload;
        }

        if ($container->bound('sentinel')) {
            $user = $container->make('sentinel')->getUser();
            $payload['user_id'] = ($user)? $user->id : null;
        }

        if ($container->bound('request')) {
            $payload['ip_address'] = $container->make('request')->ip();

            $payload['user_agent'] = substr(
                (string) $container->make('request')->header('User-Agent'), 0, 500
            );
        }

        return $payload;
    }

    /**
     * {@inheritdoc}
     */
    public function gc($lifetime)
    {
        $request = request();
        if(!($request->expectsJson() && $request->get('request') === 'sessioncheck')){
            $timestamp = round(microtime(true) * 1000);
            $delete_query = $this->getQuery()->where('last_activity', '<=', $timestamp - $lifetime * 1000)->delete();
            if ($delete_query) $this->exists = false;
        }
    }

    /**
     * Check if user session exists
     *
     * @return boolean
     */
    public function exists(){
        return (bool)$this->exists;
    }
    /**
     * Get current user's last activity (logged or guest)
     *
     * @return string
     */
    public function userLastActivity()
    {
        $session = (object) $this->getQuery()->find(Session::getId());
        return $session->last_activity;
    }

    /**
     * Get any logged user's last activity
     *
     * @param $id
     * @return string
     */
    public function loggedUserLastActivity($id)
    {
        $activity = $this->getQuery()->where('user_id', $id);
        return $activity->get(['last_activity']);
    }

    /**
     * Check if a user is still active
     *
     * @param $id
     * @return bool
     */
    public function isUserActive($id)
    {
        if($session = $this->getQuery()->where('user_id', $id)->get(['last_activity'])){
            $lastActivityTime = Carbon::parse(strtotime(head($session)->last_activity));
            $timeSinceLastActive = Carbon::now()->diffInMinutes($lastActivityTime);
            if ($timeSinceLastActive < 10)
                return true;
        }
        return false;
    }

    /**
     * Check if user is logged in
     *
     * @param $id
     * @return bool
     */
    public function isUserLoggedIn($id)
    {
        if($session = $this->getQuery()->where('user_id', $id)->get()){
            return true;
        }
        return false;
    }
}
