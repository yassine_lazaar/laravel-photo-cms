<?php

namespace App\Extensions;

use Illuminate\Support\Collection;
use Illuminate\Pagination\BootstrapThreePresenter;
use Illuminate\Pagination\LengthAwarePaginator as BasePaginator;

class Paginator extends BasePaginator{

     /**
     * Create a new paginator instance.
     *
     * @param  mixed  $items
     * @param  int  $perPage
     * @param  int $page
     * @param  string $path Base path
     * @return void
     */
    public function __construct($items, $perPage, $page, $path, $total = null){
        // Set the "real" items that will appear here
        $trueItems = [];

        // That is, add the correct items
        for ( $i = $perPage*($page-1) ; $i < min(count($items),$perPage*$page) ; $i++ ){
            $trueItems[] = $items[$i];
        }

        // Set path as provided
        $this->path = $path;

        // Call parent
        if(isset($total))
            parent::__construct($trueItems,$total,$perPage);
        else
            parent::__construct($trueItems,count($items),$perPage);

        // Override "guessing" of page
        $this->currentPage = $page;
    }

    /**
     * Get a URL for a given page number.
     *
     * @param  int  $page
     * @return string
     */
    public function url($page){
        if ($page <= 0) $page = 1;
        if ($page == 1) return $this->path;
        return $this->path.'/page-'.$page;
    }
}