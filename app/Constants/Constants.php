<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 20/10/2016
 * Time: 01:11
 */

// Requests codes
define('TOKEN_MISMATCH_ERROR_CODE', 1000);
define('MODEL_NOT_FOUND_ERROR_CODE', 1001);
define('MODEL_CREATE_ERROR_CODE', 1002);
define('MODEL_EDIT_ERROR_CODE', 1003);
define('MODEL_DELETE_ERROR_CODE', 1004);
define('RESOLUTION_NOT_SUPPORTED_CODE', 1005);
define('GALLERY_PROTECTED_CODE', 1006);
define('DOWNLOAD_LIMIT_EXCEEDED_CODE', 1007);
define('ADDED_TO_FAVORITES_CODE', 1008);
define('REMOVED_FROM_FAVORITES_CODE', 1009);
define('PUBLISH_UPLOAD_CODE', 1010);
define('DISCARD_UPLOAD_CODE', 1011);
define('CREATE_GALLERY_CODE', 1012);
define('EDIT_GALLERY_CODE', 1013);
define('DELETE_GALLERY_CODE', 1014);
define('SCAN_GALLERY_EMPTY_CODE', 1015);
define('PROCESS_WALLPAPER_CODE', 1016);
define('EDIT_WALLPAPER_CODE', 1017);
define('MOVE_WALLPAPER_CODE', 1018);
define('DELETE_WALLPAPER_CODE', 1019);
define('PUBLISH_WALLPAPERS_CODE', 1020);
define('CREATE_DEVICE_CODE', 1021);
define('EDIT_DEVICE_CODE', 1022);
define('DELETE_DEVICE_CODE', 1023);
define('EDIT_TAG_CODE', 1024);
define('DELETE_TAG_CODE', 1025);
define('DEVICE_PICKER_CODE', 1026);
define('EDIT_SETTINGS_CODE', 1027);
define('GENERATE_SITEMAP_CODE', 1028);
define('EDIT_USER_SETTINGS_CODE', 1029);
define('UPLOAD_AVATAR_CODE', 1030);
define('DELETE_AVATAR_CODE', 1031);
define('UPLOAD_COVER_CODE', 1032);
define('UPLOAD_WALLPAPER_CODE', 1033);
define('NOTIFICATION_PULL_CODE', 1034);
define('NOTIFICATION_PULL_END_CODE', 1035);
define('NOTIFICATION_READ_CODE', 1036);
define('SEARCH_SUGGESTION_CODE', 1037);
define('REORDER_GALLERY_CODE', 1038);
define('REORDER_GALLERY_FAIL_CODE', 1039);
define('UPLOAD_AVATAR_SIZE_FAIL_CODE', 1040);
define('UPLOAD_AVATAR_MIME_FAIL_CODE', 1041);
define('UPLOAD_AVATAR_DIMENSION_FAIL_CODE', 1042);
define('PUBLISH_WALLPAPER_CODE', 1043);
define('DOWNLOAD_ERROR_CODE', 1044);
define('DELETE_COVER_CODE', 1045);
define('DELETE_COVER_FAIL_CODE', 1046);
define('DELETE_AVATAR_FAIL_CODE', 1047);


// Messages
define('TOKEN_MISMATCH_ERROR_MESSAGE', 'Token mismatch.');
define('MODEL_NOT_FOUND_ERROR_MESSAGE', 'Item not found.');
define('MODEL_CREATE_ERROR_MESSAGE', 'Could not add item.');
define('MODEL_EDIT_ERROR_MESSAGE', 'Could not edit this item. Changes did not persist.');
define('MODEL_DELETE_ERROR_MESSAGE', 'Could not delete this item.');
define('RESOLUTION_NOT_SUPPORTED_MESSAGE', 'Could not process the image. Resolution is not supported.');
define('DOWNLOAD_LIMIT_EXCEEDED_MESSAGE', 'You exceeded the number of downloads allowed daily. Please login.');
define('GALLERY_PROTECTED_MESSAGE', 'This gallery is protected. You cannot edit it.');
define('ADDED_TO_FAVORITES_MESSAGE', 'Added to favorites.');
define('REMOVED_FROM_FAVORITES_MESSAGE', 'Removed from favorites.');
define('PUBLISH_UPLOAD_MESSAGE', 'Upload published successfully.');
define('DISCARD_UPLOAD_MESSAGE', 'Upload discarded successfully.');
define('CREATE_GALLERY_MESSAGE', 'Gallery was created successfully.');
define('EDIT_GALLERY_MESSAGE', 'Gallery updated.');
define('DELETE_GALLERY_MESSAGE', 'Gallery deleted.');
define('SCAN_GALLERY_EMPTY_MESSAGE', 'No new files found.');
define('PROCESS_WALLPAPER_MESSAGE', 'Wallpaper processed.');
define('EDIT_WALLPAPER_MESSAGE', 'Wallpaper updated.');
define('MOVE_WALLPAPER_MESSAGE', 'Wallpaper moved.');
define('DELETE_WALLPAPER_MESSAGE', 'Wallpaper deleted.');
define('PUBLISH_WALLPAPERS_MESSAGE', 'Wallpapers published.');
define('CREATE_DEVICE_MESSAGE', 'New device was created successfully.');
define('EDIT_DEVICE_MESSAGE', 'Device updated.');
define('DELETE_DEVICE_MESSAGE', 'Device(s) deleted.');
define('EDIT_TAG_MESSAGE', 'Tag updated.');
define('DELETE_TAG_MESSAGE', 'Tag deleted.');
define('DEVICE_PICKER_MESSAGE', 'Device %s was selected.');
define('EDIT_SETTINGS_MESSAGE', 'Settings Updated.');
define('GENERATE_SITEMAP_MESSAGE', 'Sitemap generated.');
define('EDIT_USER_SETTINGS_MESSAGE', 'Your account settings were updated successfully.');
define('UPLOAD_AVATAR_MESSAGE', 'Your profile picture has been updated.');
define('DELETE_AVATAR_MESSAGE', 'Your profile picture has been deleted.');
define('DELETE_AVATAR_FAIL_MESSAGE', 'There seem to be a problem. Could not delete your avatar image.');
define('UPLOAD_COVER_MESSAGE', 'Your cover picture has been updated.');
define('DELETE_COVER_MESSAGE', 'Your cover picture has been deleted.');
define('DELETE_COVER_FAIL_MESSAGE', 'There seem to be a problem. Could not delete your cover image.');
define('UPLOAD_WALLPAPER_MESSAGE', 'Your image has been uploaded successfully and is awaiting moderation.');
define('NOTIFICATION_PULL_MESSAGE', 'Pulling read notifications.');
define('NOTIFICATION_PULL_END_MESSAGE', 'No more read notifications.');
define('NOTIFICATION_READ_MESSAGE', 'Notification was marked as read.');
define('SEARCH_SUGGESTION_MESSAGE', 'Pulling search suggestions.');
define('REORDER_GALLERY_MESSAGE', 'Gallery reorder succeeded.');
define('UPLOAD_AVATAR_SIZE_FAIL_MESSAGE', 'Upload failed. Image size is more than 1Mb.');
define('UPLOAD_AVATAR_MIME_FAIL_MESSAGE', 'Upload failed. Incorrect file type.');
define('UPLOAD_AVATAR_DIMENSION_FAIL_MESSAGE', 'Uplaod failed. The image is too small.');
define('PUBLISH_WALLPAPER_MESSAGE', 'Wallpaper published.');
define('DOWNLOAD_ERROR_MESSAGE', 'Oops! Something went wrong while downloading file. Please try again later.');
define('INAPPROPRIATE_IMAGE_MESSAGE', 'The provided image is inappropriate. Please provide suitable content.');


// Auth Messages

// Cache Keys
define('SITE_SETTINGS_CACHE_KEY', 'siteSettings');
define('HOME_CONTENT_CACHE_KEY', 'homeContent');
define('GALLERY_CACHE_KEY', 'galleries');
define('GALLERY_ORDER_CACHE_KEY', 'galleriesOrder');
define('TRENDIND_TAGS_CACHE_KEY', 'trendingTags');


// Thumb Size
define('DEFAULT_THUMB_WIDTH', 300);
define('DEFAULT_THUMB_HEIGHT', 250);
define('DEFAULT_BIG_THUMB_WIDTH', 610);
define('DEFAULT_BIG_THUMB_HEIGHT', 510);

// Preview Sizes
define('DEFAULT_PREVIEW_WIDTH', 750);

// Session Injection
define('SESSION_DOWNLOADS', 'sessionDl');
define('SESSION_DEVICE_ID_KEY', 'sessionDeviceId');
define('SESSION_DEVICE_NAME_KEY', 'sessionDeviceName');
define('SESSION_DEVICE_MAKER_KEY', 'sessionDeviceMaker');
define('SESSION_DEVICE_SLUG_KEY', 'sessionDeviceSlug');
define('SESSION_DEVICE_IMAGE_KEY', 'sessionDeviceImage');
define('SESSION_LOGGED_KEY', 'logged');
define('SESSION_LIFETIME_KEY', 'sessionLifetime');
define('SESSION_MODAL_MESSAGE_KEY', 'redirectMessage');
define('SESSION_MODAL_REDIRECT_KEY', 'redirect');

// Avatar
define('AVATAR_DEFAULT', 'stock_profile');
define('AVATAR_SIZE_4X', 256);

// Covers
define('COVER_MAX_FILE_SIZE', 3 * 1024);
define('COVER_MIN_WIDTH', 1440);
define('COVER_MIN_HEIGHT', 400);

// Devices
define('DEVICE_FEATURE_DEFAULT', 'stock_device');
define('DEVICE_IMAGE_SIZE_3X', 256);

// Paths
define('GALLERIES_PATH', 'wallpaper/');
define('GALLERY_FEATURES_FOLDER', 'gallery/');
define('DOWNLOADS_PATH', 'download/');
define('PREVIEWS_PATH', 'preview/');
define('THUMBS_PATH', 'thumb/');
define('DELETED_PATH', 'deleted/');
define('UPLOADS_PATH', 'upload/');
define('DEVICE_FEATURE_IMAGE_PATH', 'device/');
define('PROFILE_IMAGE_PATH', 'avatar/');
define('COVER_IMAGE_PATH', 'cover/');

// Gallery Order Terms
define('GALLERY_ORDER_VIEWS', 'views');
define('GALLERY_ORDER_FAVORITES', 'favorites');
define('GALLERY_ORDER_DOWNLOADS', 'downloads');
define('GALLERY_ORDER_TRENDING', 'trending');
define('GALLERY_ORDER_FEATURED', 'featured');
define('GALLERY_ORDER_REGEX', 'views|favorites|downloads|trending|featured');

// Wallpaper
define('WALLPAPER_TRENDING_THRESHOLD', 2);

// Accepted Extensions
define ('ACCEPTED_MIME_TYPES', serialize(
        array(
            'image/jpg',
            'image/jpeg',
            'image/png'
        )
    )
);

// Licenses
define ('LICENSES', serialize(
        array(
            'Attribution',
            'Attribution-ShareAlike',
            'Attribution-NoDerivs',
            'Attribution-NonCommercial',
            'Attribution-NonCommercial-ShareAlike',
            'Attribution-NonCommercial-NoDerivs'
        )
    )
);

// Devices types
define ('DEVICES_TYPES',
    serialize(
        array(
            'Phones',
            'Tablets',
            'Laptops',
            'Desktops',
            'Monitors'
        )
    )
);

// Plain Emails
define('ACTIVATION_MAIL_PLAIN', 'Hello %s, Thank you for registering at %s. Please activate your newly registered account by following this link:<a href="%s">%s</a> The link is will stay valid for the next %d hours. Please let us know if you have any questions by writing to <a href="mailto:%s" target="_top">%s</a>. Best Regards, The %s Team');
define('REMINDER_MAIL_PLAIN', 'Hello %s, A password reset has been requested for your %s account. Click on the following link if you would like to reset your password:<a href="%s">%s</a> The link is will stay valid for the next %d hours. Please let us know if you have any questions by writing to <a href="mailto:%s" target="_top">%s</a>. Best Regards, The %s Team');
define('USER_MAIL_PLAIN', 'Message from %s (%s:%s), %s');
define('UPLOAD_MAIL_PLAIN', '%s has uploaded new content under %s. Please moderate.');


// Notifications type
define('NOTIFICATION_TYPE_MODERATION_PUBLISH', 1);
define('NOTIFICATION_TYPE_MODERATION_DISCARD', 2);
define('NOTIFICATION_TYPE_FAVORITE', 3);

// Notifications data keys
define('NOTIFICATION_TYPE_KEY', 'type');
define('NOTIFICATION_USER_ID_KEY', 'user');
define('NOTIFICATION_OWNER_ID_KEY', 'owner');
define('NOTIFICATION_UPLOAD_TITLE_KEY', 'title');
define('NOTIFICATION_WALLPAPER_ID_KEY', 'wallpaper');
define('NOTIFICATION_DISCARD_REASON_KEY', 'discard_reason');
define('NOTIFICATION_TIMESTAMP_KEY', 'timestamp');

// Upload discard reasons
define('DISCARD_REASON_STANDARD_DEF', json_encode(['k' => 1, 'v' => 'Standard definition', 'msg' => 'Your upload is in standard definition. High quality images are required.']));
define('DISCARD_REASON_DUPLICATE', json_encode(['k' => 2, 'v' => 'Duplicate content', 'msg' => 'Your upload is already published and it may be a duplicate.']));
define('DISCARD_REASON_NSFW', json_encode(['k' => 3, 'v' => 'NSFW content', 'msg' => 'Your upload may contain nudity, intense sexuality, profanity or disturbing content.']));
define('DISCARD_REASON_RESOLUTION_NOT_SUPPORTED', json_encode(['k' =>4, 'v' => 'Resolution not supported', 'msg' => 'Your upload\'s resolution is currently not supported.']));
define('DISCARD_REASON_COPYRIGHT_PROTECTED', json_encode(['k' => 5, 'v' => 'Protected by copyright', 'msg' => 'Your upload is subject to copyright protection.']));

define ('UPLOAD_DISCARD_REASONS',
    serialize(
        array(
            json_decode(DISCARD_REASON_STANDARD_DEF),
            json_decode(DISCARD_REASON_DUPLICATE),
            json_decode(DISCARD_REASON_NSFW),
            json_decode(DISCARD_REASON_RESOLUTION_NOT_SUPPORTED),
            json_decode(DISCARD_REASON_COPYRIGHT_PROTECTED)
        )
    )
);

// IPTC KEYS
define('IPTC_HEADLINE_KEY', 'title');
define('IPTC_CAPTION_KEY', 'caption');
define('IPTC_CONTACT_KEY', 'contact');
define('IPTC_CREATED_DATE_KEY', 'creation_date');
define('IPTC_KEYWORDS_KEY', 'keywords');

// Settings
define('SETTING_SITE_NAME', 'site_name');
define('SETTING_SITE_DESCRIPTION', 'site_description');
define('SETTING_SITE_TOS', 'site_tos');
define('SETTING_SITE_PRIVACY_POLICY', 'site_privacy_policy');
define('SETTING_SITE_COPYRIGHT_POLICY', 'site_copyright_policy');
define('SETTING_SITE_FAQ', 'site_faq');
define('SETTING_SITE_ABOUT', 'site_about');

define('SETTING_WALLPAPERS_PER_PAGE', 'wallpapers_per_page');
define('SETTING_GUEST_DOWNLOAD_LIMIT', 'guest_download_limit');
define('SETTING_MAX_UPLOAD_SIZE', 'max_upload_size');
//define('SETTING_AUTO_APPROVE_UPLOADS', 'auto_approve_uploads');

define('SETTING_SITEMAP_INCLUDE_GALLERIES', 'sitemap_include_galleries');
define('SETTING_SITEMAP_INCLUDE_TAGS', 'sitemap_include_tags');
define('SETTING_SITEMAP_INCLUDE_IMAGES', 'sitemap_include_images');
define('SETTING_SITEMAP_INCLUDE_DEVICES', 'sitemap_include_devices');

define('SETTING_ANALYTICS_CODE', 'analytics_code');

define('SETTING_SEO_HOMEPAGE_TITLE_TEMPLATE', 'seo_homepage_title_template');
define('SETTING_SEO_HOMEPAGE_DESCRIPTION_TEMPLATE', 'seo_homepage_description_template');
//define('SETTING_SEO_HOMEPAGE_KEYWORDS_TEMPLATE', 'seo_homepage_keywords_template');
define('SETTING_SEO_GALLERY_TITLE_TEMPLATE', 'seo_gallery_title_template');
define('SETTING_SEO_GALLERY_DESCRIPTION_TEMPLATE', 'seo_gallery_description_template');
//define('SETTING_SEO_GALLERY_KEYWORDS_TEMPLATE', 'seo_gallery_keywords_template');
define('SETTING_SEO_WALLPAPER_TITLE_TEMPLATE', 'seo_wallpaper_title_template');
define('SETTING_SEO_WALLPAPER_DESCRIPTION_TEMPLATE', 'seo_wallpaper_description_template');
//define('SETTING_SEO_WALLPAPER_KEYWORDS_TEMPLATE', 'seo_wallpaper_keywords_template');
define('SETTING_SEO_TAG_TITLE_TEMPLATE', 'seo_tag_title_template');
define('SETTING_SEO_TAG_DESCRIPTION_TEMPLATE', 'seo_tag_description_template');
//define('SETTING_SEO_TAG_KEYWORDS_TEMPLATE', 'seo_tag_keywords_template');
define('SETTING_SEO_DEVICE_TITLE_TEMPLATE', 'seo_device_title_template');
define('SETTING_SEO_DEVICE_DESCRIPTION_TEMPLATE', 'seo_device_description_template');
//define('SETTING_SEO_DEVICE_KEYWORDS_TEMPLATE', 'seo_device_keywords_template');
define('SETTING_SEO_SEARCH_TITLE_TEMPLATE', 'seo_search_title_template');
define('SETTING_SEO_SEARCH_DESCRIPTION_TEMPLATE', 'seo_search_description_template');
//define('SETTING_SEO_SEARCH_KEYWORDS_TEMPLATE', 'seo_search_keywords_template');
define('SETTING_SEO_404_TITLE_TEMPLATE', 'seo_404_title_template');
define('SETTING_SEO_404_DESCRIPTION_TEMPLATE', 'seo_404_description_template');
//define('SETTING_SEO_404_KEYWORDS_TEMPLATE', 'seo_404_keywords_template');


// SEO KEYS
define('TITLE_KEY', 't');
define('DESCRIPTION_KEY', 'd');
//define('KEYWORDS_KEY', 'k');

// SEO placeholders
define('PLACEHOLDER_SITE_NAME', '%%site_name%%');
define('PLACEHOLDER_SITE_DESCRIPTION', '%%site_description%%');
define('PLACEHOLDER_WALLPAPER_TITLE', '%%wallpaper_title%%');
define('PLACEHOLDER_WALLPAPER_GALLERY', '%%wallpaper_gallery%%');
define('PLACEHOLDER_WALLPAPER_DESCRIPTION', '%%wallpaper_description%%');
define('PLACEHOLDER_WALLPAPER_UPLOADER', '%%wallpaper_uploader%%');
define('PLACEHOLDER_WALLPAPER_AUTHOR', '%%wallpaper_author%%');
define('PLACEHOLDER_TAG_NAME', '%%tag_name%%');
define('PLACEHOLDER_GALLERY_NAME', '%%gallery_name%%');
define('PLACEHOLDER_GALLERY_DESCRIPTION', '%%gallery_description%%');
define('PLACEHOLDER_DEVICE_NAME', '%%device_name%%');
define('PLACEHOLDER_DEVICE_MAKER', '%%device_maker%%');
define('PLACEHOLDER_SEARCH_TERM', '%%search_term%%');
define('PLACEHOLDER_PAGE_NUMBER', '%%page_number%%');

define ('SEO_HOMEPAGE_PLACEHOLDERS',
        serialize(
            array(
                PLACEHOLDER_SITE_NAME,
                PLACEHOLDER_SITE_DESCRIPTION,
                PLACEHOLDER_PAGE_NUMBER
            )
        )
);
define ('SEO_WALLPAPER_PLACEHOLDERS',
    serialize(
        array(
            PLACEHOLDER_SITE_NAME,
            PLACEHOLDER_SITE_DESCRIPTION,
            PLACEHOLDER_WALLPAPER_TITLE,
            PLACEHOLDER_WALLPAPER_GALLERY,
            PLACEHOLDER_WALLPAPER_DESCRIPTION,
            PLACEHOLDER_WALLPAPER_UPLOADER,
            PLACEHOLDER_WALLPAPER_AUTHOR
        )
    )
);
define ('SEO_GALLERY_PLACEHOLDERS',
    serialize(
        array(
            PLACEHOLDER_SITE_NAME,
            PLACEHOLDER_SITE_DESCRIPTION,
            PLACEHOLDER_GALLERY_NAME,
            PLACEHOLDER_GALLERY_DESCRIPTION,
            PLACEHOLDER_PAGE_NUMBER
        )
    )
);
define ('SEO_TAG_PLACEHOLDERS',
    serialize(
        array(
            PLACEHOLDER_SITE_NAME,
            PLACEHOLDER_SITE_DESCRIPTION,
            PLACEHOLDER_TAG_NAME,
            PLACEHOLDER_PAGE_NUMBER
        )
    )
);
define ('SEO_DEVICE_PLACEHOLDERS',
    serialize(
        array(
            PLACEHOLDER_SITE_NAME,
            PLACEHOLDER_SITE_DESCRIPTION,
            PLACEHOLDER_DEVICE_NAME,
            PLACEHOLDER_DEVICE_MAKER,
            PLACEHOLDER_PAGE_NUMBER
        )
    )
);
define ('SEO_SEARCH_PLACEHOLDERS',
    serialize(
        array(
            PLACEHOLDER_SITE_NAME,
            PLACEHOLDER_SITE_DESCRIPTION,
            PLACEHOLDER_SEARCH_TERM,
            PLACEHOLDER_PAGE_NUMBER
        )
    )
);
define ('SEO_404_PLACEHOLDERS',
    serialize(
        array(
            PLACEHOLDER_SITE_NAME,
            PLACEHOLDER_SITE_DESCRIPTION
        )
    )
);


define ('MAKERS', serialize(
        array(
            'N/A',
            'Apple',
            'Samsung',
            'OnePlus',
            'HTC',
            'Sony',
            'Motorola',
            'Huwaei',
            'LG',
            'Google'
        )
    )
);

define ('COUNTRIES',
    serialize(
        array(
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua and Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BQ' => 'Bonaire, Saint Eustatius and Saba',
            'BA' => 'Bosnia and Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'VG' => 'British Virgin Islands',
            'BN' => 'Brunei',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CW' => 'Curacao',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'CD' => 'Democratic Republic of the Congo',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'TL' => 'East Timor',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island and McDonald Islands',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'CI' => 'Ivory Coast',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'XK' => 'Kosovo',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Laos',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'KP' => 'North Korea',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'CG' => 'Republic of the Congo',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russia',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthelemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts and Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre and Miquelon',
            'VC' => 'Saint Vincent and the Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome and Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SX' => 'Sint Maarten',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia and the South Sandwich Islands',
            'KR' => 'South Korea',
            'SS' => 'South Sudan',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard and Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syria',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad and Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks and Caicos Islands',
            'TV' => 'Tuvalu',
            'VI' => 'U.S. Virgin Islands',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Minor Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VA' => 'Vatican',
            'VE' => 'Venezuela',
            'VN' => 'Vietnam',
            'WF' => 'Wallis and Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
        )
    )
);


// IPTC Constants
define('IPTC_OBJECT_NAME', '005');
define('IPTC_EDIT_STATUS', '007');
define('IPTC_PRIORITY', '010');
define('IPTC_CATEGORY', '015');
define('IPTC_SUPPLEMENTAL_CATEGORY', '020');
define('IPTC_FIXTURE_IDENTIFIER', '022');
define('IPTC_KEYWORDS', '025');
define('IPTC_RELEASE_DATE', '030');
define('IPTC_RELEASE_TIME', '035');
define('IPTC_SPECIAL_INSTRUCTIONS', '040');
define('IPTC_REFERENCE_SERVICE', '045');
define('IPTC_REFERENCE_DATE', '047');
define('IPTC_REFERENCE_NUMBER', '050');
define('IPTC_CREATED_DATE', '055');
define('IPTC_CREATED_TIME', '060');
define('IPTC_ORIGINATING_PROGRAM', '065');
define('IPTC_PROGRAM_VERSION', '070');
define('IPTC_OBJECT_CYCLE', '075');
define('IPTC_BYLINE', '080');
define('IPTC_BYLINE_TITLE', '085');
define('IPTC_CITY', '090');
define('IPTC_PROVINCE_STATE', '095');
define('IPTC_COUNTRY_CODE', '100');
define('IPTC_COUNTRY', '101');
define('IPTC_ORIGINAL_TRANSMISSION_REFERENCE', '103');
define('IPTC_HEADLINE', '105');
define('IPTC_CREDIT', '110');
define('IPTC_SOURCE', '115');
define('IPTC_COPYRIGHT_STRING', '116');
define('IPTC_CONTACT', '118');
define('IPTC_CAPTION', '120');
define('IPTC_LOCAL_CAPTION', '121');