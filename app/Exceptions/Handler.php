<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        //\Symfony\Component\HttpKernel\Exception\HttpException::class,
        //\Illuminate\Database\Eloquent\ModelNotFoundException::class,
        //\Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($this->shouldReport($exception)) {
            //app('sentry')->captureException($exception);
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if($e instanceof NotFoundHttpException) {
            $breadcrumb =  [
                ['a' => '404 Not found']
            ];
            return response()->view('errors.404', [
                'breadcrumb' => $breadcrumb,
                'title' => \Seo::get404Data(TITLE_KEY),
                'description' => \Seo::get404Data(DESCRIPTION_KEY),
                'timestamp' => \Carbon\Carbon::now()->getTimestamp()
            ], 404);
        }

        if ($e instanceof TokenMismatchException) {
            if ($request->expectsJson())
                return response()->json(['error' => TOKEN_MISMATCH_ERROR_MESSAGE, 'code' => TOKEN_MISMATCH_ERROR_CODE], 500);
            return redirect()->back()->withErrors(TOKEN_MISMATCH_ERROR_MESSAGE);
        }

        if ($e instanceof ModelNotFoundException) {
            if ($request->expectsJson())
                return response()->json(['error' => MODEL_NOT_FOUND_ERROR_MESSAGE, 'code' => MODEL_NOT_FOUND_ERROR_CODE], 500);
            return redirect()->back()->withErrors('errors', MODEL_NOT_FOUND_ERROR_MESSAGE);
        }

        if ($request->expectsJson())
            return response()->json(['error' => $e->getMessage(), 'code' => $e->getCode()], 500);

        return parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }
}
