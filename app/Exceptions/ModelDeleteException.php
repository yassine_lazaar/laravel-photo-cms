<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 20/10/2016
 * Time: 00:41
 */

namespace App\Exceptions;

use Exception;

class ModelDeleteException extends Exception
{
    public function __construct() {
        $this->message = MODEL_DELETE_ERROR_MESSAGE;
        $this->code = MODEL_DELETE_ERROR_CODE;
    }
}