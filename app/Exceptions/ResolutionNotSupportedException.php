<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 20/10/2016
 * Time: 01:24
 */

namespace App\Exceptions;

use Exception;

class ResolutionNotSupportedException extends Exception
{
    public function __construct() {
        $this->message = RESOLUTION_NOT_SUPPORTED_MESSAGE;
        $this->code = RESOLUTION_NOT_SUPPORTED_CODE;
    }
}