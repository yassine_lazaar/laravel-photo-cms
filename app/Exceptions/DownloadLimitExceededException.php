<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 20/10/2016
 * Time: 01:24
 */

namespace App\Exceptions;

use Exception;

class DownloadLimitExceededException extends Exception
{
    public function __construct() {
        $this->message = DOWNLOAD_LIMIT_EXCEEDED_MESSAGE;
        $this->code = DOWNLOAD_LIMIT_EXCEEDED_CODE;
    }
}