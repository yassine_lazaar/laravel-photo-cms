<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 20/10/2016
 * Time: 01:24
 */

namespace App\Exceptions;

use Exception;

class ModelCreateException extends Exception
{
    public function __construct() {
        $this->message = MODEL_CREATE_ERROR_MESSAGE;
        $this->code = MODEL_CREATE_ERROR_CODE;
    }
}