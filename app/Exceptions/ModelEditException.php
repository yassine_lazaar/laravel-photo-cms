<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 20/10/2016
 * Time: 01:23
 */

namespace App\Exceptions;

use Exception;

class ModelEditException extends Exception
{
    public function __construct() {
        $this->message = MODEL_EDIT_ERROR_MESSAGE;
        $this->code = MODEL_EDIT_ERROR_CODE;
    }
}