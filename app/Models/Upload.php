<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{

    protected $table = 'uploads';

    protected $guarded = ['id'];

    protected $fillable = [
            'title',
            'description',
            'tags',
            'filename',
            'width',
            'height',
            'license',
            'author',
            'author_link',
            'user_id',
            'gallery_id'
        ];                                  
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function path()
    {
        return '/storage/'.UPLOADS_PATH.$this->filename;
    }

    public function thumbnailPath()
    {
        return '/storage/'.UPLOADS_PATH.'_thumbs/'.$this->filename;
    }

}
