<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{

    protected $table = 'devices';

    protected $guarded = ['id'];

    protected $fillable = [
        'name',
        'slug',
        'type',
        'maker',
        'release_date',
        'featured_image',
        'excluded'
    ];

    public function resolutions()
    {
        return $this->belongsToMany(Resolution::class);
    }

    /*
    public function resolution()
    {
        return $this->belongsTo(Resolution::class);
    }

    public function wallpapers()
    {
        return $this->resolution()->wallpapers();
        //return $this->hasManyThrough(Wallpaper::class, Resolution::class);
    }
    */
}
