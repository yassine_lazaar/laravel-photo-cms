<?php


namespace App\Models;

use Storage;
use Michaeljennings\Feed\Contracts\Notifiable as NotifiableContract;
use Michaeljennings\Feed\Store\Eloquent\Notifiable;
use \Cartalyst\Sentinel\Users\EloquentUser as EloquentUser;
use \Cartalyst\Sentinel\Addons\Social\Models\Link as Link;

class User extends EloquentUser  implements NotifiableContract
{
    use Notifiable;

    protected $table = 'users';

    protected $guarded = ['id'];

    protected $fillable = [
        'email',
        'password',
        'username',
        'last_name',
        'first_name',
        'bio',
        'website',
        'facebook',
        'twitter',
        'instagram',
        'permissions',
        'banned',
        'avatar',
        'gender',
        'birthdate',
        'address',
        'zip_code',
        'city',
        'country',
        'phone_number',
        'social_providers'
    ];


    protected $hidden = [
        'password',
        'permissions'
    ];

    /**
     * The Eloquent throttling model name.
     *
     * @var string
     */
    protected static $settingsModel = 'UserSettings';

    /**
     * Avatar accessor
     *
     * @param $value
     * @return mixed
     */
    public function getAvatarAttribute($value)
    {
        if(!$value)
            return (AVATAR_DEFAULT);
        return $value;
    }

    /**
     * Avatar path
     *
     * @return string
     */
    public function avatarPath()
    {
        //return Storage::url(PROFILE_IMAGE_PATH.$this->avatar);
        return '//static3.'.env('APP_DOMAIN').'/'.PROFILE_IMAGE_PATH.$this->avatar;
    }

    /**
     * Birthdate accessor
     *
     * @param $value
     * @return string
     */
    public function getBirthdateAttribute($value)
    {
        if(isset($value))
            return \Carbon\Carbon::parse($value)->format('m-d-Y');
        return $value;
    }

    /**
     * Check if cover exists
     *
     * @return mixed
     */
    public function getCover()
    {
        $coverFile = $this->id.'_'.$this->username.'_cover.jpg';
        if(!\Storage::disk('local')->exists('public/'.COVER_IMAGE_PATH.$coverFile))
            return null;
        return '//static3.'.env('APP_DOMAIN').'/'.COVER_IMAGE_PATH.$coverFile;
    }

/*
    /**
     * Social providers accessor
     *
     * @param $value
     * @return mixed
     *//*
    public function getSocialProvidersAttribute($value)
    {
        if(!$value)
            return array();
        return json_decode($value, true);
    }*/

    /**
     * Social providers mutator
     *
     * @param  string  $value
     * @return string
     *//*
    public function setSocialProvidersAttribute($value)
    {
        $providers = $this->social_providers;
        if(!array_search($value ,$providers)){
            $providers['pr'.(count($providers)+1)] = $value;
            $this->attributes['social_providers'] = json_encode($providers);
        }
    }*/

    /**
     * Check if provider is already linked
     *
     * @param $provider
     * @return mixed
     *//*
    public function isProviderLinked($provider)
    {
        $providers = $this->social_providers;
        return array_search($provider ,$providers);
    }*/

    /**
     * Returns the settings relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function settings()
    {
        return $this->hasOne(static::$settingsModel, 'user_id');
    }

    public function wallpapers()
    {
        return $this->hasMany(Wallpaper::class);
    }

    public function tags()
    {
        return $this->hasMany(Tag::class);
    }

    public function uploads()
    {
        return $this->hasMany(Upload::class)->orderBy('created_at', 'desc');
    }

    public function links()
    {
        return $this->hasMany(Link::class);
    }

    public function favorites($getTrashed = true)
    {
        if($getTrashed)
            return $this->hasMany(Favorite::class)->withTrashed()->orderBy('updated_at', 'desc');
        return $this->hasMany(Favorite::class)->orderBy('updated_at', 'desc');
    }

}
