<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 03/11/2016
 * Time: 02:27
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Model
{
    use SoftDeletes;
    protected $table = 'favorites';

    protected $guarded = ['id'];

    protected $fillable = [
        'user_id',
        'wallpaper_id'
    ];

    public function user()
    {
            return  $this->belongsTo(User::class);
    }

    public function wallpaper()
    {
        return $this->belongsTo(Wallpaper::class);
    }
}