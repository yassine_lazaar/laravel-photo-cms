<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 02/11/2016
 * Time: 23:40
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Resolution extends Model
{
    protected $table = 'resolutions';

    protected $guarded = ['id'];

    protected $fillable = [
        'width',
        'height',
        'aspect_ratio'
    ];

    public function devices()
    {
        return $this->belongsToMany(Device::class);
    }
/*
    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function wallpapers()
    {
        return $this->hasMany(Wallpaper::class);
    }
*/
    public function xFormat()
    {
        return $this->width.'x'.$this->height;
    }

}