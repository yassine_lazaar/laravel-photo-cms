<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 03/11/2016
 * Time: 01:10
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';

    protected $guarded = ['id'];

    protected $fillable = [
        'name',
        'slug',
        'user_id',
        'published'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function wallpapers()
    {
        return $this->belongsToMany(Wallpaper::class);
    }

}