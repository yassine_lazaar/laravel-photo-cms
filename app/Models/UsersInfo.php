<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersInfo extends Model
{
    use SoftDeletes;

    protected $table = 'users_infos';

    protected $guarded = ['id'];
}