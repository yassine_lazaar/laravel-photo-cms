<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{

    protected $table = 'galleries';

    protected $guarded = ['id'];

    protected $fillable = [
        'name',
        'slug',
        'description',
        'folder',
        'previous',
        'next',
        'published'
    ];

    public static function scopePublished()
    {
        return static::where('published', '=', 1);
    }

    public function wallpapers()
    {
        return $this->hasMany(Wallpaper::class);
    }

        public function uploads()
    {
        return $this->hasMany(Upload::class);
    }
}