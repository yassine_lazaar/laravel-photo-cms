<?php

namespace App\Models;

use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
//use AlgoliaSearch\Laravel\AlgoliaEloquentTrait;

class Wallpaper extends Model
{
    //use AlgoliaEloquentTrait;

    protected $table = 'wallpapers';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'thumbnail_params' => 'array',
    ];

    protected $fillable = [
        'id',
        'title',
        'slug',
        'description',
        'filename',
        'license',
        'author',
        'author_link',
        'published',
        'featured',
        'views',
        'downloads',
        'user_id',
        'gallery_id',
        'width',
        'height',
        'thumbnail_params'
    ];

/*
    public function getAlgoliaRecord()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'published' => $this->published,
            'gallery_id' => $this->gallery->id,
            'gallery_published' => $this->gallery->published,
            'gallery_slug' => $this->gallery->slug,
            'filename' => $this->filename,
            'resolution' => $this->resolution->width.'x'.$this->resolution->height
        ];
    }
*/
    public function getLicense()
    {
        return unserialize(LICENSES)[$this->license];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function resolution()
    {
        return $this->width.'x'.$this->height;
    }

/*
    public function resolution()
    {
        return $this->belongsTo(Resolution::class);
    }

    public function devices()
    {
        return $this->resolution()->devices();
        //return $this->hasManyThrough(Device::class, Resolution::class);
    }
*/
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    public function thumbnailPath()
    {
        $subdomainId = ($this->id % 2 == 0)? 1: 2;
        return 'https://static'.$subdomainId.'.'.env('APP_DOMAIN').'/'.THUMBS_PATH.$this->gallery->folder.'/'.$this->filename;
    }

    public function previewPath()
    {
        $subdomainId = ($this->id % 2 == 0)? 1: 2;
        return 'https://static'.$subdomainId.'.'.env('APP_DOMAIN').'/'.PREVIEWS_PATH.$this->gallery->folder.'/'.$this->filename.'.jpg';
    }

    public function path()
    {
        return 'storage/'.GALLERIES_PATH.$this->gallery->folder.'/'.$this->filename;
    }

    public function link()
    {
        return '/'.$this->slug.'.htm';
    }

    public function pointOfInterest()
    {
        return [
            intval(abs($this->thumbnail_params['x']) / $this->thumbnail_params['zoom'] + DEFAULT_THUMB_WIDTH / (2 * $this->thumbnail_params['zoom'])),
            intval(abs($this->thumbnail_params['y']) / $this->thumbnail_params['zoom'] + DEFAULT_THUMB_HEIGHT / (2 * $this->thumbnail_params['zoom']))
        ];
    }


}