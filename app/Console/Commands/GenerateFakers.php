<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User as User;
use App\Providers\Managers\AvatarManager as AvatarManager;

class GenerateFakers extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:fakers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate 10 fake users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faker = \Faker\Factory::create();
        $role = \App::make('sentinel.roles')->findByName('Admin');
        for ($i = 0; $i < 30; $i++)
        {
            $user = User::create([
                'username' => $faker->userName,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => sprintf(env('FAKE_ACCOUNT_EMAIL'),$i),
                'password' => \Sentinel::getHasher()->hash(sprintf(env('FAKE_ACCOUNT_PASS'),$i)),
                'country' => $faker->countryCode
            ]);

            $activation = \Activation::create($user);
            \Activation::complete($user, $activation->code);
            $role->users()->attach($user);

            $filename = AvatarManager::getFilename($user);
            $avatar_1x_path = public_path('storage/'. PROFILE_IMAGE_PATH . $filename. '_1x.jpg');
            $avatar_2x_path = public_path('storage/'. PROFILE_IMAGE_PATH . $filename. '_2x.jpg');
            $avatar_3x_path = public_path('storage/'. PROFILE_IMAGE_PATH . $filename. '_3x.jpg');
            $avatar_4x_path = public_path('storage/'. PROFILE_IMAGE_PATH . $filename. '_4x.jpg');

            $avatar_url = $faker->imageUrl(256, 256);

            $avatar_4x = \Image::make($avatar_url)
                ->resize(AVATAR_SIZE_4X, AVATAR_SIZE_4X)
                ->save($avatar_4x_path, 100);
                optimizeImageFile($avatar_4x_path);
                
            $avatar_3x = $avatar_4x->resize(AVATAR_SIZE_4X / 2, AVATAR_SIZE_4X / 2)
                ->save($avatar_3x_path, 100);
                optimizeImageFile($avatar_3x_path);

            $avatar_2x = $avatar_3x->resize(AVATAR_SIZE_4X / 4, AVATAR_SIZE_4X / 4)
                ->save($avatar_2x_path, 100);
                optimizeImageFile($avatar_2x_path);
                
            $avatar_2x->resize(AVATAR_SIZE_4X / 8, AVATAR_SIZE_4X / 8)
                ->save($avatar_1x_path, 100);
                optimizeImageFile($avatar_1x_path);
                
            $avatar_4x->destroy();

            $user->avatar = $filename;
            $user->save();
        }
    }
}
