<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class OptimizeImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:optimize {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compress images with imagemagick.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $path = $this->argument('path');
            
            if(!\File::exists($path) && !\File::isDirectory($path)){
                $this->line($path. 'does not exist.');
                return;
            }
            $presentFiles = collect(\File::allFiles($path));
            $presentFiles = $presentFiles->map(function ($file) {
                return \File::basename($file);
            });
            foreach($presentFiles as $file){
                $this->line('Compressing image '.$file);
                try {
                    optimizeImageFile($file);
                } catch(\Exception $e){
                    $this->error('Exception '.$e->getMessage());
                    continue;
                }
            }
        } catch (\Exception $e){
            $this->error('Exception '.$e->getMessage());
        }
    }
}
