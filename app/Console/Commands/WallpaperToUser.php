<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App;
use App\Repositories\WallpapersRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class WallpaperToUser extends Command
{
    private $user;
    private $wallpaper;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wallpaper:user {wallpaper_id} {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move wallpaper ownership to specific user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->user = App::make('sentinel.users');
        $this->wallpaper = new WallpapersRepository(app());
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('user_id');
        $wallpaperId = $this->argument('wallpaper_id');
        try {
            $user  = $this->user->find($userId);
        } catch (ModelNotFoundException $e){
            $this->error('User not found!');
            return;
        }
        try {
            $wallpaper  = $this->wallpaper->find($wallpaperId);
        } catch (ModelNotFoundException $e){
            $this->error('Wallpaper not found!');
            return;
        }

        $wallpaper->user_id = $userId;
        $wallpaper->save();
        $this->line('Wallpaper '.$wallpaperId.' ('.$wallpaper->title.') >>> User '.$userId.' ('.$user->username.').');
    }
}
