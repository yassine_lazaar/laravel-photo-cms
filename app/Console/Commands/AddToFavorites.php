<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Wallpaper;
use App;

class AddToFavorites extends Command
{
    private $user;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:favorite {user_id} {wallpaper_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add wallpaper to user favorites. eg : php -dextension=./redis.so artisan user:favorite 5 1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->user = App::make('sentinel.users');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('user_id');
        $wallpaperId = $this->argument('wallpaper_id');
        try {
            $user  = $this->user->find($userId);
        } catch (ModelNotFoundException $e){
            $this->error('User not found!');
            return;
        }
        Wallpaper::favorite($wallpaperId, $user);
    }
}
