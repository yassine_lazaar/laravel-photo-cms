<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\WallpapersRepository;
use App\Repositories\GalleriesRepository;

class BuildWallpaperPreview extends Command
{
    private $wallpaper;
    private $gallery;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wallpaper:preview {wallpaper_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild wallpaper preview.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->wallpaper = new WallpapersRepository(app());
        $this->gallery = new GalleriesRepository(app());
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $wallpaperId = $this->argument('wallpaper_id');
            $wallpaper  = $this->wallpaper->find($wallpaperId);
            $gallery = $this->gallery->find($wallpaper->gallery_id);
            $filePath = storage_path().'/app/public/'.GALLERIES_PATH.$gallery->folder.'/'.$wallpaper->filename.'.jpg';
            $previewPath = storage_path().'/app/public/'.PREVIEWS_PATH.$gallery->folder.'/'.$wallpaper->filename.'.jpg';
            
            $image = \Image::make($filePath);
            $w = $image->width();
            $h = $image->height();
            $ar = $w / $h;

            $image->fit(DEFAULT_PREVIEW_WIDTH,  intval(DEFAULT_PREVIEW_WIDTH / $ar))
                ->save($previewPath, 100);
                
            optimizeImageFile($previewPath);  
            $this->line('Preview rebuilt.');
            
        } catch (\Exception $e){
            $this->error('Preview could not be built | '.$e->getMessage());
        }
    }
}
