<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon as Carbon;
use File;

class PurgeDownload extends Command
{
    private $storage;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wallpaper:purgedl {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purge the download folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->storage = Storage::disk('local');
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Download folder purge.";
        $purgeAll = $this->option('all');
        $downloadPath = 'public/'.DOWNLOADS_PATH;

        if(!$this->storage->exists($downloadPath)){
            $this->error('Download folder does not exist.');
            return;
        }

        $presentFiles = collect($this->storage->files($downloadPath));
        if($purgeAll)
            $filesToDelete = $presentFiles;
        else 
            $filesToDelete = $presentFiles->filter(function($file){
                $filePath = public_path().$this->storage->url($file);
                $lastModified = Carbon::createFromTimestamp(File::lastModified($filePath));
                return Carbon::now()->diffInDays() < 1;
            });
        
            
        foreach ($filesToDelete as $file)
        {
            try {
                $this->line('Deleting file '.$file);
                $this->storage->delete($file);
            } catch (\Exception $e){
                $this->error('File '.$file.' Could not be deleted.');
            }
        }
    }
}
