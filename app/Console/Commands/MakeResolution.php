<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\ResolutionsRepository;
use Validator;

class MakeResolution extends Command
{
    private $resolution;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resolution:make {width} {height} {aspect_ratio}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add new resolution.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->resolution = new ResolutionsRepository(app());
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $width = $this->argument('width');
        $height = $this->argument('height');
        $aspectRatio = $this->argument('aspect_ratio');

        $commandArgs = [
            'width' => $width,
            'height' => $height,
            'aspect_ratio' => $aspectRatio
        ];

        $v = Validator::make($commandArgs, [
            'width' => 'integer|min:1',
            'height' => 'integer|min:1',
            'aspect_ratio' => 'string|regex:/^(\d+(\.\d+)?):(\d+(\.\d+)?)$/',
        ]);
        if ($v->fails()) {
            $this->error($v->errors());
            return;
        }

        $this->resolution->create($commandArgs);
        $this->line('Resoultion created.');

    }
}
