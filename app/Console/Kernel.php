<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\GenerateFakers',
        'App\Console\Commands\WallpaperToUser',
        'App\Console\Commands\PurgeDownload',
        'App\Console\Commands\AddToFavorites',
        'App\Console\Commands\OptimizeImages',
        'App\Console\Commands\MakeResolution',
        'App\Console\Commands\BuildWallpaperPreview'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $filePath = storage_path() . '/logs/cron.log';
        $schedule->command('wallpaper:purgedl')->everyMinute()->sendOutputTo($filePath);
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
