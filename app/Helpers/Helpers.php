<?php

use Rfd\ImageMagick\ImageMagick;
use Rfd\ImageMagick\CLI\Operation\Factory as OperationFactory;
use Rfd\ImageMagick\Image\File;
use Rfd\ImageMagick\Options\CommonOptions;
use Google\Cloud\Vision\VisionClient;
/**
 * Format an array of objects to a regular array(K,V)
 *
 * @param $array
 * @return array
 */
function objectArrayToArray($array){
    $formattedArray = array();
    foreach($array as $entry){
        $formattedArray[$entry['setting']] = $entry['setting_value'];
    }
    return $formattedArray;
}

/**
 * Generate a random string
 *
 * @param int $length
 * @return string
 */
function generateRandomString($length = 10, $lowercase = false) {
    $characters = ($lowercase)? '0123456789abcdefghijklmnopqrstuvwxyz':'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/**
 * Shorten large numbers into abbreviations (i.e. 1,500 = 1.5k)
 *
 * @param int $number Number to shorten
 * @return String A number with a symbol
 */ 
function numberAbbreviation($number) {
    $abbrevs = array(12 => "T", 9 => "B", 6 => "M", 3 => "K", 0 => "");
    foreach($abbrevs as $exponent => $abbrev) {
        if($number >= pow(10, $exponent)) {
        	$display_num = $number / pow(10, $exponent);
        	$decimals = ($exponent >= 3 && round($display_num) < 100) ? 1 : 0;
            return number_format($display_num,$decimals) . $abbrev;
        }
        return $number;
    }
}

function optimizeImageFile($filePath, $quality = 85, $options = CommonOptions::FORMAT_JPG){
    $im = new ImageMagick(new OperationFactory());
    $image = new File($filePath);
    $output = new File($filePath);
    $operation_builder = $im->getOperationBuilder($image);
    $operation_builder
        ->convert($options)
        ->strip()
        ->quality($quality)
        ->finish($output);
}

function pepareNotificationData($notifications){
    $notifications = $notifications->transform(function ($item, $key) {
                    $data = json_decode($item['data']);
                    switch ($data->type) {
                        case NOTIFICATION_TYPE_MODERATION_PUBLISH:
                            try {
                                $wallpaper = Wallpaper::findById($data->wallpaper);
                            } catch(\Exception $e){
                                return;
                            }
                            if($wallpaper->user_id != \Sentinel::check()->id)
                                return;
                            $data->title = $wallpaper->title;
                            $data->slug = $wallpaper->slug;
                            $data->gallery = $wallpaper->gallery->name;
                            $data->gallery_slug = $wallpaper->gallery->slug;
                            $data->thumb = $wallpaper->thumbnailPath();
                            $data->link = $data->slug;
                            break;
                        case NOTIFICATION_TYPE_MODERATION_DISCARD:
                            break;
                        case NOTIFICATION_TYPE_FAVORITE:
                            try {
                                $wallpaper = Wallpaper::findById($data->wallpaper);
                                $user = Sentinel::findById($data->user);
                            } catch(\Exception $e){
                                return;
                            }
                            if($wallpaper->user_id != \Sentinel::check()->id)
                                return;
                            $data->title = $wallpaper->title;
                            $data->slug = $wallpaper->slug;
                            $data->thumb = $wallpaper->thumbnailPath();
                            $data->username = $user->username;
                            $data->user_avatar = $user->avatarPath();
                            $data->link = $data->slug;
                            break;
                        default:
                            break;
                    }
                    $data->id = $item['id'];
                    $data->read = $item['read'];
                    return $data;
                });
                $filtered = $notifications->filter(function ($value, $key) {
                    return isset($value);
                });
    //\Log::debug('filtered '.($filtered));
    return $filtered;
                
}

/**
 * Test if image is SFW or not
 *
 * @param string $path
 * @return bool
 */
function isSFW($file){
    $vision = new VisionClient([
        'projectId' => env('GOOGLE_APPLICATION_ID'),
        'keyFile' => json_decode(\Storage::get(env('GOOGLE_KEY_FILE')), true)
    ]);
    $image = $vision->image($file, [
        'SAFE_SEARCH_DETECTION'
    ]);
    $result = $vision->annotate($image);
    $safe = $result->safeSearch();
    //\Log::debug('Adult: '. ($safe->isAdult()? 'yes' : 'no'));
    //\Log::debug('Spoof: '. ($safe->isSpoof()? 'yes' : 'no'));
    //\Log::debug('Medical: '. ($safe->isMedical()? 'yes' : 'no'));
    //\Log::debug('Violence: '. ($safe->isViolent()? 'yes' : 'no'));
    return (!$safe->isAdult() && !$safe->isSpoof() && !$safe->isMedical() && !$safe->isViolent());
}

function slugify($text){
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) {
        return '';
    }
    return $text;
}

function setNullIfEmpty($value){
    return (empty($value))? null : $value;
}

function addHttp($url) {
    if (!empty($url) && !preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}