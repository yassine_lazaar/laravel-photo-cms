<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 01/03/2016
 * Time: 14:38
 */

namespace App\Composers;

use Illuminate\Contracts\View\View;
use Tag;


class FooterComposer implements ComposerInterface
{

    /**
     * Comopose view
     *
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        $view->with([
            'trendingTags' => $this->getTrendingTags()
        ]);
    }

    /**
     * Get trending tags device filterd or not 
     *
     * @return Collection
     */
    private function getTrendingTags()
    {
        if(session()->has(SESSION_DEVICE_ID_KEY))
            return Tag::getTrendingTagsFiltered();
        return Tag::getTrendingTags();
    }
}