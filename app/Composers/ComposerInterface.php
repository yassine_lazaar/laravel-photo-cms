<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 01/03/2016
 * Time: 13:21
 */

namespace App\Composers;

use Illuminate\Contracts\View\View;

interface ComposerInterface
{

    /**
     * Comopose view
     *
     * @param View $view
     * @return mixed
     */
    public function compose(View $view);
}