<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 01/03/2016
 * Time: 14:38
 */

namespace App\Composers;


use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Contracts\View\View;
use Gallery;
use Device;


class FrontComposer implements ComposerInterface
{

    /**
     * Comopose view
     *
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        // Check if picked device is not excluded
        if(session()->has(SESSION_DEVICE_ID_KEY)){
            $pickedDevice = Device::getPickedDevice();
            if(isset($pickedDevice)){
                if($pickedDevice->excluded == 1){
                    session()->forget(SESSION_DEVICE_ID_KEY);
                    session()->forget(SESSION_DEVICE_NAME_KEY);
                }
            }
        }

        if(Sentinel::check()){
            $user = Sentinel::getUser();
            $notifications = feed()->pull($user);
            $view->with([
                'user' => $user,
                'notifications' => pepareNotificationData($notifications)
            ]);
        }
        $view->with([
            'galleries' => Gallery::getOrderedGalleries(true)
        ]);
    }
}