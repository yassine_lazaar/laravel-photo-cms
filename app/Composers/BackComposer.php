<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 01/03/2016
 * Time: 14:38
 */

namespace App\Composers;


use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Contracts\View\View;


class BackComposer implements ComposerInterface
{

    /**
     * Comopose view
     *
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        $user = Sentinel::getUser();
        $view->with('user', $user);
    }
}