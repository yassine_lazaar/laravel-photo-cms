<?php

namespace App\Providers;

use App\Providers\Managers\WallpaperManager;
use Illuminate\Support\ServiceProvider;

class WallpaperServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('wallpaper', function ($app) {
            return new WallpaperManager();
        });
    }


    public function provides()
    {
        return ['wallpaper'];
    }


}
