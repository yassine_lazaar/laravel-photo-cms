<?php

namespace App\Providers;

use App\Providers\Managers\DeviceManager;
use Illuminate\Support\ServiceProvider;

class DeviceServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('device', function ($app) {
            return new DeviceManager();
        });
    }

    public function provides()
    {
        return ['device'];
    }

}
