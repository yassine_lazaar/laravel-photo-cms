<?php

namespace App\Providers;

use App\Providers\Managers\GalleryManager;
use Illuminate\Support\ServiceProvider;

class GalleryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('gallery', function ($app) {
            return new GalleryManager();
        });
    }

    public function provides()
    {
        return ['gallery'];
    }

}
