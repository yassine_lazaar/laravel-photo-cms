<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 30/03/2016
 * Time: 15:15
 */

namespace App\Providers\Managers;

use LRedis;
use App;
use Tag;
use Device;
use DB;
use Exception;
use PDO;
use Wallpaper;
use Gallery;
use Carbon\Carbon;


class SettingsManager
{
    private $cache;

    /**
     * SettingsManager constructor.
     */
    public function __construct()
    {
        $this->cache = LRedis::connection();
    }

    /**
     *  Get site cached settings
     *
     * 
     * @return mixed
     */
    public function getCachedSettings(){
        if ($this->cache->exists(SITE_SETTINGS_CACHE_KEY))
            $settings = unserialize($this->cache->get(SITE_SETTINGS_CACHE_KEY));
        else{
            DB::setFetchMode(PDO::FETCH_ASSOC);
            $settings = objectArrayToArray(DB::table('site_settings')->whereNotIn('setting', [SETTING_SITE_TOS, SETTING_SITE_PRIVACY_POLICY, SETTING_SITE_COPYRIGHT_POLICY, SETTING_SITE_FAQ])->get());
            DB::setFetchMode(PDO::FETCH_CLASS);

            $this->cache->set(SITE_SETTINGS_CACHE_KEY, serialize($settings));
            $this->cache->persist(SITE_SETTINGS_CACHE_KEY);
        }
        //\Log::debug('siteSettings '.json_encode($settings));
        return (array)$settings;
    }

    /**
     *  Get specific setting
     *
     * @param string $setting
     * @return mixed
     */
    public function getSetting($setting){
        switch($setting){
            case SETTING_SITE_TOS:
            case SETTING_SITE_PRIVACY_POLICY:
            case SETTING_SITE_COPYRIGHT_POLICY:
            case SETTING_SITE_FAQ:
                return DB::table('site_settings')->where('setting', $setting)
                ->get()
                ->transform(function ($item, $key) {
                    return [$item->setting => $item->setting_value];
                })
                ->collapse()
                ->toArray()[$setting];
            break;
            default:
                return $this->getCachedSettings()[$setting];
        }
    }

    /**
     *  Get specific setting
     *
     * @return mixed
     */
    public function allSettings(){
        return array_merge(
            DB::table('site_settings')
            ->whereIn('setting', [SETTING_SITE_TOS, SETTING_SITE_PRIVACY_POLICY, SETTING_SITE_COPYRIGHT_POLICY, SETTING_SITE_FAQ])->get()
            ->transform(function ($item, $key) {
                return [$item->setting => $item->setting_value];
            })
            ->collapse()
            ->toArray(),
            $this->getCachedSettings()
        );
    }


    /**
     *  Get specific setting
     *
     * @param $request
     * @return mixed
     */
    public function setSettings($request){
        try {
            $request = collect($request);
            $settings = $request->keys()->toArray();
            $query = "UPDATE `site_settings` SET `setting_value` = CASE";
            foreach($settings as $s){
                $query .= " WHEN `setting` = '".$s."' THEN '".addslashes($request[$s])."'";
            }
            $query .=" END WHERE `setting` IN ('".implode('\',\'',$settings)."')";
            DB::statement($query);
            $this->cache->del(SITE_SETTINGS_CACHE_KEY);

        } catch (Exception $e){
            \Log::debug('setSettings Exception with message: '.$e->getMessage());
        }
    }

    /*
    * Generate sitemap
    *
    */
    public function generateSitemap(){
        $sitemap = App::make("sitemap");
        $baseUrl = url('/');

        // Home
        $sitemap->add($baseUrl, Carbon::now(), '1', 'daily');

        // Wallpapers
        $filterFn = function($q) {
            $q->where('published', 1);
            $q->whereHas('gallery', function($query){
                $query->where('published', 1);
            });
        };
        $wallpapers = Wallpaper::query(
                    $filterFn, 
                    ['title', 'slug', 'gallery_id', 'filename', 'updated_at'],
                    'created_at'
                );
        if(!$wallpapers->isEmpty()){
            foreach ($wallpapers as $wallpaper) {
                $url = route('getwallpaper', $wallpaper->slug.'.htm');
                if(strcmp($this->getSetting(SETTING_SITEMAP_INCLUDE_IMAGES), 'on') == 0){
                    $previewPath = $wallpaper->previewPath();
                    $images = [
                        ['url' => $previewPath , 'title' => $wallpaper->title, 'caption' => $wallpaper->description]
                    ];
                    $sitemap->add($url, $wallpaper->updated_at, '0.6', 'weekly', $images);
                } else {
                    $sitemap->add($url, $wallpaper->updated_at, '0.6', 'weekly');
                }
            }
        }

        // Galleries
        if(strcmp($this->getSetting(SETTING_SITEMAP_INCLUDE_GALLERIES), 'on') == 0){
            $filters = [
                ['published', '=', '1']
            ];
            $galleries = Gallery::query(
                        $filters, 
                        ['id', 'slug', 'updated_at'],
                        'created_at',
                        ['wallpapers' => function ($query) {
                            $query->with('gallery');
                            $query->where('published', '=', '1');
                        }]
                    );
                    
            if(!$galleries->isEmpty()){
                 foreach ($galleries as $gallery) {
                    $images = $gallery->wallpapers->map(function ($item, $key) use ($baseUrl){
                        return [
                            'url' => $item->previewPath(),
                            'title' => $item->title,
                            'caption' => ($item->description)? $item->description : $item->title
                        ];
                    })->toArray();
                    $url = route('getgallery', $gallery->slug);
                    $sitemap->add($url, $gallery->updated_at, '0.6', 'weekly', $images);
                }
            }
        }

        // Tags
        if(strcmp($this->getSetting(SETTING_SITEMAP_INCLUDE_TAGS), 'on') == 0){
            $filters = [
                ['published', '=', '1']
            ];
            $tags = Tag::query(
                        $filters, 
                        ['id', 'slug', 'updated_at'],
                        'created_at',
                        ['wallpapers' => function ($query) {
                            $query->with('gallery');
                            $query->where('published', '=', '1');
                        }]
                    );
            if(!$tags->isEmpty()){
                 foreach ($tags as $tag) {
                    $images = $tag->wallpapers->map(function ($item, $key) use ($baseUrl){
                        return [
                            'url' => $item->previewPath(),
                            'title' => $item->title,
                            'caption' => ($item->description)? $item->description : $item->title
                        ];
                    })->toArray();
                    $url = route('gettag', $tag->slug);
                    $sitemap->add($url, $tag->updated_at, '0.6', 'weekly', $images);
                }
            }
        }

        // Devices
        if(strcmp($this->getSetting(SETTING_SITEMAP_INCLUDE_DEVICES), 'on') == 0){
            $filters = [
                ['excluded', '=', '0']
            ];
            $devices = Device::query(
                        $filters, 
                        ['id', 'slug', 'updated_at'],
                        'created_at'
                    );

            if(!$devices->isEmpty()){
                 foreach ($devices as $device) {
                    $deviceLowestResolution = Device::getDeviceLowestResolution($device);
                    $filters = [
                        ['published', '=', '1'],
                        ['width' ,'>=', $deviceLowestResolution->width],
                        ['height' ,'>=', $deviceLowestResolution->height]
                    ];
                    $filterFn = function($q) use($filters)
                    {
                        $q->where($filters);
                        $q->whereHas('gallery', function($query){
                            $query->where('published', 1);
                        });
                    };
                    $images = Wallpaper::query(
                        $filterFn, 
                        ['id', 'title', 'slug', 'description', 'filename', 'gallery_id']
                    );
                    $images = $images->map(function ($item, $key) use ($baseUrl){
                        return [
                            'url' => $item->previewPath(),
                            'title' => $item->title,
                            'caption' => ($item->description)? $item->description : $item->title
                        ];
                    })->toArray();
                    $url = route('getdevice', $device->slug);
                    $sitemap->add($url, $device->updated_at, '0.6', 'weekly', $images);
                }
            }
        }

        $sitemap->store('xml', 'sitemap');
    }

    /**
     * Number of images displayed per page
     *
     * @return mixed
     */
    public function perPage(){
        return $this->getSetting(SETTING_WALLPAPERS_PER_PAGE);
    }

    /**
     * Max upload size
     *
     * @return mixed
     */
    public function maxUploadSize(){
        return $this->getSetting(SETTING_MAX_UPLOAD_SIZE);
    }



}