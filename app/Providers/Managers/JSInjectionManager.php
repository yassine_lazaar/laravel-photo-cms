<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 25/02/2016
 * Time: 08:36
 */

namespace App\Providers\Managers;

use JavaScript;
use Sentinel;
use URL;
use App\Models\User as User;

class JSInjectionManager
{
    private $sessionLifetime;
    private $sessionModalMessage;
    private $sessionRedirectURL;
    private $jsData;

    /**
     * JSInjection constructor.
     */
    public function __construct()
    {
        $this->sessionLifetime = config('session.lifetime');
        $this->jsData = array();
    }

    /**
     * Add session modal JS data (msg and url)
     *
     * @param bool $hasPostRequest Whether post request are sent from this route or not
     */
    public function addSessionModalData($hasPostRequest = false){
        $loggedCheck = Sentinel::check();
        $rememberCheck = cookie(config('cartalyst.sentinel.cookie'));

        if ($loggedCheck && !$rememberCheck){ // Logged and not remembered
            $this->sessionModalMessage = 'Your session is no longer valid. You\'ve been logged out.';
            $this->sessionRedirectURL = route('getlogin');
            return;
        }

        else if ($loggedCheck && $rememberCheck && $hasPostRequest){ // Logged and remembered
            $this->sessionModalMessage = 'Your session is no longer valid. Please refresh the page.';
            $this->sessionRedirectURL = URL::current();
            return;
        }

        else if (!$loggedCheck && $hasPostRequest){ // Guest
            $this->sessionModalMessage = 'Your session is no longer valid. Please refresh the page.';
            $this->sessionRedirectURL = URL::current();
        }

    }

    /**
     * add specific JS data to be injected
     *
     * @param array $data
     */
    public function addData(array $data){
        $this->jsData = $data;
    }

    /**
     * Inject JS data into views
     *
     * @return array
     */
    public function inject(){
        $this->jsData = array_merge(
                $this->jsData,
                [SESSION_LOGGED_KEY => (\Sentinel::check() instanceof User)? true : false]
            );
            
        if($this->sessionModalMessage && $this->sessionRedirectURL){
            $this->jsData = array_merge(
                $this->jsData,
                [SESSION_LIFETIME_KEY => $this->sessionLifetime]
            );
            $sessionJSData = [SESSION_MODAL_MESSAGE_KEY => $this->sessionModalMessage,
                SESSION_MODAL_REDIRECT_KEY => $this->sessionRedirectURL];
            $this->jsData = array_merge($this->jsData, $sessionJSData);
        }
        if($this->jsData){
            return JavaScript::put($this->jsData);
        }
        return null; // TODO: throw exception
    }




}
