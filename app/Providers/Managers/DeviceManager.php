<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 25/02/2016
 * Time: 08:36
 */

namespace App\Providers\Managers;

use App\Repositories\DevicesRepository;
use Resolution;
use Exception;

class DeviceManager
{

    private $device;

    /**
     * DeviceManager constructor.
     */
    public function __construct()
    {
        $this->device = new DevicesRepository(app());
    }

    /**
     * Get device.
     *
     * @param integer $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->device->find($id);
    }

    /**
     * Custom device query
     *
     * @param array $filters
     * @param array $columns
     * @param string $orderBy
     * @return mixed
     */
    public function query($filters, $columns = array('*'), $orderBy = 'created_at', $eager = [])
    {
        if(!empty($filters))
            return $this->device->where($filters, $columns, $orderBy, $eager);
        else
            return $this->device->selectAndOrder($columns, $orderBy);            
    }

    public function getDeviceFilter()
    {
        if(session(SESSION_DEVICE_ID_KEY)){    
            $device = $this->getPickedDevice();
            $lowestResolution = $this->getDeviceLowestResolution($device);
            return [
                ['width' ,'>=', $lowestResolution->width],
                ['height' ,'>=', $lowestResolution->height]
            ];
        }
    }

    public function getDeviceLowestResolution($device){
        return $this->getSortedResolutions($device)->last();
    }

    public function getSortedResolutions($device){
        return $device->resolutions->sortByDesc(function ($item, $key) {
            return $item['width'] * $item['height'];
        })->flatten(1);
        return $r;        
    }

    public function getPickedDevice(){
        //\Log::debug('session '.session(SESSION_DEVICE_ID_KEY));
        if(session()->has(SESSION_DEVICE_ID_KEY))
            return $this->device->find(session(SESSION_DEVICE_ID_KEY));
        return null;
    }

    public function filterWallpapersToPickedDevice($wallpapers){
        if(!empty($this->getPickedDevice()) && !empty($wallpapers)){
            $device = $this->getPickedDevice();
            $lowestResolution = $this->getDeviceLowestResolution($device);
            $wallpapers->filter(function ($item, $key) use($lowestResolution) {
                return ($item->width > $lowestResolution->width)  &&
                    ($item->height > $lowestResolution->height);
            });
        }
        return $wallpapers;
    }
}