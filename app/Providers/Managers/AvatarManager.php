<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 25/02/2016
 * Time: 08:36
 */

namespace App\Providers\Managers;

use App;
use App\Models\User;
use Cartalyst\Sentinel\Users\UserInterface;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AvatarManager
{

    private $user;
    private $storage;

    /**
     * AvatarManager constructor.
     */
    public function __construct()
    {
        $this->user = App::make('sentinel.users');
        $this->storage = Storage::disk('local');
    }
    
    /**
     * Save avatar and associated data
     *
     * @param \Intervention\Image\Image $image
     * @param array $params // Cropping params
     * @param $user
     */
    public function saveAvatar($image, $params, $user){
        $filename = $this->getFilename($user);
        $this->storeUserAvatar($image, $params, $filename);
        $this->user->update(
            $user->id,
            ['avatar' => $filename]
        );
    }

    /**
     * Save avatar from social provider
     *
     * @param \Intervention\Image\Image $image
     * @param $user
     */
    public function saveSocialAvatar($image, $user){
        $filename = $this->getFilename($user);
        $this->storeSocialAvatar($image, $filename);
        $this->user->update(
            $user->id,
            ['avatar' => $filename]
        );
    }

    /**
     * Delete avatar and associated data
     * @param UserInterface $user
     *
     * @return bool
     */
    public function deleteAvatar(UserInterface $user){
        $filename = $user->avatar;
        if(strcmp($filename, AVATAR_DEFAULT) == 0)
            return false;
        
        $this->user->update(
            $user->id,
            ['avatar' => AVATAR_DEFAULT]
        );

        try {
            return $this->storage->delete([
                'public/'.PROFILE_IMAGE_PATH.$filename.'_1x.jpg',
                'public/'.PROFILE_IMAGE_PATH.$filename.'_2x.jpg',
                'public/'.PROFILE_IMAGE_PATH.$filename.'_3x.jpg',
            ]);
        } catch (Exception $e){
            return false;
        }

    }


    /**
     * Store avatar image
     * TODO:: Refactor
     * @param \Intervention\Image\Image $image
     * @param array $params // Cropping params
     * @param $filename
     *
     */
    private function storeUserAvatar($image, $params, $filename){
        $avatarPath = 'storage/'.PROFILE_IMAGE_PATH;

        $image->backup();
        $avatar4xPath = $avatarPath.$filename.'_4x.jpg';
        $zoom = $params['zoom'];
        $avatar4x = $image->resize(intval($image->width() * $zoom), intval($image->height() * $zoom))
            ->crop(AVATAR_SIZE_4X, AVATAR_SIZE_4X, abs(intval($params['x'])), abs(intval($params['y'])))
            ->save($avatar4xPath, 100);
        optimizeImageFile($avatar4xPath);

        $avatar3xPath = $avatarPath.$filename.'_3x.jpg';
        $avatar3x = $avatar4x->resize(AVATAR_SIZE_4X/2, AVATAR_SIZE_4X/2)
            ->save($avatar3xPath, 100);
        optimizeImageFile($avatar3xPath);

        $avatar2xPath = $avatarPath.$filename.'_2x.jpg';        
        $avatar2x = $avatar3x->resize(AVATAR_SIZE_4X/4, AVATAR_SIZE_4X/4)
            ->save($avatar2xPath, 100);
        optimizeImageFile($avatar2xPath);
            
        $avatar1xPath = $avatarPath.$filename.'_1x.jpg';        
        $avatar2x->resize(AVATAR_SIZE_4X/8, AVATAR_SIZE_4X/8)
            ->save($avatar1xPath, 100);
        optimizeImageFile($avatar1xPath);
        
        $image->destroy(); // Destroy what you make
            
    }

    /**
     * Store social avatar image
     * TODO:: Refactor
     * @param \Intervention\Image\Image $image
     * @param $filename
     *
     */
    private function storeSocialAvatar($image, $filename){
        $avatarPath = 'storage/'.PROFILE_IMAGE_PATH;
        $avatar4xPath = $avatarPath.$filename.'_4x.jpg';
        $avatar4x = $image->fit(AVATAR_SIZE_4X, AVATAR_SIZE_4X)
            ->save($avatar4xPath, 100);
        optimizeImageFile($avatar4xPath);

        $avatar3xPath = $avatarPath.$filename.'_3x.jpg';
        $avatar3x = $avatar4x->resize(AVATAR_SIZE_4X/2, AVATAR_SIZE_4X/2)
            ->save($avatar3xPath, 100);
        optimizeImageFile($avatar3xPath);

        $avatar2xPath = $avatarPath.$filename.'_2x.jpg';        
        $avatar2x = $avatar3x->resize(AVATAR_SIZE_4X/4, AVATAR_SIZE_4X/4)
            ->save($avatar2xPath, 100);
        optimizeImageFile($avatar2xPath);
            
        $avatar1xPath = $avatarPath.$filename.'_1x.jpg';        
        $avatar2x->resize(AVATAR_SIZE_4X/8, AVATAR_SIZE_4X/8)
            ->save($avatar1xPath, 100);
        optimizeImageFile($avatar1xPath);
        
        $image->destroy(); // Destroy what you make
    }


    /**
     * Get avatar filename
     *
     * @param User $user
     * @return string
     */
    public static function getFilename(User $user){
        if(strcmp($user->avatar, AVATAR_DEFAULT) == 0)
            return $user->getUserId().'_avatar';
        else
            return $user->avatar;

    }

    /**
     * Store cover image
     * 
     * @param String $imageData
     * @param Eloquent user $user
     */
    public function saveCover($imageData, $user){
        $filename = $user->id.'_'.$user->username.'_cover.jpg';
        $image = \Image::make($imageData);
        $coverPath = 'storage/'.COVER_IMAGE_PATH.$filename;
        $image->save($coverPath, 100);
        optimizeImageFile($coverPath);
        $image->destroy();
        $url = 'https://static3.'.env('APP_DOMAIN').'/'.COVER_IMAGE_PATH.$filename;
        $output = \Artisan::call('cloudflare:cache:purge', [
            '--file' => [$url]
        ]);
    }

    /**
     * Delete cover image
     * 
     * @param Eloquent user $user
     */
    public function deleteCover($user){
        $filename = $user->id.'_'.$user->username.'_cover.jpg';
         try {
            $url = 'https://static3.'.env('APP_DOMAIN').'/'.COVER_IMAGE_PATH.$filename;
            $output = \Artisan::call('cloudflare:cache:purge', [
                '--file' => [$url]
            ]);
            return $this->storage->delete([
                'public/'.COVER_IMAGE_PATH.$filename,
            ]);
        } catch (Exception $e){
            return false;
        }
    }
}