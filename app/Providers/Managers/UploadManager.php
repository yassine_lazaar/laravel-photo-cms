<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 04/11/2016
 * Time: 22:58
 */

namespace App\Providers\Managers;

use Exception;
use Carbon\Carbon;
use Gallery;
use Tag;
use Wallpaper;
use App\Repositories\UploadsRepository;
use Storage;
use Image;
use Resolution;

class UploadManager
{
    private $upload;
    private $storage;

    /**
     * ImageManager constructor.
     */
    public function __construct()
    {
        $this->upload = new UploadsRepository(app());
        $this->storage = Storage::disk('local');
    }

    
    /**
     * Get all uploads
     *
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        return $this->upload->all($columns);
    }


    /**
     * Get upload by id.
     *
     * @param integer $id
     * @return mixed
     * @throws \App\Exceptions\ModelEditException
     */
    public function findById($id)
    {
        return $this->upload->find($id);
    }

    /**
     * User upload.
     */
    public function upload(array $data){
        try {
            $user = \Sentinel::getUser();
            $filename = $user->username.'_'.Carbon::now()->getTimestamp();
            $filePath = 'storage/'.UPLOADS_PATH.'/'.$filename;
            $image = Image::make($data['file']);
            $w = $image->width();
            $h = $image->height();
            $image->backup();
            $image->save($filePath.'.jpg', 100);

            $thumbPath = 'storage/'.UPLOADS_PATH.'_thumbs/';

            $thumbBigPath = $thumbPath.$filename.'_big.jpg';
            $thumbBig = $image->fit(DEFAULT_BIG_THUMB_WIDTH, DEFAULT_BIG_THUMB_HEIGHT)
                ->save($thumbBigPath, 100);
                optimizeImageFile($thumbBigPath);
            $image->reset();
            
            $thumb3xPath = $thumbPath.$filename.'_3x.jpg';
            $thumb3x = $image->fit(DEFAULT_THUMB_WIDTH, DEFAULT_THUMB_HEIGHT)
                ->save($thumb3xPath, 100);
                optimizeImageFile($thumb3xPath);
                
            $thumb2xPath = $thumbPath.$filename.'_2x.jpg';            
            $thumb2x = $thumb3x->resize(DEFAULT_THUMB_WIDTH/2, DEFAULT_THUMB_HEIGHT/2)
                ->save($thumb2xPath, 100);
                optimizeImageFile($thumb2xPath);
                
            $thumb1xPath = $thumbPath.$filename.'_1x.jpg';                    
            $thumb2x->resize(DEFAULT_THUMB_WIDTH/4, DEFAULT_THUMB_HEIGHT/4)
                ->save($thumb1xPath, 100);
                optimizeImageFile($thumb1xPath);

            $result = $this->upload->create([
                'title' => $data['title'],
                'description' => $data['description'],
                'tags' => setNullIfEmpty(collect($data['tags'])->map(function($item){
                    return ['name' => $item];
                })),
                'filename' => $filename,
                'width' => $w,
                'height' => $h,
                'license' => $data['license'],
                'author' => setNullIfEmpty($data['author']),
                'author_link' => setNullIfEmpty($data['author_website']),
                'user_id' => $user->id,
                'gallery_id' => $data['category']
            ]);

            $image->destroy();
            return $result;
        } catch (Exception $e) {
            \Log::debug('Error while saving user upload: '.$e->getMessage());
            return false;
        }

    }


    /**
     * Update upload details and publish
     *
     * @param int $id
     * @param Collection $data
     * @return mixed
     */
    public function edit($id, $data)
    {
        $upload = $this->findById($id);
        $filename = slugify($upload->title);

        $params = $data['upload_crop_params'];
        $zoom = $params['zoom'];

        $wallpaper = Wallpaper::make([
                'title' => $data['upload_title'],
                'slug' => $data['upload_slug'],
                'description' => setNullIfEmpty($data['upload_description']),
                'filename' => $filename,
                'user_id' => $upload->user_id,
                'gallery_id' => $data['upload_gallery'],
                'width' => $upload->width,
                'height' => $upload->height,
                'license' => $data['upload_license'],
                'author' => setNullIfEmpty($data['upload_author']),
                'author_link' => setNullIfEmpty($data['upload_author_link']),
                'thumbnail_params' => $params,
                'published' => 1,
                'featured' => $data['upload_featured']
            ]);
            
        $filename = $filename.'_'.$wallpaper->id;
        $wallpaper->filename = $filename;
        $wallpaper->save();

        $gallery = Gallery::findById($data['upload_gallery']);

        // Move original and delete thumbs
        $this->storage->move(
            'public/'.UPLOADS_PATH.$upload->filename.'.jpg',
            'public/'.GALLERIES_PATH.$gallery->folder.'/'.$filename.'.jpg'
        );
        $this->storage->delete(
            'public/'.UPLOADS_PATH.'_thumbs/'.$upload->filename.'_3x.jpg'
        );
        $this->storage->delete(
            'public/'.UPLOADS_PATH.'_thumbs/'.$upload->filename.'_2x.jpg'
        );
        $this->storage->delete(
            'public/'.UPLOADS_PATH.'_thumbs/'.$upload->filename.'_1x.jpg'
        );
        $this->storage->delete(
            'public/'.UPLOADS_PATH.'_thumbs/'.$upload->filename.'_big.jpg'
        );

        try {
            // Crop and generate thumbs and preview
            $filePath = 'storage/'.GALLERIES_PATH.$gallery->folder.'/'.$filename.'.jpg';
            $thumbPath = 'storage/'.THUMBS_PATH.$gallery->folder.'/'.$filename;
            $previewPath = 'storage/'.PREVIEWS_PATH.$gallery->folder.'/'.$filename.'.jpg';
            
            $image = Image::make($filePath);
            $ar = $image->width() / $image->height();     
            $image->backup();

            $image->fit(DEFAULT_PREVIEW_WIDTH, intval(DEFAULT_PREVIEW_WIDTH / $ar))
                ->save($previewPath, 100);
                optimizeImageFile($previewPath);  

            $image->reset();
            
            $thumbnailBigPath = $thumbPath.'_big.jpg';
            $wr = DEFAULT_BIG_THUMB_WIDTH / DEFAULT_THUMB_WIDTH;
            $hr = DEFAULT_BIG_THUMB_HEIGHT / DEFAULT_THUMB_HEIGHT;
            /*
            $thumbnailBig = $image->resize(intval($image->width() * $zoom * $wr), intval($image->height() * $zoom * $hr))
                ->crop(DEFAULT_BIG_THUMB_WIDTH, DEFAULT_BIG_THUMB_HEIGHT, abs(intval($params['x'] * $wr)), abs(intval($params['y'] * $hr)))
                ->save($thumbnailBigPath, 100);
            optimizeImageFile($thumbnailBigPath);
            */
            $cropW = intval(DEFAULT_BIG_THUMB_WIDTH / ($wr * $zoom));
            $cropH = intval(DEFAULT_BIG_THUMB_HEIGHT / ($hr * $zoom));
            $cropX = intval(abs($params['x']) / $zoom);
            $cropY = intval(abs($params['y']) / $zoom);
            $thumbnailBig = $image->crop($cropW, $cropH, $cropX, $cropY)
                ->resize(DEFAULT_BIG_THUMB_WIDTH, DEFAULT_BIG_THUMB_HEIGHT)
                ->save($thumbnailBigPath, 100);
            optimizeImageFile($thumbnailBigPath);

            $thumbnail3xPath = $thumbPath.'_3x.jpg';
            $thumbnail3x = $thumbnailBig->resize(DEFAULT_THUMB_WIDTH, DEFAULT_THUMB_HEIGHT)
                ->save($thumbnail3xPath, 100);
            optimizeImageFile($thumbnail3xPath);

            $thumbnail2xPath = $thumbPath.'_2x.jpg';
            $thumbnail2x = $thumbnail3x->resize(DEFAULT_THUMB_WIDTH/2, DEFAULT_THUMB_HEIGHT/2)
                ->save($thumbnail2xPath, 100);
            optimizeImageFile($thumbnail2xPath);

            $thumbnail1xPath = $thumbPath.'_1x.jpg';
            $thumbnail2x->resize(DEFAULT_THUMB_WIDTH/4, DEFAULT_THUMB_HEIGHT/4)
                ->save($thumbnail1xPath, 100);
            optimizeImageFile($thumbnail1xPath);

            $image->destroy();

            Wallpaper::addIPTC([
                IPTC_HEADLINE_KEY => $wallpaper->title,
                IPTC_CAPTION_KEY => $wallpaper->description,
                IPTC_CONTACT_KEY => $wallpaper->author,
                IPTC_CREATED_DATE_KEY => $wallpaper->created_at,
                IPTC_KEYWORDS_KEY => $wallpaper->tags->pluck('name')->implode(','),
            ], $previewPath);

            $inputTags = $data['upload_tags'];
            Tag::processSuggestedTags($wallpaper, $inputTags);
            $upload->delete();
            
            $notifData = array(
                NOTIFICATION_TYPE_KEY => NOTIFICATION_TYPE_MODERATION_PUBLISH,
                NOTIFICATION_WALLPAPER_ID_KEY => $wallpaper->id,
                NOTIFICATION_TIMESTAMP_KEY => $wallpaper->created_at
            );
            feed()->push([
                'data' => json_encode($notifData)
            ], $wallpaper->user);

            \Wallpaper::purgeHomeContent();
            \Tag::purgeTrendingTags();

            return $wallpaper;

        } catch (Exception $e){
            \Log::debug('Could not create a wallpaper out of upload | '.$e->getMessage());
        }
    }


    /**
     * Discard upload
     *
     * @param $id
     * @param $reason
     * @return mixed
     */
    public function discard($id, $reason)
    {
        try{
            $upload = $this->upload->find($id);
            $notifData = array(
                NOTIFICATION_TYPE_KEY => NOTIFICATION_TYPE_MODERATION_DISCARD,
                NOTIFICATION_UPLOAD_TITLE_KEY => $upload->title,
                NOTIFICATION_DISCARD_REASON_KEY => ($reason != '')? $reason : 0,
                NOTIFICATION_TIMESTAMP_KEY => \Carbon\Carbon::now()
            );
            feed()->push([
                'data' => json_encode($notifData)
            ], $upload->user);

            // Delete files
            $this->storage->delete(
                'public/'.UPLOADS_PATH.$upload->filename.'.jpg'
            );
            $this->storage->delete(
                'public/'.UPLOADS_PATH.'_thumbs/'.$upload->filename.'_3x.jpg'
            );
            $this->storage->delete(
                'public/'.UPLOADS_PATH.'_thumbs/'.$upload->filename.'_2x.jpg'
            );
            $this->storage->delete(
                'public/'.UPLOADS_PATH.'_thumbs/'.$upload->filename.'_1x.jpg'
            );
            $this->storage->delete(
                'public/'.UPLOADS_PATH.'_thumbs/'.$upload->filename.'_big.jpg'
            );

            $upload->delete();
           
            return true;
            
        } catch (Exception $e){
            \Log::debug('Could not discard upload | '.$e->getMessage());
        }
    }        

}