<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 04/11/2016
 * Time: 22:58
 */

namespace App\Providers\Managers;


use Tag;
use Resolution;
use Settings;
use Device;
use App\Repositories\GalleriesRepository;
use App\Repositories\WallpapersRepository;
use App\Repositories\FavoritesRepository;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\AuthenticationException;
use Intervention\Image\Facades\Image;
use App\Models\Favorite;
use App\Models\Wallpaper;
use App\Models\Gallery;
use App\Models\User;
use App\Exceptions\DownloadLimitExceededException;
use Storage;
use App\Helpers\IPTC as IPTC;
use LRedis;
use Exception;

class WallpaperManager
{
    private $wallpaper;
    private $gallery;
    private $favorite;
    private $storage;
    private $cache;

    /**
     * ImageManager constructor.
     */
    public function __construct()
    {
        $this->wallpaper = new WallpapersRepository(app());
        $this->gallery = new GalleriesRepository(app());
        $this->favorite = new FavoritesRepository(app());
        $this->storage = Storage::disk('local');
        $this->cache = \LRedis::connection();
    }


    /**
     * Get all wallpapers
     *
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        return $this->wallpaper->all($columns);
    }


    /**
     * Filter wallpapers
     *
     * @param array $filters
     * @return mixed
     */
    public function filter($filters)
    {
        if(is_null($filters)) // Opaque
            return collect();

        if(count($filters) == 0) // Empty
            return $this->all();

        try {
            return $this->wallpaper->where($filters);
        } catch (ModelNotFoundException $e){
            return collect();
        }
    }

    /**
     * Make a new wallpaper
     *
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\ModelCreateException
     */
    public function make($data)
    {
        return $this->wallpaper->create($data);
    }


    public function move($id, $galleryId)
    {
        $wallpaper = $this->wallpaper->find($id);
        $gallery = $this->gallery->find($galleryId);

        if($wallpaper->gallery->id == $galleryId) // Same gallery. No need to move.
            return null;

        // Move files
        $this->storage->move(
            'public/'.GALLERIES_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'.jpg',
            'public/'.GALLERIES_PATH.$gallery->folder.'/'.$wallpaper->filename.'.jpg'
        );
        $this->storage->move(
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_3x.jpg',
            'public/'.THUMBS_PATH.$gallery->folder.'/'.$wallpaper->filename.'_3x.jpg'
        );
        $this->storage->move(
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_2x.jpg',
            'public/'.THUMBS_PATH.$gallery->folder.'/'.$wallpaper->filename.'_2x.jpg'
        );
        $this->storage->move(
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_1x.jpg',
            'public/'.THUMBS_PATH.$gallery->folder.'/'.$wallpaper->filename.'_1x.jpg'
        );
        $this->storage->move(
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_big.jpg',
            'public/'.THUMBS_PATH.$gallery->folder.'/'.$wallpaper->filename.'_big.jpg'
        );
        $this->storage->move(
            'public/'.PREVIEWS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'.jpg',
            'public/'.PREVIEWS_PATH.$gallery->folder.'/'.$wallpaper->filename.'.jpg'
        );
        $output = $this->wallpaper->update(
            ['gallery_id' => $galleryId],
            $id
        );
        return $output;
        
    }

    /**
     * Publish wallpaper
     *
     * @return bool
     */
    public function publish($wallpaperId)
    {
        $wallpaper = $this->wallpaper->find($wallpaperId);
        $wallpaper->published = 1;
        $wallpaper->save();
        return true;
    }

    /**
     * Delete wallpaper
     *
     * @return bool
     */
    public function delete($wallpaper)
    {
        if(!($wallpaper instanceof Wallpaper)){
            $w = $this->wallpaper->find($wallpaper);
            $this->moveDeletedWallpaperFiles($w);
            $this->removeFavoritesAndTags($w);
            return $this->wallpaper->delete($w->id);
        }
        $this->moveDeletedWallpaperFiles($wallpaper);
        $this->removeFavoritesAndTags($wallpaper);
        return $this->wallpaper->delete($wallpaper->id);
    }

    private function removeFavoritesAndTags(Wallpaper $wallpaper){
        foreach ($wallpaper->tags as $tag){
            $tag->wallpapers()->detach($wallpaper->id);
        }
        foreach ($wallpaper->favorites as $favorite){
            $this->favorite->delete($favorite->id);
        }
    }

    private function moveDeletedWallpaperFiles($wallpaper){
        // Move and delete files
        $this->storage->move(
            'public/'.GALLERIES_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'.jpg',
            'public/'.DELETED_PATH.$wallpaper->filename.'.jpg'
            );

        $this->storage->delete([
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_3x.jpg',
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_2x.jpg',
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_1x.jpg',
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_big.jpg',
            'public/'.PREVIEWS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'.jpg'
        ]);
    }

    /**
     * Generate a unique title for processing
     *
     * @return string
     */
    public function generateUniqueTitle()
    {
        $title = '';
        while(true){
            try {
                $title = generateRandomString();
                $model = $this->wallpaper->findBy('title', $title);
            } catch(ModelNotFoundException $e){
                break;
            }
        }
        return $title;
    }

    private function deleteWallpaperFiles($wallpaper){

        $this->storage->delete([
            'public/'.GALLERIES_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'.jpg',
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_3x.jpg',
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_2x.jpg',
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_1x.jpg',
            'public/'.THUMBS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'_big.jpg',
            'public/'.PREVIEWS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'.jpg'
        ]);
    }

    /**
     * Generate a unique slug for processing
     *
     * @return string
     */
    public function generateUniqueSlug()
    {
        $slug = '';
        while(true){
            try {
                $slug = generateRandomString(10, true);
                $model = $this->wallpaper->findBy('slug', $slug);
            } catch(ModelNotFoundException $e){
                break;
            }
        }
        return $slug;
    }

    /**
     * Generate a unique filename for processing
     *
     * @param $path
     * @return string
     */
    public function generateUniqueFilename($path)
    {
        $filename = '';
        while(true){
            $filename = generateRandomString();
            if(!$this->storage->exists($path.'/'.$filename.'.jpg'))
                break;
        }
        return $filename;
    }

    /**
     * Update wallpaper details
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    public function edit($id, $data)
    {
        $wallpaper = $this->wallpaper->find($id);
        $sync = [];

        $inputTags = $data['wallpaper_tags'];
        Tag::processSuggestedTags($wallpaper, $inputTags);

        // Check if thumb params or title changed to avoid rebuilding thumbs
        $filename =  slugify($data['wallpaper_title']).'_'.$wallpaper->id;
        $saved_params = $wallpaper->thumbnail_params;
        $params = $data['wallpaper_thumbnail_params'];
        $zoom = $params['zoom'];

        $previewPath = 'storage/'.PREVIEWS_PATH.$wallpaper->gallery->folder.'/'.$wallpaper->filename.'.jpg';
        $newPreviewPath = 'storage/'.PREVIEWS_PATH.$wallpaper->gallery->folder.'/'.$filename.'.jpg';
        
        if ($saved_params != $params || $wallpaper->filename != $filename) {
            try {
                $imagePath = 'storage/'.GALLERIES_PATH. $wallpaper->gallery->folder.'/'.$wallpaper->filename.'.jpg';
                try {
                    $image = Image::make($imagePath);
                } catch(Exception $e){
                    \Log::debug('Error making image '.$e->getMessage());
                }
                $image->save('storage/'.GALLERIES_PATH. $wallpaper->gallery->folder.'/'.$filename.'.jpg', 100);

                $thumbnailPath = 'storage/'.THUMBS_PATH. $wallpaper->gallery->folder;
                
                $thumbnailBigPath = $thumbnailPath.'/'.$filename.'_big.jpg';
                /*
                $wr = DEFAULT_BIG_THUMB_WIDTH / DEFAULT_THUMB_WIDTH;
                $hr = DEFAULT_BIG_THUMB_HEIGHT / DEFAULT_THUMB_HEIGHT;
                $thumbnailBig = $image->resize(intval($image->width() * $zoom * $wr), intval($image->height() * $zoom * $hr))
                    ->crop(DEFAULT_BIG_THUMB_WIDTH, DEFAULT_BIG_THUMB_HEIGHT, abs(intval($params['x'] * $wr)), abs(intval($params['y'] * $hr)))
                    ->save($thumbnailBigPath, 100);
                */

                $wr = DEFAULT_BIG_THUMB_WIDTH / DEFAULT_THUMB_WIDTH;
                $hr = DEFAULT_BIG_THUMB_HEIGHT / DEFAULT_THUMB_HEIGHT;
                //\Log::debug('z '.$zoom);
                //\Log::debug('x '.$params['x']);
                //\Log::debug('y '.$params['y']);
                $cropW = intval(DEFAULT_BIG_THUMB_WIDTH / ($wr * $zoom));
                $cropH = intval(DEFAULT_BIG_THUMB_HEIGHT / ($hr * $zoom));
                $cropX = intval(abs($params['x']) / $zoom);
                $cropY = intval(abs($params['y']) / $zoom);
                //\Log::debug('crop w '.$cropW);
                //\Log::debug('crop h '.$cropH);
                //\Log::debug('crop x '.$cropX);
                //\Log::debug('crop y '.$cropY);
                $thumbnailBig = $image->crop($cropW, $cropH, $cropX, $cropY)
                    ->resize(DEFAULT_BIG_THUMB_WIDTH, DEFAULT_BIG_THUMB_HEIGHT)
                    ->save($thumbnailBigPath, 100);
                optimizeImageFile($thumbnailBigPath);

                $thumbnail3xPath = $thumbnailPath.'/'.$filename.'_3x.jpg';
                $thumbnail3x = $thumbnailBig->resize(DEFAULT_THUMB_WIDTH, DEFAULT_THUMB_HEIGHT)
                    ->save($thumbnail3xPath, 100);
                optimizeImageFile($thumbnail3xPath);

                $thumbnail2xPath = $thumbnailPath.'/'.$filename.'_2x.jpg';
                $thumbnail2x = $thumbnail3x->resize(DEFAULT_THUMB_WIDTH/2, DEFAULT_THUMB_HEIGHT/2)
                    ->save($thumbnail2xPath, 100);
                optimizeImageFile($thumbnail2xPath);

                $thumbnail1xPath = $thumbnailPath.'/'.$filename.'_1x.jpg';
                $thumbnail2x->resize(DEFAULT_THUMB_WIDTH/4, DEFAULT_THUMB_HEIGHT/4)
                    ->save($thumbnail1xPath, 100);
                optimizeImageFile($thumbnail1xPath);
                $image->destroy();
                
                $previewImage = Image::make($previewPath);
                $previewImage->save($newPreviewPath, 100);
                $previewImage->destroy();

                if ($wallpaper->filename != $filename)
                    $this->deleteWallpaperFiles($wallpaper); // Delete old files

            } catch(Exception $e){
                \Log::debug('Something went wrong. '.$e->getMessage());
            }
            
        }
        $this->addIPTC([
                IPTC_HEADLINE_KEY => $data['wallpaper_title'],
                IPTC_CAPTION_KEY => $data['wallpaper_description'],
                IPTC_CONTACT_KEY => $data['wallpaper_author'],
                IPTC_CREATED_DATE_KEY => $wallpaper->created_at,
                IPTC_KEYWORDS_KEY => $wallpaper->tags->pluck('name')->implode(','),
            ], ($wallpaper->filename != $filename)? $newPreviewPath : $previewPath);
        
        $output = $this->wallpaper->update([
            'title' => $data['wallpaper_title'],
            'slug' => $data['wallpaper_slug'],
            'filename' => $filename,
            'description' => setNullIfEmpty($data['wallpaper_description']),
            'license' => $data['wallpaper_license'],
            'author' => setNullIfEmpty($data['wallpaper_author']),
            'author_link' => setNullIfEmpty($data['wallpaper_author_link']),
            'published' => $data['wallpaper_published'],
            'featured' => $data['wallpaper_featured'],
            'thumbnail_params' => json_encode($params)
        ], $wallpaper->id);
        return $output;
    }

    /**
     * Custom wallpapers query
     *
     * @param array $filters
     * @param array $columns
     * @param string $orderBy
     * @return mixed
     */
    public function query($filters, $columns = array('*'), $orderBy = 'created_at', $eager = ['user', 'gallery', 'favorites'])
    {
        return $this->wallpaper->where($filters, $columns, $orderBy, $eager);
    }


    /**
     * Custom wallpapers query with favorites count
     *
     * @param array $filters
     * @param array $columns
     * @param string $orderBy
     * @return mixed
     */
    public function queryWithFavoritesCount($filters, $columns = array('*'), $eager = ['user', 'gallery', 'favorites'])
    {
        return $this->wallpaper->makeModel()->withCount('favorites')->where($filters)->with($eager)->orderBy('favorites_count', 'desc')->get($columns);
    }

    /**
     * Get wallpaper by slug.
     *
     * @param string $slug
     * @return mixed
     * @throws \App\Exceptions\ModelEditException
     */
    public function findBySlug($slug, $columns = array('*'), $eager = [])
    {
        return $this->wallpaper->findBy('slug', $slug, $columns, $eager);
    }

    /**
     * Get wallpaper by id.
     *
     * @param integer $id
     * @param array $columns
     * @param array $eager
     * @return mixed
     * @throws \App\Exceptions\ModelEditException
     */
    public function findById($id, $columns = array('*'), $eager = [])
    {
        return $this->wallpaper->find($id, $columns, $eager);
    }

    /**
     * Get related wallpapers from same gallery (TODO: Search tags too)
     *
     * @param $wallpaper
     * @return mixed
     * @throws \App\Exceptions\ModelEditException
     */
    public function getGalleryRelated($wallpaper)
    {
        try {
            $filters = [
                ['id', '!=', $wallpaper->id],
                ['gallery_id', '=', $wallpaper->gallery_id],
                ['published', '=', 1]
            ];
            $deviceFilter = Device::getDeviceFilter();
            $filterFn = function($q) use($filters, $deviceFilter){
                    $q->where($filters);
                    $q->whereHas('gallery', function($query){
                            $query->where('published', 1);
                    });
                    if(!is_null($deviceFilter))
                        $q->where($deviceFilter);
                };
            return $this->wallpaper->random($filterFn, array('*'),  6);
        } catch(ModelNotFoundException $e){
            return collect();
        }
    }

    /**
     * Get related wallpapers from same user
     *
     * @param $wallpaper
     * @return mixed
     * @throws \App\Exceptions\ModelEditException
     */
    public function getUserRelated($wallpaper)
    {
        try {
            $filters = [
                ['id', '!=', $wallpaper->id],
                ['user_id', '=', $wallpaper->user_id],
                ['published', '=', 1]
            ];
            $deviceFilter = Device::getDeviceFilter();
            $filterFn = function($q) use($filters, $deviceFilter){
                    $q->where($filters);
                    $q->whereHas('gallery', function($query){
                            $query->where('published', 1);
                    });
                    if(!is_null($deviceFilter))
                        $q->where($deviceFilter);
                };
            return $this->wallpaper->random($filterFn, array('*'), 6);
        } catch(ModelNotFoundException $e){
            return collect();
        }
    }


    /**
     * Wallpaper download
     *
     * @param string $gallerySlug
     * @param integer $deviceId
     * @return mixed
     * @throws DownloadLimitExceededException
     */
    public function download($wallpaperSlug, $deviceId)
    {
        if (!Sentinel::getUser()){
            if(session(SESSION_DOWNLOADS) >= Settings::getSetting(SETTING_GUEST_DOWNLOAD_LIMIT))
                throw new DownloadLimitExceededException();
            else {
                session([SESSION_DOWNLOADS => session(SESSION_DOWNLOADS) + 1]);
            }
        }

        $wallpaper = $this->findBySlug($wallpaperSlug);
        $device = Device::find($deviceId);
        $resolutions = Device::getSortedResolutions($device);
        if (!$wallpaper || !$device || !$resolutions)
            return null;
        
        // Figure out the best resolution and coordinates to crop to
        $pos = $wallpaper->pointOfInterest();
        $cropCoordinates = [];
        $cropResolution = null;
        foreach($resolutions as $resolution){
            if($resolution->width > $wallpaper->width || $resolution->height > $wallpaper->height)
                continue;

            $cropResolution = $resolution;
            $cropCoordinates = [
                    $pos[0] - intval($resolution->width / 2), 
                    $pos[1] - intval($resolution->height / 2)
                ];
            if($cropCoordinates[0] < 0)
                $cropCoordinates[0] = 0;
            if($cropCoordinates[0] + $resolution->width > $wallpaper->width)
                $cropCoordinates[0] = $wallpaper->width - $resolution->width;
            if($cropCoordinates[1] < 0)
                $cropCoordinates[1] = 0;
            if($cropCoordinates[1] + $resolution->height > $wallpaper->height)
                $cropCoordinates[1] = $wallpaper->height - $resolution->height;
            
            //\Log::debug('pos '.json_encode($pos));
            //\Log::debug('crop resolution '.json_encode($cropResolution));
            //\Log::debug('crop coordinates '.json_encode($cropCoordinates));
            
            break;
        }

        $dlFilename = $wallpaper->user->username.'_'.$cropCoordinates[0].'_'.$cropCoordinates[1].'_'.$cropResolution->width.'_'.$cropResolution->height.'_'.$wallpaper->filename.'.jpg';
        $filepath = 'storage/'.DOWNLOADS_PATH.$dlFilename;
        if (!Storage::disk('local')->exists($filepath)) {
            $image = Image::make($wallpaper->path().'.jpg')
                ->crop($cropResolution->width, $cropResolution->height, $cropCoordinates[0], $cropCoordinates[1])
                ->save($filepath, 100);      
        }
        $wallpaper->downloads += 1;
        $wallpaper->save();
        return '//static3.'.env('APP_DOMAIN').'/download/'.$dlFilename;
    }

    /**
     * Wallpaper favorite
     *
     * @param integer $wallpaperId
     * @param EloquentUser $user
     * @return mixed
     * @throws DownloadLimitExceededException
     */
    public function favorite($wallpaperId, $user = null)
    {
        $user = ($user)? $user : Sentinel::check();
        $wallpaper = $this->wallpaper->find($wallpaperId);

        if (!$wallpaper)
            return null;
            
        $favorite = $user->favorites()->where(['wallpaper_id' => $wallpaper->id])->first();

        if (!isset($favorite)) {
            $favorite = new Favorite;
            $favorite->user_id = $user->id;
            $favorite->wallpaper_id = $wallpaper->id;
            $favorite->save();

            // Check if the favorite is coming from the uploader himself so no notification
            if($wallpaper->user_id != $user->id){
                $notifData = array(
                    NOTIFICATION_TYPE_KEY => NOTIFICATION_TYPE_FAVORITE,
                    NOTIFICATION_WALLPAPER_ID_KEY => $wallpaper->id,
                    NOTIFICATION_OWNER_ID_KEY => $wallpaper->user->id,
                    NOTIFICATION_USER_ID_KEY => $user->id,
                    NOTIFICATION_TIMESTAMP_KEY => \Carbon\Carbon::now()
                );
                feed()->push([
                    'data' => json_encode($notifData)
                ], $wallpaper->user);   
            }
            return true;
        } else {
            if (is_null($favorite->deleted_at)) {
                $favorite->delete();
                return false; 
            } else {
                $favorite->restore();
                return true; 
            }
        }
    }


    public function assignFavorites($wallpapers)
    {
        $favorites = [];
        $user = Sentinel::getUser();
        if ($user){
            $favorites = $user->favorites(false)->pluck('wallpaper_id')->toArray();
        } else {
            return $wallpapers;
        }

        if (!empty($favorites)){
            foreach($wallpapers as $wallpaper){
                if (in_array($wallpaper['id'], $favorites))
                    $wallpaper['favorited'] = true;
            }
        }
        return $wallpapers;
    }

    public function addIPTC($data, $filepath){
        $objIPTC = new IPTC($filepath);
        $objIPTC->setValue(IPTC_HEADLINE, $data[IPTC_HEADLINE_KEY]);
        if(isset($data[IPTC_CAPTION_KEY]))
            $objIPTC->setValue(IPTC_CAPTION, $data[IPTC_CAPTION_KEY]);
        if(isset($data[IPTC_CONTACT_KEY]))
            $objIPTC->setValue(IPTC_CONTACT, $data[IPTC_CONTACT_KEY]);
        if(isset($data[IPTC_CREATED_DATE_KEY]))
            $objIPTC->setValue(IPTC_CREATED_DATE, $data[IPTC_CREATED_DATE_KEY]);
        if(isset($data[IPTC_KEYWORDS_KEY]))
            $objIPTC->setValue(IPTC_KEYWORDS, $data[IPTC_KEYWORDS_KEY]);
    }


    public function search($searchQuery, $suggest = false){
        try {
            $searchQuery = urldecode($searchQuery);
            foreach(explode(' ', $searchQuery) as $term){
                $filters[] = ['title', 'like', "%{$term}%"];
            }
            $deviceFilter = Device::getDeviceFilter();
            $filterFn = function($q) use($filters, $deviceFilter)
            {
                foreach($filters as $f){
                    $q->orWhere($f[0], $f[1], $f[2]);
                }
                $q->where('published', 1);
                $q->whereHas('gallery', function($query){
                    $query->where('published', 1);
                });
                if(!is_null($deviceFilter))
                    $q->where($deviceFilter);
            };

            if ($suggest) {
                $results = $this->query(
                    $filterFn, 
                    ['title', 'slug', 'gallery_id'],
                    'created_at',
                    ['gallery']
                )->take(5);
            } else {
                $results = $this->query(
                    $filterFn, 
                    ['id', 'title', 'slug', 'filename', 'featured', 'views', 'downloads', 'gallery_id', 'user_id']
                );
            }

            //\Log::debug('results '.json_encode($results));
            return $results;
        } catch (ModelNotFoundException $e){
            return [];
        }
    }


    /**
     * Renew home content cache
     */
    public function purgeHomeContent($renew = true){
        $this->cache->del(HOME_CONTENT_CACHE_KEY);
        if($renew)
            $this->getHomeContent();
    }

    /**
     * Get home content
     *
     * @return array
    */
    public function getHomeContent(){
        if ($this->cache->exists(HOME_CONTENT_CACHE_KEY)) {
            return unserialize($this->cache->get(HOME_CONTENT_CACHE_KEY));
        }

        try {
            $filters = [
                ['published', '=', '1']
            ];
            $filterFn = function($q) use($filters)
            {
                $q->where($filters);
                $q->whereHas('gallery', function($query){
                    $query->where('published', 1);
                });
            };

            $wallpapers = $this->query(
                    $filterFn, 
                    ['id', 'title', 'slug', 'filename', 'featured', 'views', 'downloads', 'gallery_id', 'user_id'],
                    'created_at'
                )->take(Settings::perPage());

            $homeContent = [
                'count' => $this->wallpaper->where(
                    [['published', '=', '1']],
                    ['id']
                )->count(),
                'wallpapers' => $wallpapers
            ];
            $this->cache->set(HOME_CONTENT_CACHE_KEY, serialize($homeContent));
            $this->cache->expire(HOME_CONTENT_CACHE_KEY, 120 * 60); // in seconds
            return $homeContent;
        } catch (Exception $e){
            \Log::debug('Error fetching home content | '.$e->getMessage());
            return [
                'count' => 0,
                'wallpapers' => collect()
            ];
        }
    }

}