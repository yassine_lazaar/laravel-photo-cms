<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 20/04/2017
 * Time: 08:36
 */

namespace App\Providers\Managers;

use App\Repositories\ResolutionsRepository;
use App\Repositories\DevicesRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class ResolutionManager
{

    private $resolution;

    /**
     * ResolutionManager constructor.
     */
    public function __construct()
    {
        $this->resolution = new ResolutionsRepository(app());
    }

    /**
     * Get resolution.
     *
     * @param integer $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->resolution->find($id);
    }

    public function getCompatibleResolutions($item, $eager = [])
    {
        try {
            //\Log::debug('item '.json_encode($item));
            return $this->resolution->where(
                [
                    ['width', '<=', $item->width],
                    ['height', '<=', $item->height]
                ],
                array('*'),
                'id',
                $eager
            );
        } catch (ModelNotFoundException $e){
            \Log::debug('Error fetching compatible resolutions | '.$e->getMessage());
            return collect();
        }
    }

    public function getWallpaperCompatibleDevices($item)
    {
        $compatibleDevices = $this->getCompatibleResolutions($item, ['devices'])
        ->pluck('devices')
        ->flatten(1)
        ->unique('id')
        ->filter(function ($item, $key) {
            return !$item->excluded;
        })
        ->sort(function ($item1, $item2) {
            if($item1->type != $item2->type){
               return $item1->type > $item2->type;    
            }
            $makerSort = strcmp($item1->maker, $item2->maker);
            if($makerSort != 0)
                return ($makerSort > 0)? true : false;
            $nameSort = strcmp($item1->name, $item2->name);
            return ($nameSort > 0)? true : false;
        })->values()->flatten(1);
        //\Log::debug('compatibleDevices '.$compatibleDevices);
        
        if(session()->has(SESSION_DEVICE_ID_KEY)){
            $deviceFilterKey = null;
            $deviceFilter = $compatibleDevices->search(function ($item, $key) use (&$deviceFilterKey){
                $deviceFilterKey = $key;
                return $item->id == session(SESSION_DEVICE_ID_KEY);
            });
            $deviceFilter = $compatibleDevices[$deviceFilter];
            \Log::debug('deviceFilter '.$deviceFilter);
            if($deviceFilter){
                $compatibleDevices->splice($deviceFilterKey, 1)[0];
                $compatibleDevices->prepend($deviceFilter);
            }
        }
        return $compatibleDevices;
    }

    // Todo: cache this
    public function getLowestResolutions(){
        $resolutions = $this->resolution->all();
        if(empty($resolutions))
            return null;
        /*
        $resolutions->sortBy(function ($item, $key) {
            return $item->width * $item->height;
        });
        
        return $resolutions->first();
        */

        $lowest = [];
        $resolutions->each(function ($item, $key) use(&$lowest){
            if($item->width >=  $item->height){
                if(!array_key_exists('vertical', $lowest)){
                    $lowest['vertical'] = [
                        'width' => $item->width,
                        'height' => $item->height
                    ];                    
                } else {
                    if($lowest['vertical']['width'] * $lowest['vertical']['height'] > $item->width * $item->height){
                        $lowest['vertical'] = [
                            'width' => $item->width,
                            'height' => $item->height
                        ];
                    }
                }
            } else {
                if(!array_key_exists('horizontal', $lowest)){
                    $lowest['horizontal'] = [
                        'width' => $item->width,
                        'height' => $item->height
                    ];
                } else {
                    if($lowest['horizontal']['width'] * $lowest['horizontal']['height'] > $item->width * $item->height){
                        $lowest['horizontal'] = [
                        'width' => $item->width,
                        'height' => $item->height
                    ];
                    }
                }
            }
        });
        return $lowest;
    }



}