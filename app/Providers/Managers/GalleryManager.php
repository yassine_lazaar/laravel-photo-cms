<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 06/11/2016
 * Time: 02:59
 */

namespace App\Providers\Managers;

use App\Exceptions\ResolutionNotSupportedException;
use App\Models\Gallery;
use Wallpaper;
use App\Repositories\GalleriesRepository;
use App\Repositories\ResolutionsRepository;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\Wallpaper as WallpaperModel;
use LRedis;


class GalleryManager
{
    private $gallery;
    private $resolution;
    private $storage;
    private $cache;

    /**
     * ImageManager constructor.
     */
    public function __construct()
    {
        $this->gallery = new GalleriesRepository(app());
        $this->resolution = new ResolutionsRepository(app());
        $this->storage = Storage::disk('local');
        $this->cache = LRedis::connection();
    }

    /**
     * Create gallery.
     *
     * @param array $data
     * @return mixed
     * @throws \App\Exceptions\ModelCreateException
     * @throws \App\Exceptions\ModelEditException
     */
    public function create(array $data)
    {
        $galleryData = [
            'name' => $data['gallery_name'],
            'slug' => $data['gallery_slug'],
            'description' => $data['gallery_description'],
            'folder' => $data['gallery_folder'],
            'published' => false
        ];

        try {
            $galleryPath = 'public/'.GALLERIES_PATH.$galleryData['folder'];
            $previewPath = 'public/'.PREVIEWS_PATH.$galleryData['folder'];
            $thumbPath = 'public/'.THUMBS_PATH.$galleryData['folder'];
            if(!$this->storage->exists($galleryPath)) {
                $this->storage->makeDirectory($galleryPath);
            }
            if(!$this->storage->exists($previewPath)) {
                $this->storage->makeDirectory($previewPath);
            }
            if(!$this->storage->exists($thumbPath)) {
                $this->storage->makeDirectory($thumbPath);
            }
        } catch (Exception $e){
            \Log::debug('Could not create gallery folders | '.$e->getMessage());
            return;
        }

        if ($newGallery = $this->gallery->create($galleryData)){
            // Add to galleries order cache
            $order = $this->getGalleryOrder();
            $order[] = $newGallery->id;
            $this->setGalleryOrder($order);

            return $newGallery;
        }
    }

    /**
     * Edit gallery.
     *
     * @param array $data
     * @return mixed
     * @throws \App\Exceptions\ModelEditException
     */
    public function edit($data)
    {
        // TODO: Check if folder is valid
        $gallery =  $this->gallery->find($data['gallery_id']);
        $slug = $data['gallery_slug'];
        $folder = $data['gallery_folder'];
        $folderPath = 'public/'.GALLERIES_PATH.$folder;
        if($gallery->protected)
            return response()->json(['error' => GALLERY_PROTECTED_MESSAGE, 'code' => GALLERY_PROTECTED_CODE], 500);
        
        if(strcmp($gallery->folder, $folder) != 0){
            $previewPath = 'public/'.PREVIEWS_PATH.$folder;
            $thumbPath = 'public/'.THUMBS_PATH.$folder;
            $this->storage->move('public/'.GALLERIES_PATH.$gallery->folder, $folderPath);
            $this->storage->move('public/'.PREVIEWS_PATH.$gallery->folder, $previewPath);
            $this->storage->move('public/'.THUMBS_PATH.$gallery->folder, $thumbPath);
        }

        if(strcmp($gallery->slug, $slug) != 0){
            $featurePath = 'public/'.GALLERY_FEATURES_FOLDER;
            $this->storage->move($featurePath.$gallery->slug.'.jpg', $featurePath.$slug.'.jpg');
        }

        $output = $this->gallery->update(array(
            'name' => $data['gallery_name'],
            'slug' => $slug,
            'description' => $data['gallery_description'],
            'folder' => $folder,
            'published' => filter_var($data['gallery_published'], FILTER_VALIDATE_BOOLEAN)
        ), $gallery->id);

        return $output;
    }

    /**
     * Delete gallery
     *
     * @param $id
     * @return mixed
     * @throws \App\Exceptions\ModelDeleteException
     */
    public function delete($id)
    {
        $g = $this->gallery->find($id);
        if($g->protected)
            return response()->json(['error' => GALLERY_PROTECTED_MESSAGE], GALLERY_PROTECTED);

        foreach($g->wallpapers as $w){
            Wallpaper::delete(new WallpaperModel($w->toArray()));
        }

        $galleryPath = 'public/'.GALLERIES_PATH.$g->folder;
        $previewPath = 'public/'.PREVIEWS_PATH.$g->folder;
        $thumbPath = 'public/'.THUMBS_PATH.$g->folder;

        if($this->folderExists($galleryPath))
            $this->storage->deleteDirectory($galleryPath);
        if($this->folderExists($previewPath))
            $this->storage->deleteDirectory($previewPath);
        if($this->folderExists($thumbPath))
            $this->storage->deleteDirectory($thumbPath);

        $output = $this->gallery->delete($id);

        // Remove from galleries order cache
        $order = $this->getGalleryOrder();
        array_splice($order, array_search($id, $order), 1);
        $this->setGalleryOrder($order);

        return $output;
    }


    /**
     * Get galleries in an order specified by the user
     *
     * @param bool $filterUnpublished (for frontend)
     * @return array
     */
    public function getOrderedGalleries($filterUnpublished = false)
    {
        if($this->cache->exists(GALLERY_CACHE_KEY) && $filterUnpublished) {
            return json_decode($this->cache->get(GALLERY_CACHE_KEY));
        }
        try {
            $galleries = null;
            $order = $this->getGalleryOrder();
            $filterFn = function($q) use($order){
                        $q->whereIn('id', $order);
                    };
            $galleries = $this->query($filterFn, ['id', 'name', 'slug', 'published', 'folder', 'description'], $order);
            
            if($filterUnpublished){
                $galleries = $galleries->where('published', '1');
                $this->cache->set(GALLERY_CACHE_KEY, json_encode($galleries));
                $this->cache->persist(GALLERY_CACHE_KEY);
            }
            return $galleries;
        } catch (Exception $e){
            \Log::debug('Could not get galleries | '.$e->getMessage());
            return [];
        }
    }


    /**
     * Get gallery order cache.
     * 
     * @return array
     */
    public function getGalleryOrder()
    {
        if($this->cache->exists(GALLERY_ORDER_CACHE_KEY))
            return json_decode($this->cache->get(GALLERY_ORDER_CACHE_KEY));

        $galleries = $this->gallery->where([
                        ['protected', '=', '0']
                    ]);
        $order = $galleries->pluck('id')->toArray();
        $this->setGalleryOrder($order);
        return $order;
    }

    /**
     * Set gallery order cache.
     * 
     * @param array $order
     * @return void
     */
    public function setGalleryOrder(array $order)
    {
        $this->cache->del(GALLERY_ORDER_CACHE_KEY);
        $this->cache->set(GALLERY_ORDER_CACHE_KEY, json_encode($order));
        $this->cache->persist(GALLERY_ORDER_CACHE_KEY);
    }

    /**
     * Reorder galleries
     *
     * @param $changes
     * @throws \App\Exceptions\ModelEditException
     */
    public function reorder($changes)
    {
        try{
            $changedGallery = $changes['gallery_id'];
            $old = $changes['old'];
            $new = $changes['new'];

        $order = $this->getGalleryOrder();
        array_splice($order, max($new,0), 0, array_splice($order, max($old,0), 1));
        $order = $this->setGalleryOrder($order);
        
        return true;
        } catch (Exception $e){
            \Log::debug('Could not reorder galleries | '.$e->getMessage());
            return false;
        }
    }

    /**
     * Custom gallery query
     *
     * @param array $filters
     * @param array $columns
     * @param string $orderBy
     * @return mixed
     */
    public function query($filters, $columns = array('*'), $orderBy = 'created_at', $eager = [])
    {
        if(!empty($filters))
            return $this->gallery->where($filters, $columns, $orderBy, $eager);
        else
            return $this->gallery->selectAndOrder($columns, $orderBy);            
    }

    /**
     * Scan gallery folder for new files
     *
     * @param $id
     * @return array|null
     */
    public function scanGalleryPath($id)
    {
        $gallery = $this->gallery->find($id);
        $galleryPath = 'public/'.GALLERIES_PATH.$gallery->folder;

        if(!$this->storage->exists($galleryPath))
            return null;

        $presentFiles = collect($this->storage->files($galleryPath));
        $presentFiles = $presentFiles->map(function ($file) {
            return File::basename($file);
        });
        \Log::debug('present :'.$presentFiles);

        try{
            $processedFiles = collect(Wallpaper::query([['gallery_id', '=', $gallery->id]], ['filename'])->toArray())
                ->flatten()
                ->map(function ($file) {
                    return $file.'.jpg';
                });
        } catch (ModelNotFoundException $e){
            $processedFiles = collect();
        }
        \Log::debug('$processed :'.$processedFiles);

        $newFiles = $presentFiles->diff($processedFiles)->flatten();
        \Log::debug('new :'.$newFiles);

        $validFiles = collect();
        foreach ($newFiles as $filename)
        {
            $path = 'storage/'.GALLERIES_PATH.$gallery->folder.'/'.$filename;
            $bytes = File::size($path);
            if ($bytes > \Settings::getSetting(SETTING_MAX_UPLOAD_SIZE) * 1024 * 1024){
                \Log::debug('$bytes :'.$bytes);
                continue;
            }
            try {
                $image = Image::make($path);
            } catch (Exception $e){
                \Log::debug('File not readable :'.$path);
                continue;
            }
            // TODO: check if basename exceeds 255 characters.
            if (!in_array($image->mime(), unserialize(ACCEPTED_MIME_TYPES)) || !getimagesize(storage_path('app/'.$galleryPath.'/'.$filename)))
                continue;
            $validFiles->push($filename);
            $image->destroy();
        }
        \Log::debug('valid :'.$validFiles);
        return $validFiles;
    }


    /**
     * Process image file and add it to db
     *
     * @param $id
     * @param $filename
     * @return mixed
     * @throws ResolutionNotSupportedException
     */
    public function processNewFile($id, $filename)
    {
        $gallery = $this->gallery->find($id);
        $filePath = 'storage/'.GALLERIES_PATH.$gallery->folder.'/'.$filename;

        $image = Image::make($filePath);
        $w = $image->width();
        $h = $image->height();
        $ar = $w / $h;
        $lowestResolutions = \Resolution::getLowestResolutions();
        
        if($w >= $h){
            if(($w < $lowestResolutions['vertical']['width']) || ($h < $lowestResolutions['vertical']['height']))
                throw new ResolutionNotSupportedException();
        } else {
            if(($w < $lowestResolutions['horizontal']['width']) || ($h < $lowestResolutions['horizontal']['height']))
                throw new ResolutionNotSupportedException();
        }
        
        $uniqueFilename = Wallpaper::generateUniqueFilename('storage/'.GALLERIES_PATH.$gallery->folder);
        rename($filePath, 'storage/'.GALLERIES_PATH.$gallery->folder.'/'.$uniqueFilename.'.jpg');

        $thumbPath = 'storage/'.THUMBS_PATH.$gallery->folder.'/';

        $zoom = ($w > $h)? DEFAULT_THUMB_HEIGHT/$h : DEFAULT_THUMB_WIDTH/$w;
        $x = ($w > $h)? -(($w - (DEFAULT_THUMB_WIDTH / $zoom)) / 2) * $zoom : 0;
        $y = ($w > $h)? 0 : -(($h - (DEFAULT_THUMB_HEIGHT / $zoom)) / 2) * $zoom;

        $params = [
            'x' => $x,
            'y' => $y,
            'zoom' => $zoom
        ];

        $previewPath = 'storage/'.PREVIEWS_PATH.$gallery->folder.'/'.$uniqueFilename.'.jpg';
        $image->fit(DEFAULT_PREVIEW_WIDTH,  intval(DEFAULT_PREVIEW_WIDTH / $ar))
            ->save($previewPath, 100);
            optimizeImageFile($previewPath);  

        $thumbBigPath = $thumbPath.$uniqueFilename.'_big.jpg';
        $thumbBig = $image->fit(DEFAULT_BIG_THUMB_WIDTH, DEFAULT_BIG_THUMB_HEIGHT)
            ->save($thumbBigPath, 100);
            optimizeImageFile($thumbBigPath);
        
        $thumb3xPath = $thumbPath.$uniqueFilename.'_3x.jpg';
        $thumb3x = $image->fit(DEFAULT_THUMB_WIDTH, DEFAULT_THUMB_HEIGHT)
            ->save($thumb3xPath, 100);
            optimizeImageFile($thumb3xPath);
            
        $thumb2xPath = $thumbPath.$uniqueFilename.'_2x.jpg';            
        $thumb2x = $thumb3x->resize(DEFAULT_THUMB_WIDTH/2, DEFAULT_THUMB_HEIGHT/2)
            ->save($thumb2xPath, 100);
            optimizeImageFile($thumb2xPath);
            
        $thumb1xPath = $thumbPath.$uniqueFilename.'_1x.jpg';                    
        $thumb2x->resize(DEFAULT_THUMB_WIDTH/4, DEFAULT_THUMB_HEIGHT/4)
            ->save($thumb1xPath, 100);
            optimizeImageFile($thumb1xPath);
            
        $image->destroy();

        $processed =  Wallpaper::make([
            'title' => Wallpaper::generateUniqueTitle(),
            'slug' => Wallpaper::generateUniqueSlug(),
            'filename' => $uniqueFilename,
            'license' => 1,
            'published' => false,
            'featured' => false,
            'user_id' => Sentinel::getUser()->id,
            'width' => $w,
            'height' => $h,
            'gallery_id' => $id,
            'thumbnail_params' => $params
        ]);

        Wallpaper::addIPTC([
            IPTC_HEADLINE_KEY => $processed->title,
            IPTC_CREATED_DATE_KEY => $processed->created_at,
        ], $previewPath);

        return $processed;
    }

    /**
     * Get gallery by id.
     *
     * @param integer $id
     * @return mixed
     * @throws \App\Exceptions\ModelEditException
     */
    public function findById($id)
    {
        return $this->gallery->find($id);
    }

    /**
     * Get gallery by slug.
     *
     * @param string $slug
     * @return mixed
     * @throws \App\Exceptions\ModelEditException
     */
    public function findBySlug($slug)
    {
        $gallery = $this->gallery->findBy('slug', $slug);
        if ($gallery->protected){
            return null;
        } else {
            return $gallery;
        }
    }

    /**
     * Renew gallery cache
     * 
     * @return void
     * 
     */
    public function purgeAndRenewGalleries(){
        \Log::debug('purgeAndRenewGalleries');
        $this->cache->del(GALLERY_CACHE_KEY);
        $renewed = $this->getOrderedGalleries(true);
    }

    /**
     * Check if folder exists.
     *
     * @param string $folder
     * @return bool
     * 
     */
    private function folderExists($folder){
        if($this->storage->exists($folder))
            return true;
        return false;
    }

}