<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 04/11/2016
 * Time: 22:58
 */

namespace App\Providers\Managers;


use App\Repositories\TagsRepository;
use Device;
use Resolution;
use Sentinel;
use DoctrineTest\InstantiatorTestAsset\ExceptionAsset;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\Wallpaper as Wallpaper;


class TagManager
{
    private $tag;
    private $cache;

    /**
     * TagManager constructor.
     */
    public function __construct()
    {
        $this->tag = new TagsRepository(app());
        $this->cache = \LRedis::connection();
    }


    /**
     * Get all tags
     *
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        return $this->tag->all($columns);
    }

    /**
     * TODO: Move to RepositoryInterface
     * Filter tags
     *
     * @param array $filters
     * @return mixed
     */
    public function filter($filters)
    {
        if(is_null($filters)) // Opaque
            return collect();

        if(count($filters) == 0) // Empty
            return $this->all();

        try {
            return $this->tag->where($filters);
        } catch (ModelNotFoundException $e){
            return collect();
        }
    }


    /**
     * Make a new tag
     *
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\ModelCreateException
     */
    public function make($data)
    {
        return $this->tag->create($data);
    }

    /**
     * Edit existing tag
     *
     * @param $id
     * @param $name
     * @param $published
     * @return mixed
     * @internal param $title
     */
    public function edit($id, $name, $published)
    {
        $tag = $this->tag->find($id);
        try{
            $similarTag = $this->tag->whereFirst([
                ['id', '!=', $id],
                ['name', '=', $name]
            ]);
            if($similarTag){
                $similarTagWallpapers = $similarTag->wallpapers()->get();
                if ($similarTagWallpapers->count() > 0) {
                    $tagWallpapers = $tag->wallpapers()->get();
                    if($tagWallpapers->count() > 0){
                        $tag->wallpapers()->sync(
                            $tagWallpapers->merge($similarTagWallpapers)
                        );
                    } else {
                        $tag->wallpapers()->attach($similarTagWallpapers);
                    }
                }
                $similarTag->wallpapers()->sync([]);
                $this->tag->delete($similarTag->id);
            }
        } catch(Exception $e){
            \Log::debug('Exception '.$e->getMessage());
        }
        $tag->name = $name;
        $tag->slug = str_slug($name);
        $tag->published = $published;
        $tag->save();
        return $tag;
    }

    /**
     * Delete existing tag
     *
     * @param $ids
     * @return mixed
     * @throws \App\Exceptions\ModelDeleteException
     */
    public function delete($ids)
    {
        try {
            foreach($ids as $id){
                $tag = $this->tag->find($id);
                $tag->wallpapers()->sync([]);
                $tag->find($id)->delete();
            }
        } catch (Exception $e){
            \Log::debug('Could Not delete tag | '.$e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Get tag by slug
     *
     * @param string $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        return $this->tag->findBy('slug', $slug);
    }

    /**
     * Get tag by id
     *
     * @param integer $id
     * @return mixed
     */
    public function findById($id)
    {
        try {
            return $this->tag->find($id);
        } catch (ModelNotFoundException $e){
            return null;
        }
    }


    /**
     * Get tags for wallpaper
     *
     * @param Wallpaper $wallpaper
     * @return mixed
     */
    public function getPublishedTagsForWallpaper($wallpaper)
    {
        try {
            $tags = $this->tag->makeModel()
            ->whereHas('wallpapers', function($q) use ($wallpaper) {
                $q->where('id', '=', $wallpaper->id);
            })
            ->where('published', '=', 1)
            ->get();
            return $tags;
        } catch (ModelNotFoundException $e){
            return null;
        }
    }

    /**
     * Extract tag filter from request
     *
     * @param Request $request
     * @return mixed
     */
    public function getFilters(Request $request)
    {
        //\Log::debug('Tag suggest request '.json_encode($request->all()));
        $filters = [];
        if($request->get('filter_n') != '')
        {
            $filters[] = ['name', 'like', "%{$request->get('filter_n')}%"];
        }
        return $filters;
    }


    /**
     * Get random tags
     *
     * @param $quantity
     * @return mixed
     */
    public function random($quantity)
    {
       return $this->filterTagsToDeviceAndGalleryScope(
            $this->tag->makeModel()
            ->where('published', '=', 1)
            ->whereHas('wallpapers', function ($query) {
                $query->where('published', '=', '1');
            })
            ->take($quantity)
            ->get()
       );
    }


    /**
     * Custom tag query
     *
     * @param array $filters
     * @param array $columns
     * @param string $orderBy
     * @return mixed
     */
    public function query($filters, $columns = array('*'), $orderBy = 'created_at', $eager = [])
    {
        if(!empty($filters))
            return $this->tag->where($filters, $columns, $orderBy, $eager);
        else
            return $this->tag->selectAndOrder($columns, $orderBy);            
    }

    /**
     * Filter tags inside of picked device's scope and under published galleries (opt-out wallpapers that dont match the picked resolution and its compatible resolutions or whose gallery's unpublished')
     * and unpublished categories
     * 
     * @param $tags
     * @return mixed
     */
    public function filterTagsToDeviceAndGalleryScope($tags)
    {
        $wallpapers = null;
        $filteredTags = $tags->filter(function($tag) {
            
            $wallpapers = $tag->wallpapers->filter(function($w) {
                return $w->gallery->published;
            });

            $wallpapers = Device::filterWallpapersToPickedDevice($wallpapers);
            $tag['validCount'] = $wallpapers->count();
            return $wallpapers->count() > 0;
        });
        return $filteredTags;
    }
    

    /**
     * Sync suggested tags with wallpaper
     * 
     * @param Wallpaper $wallpaper
     * @param $inputTags
     * @return mixed
     */
    public function processSuggestedTags(Wallpaper $wallpaper, $inputTags){
        $sync = [];
        if (isset($inputTags)) {
            foreach($inputTags as $tag){
                if (array_has($tag, 'id')) {
                    $tag = $this->findbyId($tag['id']);
                    if ($tag && $tag->published)
                        $sync[] = $tag['id'];
                } else {
                    if (array_has($tag, 'name')) {
                        try{
                            // Look for a tag with similar slug instead of creating a new one.
                            $similar = $this->findBySlug(str_slug($tag['name']));
                        } catch(ModelNotFoundException $e){
                            $similar = null;
                        }
                        if (isset($similar) && $similar->published) {
                            if (!in_array($similar->id, $sync, true)) {
                                $sync[] = $similar->id;
                            }
                        } else {  // Create a new tag.
                            $newTag = $this->make([
                                'name' => $tag['name'],
                                'slug' => str_slug($tag['name']),
                                'user_id' => Sentinel::getUser()->id
                            ]);
                            $sync[] = $newTag->id;
                        }
                    }
                }
            }
            $wallpaper->tags()->sync($sync);
        }
    }

    /**
     * Renew tags cache
     */
    public function purgeTrendingTags($renew = true){
        $this->cache->del(TRENDIND_TAGS_CACHE_KEY);
        if($renew)
            $this->getTrendingTags();
    }


    /**
     * Get trending tags
     *
     * @return mixed
     */
    public function getTrendingTags()
    {
        if ($this->cache->exists(TRENDIND_TAGS_CACHE_KEY)) {
            return unserialize($this->cache->get(TRENDIND_TAGS_CACHE_KEY));
        }
        try {
            $trending = $this->tag->makeModel()->take(10)
                ->where('published', '=', 1)
                ->where('created_at', '>=', \Carbon\Carbon::now()->subWeek())
                ->whereHas('wallpapers', function ($query) {
                    $query->where('published', '=', '1');
                    $query->whereHas('gallery', function($q){
                        $q->where('published', 1);
                    });
                }, '>=', 1)
                ->get();

            if($trending->count() < 10){
                $trending = $this->tag->makeModel()->take(10)
                    ->where('published', '=', 1)
                    ->whereHas('wallpapers', function ($query) {
                        $query->where('published', '=', '1');
                        $query->whereHas('gallery', function($q){
                            $q->where('published', 1);
                        });
                    }, '>=', 1)
                    ->get();
            }
            $this->cache->set(TRENDIND_TAGS_CACHE_KEY, serialize($trending));
            $this->cache->expire(TRENDIND_TAGS_CACHE_KEY, 120 * 60); // in seconds
            return $trending;
        } catch (Exception $e){
            \Log::debug('Error fetching trending tags | '.$e->getMessage());
            return collect();
        }
    }

    /**
     * Get trending tags filterd for device
     *
     * @return mixed
     */
    public function getTrendingTagsFiltered()
    {
        try {
            $deviceFilter = Device::getDeviceFilter();
            $trending = $this->tag->makeModel()->take(10)
                ->where('published', '=', 1)
                ->where('created_at', '>=', \Carbon\Carbon::now()->subWeek())
                ->whereHas('wallpapers', function ($query) use($deviceFilter){
                    $query->where('published', '=', '1');
                    if(!is_null($deviceFilter))
                        $query->where($deviceFilter);
                    $query->whereHas('gallery', function($q){
                        $q->where('published', 1);
                    });
                }, '>=', 1)
                ->get();
            return $trending;
        } catch (Exception $e){
            \Log::debug('Error fetching trending tags filtered | '.$e->getMessage());
            return collect();
        }
    }
    
}