<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 19/01/2017
 * Time: 16:47
 */

namespace App\Providers\Managers;

use App\Models\Device;
use App\Models\Gallery;
use App\Models\Tag;
use App\Models\Wallpaper;
use Settings;

class SeoManager
{
    public function getHomePageData($key, $page){
        $replacements = array(
            Settings::getSetting(SETTING_SITE_NAME),
            Settings::getSetting(SETTING_SITE_DESCRIPTION), 
            ($page != 1)? ' | page - '.$page : ''
        );
        switch($key){
            case TITLE_KEY:
                return str_replace(
                    unserialize(SEO_HOMEPAGE_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_HOMEPAGE_TITLE_TEMPLATE)
                );
                break;
            case DESCRIPTION_KEY:
                return str_replace(
                    unserialize(SEO_HOMEPAGE_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_HOMEPAGE_DESCRIPTION_TEMPLATE)
                );
                break;/*
            case KEYWORDS_KEY:
                return Settings::getSetting(SETTING_SEO_HOMEPAGE_KEYWORDS_TEMPLATE);
                break;*/
            default:
                return '';
        }
    }

    public function getWallpaperData($key, Wallpaper $wallpaper){
        $replacements = array(
            Settings::getSetting(SETTING_SITE_NAME),
            Settings::getSetting(SETTING_SITE_DESCRIPTION),
            $wallpaper->title,
            $wallpaper->gallery->name,
            $wallpaper->description,
            $wallpaper->user->username,
            $wallpaper->author,
            $wallpaper->resolution(),
        );
        switch($key){
            case TITLE_KEY:
                return str_replace(
                    unserialize(SEO_WALLPAPER_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_WALLPAPER_TITLE_TEMPLATE)
                );
                break;
            case DESCRIPTION_KEY:
                return str_replace(
                    unserialize(SEO_WALLPAPER_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_WALLPAPER_DESCRIPTION_TEMPLATE)
                );
                break;/*
            case KEYWORDS_KEY:
                return str_replace(
                    unserialize(SEO_WALLPAPER_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_WALLPAPER_KEYWORDS_TEMPLATE)
                );
                break;*/
            default:
                return '';
                }
    }

    public function getGalleryData($key, Gallery $gallery, $page){
        $replacements = array(
            Settings::getSetting(SETTING_SITE_NAME),
            Settings::getSetting(SETTING_SITE_DESCRIPTION),
            $gallery->name,
            $gallery->description,
            ($page != 1)? ' | page - '.$page : ''
        );
        switch($key){
            case TITLE_KEY:
                return str_replace(
                    unserialize(SEO_GALLERY_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_GALLERY_TITLE_TEMPLATE)
                );
                break;
            case DESCRIPTION_KEY:
                return str_replace(
                    unserialize(SEO_GALLERY_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_GALLERY_DESCRIPTION_TEMPLATE)
                );
                break;/*
            case KEYWORDS_KEY:
                return str_replace(
                    unserialize(SEO_GALLERY_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_GALLERY_KEYWORDS_TEMPLATE)
                );
                break;*/
            default:
                return '';
        }
    }

    public function getTagData($key, Tag $tag, $page){
        $replacements = array(
            Settings::getSetting(SETTING_SITE_NAME),
            Settings::getSetting(SETTING_SITE_DESCRIPTION),
            $tag->name,
            ($page != 1)? ' | page - '.$page : ''
        );
        switch($key){
            case TITLE_KEY:
                return str_replace(
                    unserialize(SEO_TAG_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_TAG_TITLE_TEMPLATE)
                );
                break;
            case DESCRIPTION_KEY:
                return str_replace(
                    unserialize(SEO_TAG_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_TAG_DESCRIPTION_TEMPLATE)
                );
                break;/*
            case KEYWORDS_KEY:
                return str_replace(
                    unserialize(SEO_TAG_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_TAG_KEYWORDS_TEMPLATE)
                );
                break;*/
            default:
                return '';
        }
    }

    public function getDeviceData($key, Device $device, $page){
        if(strcmp('N/A', $device->maker) == 0){
            $replacements = array(
                Settings::getSetting(SETTING_SITE_NAME),
                Settings::getSetting(SETTING_SITE_DESCRIPTION),
                $device->name,
                ($page != 1)? ' | page - '.$page : ''
            );
        } else {
            $replacements = array(
                Settings::getSetting(SETTING_SITE_NAME),
                Settings::getSetting(SETTING_SITE_DESCRIPTION),
                $device->name,
                $device->maker,
                ($page != 1)? ' | page - '.$page : ''
            );
        }
        switch($key){
            case TITLE_KEY:
                return trim(str_replace(
                    unserialize(SEO_DEVICE_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_DEVICE_TITLE_TEMPLATE)
                ));
                break;
            case DESCRIPTION_KEY:
                return trim(str_replace(
                    unserialize(SEO_DEVICE_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_DEVICE_DESCRIPTION_TEMPLATE)
                ));
                break;
            case KEYWORDS_KEY:
                return str_replace(
                    unserialize(SEO_DEVICE_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_DEVICE_KEYWORDS_TEMPLATE)
                );
                break;
            default:
                return '';
        }
    }

    public function getSearchData($key, $search_term, $page){
        $replacements = array(
            Settings::getSetting(SETTING_SITE_NAME),
            Settings::getSetting(SETTING_SITE_DESCRIPTION),
            $search_term,
            ($page != 1)? ' | page - '.$page : ''
        );
        switch($key){
            case TITLE_KEY:
                return str_replace(
                    unserialize(SEO_SEARCH_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_SEARCH_TITLE_TEMPLATE)
                );
                break;
            case DESCRIPTION_KEY:
                return str_replace(
                    unserialize(SEO_SEARCH_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_SEARCH_DESCRIPTION_TEMPLATE)
                );
                break;/*
            case KEYWORDS_KEY:
                return str_replace(
                    unserialize(SEO_SEARCH_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_SEARCH_KEYWORDS_TEMPLATE)
                );
                break;*/
            default:
                return '';
        }
    }

    public function get404Data($key){
        $replacements = array(
            Settings::getSetting(SETTING_SITE_NAME),
            Settings::getSetting(SETTING_SITE_DESCRIPTION),
        );
        switch($key){
            case TITLE_KEY:
                return str_replace(
                    unserialize(SEO_404_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_404_TITLE_TEMPLATE)
                );
                break;
            case DESCRIPTION_KEY:
                return str_replace(
                    unserialize(SEO_404_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_404_DESCRIPTION_TEMPLATE)
                );
                break;
            /*case KEYWORDS_KEY:
                return str_replace(
                    unserialize(SEO_404_PLACEHOLDERS),
                    $replacements,
                    Settings::getSetting(SETTING_SEO_404_KEYWORDS_TEMPLATE)
                );
                break;*/
            default:
                return '';
        }
    }

}