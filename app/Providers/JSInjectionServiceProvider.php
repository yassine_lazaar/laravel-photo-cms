<?php

namespace App\Providers;

use App\Providers\Managers\JSInjectionManager;
use Illuminate\Support\ServiceProvider;

class JSInjectionServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('jsInjection', function () {
            return new JSInjectionManager();
        });
    }

    public function provides()
    {
        return ['jsInjection'];
    }

}
