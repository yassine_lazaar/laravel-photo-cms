<?php

namespace App\Providers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use App\Extensions\DatabaseSessionHandler;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->extendValidator();
        \View::addNamespace('css', public_path().'/storage/css');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerDevPackages();
        $this->registerSessionExtention();
    }

    private function registerDevPackages()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\Bpocallaghan\Generators\GeneratorsServiceProvider::class);
        }
    }


    private function registerSessionExtention()
    {
        // Extending the session to monitor users activity
        Session::extend('app.database', function($app)
        {
            $connectionName     = $app->app->config->get('session.connection');
            $databaseConnection = $app->app->db->connection($connectionName);
            $table = $databaseConnection->getTablePrefix().$app['config']['session.table'];

            return new DatabaseSessionHandler($databaseConnection, $table, config('session.lifetime'), $app);
        });

    }

    private function extendValidator()
    {
        Validator::extend('country_code', function ($attribute, $value, $parameters, $validator) {
            return array_key_exists($value, unserialize(COUNTRIES));
        });

        Validator::replacer('country_code', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Invalid country.');
        });

        Validator::extend('zip_code', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^\d{5}(?:[-\s]\d{4})?$/', $value);
        });

        Validator::replacer('zip_code', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Invalid zip code.');
        });

        Validator::extend('phone', function($attribute, $value, $parameters, $validator) {
            //return preg_match('/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/', $value) && strlen($value) >= 10;
            return preg_match('%^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$%i', $value) && strlen($value) >= 10;
        });

        Validator::replacer('phone', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Invalid phone number.');
        });

        Validator::extend('min_resolutions', function($attribute, $value, $parameters)
    	{
            // TODO: catch reading errors.
            $file = \Request::file($attribute);
            $image_info = getimagesize($file);
            $image_width = $image_info[0];
            $image_height = $image_info[1];

            $lowestVertical = explode('x', $parameters[0]);
            $lowestHorizontal = explode('x', $parameters[0]);

            if( (isset($parameters[0]) && $parameters[0] != 0) && $image_width <= $image_height && ($image_width < $lowestVertical[0] || $image_height < $lowestVertical[1])) return false;
            if( (isset($parameters[1]) && $parameters[1] != 0) && $image_width > $image_height  && ($image_width < $lowestHorizontal[0] || $image_height < $lowestHorizontal[1])) return false;
            return true;
        });

        Validator::replacer('min_resolutions', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, 'Image is too small.');
        });
        
    }


}
