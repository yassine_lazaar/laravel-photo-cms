<?php

/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 25/02/2016
 * Time: 08:36
 */

namespace App\Providers\Facades;

use Illuminate\Support\Facades\Facade;

class Resolution extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'resolution';
    }
}
