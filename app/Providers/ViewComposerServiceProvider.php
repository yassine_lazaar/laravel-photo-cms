<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeHeader();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     *  Composer header view
     */
    private function composeHeader(){
        view()->composer('frontend.master.index', 'App\Composers\FrontComposer@compose');
        view()->composer('backend.master.index', 'App\Composers\BackComposer@compose');
        view()->composer('frontend.partials.footer', 'App\Composers\FooterComposer@compose');
    }

}
