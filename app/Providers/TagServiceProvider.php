<?php

namespace App\Providers;

use App\Providers\Managers\TagManager;
use Illuminate\Support\ServiceProvider;

class TagServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('tag', function ($app) {
            return new TagManager();
        });
    }

    public function provides()
    {
        return ['tag'];
    }

}
