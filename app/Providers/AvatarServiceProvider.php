<?php

namespace App\Providers;

use App\Providers\Managers\AvatarManager;
use Illuminate\Support\ServiceProvider;

class AvatarServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('avatar', function () {
            return new AvatarManager();
        });
    }

    public function provides()
    {
        return ['avatar'];
    }


}
