<?php

namespace App\Providers;

use App\Providers\Managers\SeoManager;
use Illuminate\Support\ServiceProvider;

class SeoServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('seo', function () {
            return new SeoManager();
        });
    }

    public function provides()
    {
        return ['seo'];
    }



}
