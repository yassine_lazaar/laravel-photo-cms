<?php

namespace App\Providers;

use App\Repositories\RolesRepository;
use App\Repositories\UsersRepository;
use Cartalyst\Sentinel\Laravel\SentinelServiceProvider;

class SentinelExtensionServiceProvider extends SentinelServiceProvider
{
    /**
     * Registers the users.
     *
     * @return void
     */
    protected function registerUsers()
    {
        $this->registerHasher();

        $this->app->singleton('sentinel.users', function ($app) {
            $config = $app['config']->get('cartalyst.sentinel');

            $users = $config['users']['model'];
            $roles = $config['roles']['model'];
            $persistences = $config['persistences']['model'];
            $permissions = $config['permissions']['class'];

            if (class_exists($roles) && method_exists($roles, 'setUsersModel')) {
                forward_static_call_array([$roles, 'setUsersModel'], [$users]);
            }

            if (class_exists($persistences) && method_exists($persistences, 'setUsersModel')) {
                forward_static_call_array([$persistences, 'setUsersModel'], [$users]);
            }

            if (class_exists($users) && method_exists($users, 'setPermissionsClass')) {
                forward_static_call_array([$users, 'setPermissionsClass'], [$permissions]);
            }

            return new UsersRepository($app['sentinel.hasher'], $app['events'], $users);
        });
    }

    /**
     * Registers the roles.
     *
     * @return void
     */
    protected function registerRoles()
    {
        $this->app->singleton('sentinel.roles', function ($app) {
            $config = $app['config']->get('cartalyst.sentinel');

            $model = $config['roles']['model'];
            $users = $config['users']['model'];

            if (class_exists($users) && method_exists($users, 'setRolesModel')) {
                forward_static_call_array([$users, 'setRolesModel'], [$model]);
            }

            return new RolesRepository($model);
        });
    }
}
