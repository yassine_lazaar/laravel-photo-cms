<?php

namespace App\Providers;

use App\Providers\Managers\ResolutionManager;
use Illuminate\Support\ServiceProvider;

class ResolutionServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('resolution', function ($app) {
            return new ResolutionManager();
        });
    }

    public function provides()
    {
        return ['resolution'];
    }

}
