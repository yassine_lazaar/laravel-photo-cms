<?php

namespace App\Providers;

use App\Providers\Managers\UploadManager;
use Illuminate\Support\ServiceProvider;

class UploaderServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('uploader', function ($app) {
            return new UploadManager();
        });
    }


    public function provides()
    {
        return ['uploader'];
    }


}
