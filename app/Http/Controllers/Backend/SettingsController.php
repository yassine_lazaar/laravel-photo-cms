<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use JSInjection;
use Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use URL;
use Validator;
use Wallpaper;
use Carbon\Carbon;
use LRedis;


class SettingsController extends Controller
{

    /**
     * Admin dashboard - Settings
     *
     * @return \Illuminate\View\View
     */
    public function getSettings()
    {
        $settings = Settings::allSettings();
        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('backend.settings')->with([
                'title' => 'Dashboard - Site Settings | '.env('APP_NAME'),
                'description' => 'Dashboard - Site Settings | '.env('APP_NAME'),
                'settings' => $settings,
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
    }

    /**
    * Update general settings
    *
    * @param Request $request
    * @return mixed
    */
    public function postGeneralSettings(Request $request)
    {
        $input = $request->toArray();
        array_shift($input);

        $v = Validator::make($input, [
            SETTING_SITE_NAME => 'required|string|max:50',
            SETTING_SITE_DESCRIPTION => 'required|string',
            SETTING_SITE_TOS => 'required|string',
            SETTING_SITE_ABOUT => 'required|string',
            SETTING_SITE_FAQ => 'required|string'
        ]);

        if ($v->fails()) {
            return redirect()->to(URL::previous().'#general')
                ->withErrors($v->getMessageBag())
                ->withInput($request->all());
        }

        $input[SETTING_SITE_TOS] = ($input[SETTING_SITE_TOS]);
        $input[SETTING_SITE_PRIVACY_POLICY] = ($input[SETTING_SITE_PRIVACY_POLICY]);
        $input[SETTING_SITE_COPYRIGHT_POLICY] = ($input[SETTING_SITE_COPYRIGHT_POLICY]);
        $input[SETTING_SITE_FAQ] = ($input[SETTING_SITE_FAQ]);
        $input[SETTING_SITE_ABOUT] = ($input[SETTING_SITE_ABOUT]);
        Settings::setSettings($input);
        return redirect()->to(URL::previous().'#general')->withSuccess(EDIT_SETTINGS_MESSAGE);
    }

    /**
     * Update general settings
     *
     * @param Request $request
     * @return mixed
     */
    public function postGallerySettings(Request $request)
    {
        $input = $request->toArray();
        array_shift($input);

        $v = Validator::make($input, [
            SETTING_WALLPAPERS_PER_PAGE => 'required|in:12,16,20,24',
            SETTING_GUEST_DOWNLOAD_LIMIT => 'required|in:10,20,30,40',
            SETTING_MAX_UPLOAD_SIZE => 'required|in:2,3,4,5,6,7,8,9,10',
        ]);

        if ($v->fails()) {
            return redirect()->to(URL::previous().'#gallery')
                ->withErrors($v->getMessageBag())
                ->withInput($request->all());
        }

        Settings::setSettings($input);
        Wallpaper::purgeHomeContent();
        return redirect()->to(URL::previous().'#gallery')->withSuccess(EDIT_SETTINGS_MESSAGE);
    }

    /**
     * Update seo settings
     *
     * @param Request $request
     * @return mixed
     */
    public function postSeoSettings(Request $request)
    {
        $input = $request->toArray();
        array_shift($input);

        $v = Validator::make($input, [
            SETTING_SEO_HOMEPAGE_TITLE_TEMPLATE => 'required|string',
            SETTING_SEO_HOMEPAGE_DESCRIPTION_TEMPLATE => 'required|string',
            SETTING_SEO_WALLPAPER_TITLE_TEMPLATE => 'required|string',
            SETTING_SEO_WALLPAPER_DESCRIPTION_TEMPLATE => 'required|string',
            SETTING_SEO_TAG_TITLE_TEMPLATE => 'required|string',
            SETTING_SEO_TAG_DESCRIPTION_TEMPLATE => 'required|string',
            SETTING_SEO_DEVICE_TITLE_TEMPLATE => 'required|string',
            SETTING_SEO_DEVICE_DESCRIPTION_TEMPLATE => 'required|string',
            SETTING_SEO_SEARCH_TITLE_TEMPLATE => 'required|string',
            SETTING_SEO_SEARCH_DESCRIPTION_TEMPLATE => 'required|string',
        ]);

        if ($v->fails()) {
            return redirect()->to(URL::previous().'#seo')
                ->withErrors($v->getMessageBag())
                ->withInput($request->all());
        }

        Settings::setSettings($input);
        return redirect()->to(URL::previous().'#seo')->withSuccess(EDIT_SETTINGS_MESSAGE);
    }

    /**
     * Update analytics settings
     *
     * @param Request $request
     * @return mixed
     */
    public function postSitemapSettings(Request $request)
    {
        $input = $request->toArray();
        array_shift($input);

        $v = Validator::make($input, [
            SETTING_SITEMAP_INCLUDE_GALLERIES => 'in:on,off',
            SETTING_SITEMAP_INCLUDE_TAGS => 'in:on,off',
            SETTING_SITEMAP_INCLUDE_IMAGES => 'in:on,off',
            SETTING_SITEMAP_INCLUDE_DEVICES => 'in:on,off'
        ]);

        if ($v->fails()) {
            return redirect()->to(URL::previous().'#sitemap')
                ->withErrors($v->getMessageBag())
                ->withInput($request->all());
        }

        $defaultCheckboxInput = [
            SETTING_SITEMAP_INCLUDE_GALLERIES => 'off',
            SETTING_SITEMAP_INCLUDE_TAGS => 'off',
            SETTING_SITEMAP_INCLUDE_IMAGES => 'off',
            SETTING_SITEMAP_INCLUDE_DEVICES => 'off'
        ];

        $sitemapSettings = array_merge($defaultCheckboxInput, $input);
        //\Log::debug('sitemapSettings: '.json_encode($sitemapSettings));

        Settings::setSettings($sitemapSettings);

        Settings::generateSitemap();
        return redirect()->to(URL::previous().'#sitemap')->withSuccess(GENERATE_SITEMAP_MESSAGE);
    }

    /**
     * Update analytics settings
     *
     * @param Request $request
     * @return mixed
     */
    public function postAnalyticsSettings(Request $request)
    {
        $input = $request->toArray();
        array_shift($input);

        $v = Validator::make($input, [
            SETTING_ANALYTICS_CODE => 'required|string'
        ]);

        if ($v->fails()) {
            return redirect()->to(URL::previous().'#analytics')
                ->withErrors($v->getMessageBag())
                ->withInput($request->all());
        }

        Settings::setSettings($input);
        return redirect()->to(URL::previous().'#analytics')->withSuccess(EDIT_SETTINGS_MESSAGE);
    }

}
