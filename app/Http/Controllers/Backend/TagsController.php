<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use JSInjection;
use Tag;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator;
use Carbon\Carbon;


class TagsController extends Controller
{
    /**
     * Admin dashboard - Tags
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getTags(Request $request)
    {
        if ($request->expectsJson())
        {
            $filters = Tag::getFilters($request);
            $tags = Tag::filter($filters);
            if ($request->has('suggest')) {
                if(empty($filters))
                    return response()->json([], 200);
                $published = $tags->where('published',1);
                return response()->json($published, 200);
            }

            $response = Datatables::of($tags)
                ->editColumn('published', function ($tag) {
                    return ($tag->published)? 'Yes' : 'No';
                })
                ->addColumn('created_by', function ($tag) {
                    return $tag->user->username;
                })
                ->addColumn('count', function($tag){
                    return count($tag->wallpapers);
                })
                ->make(true);

            return $response;
        }
        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('backend.tags')
            ->with([
                'title' => 'Dashboard - Tags | '.env('APP_NAME'),
                'description' => 'Dashboard - Tags | '.env('APP_NAME'),
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
    }


    public function postEditTag(Request $request)
    {
        if (!$request->expectsJson())
            return null;

        $v = Validator::make($request->all(),[
            'tag_id' => 'required|integer|min:1',
            'tag_name' => 'required|string|max:255',
            'tag_published' => 'required|boolean'
        ]);

        if ($v->fails()) {
            return response()->json($v->errors(), 500);
        }

        $result = Tag::edit(
            $request['tag_id'],
            $request['tag_name'],
            $request['tag_published']
        );

        if(isset($result))
            return response()->json(['success' => EDIT_TAG_MESSAGE, 'code' => EDIT_TAG_CODE, 'tag' => $result], 200);
    }

    public function postDeleteTag(Request $request)
    {
        if (!$request->expectsJson())
            return null;

        $v = Validator::make($request->all(),[
            'tags' => 'required'
        ]);

        if ($v->fails()) {
            return response()->json($v->errors(), 500);
        }

        $result = Tag::delete($request['tags']);
        //Todo: report failure
        if($result)
            return response()->json(['success' => DELETE_TAG_MESSAGE, 'code' => DELETE_TAG_CODE, 'tag' => $result], 200);
    }

}
