<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use JSInjection;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;

class DashboardController extends Controller
{

    /**
     * Admin dashboard
     *
     * @return \Illuminate\View\View
     */
    public function getDashboard()
    {
        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('backend.dashboard')->with([
                'title' => 'Dashboard | '.env('APP_NAME'),
                'description' => 'Dashboard | '.env('APP_NAME'),
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
    }
}
