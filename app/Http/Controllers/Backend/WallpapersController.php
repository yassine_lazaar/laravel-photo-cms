<?php

namespace App\Http\Controllers\Backend;

use Gallery;
use JSInjection;
use Wallpaper;
use App\Repositories\ResolutionsRepository;
use Carbon\Carbon;
use Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use Validator;

class WallpapersController extends Controller
{

    private $resolution;

    /**
     * WallpapersController constructor.
     *
     * @param ResolutionsRepository $resolution
     */
    public function __construct(ResolutionsRepository $resolution)
    {
        $this->resolution = $resolution;
    }

    /**
     * Admin dashboard - Wallpapers
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getWallpapers(Request $request)
    {
        if ($request->expectsJson())
        {
            $wallpapers = Wallpaper::filter($this->getFilters($request));
            $response = Datatables::of($wallpapers)
                ->editColumn('thumbnail', function ($wallpaper) {
                    return [
                        'path' => $wallpaper->thumbnailPath(),
                        'timestamp' => Carbon::now()->getTimestamp()
                    ];
                })
                ->editColumn('uploader', function ($wallpaper) {
                    return $wallpaper->user->username;
                })
                ->editColumn('resolution', function ($wallpaper) {
                    return $wallpaper->width.'x'.$wallpaper->height;
                })
                ->editColumn('devices', function ($wallpaper) {
                    return \Resolution::getWallpaperCompatibleDevices($wallpaper);
                })
                ->editColumn('gallery', function ($wallpaper) {
                    return $wallpaper->gallery->name;
                })
                ->editColumn('tags', function ($wallpaper) {
                    return $wallpaper->tags;
                })
                ->editColumn('published', function ($wallpaper) {
                    return ($wallpaper->published)? 'Yes' : 'No';
                })
                ->editColumn('featured', function ($wallpaper) {
                    return ($wallpaper->featured)? 'Yes' : 'No';
                })
                ->addColumn('extra', function($wallpaper){
                    return [
                        'gallery_root' => GALLERIES_PATH,
                        'gallery_folder' => $wallpaper->gallery->folder,
                        'thumb_path' => $wallpaper->thumbnailPath(),
                        'thumb_width'=> DEFAULT_THUMB_WIDTH,
                        'thumb_height' => DEFAULT_THUMB_HEIGHT
                    ];
                })
                ->make(true);

            return $response;
        }
        JSInjection::addSessionModalData(false);
        JSInjection::inject();
        return View::make('backend.wallpapers')
            ->with([
                'title' => 'Dashboard - Wallpapers | '.env('APP_NAME'),
                'description' => 'Dashboard - Wallpapers | '.env('APP_NAME'),
                'galleries' => Gallery::getOrderedGalleries(),
                'resolutions' => $this->resolution->all(),
                'maxUploadSize' => \Settings::maxUploadSize(),
                'startResolutions' => \Resolution::getLowestResolutions(),
                'licenses' => unserialize(LICENSES),
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
    }


    private function getFilters(Request $request)
    {
        $filters = [];
        if($request->get('filter_t') != ''){
            $filters[] = ['title', 'like', "%{$request->get('filter_t')}%"];
        }
        if($request->get('filter_u') != ''){
            try {
                $user = app('sentinel.users')
                    ->where([
                        ['username', 'like', "%{$request->get('filter_u')}%"]
                    ])
                    ->first();
                if($user)
                    $filters[] = ['user_id', '=', $user->id];
            } catch (ModelNotFoundException $e){
                return null; // Make the filter opaque
            }
        }
        if($request->get('filter_g') != ''){
            $filters[] = ['gallery_id', '=', $request->get('filter_g')];
        }
        if($request->get('filter_r') != ''){
            $filters[] = ['resolution_id', '=', $request->get('filter_r')];
        }
        return $filters;
    }


    public function postScanGallery(Request $request)
    {
        // TODO: Validation
        $id = $request->get('gallery_id');
        $newFiles = Gallery::scanGalleryPath($id);
        if (isset($newFiles)) {
            return response()
                ->json([
                    'id' => $id,
                    'new' => $newFiles
                ], 200);
        }
        return response()
                ->json([
                    'success' => SCAN_GALLERY_EMPTY_MESSAGE,
                    'code' => SCAN_GALLERY_EMPTY_CODE
                ], 200);
    }


    public function postProcessWallpaper(Request $request)
    {
        // TODO: Validation
        $id = $request->get('gallery_id');
        $filename = $request->get('filename');
        $result = Gallery::processNewFile($id, $filename);
        if(isset($result)){
            Wallpaper::purgeHomeContent(false);
            return response()->json(['success' => PROCESS_WALLPAPER_MESSAGE, 'code' => PROCESS_WALLPAPER_CODE, 'wallpaper' => $result], 200);
        }
    }

    public function postMoveWallpaper(Request $request)
    {
        // TODO: Validation
        $wallpaperId = $request->get('wallpaper_id');
        $galleryId = $request->get('gallery_id');
        $result = Wallpaper::move($wallpaperId, $galleryId);
        if(isset($result)){
            Wallpaper::purgeHomeContent(false);
            return response()->json(['success' => MOVE_WALLPAPER_MESSAGE, 'code' => MOVE_WALLPAPER_CODE, 'wallpaper' => $result], 200);
        }
    }

    public function postPublishWallpaper(Request $request)
    {
        // TODO: Validation
        $wallpaperId = $request->get('wallpaper_id');
        $result = Wallpaper::publish($wallpaperId);
        if(isset($result)){
            Wallpaper::purgeHomeContent();
            \Tag::purgeTrendingTags();
            return response()->json(['success' => PUBLISH_WALLPAPERS_MESSAGE, 'code' => PUBLISH_WALLPAPERS_CODE], 200);
        }
    }

    public function postDeleteWallpaper(Request $request)
    {
        // TODO: Validation
        $wallpaperId = $request->get('wallpaper_id');
        $result = Wallpaper::delete($wallpaperId);
        if(isset($result)){
            Wallpaper::purgeHomeContent();
            \Tag::purgeTrendingTags();
            return response()->json(['success' => DELETE_WALLPAPER_MESSAGE, 'code' => DELETE_WALLPAPER_CODE, 'wallpaper' => $result], 200);
        }
    }

    public function postEditWallpaper(Request $request)
    {
        if (!$request->expectsJson())
            return null;

        $decodedRequest = $request->all();
        $decodedRequest['wallpaper_tags'] = json_decode($decodedRequest['wallpaper_tags'], true);
        $decodedRequest['wallpaper_slug'] = slugify($decodedRequest['wallpaper_slug']);
        $decodedRequest['wallpaper_author_link'] = addHttp($decodedRequest['wallpaper_author_link']);
        $v = Validator::make($decodedRequest, [
            'wallpaper_id' => 'required|integer|min:1',
            'wallpaper_title' => 'required|string|max:100|unique:wallpapers,title,'.$request->get('wallpaper_id'),
            'wallpaper_slug' => 'required|string|max:150|alpha_dash|unique:wallpapers,slug,'.$request->get('wallpaper_id'),
            'wallpaper_tags'=> 'sometimes|nullable|array|max:10',
            'wallpaper_description' => 'sometimes|string|nullable|max:255',
            'wallpaper_license' => 'sometimes|integer|between:0,5',
            'wallpaper_author' => 'sometimes|string|max:25',
            'wallpaper_author_link' => 'sometimes|url|max:255',
            'wallpaper_published' => 'required|boolean',
            'wallpaper_featured' => 'required|boolean',
            'wallpaper_thumbnail_params' => 'required'
        ]);

        if ($v->fails()) {
            return response()->json($v->errors(), 500);
        }

        unset($decodedRequest['wallpaper_id']);
        $result = Wallpaper::edit($request->get('wallpaper_id'), $decodedRequest);
        if(isset($result)){
            Wallpaper::purgeHomeContent();
            \Tag::purgeTrendingTags();
            return response()->json(['success' => EDIT_WALLPAPER_MESSAGE, 'code' => EDIT_WALLPAPER_CODE, 'wallpaper' => $result], 200);
        }
    }
}
