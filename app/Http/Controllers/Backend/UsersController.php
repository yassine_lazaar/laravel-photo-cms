<?php

namespace App\Http\Controllers\Backend;

use App;
use App\Http\Controllers\Controller;
use Avatar;
use JSInjection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Mockery\Exception;
use Carbon\Carbon;

use Yajra\Datatables\Datatables;

class UsersController extends Controller
{

    private $user;
    private $role;


    /**
     * UsersController constructor.
     *
     */
    public function __construct()
    {
        $this->user = App::make('sentinel.users');
        $this->role = App::make('sentinel.roles');
    }

    /**
     * Admin dashboard - Users
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getUsers(Request $request)
    {
        if ($request->expectsJson())
        {
            $response = Datatables::of($this->user->all())
                ->addColumn('avatar', function ($user) {
                    return '<span class="thumbnail-wrapper circular bordered b-white"><img src="'.$user->avatarPath().'_1x.jpg" alt="" data-src="'.$user->avatarPath().'_1x.jpg" data-src-retina="'.$user->avatarPath().'_2x.jpg"></span>';
                })
                ->addColumn('role', function ($user) {
                    return $user->getRoles()->first()['name'];
                },true)
                ->editColumn('banned', function ($user) {
                    return ($user->banned)? 'Yes' : 'No';
                })
                ->make(true);

            return $response;
        }
        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('backend.users')
            ->with([
                'title' => 'Dashboard - Users | '.env('APP_NAME'),
                'description' => 'Dashboard - Users | '.env('APP_NAME'),
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
    }

    /**
     * Handle user deletion requests
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function postDeleteUsers(Request $request)
    {
        // TODO: Validation
        if ($request->expectsJson())
        {
            $ids = $request->get('users');
            foreach($ids as $id){
                $user = $this->user->findById($id);
                \Avatar::deleteAvatar($user);
                $user->links()->delete();
                $user->wallpapers()->delete();
                $user->tags()->delete();
                $user->roles()->detach($user);
                $user->persistences()->delete();
                $user->activations()->delete();
                $user->reminders()->delete();
                $user->throttle()->delete();
                $user->delete();
                // TODO: wipe home cache
            }
            return response()->json(['success' => 'User(s) deleted.'], 200);
        }
    }

    /**
     * Handle user edit requests
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function postEditUser(Request $request)
    {
        // TODO: Validation
        if ($request->expectsJson())
        {
            $id = $request->get('user');
            $user = $this->user->findById($id);
            $this->user->update($user->id, ['banned' => $request->get('banned')]);
            if(!$user->inRole($request->get('role'))){
                $old_role = $this->role->findById($user->getRoles()->first()['id']);
                $old_role->users()->detach($user->id);
                $role = $this->role->findByName($request->get('role'));
                $role->users()->attach($user->id);
            }
            if($request->get('delete_avatar') && strcmp($user->avatar, AVATAR_DEFAULT) != 0){
                Avatar::deleteAvatar($user);
            }
            return response()->json(['success' => 'User updated.'], 200);
        }
        return null;
    }

}
