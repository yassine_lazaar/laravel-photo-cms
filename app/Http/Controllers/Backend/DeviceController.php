<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use JSInjection;
use App\Repositories\DevicesRepository as Device;
use App\Repositories\ResolutionsRepository as Resolution;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Rfd\ImageMagick\Options\CommonOptions;


class DeviceController extends Controller
{
    private $device;
    private $resolution;

    public function __construct(Device $device, Resolution $resolution)
    {
        $this->device = $device;
        $this->resolution = $resolution;
    }

    /**
     * Admin dashboard - Devices
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getDevices(Request $request)
    {
        if ($request->expectsJson()) {

            $response = Datatables::of($this->device->all())
                ->addColumn('image', function ($device) {
                    return '<span class="thumbnail-wrapper img-rounded b-rad-sm bordered b-white bg-master-light padding-5"><img src="'.Storage::url(DEVICE_FEATURE_IMAGE_PATH.$device->featured_image.'_1x.png').'" alt="" data-src="'.Storage::url(DEVICE_FEATURE_IMAGE_PATH.$device->featured_image.'_1x.png').'" data-src-retina="'.Storage::url(DEVICE_FEATURE_IMAGE_PATH.$device->featured_image.'_2x.png').'" width="64" height="64"></span>';
                })
                ->editColumn('release_date', function ($device) {
                    return Carbon::parse($device->release_date)->format('m-d-Y');
                })
                ->editColumn('resolutions', function ($device) {
                    return \Device::getSortedResolutions($device);
                })
                ->editColumn('excluded', function ($device) {
                    return ($device->excluded)? 'Yes' : 'No';
                })
                ->make(true);

            return $response;
        }

        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('backend.devices')
            ->with([
                'title' => 'Dashboard - Devices | '.env('APP_NAME'),
                'description' => 'Dashboard - Devices | '.env('APP_NAME'),
                'timestamp' => Carbon::now()->getTimestamp(),
                'resolutions' => $this->resolution->all()
            ]);
    }

    public function postCreateDevice(Request $request)
    {
        //\Log::debug("create request: ".json_encode($request->toArray()));
        if (!$request->expectsJson())
            return null;

        $rules = array(
            'device_name' => 'required|max:255|unique:devices,name',
            'device_image' => 'sometimes|image|mimes:png|max:2048|dimensions:min_width='.DEVICE_IMAGE_SIZE_3X.',min_height='.DEVICE_IMAGE_SIZE_3X,
            'device_slug' => 'required|max:255|unique:devices,slug|alpha_dash',
            'device_type' => 'required|between:0,'.count(unserialize(DEVICES_TYPES)),
            'device_maker' => 'required|in:'.implode(',',unserialize(MAKERS)),
            'device_resolution' => 'required',
            'device_release_date' => 'required|date_format:m-d-Y'
        );
        $request['device_slug'] = slugify($request['device_slug']);
        $v = Validator::make($request->all(), $rules);
        if($v->fails())
            return response()->json($v->errors(), 400);

        $filename = $request->get('device_slug');
        $newDevice = $this->device->create([
           'name' => $request->get('device_name'),
           'slug' => $request->get('device_slug'),
           'type' =>  $request->get('device_type'),
           'maker' =>  $request->get('device_maker'),
           'featured_image' => ($request->exists('device_image'))? $filename : DEVICE_FEATURE_DEFAULT,
           'release_date' => Carbon::createFromFormat('m-d-Y', $request->get('device_release_date'))->format('Y-m-d'),
           'excluded' => 0,
        ]);
        $resolutions_ids = explode(',', $request->get('device_resolution'));
        $newDevice->resolutions()->sync($resolutions_ids);

        if ($request->exists('device_image')) {
            $imagePath = 'storage/'.DEVICE_FEATURE_IMAGE_PATH;

            $image3xPath = $imagePath.$filename.'_3x.png';
            $image3x = Image::make($request->file('device_image'))
                ->fit(DEVICE_IMAGE_SIZE_3X, DEVICE_IMAGE_SIZE_3X)
                ->save($image3xPath, 100);
                optimizeImageFile($image3xPath);

            $image2xPath = $imagePath.$filename.'_2x.png';
            $image2x = $image3x->resize(DEVICE_IMAGE_SIZE_3X/2, DEVICE_IMAGE_SIZE_3X/2)
                ->save($image2xPath, 100);
                optimizeImageFile($image2xPath);
            
            $image1xPath = $imagePath.$filename.'_1x.png';
            $image2x->resize(DEVICE_IMAGE_SIZE_3X/4, DEVICE_IMAGE_SIZE_3X/4)
                ->save($image1xPath, 100);
                optimizeImageFile($image1xPath);

            $image3x->destroy(); // Destroy what you make
        }
        return response()->json(['success' => CREATE_DEVICE_MESSAGE, 'code' => CREATE_DEVICE_CODE], 200);
    }

    public function postEditDevice(Request $request)
    {
        //\Log::debug("edit request: ".json_encode($request->toArray()));
        $request['device_slug'] = slugify($request['device_slug']);
        if (!$request->expectsJson())
            return null;

        $rules = array(
                'device_id'   => 'required',
                'device_image' => 'sometimes|image|mimes:png|max:2048|dimensions:min_width='.DEVICE_IMAGE_SIZE_3X.',min_height='.DEVICE_IMAGE_SIZE_3X,
                'device_name' => 'required|max:255|unique:devices,name,'.$request->get('device_id'),
                'device_slug' => 'required|max:255|alpha_dash|unique:devices,slug,'.$request->get('device_id'),
                'device_type' => 'required|between:0,'.count(unserialize(DEVICES_TYPES)),
                'device_maker' => 'required|in:'.implode(',',unserialize(MAKERS)),
                'device_resolution' => 'required',
                'device_release_date' => 'required|date_format:m-d-Y',
                'device_excluded' => 'required'
            );

        $v = Validator::make($request->all(), $rules);
        if($v->fails())
            return response()->json($v->errors(), 400);

        $device = $this->device->find($request->get('device_id'));

        $newSlug = $request->get('device_slug');
        $filename = (strcmp($device->slug, $newSlug) != 0)? $newSlug : $device->slug;
        $imagePath = 'storage/'.DEVICE_FEATURE_IMAGE_PATH;
        $image3xPath = $imagePath.$filename.'_3x.png';
        $image2xPath = $imagePath.$filename.'_2x.png';
        $image1xPath = $imagePath.$filename.'_1x.png';
            
        if ($request->exists('device_image')) {
            $image3x = Image::make($request->file('device_image'))
                ->fit(DEVICE_IMAGE_SIZE_3X, DEVICE_IMAGE_SIZE_3X)
                ->save($image3xPath, 100);
                optimizeImageFile($image3xPath, 85, CommonOptions::FORMAT_PNG);

            $image2x = $image3x->resize(DEVICE_IMAGE_SIZE_3X/2, DEVICE_IMAGE_SIZE_3X/2)
                ->save($image2xPath, 100);
                optimizeImageFile($image2xPath, 85, CommonOptions::FORMAT_PNG);
            
            $image2x->resize(DEVICE_IMAGE_SIZE_3X/4, DEVICE_IMAGE_SIZE_3X/4)
                ->save($image1xPath, 100);
                optimizeImageFile($image1xPath, 85, CommonOptions::FORMAT_PNG);

            $image3x->destroy(); // Destroy what you make
        }

        if(strcmp($device->slug, $newSlug) != 0){
            $featurePath = 'public/'.DEVICE_FEATURE_IMAGE_PATH;
            Storage::move($featurePath.$device->slug.'_3x.png', $featurePath.$newSlug.'_3x.png');
            Storage::move($featurePath.$device->slug.'_2x.png', $featurePath.$newSlug.'_2x.png');
            Storage::move($featurePath.$device->slug.'_1x.png', $featurePath.$newSlug.'_1x.png');
        }

        $data = [
            "name" => $request->get('device_name'),
            "slug" => $newSlug,
            "type"  => $request->get('device_type'),
            "maker"  => $request->get('device_maker'),
            "release_date" => Carbon::createFromFormat('m-d-Y', $request->get('device_release_date'))->format('Y-m-d'),
            "featured_image" => ($request->exists('device_image'))? $filename : $device->featured_image,
            "excluded" => ($request->get('device_excluded'))? true : false
        ];
        $this->device->update($data, $device->id);
        $resolutions_ids = explode(',', $request->get('device_resolution'));
        $device->resolutions()->sync($resolutions_ids);
        return response()->json(['success' => EDIT_DEVICE_MESSAGE, 'code' => EDIT_DEVICE_CODE], 200);
    }

    public function postDeleteDevices(Request $request)
    {
        // TODO: Validation
        if (!$request->expectsJson())
            return null;

        $ids = $request->get('devices');

        foreach($ids as $id){
            $device = $this->device->find($id);

            if(strcmp($device->featured_image, DEVICE_FEATURE_DEFAULT) != 0){
                $featurePath = 'public/'.DEVICE_FEATURE_IMAGE_PATH;
                Storage::delete($featurePath.$device->featured_image.'_3x.png');
                Storage::delete($featurePath.$device->featured_image.'_2x.png');
                Storage::delete($featurePath.$device->featured_image.'_1x.png');
            }

            $device->resolutions()->detach();
            $device->delete();
        }
        return response()->json(['success' => DELETE_DEVICE_MESSAGE, 'code' => EDIT_DEVICE_CODE], 200);
    }

}
