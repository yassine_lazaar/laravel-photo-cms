<?php

namespace App\Http\Controllers\Backend;

use Uploader;
use Resolution;
use Gallery;
use JSInjection;
use Datatables;
use View;
use Validator;
use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class ModerationController extends Controller
{

    /**
     * ModerationController constructor.
     *
     */
    public function __construct()
    {

    }

    /**
     * Admin dashboard - Moderation
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getUploads(Request $request)
    {
        if ($request->expectsJson())
        {
            $uploads = Uploader::all();
            $response = Datatables::of($uploads)
                ->editColumn('thumbnail', function ($upload) {
                    return $upload->thumbnailPath();
                })
                ->editColumn('title', function ($upload) {
                    $duplicate = false;
                    return [
                        'duplicate' => $duplicate,
                        'title' => $upload->title
                    ];
                })
                ->editColumn('uploader', function ($upload) {
                    return $upload->user->username;
                })
                ->editColumn('resolution', function ($upload) {
                    return $upload->width.'x'.$upload->height;
                })
                ->editColumn('gallery', function ($upload) {
                    return $upload->gallery;
                })
                ->addColumn('compat_res', function ($upload) {
                    return Resolution::getCompatibleResolutions($upload);
                })
                ->addColumn('extra', function ($upload) {
                    return [
                        'path' => $upload->path().'.jpg',
                        'thumb_width'=> DEFAULT_THUMB_WIDTH,
                        'thumb_height' => DEFAULT_THUMB_HEIGHT
                    ];
                })
                ->make(true);

            return $response;
        }
        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('backend.uploads')
            ->with([
                'title' => 'Dashboard - Uploads | '.env('APP_NAME'),
                'description' => 'Dashboard - Uploads | '.env('APP_NAME'),
                'galleries' => Gallery::getOrderedGalleries(true),
                'licenses' => unserialize(LICENSES),
                'discard_reasons' => unserialize(UPLOAD_DISCARD_REASONS),
                'timestamp' => Carbon::now()->getTimestamp(),
            ]);
    }

    /**
     * Edit user upload
     *
     * @param Request $request
     */
    public function postEditUpload(Request $request){
        //\Log::debug('upload request edit '.json_encode($request->all()));

        if (!$request->expectsJson())
            return null;

        $decodedRequest = $request->all();
        $decodedRequest['upload_tags'] = json_decode($decodedRequest['upload_tags'], true);
        $decodedRequest['upload_slug'] = slugify($decodedRequest['upload_slug']);
        $decodedRequest['upload_author_link'] = addHttp($decodedRequest['upload_author_link']);

        $v = Validator::make($decodedRequest, [
            'upload_id' => 'required|integer|min:1',
            'upload_title' => 'required|string|max:100|unique:wallpapers,title',
            'upload_slug' => 'required|string|max:150|alpha_dash|unique:wallpapers,slug',
            'upload_tags'=> 'sometimes|nullable|array|max:10',
            'upload_description' => 'sometimes|string|nullable|max:255',
            'upload_gallery' => 'required|integer',
            'upload_license' => 'sometimes|integer|between:0,5',
            'upload_author' => 'sometimes|string|max:25',
            'upload_author_link' => 'sometimes|url|max:255',
            'upload_featured' => 'required|boolean',
            'upload_crop_params' => 'required'
        ]);

        if ($v->fails()) {
            return response()->json($v->errors(), 500);
        }

        unset($decodedRequest['upload_id']);
        $result = Uploader::edit($request->get('upload_id'), $decodedRequest);
        if(isset($result))
            return response()->json(['success' => PUBLISH_UPLOAD_MESSAGE, 'code' => PUBLISH_UPLOAD_CODE, 'upload' => $result, 'code' => PUBLISH_UPLOAD_CODE], 200);
    }


    /**
     * Discard user upload
     *
     * @param Request $request
     */
    public function postDiscardUpload(Request $request){
        //\Log::debug('upload discard request'.json_encode($request->all()));

        if (!$request->expectsJson())
            return null;

        $v = Validator::make($request->all(), [
            'upload_id' => 'required|integer|min:1'
        ]);

        if ($v->fails()) {
            return response()->json($v->errors(), 500);
        }

        $result = Uploader::discard($request->get('upload_id'), $request->get('discard_reason'));
        if(isset($result))
            return response()->json(['success' => DISCARD_UPLOAD_MESSAGE, 'code' => DISCARD_UPLOAD_CODE], 200);
        
    }
}
