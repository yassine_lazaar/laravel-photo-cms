<?php

namespace App\Http\Controllers\Backend;

use Gallery;
use JSInjection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use View;
use Carbon\Carbon;


class GalleriesController extends Controller
{

    public function __construct()
    {
        //
    }

    /**
     * Admin dashboard - Galleries
     *
     * @return \Illuminate\View\View
     */
    public function getGalleries()
    {
        JSInjection::addSessionModalData(true);
        JSInjection::inject();

        return View::make('backend.galleries')
            ->with([
                'title' => 'Dashboard - Galleries | '.env('APP_NAME'),
                'description' => 'Dashboard - Galleries | '.env('APP_NAME'),
                'galleries' => Gallery::getOrderedGalleries(),
                'timestamp' => Carbon::now()->getTimestamp()
        ]);
    }

    /**
     * Reorder galleries
     *
     * @param Request $request
     */
    public function postReorderGalleries(Request $request)
    {
        //\Log::debug('changes '.json_encode($request->get('changes')));
        
        // TODO: Validation
        $output = Gallery::reorder($request->get('changes'));
        if(!$output){
            return response()->json(
                [
                    'results' => REORDER_GALLERY_FAIL_MESSAGE,
                    'code' => REORDER_GALLERY_FAIL_CODE,
                    'index' => $request->get('index')
                ], 200);
        } else {
            Gallery::purgeAndRenewGalleries();
            return response()->json(
                [
                    'results' => REORDER_GALLERY_MESSAGE,
                    'code' => REORDER_GALLERY_CODE,
                    'index' => $request->get('index')
                ], 200);
        }
        
    }

    /**
     * Create new gallery.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateGallery(Request $request)
    {
        if (!$request->expectsJson())
            return null;

        $input = [
            'gallery_name' => ucfirst($request->get('gallery_name')),
            'gallery_slug' => Str::slug($request->get('gallery_slug')),
            'gallery_description' => $request->get('gallery_description'),
            'gallery_folder' => Str::slug($request->get('gallery_folder'))
        ];

        $v = Validator::make($input, [
            'gallery_name' => 'required|max:20|unique:galleries,name',
            'gallery_slug' => 'required|max:20|unique:galleries,slug|alpha_dash',
            'gallery_description' => 'max:100',
            'gallery_folder' => 'required|max:20|unique:galleries,folder|alpha_dash'
        ]);

        if ($v->fails()) {
            return response()->json($v->errors(), 400);
        }

        $newGallery = Gallery::create($input);
        return response()->json(
            [
                'results' => CREATE_GALLERY_MESSAGE,
                'code' => CREATE_GALLERY_CODE,
                'gallery_id' => $newGallery->id,
                'gallery_name' => $newGallery->name,
                'gallery_slug' => $newGallery->slug,
                'gallery_description' => $newGallery->description,
                'gallery_folder' => $newGallery->folder,
                'gallery_published' => $newGallery->published,
            ], 200);

    }

    /**
     * Edit existing gallery.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEditGallery(Request $request)
    {
        if (!$request->expectsJson())
            return null;

        $rules = array(
            'gallery_id'   => 'required',
            'gallery_name' => 'required|max:255|unique:galleries,name,'.$request->get('gallery_id'),
            'gallery_slug' => 'required|max:255|alpha_dash|unique:galleries,slug,'.$request->get('gallery_id'),
            'gallery_description' => 'max:100',
            'gallery_folder' => 'required|max:255|alpha_dash|unique:galleries,folder,'.$request->get('gallery_id'),
            'gallery_published' => 'required'
        );

        $v = Validator::make($request->all(), $rules);

        if ($v->fails())
            return response()->json($v->errors(), 500);
        
        $result = Gallery::edit($request->all());
        if (isset($result)) {
            Gallery::purgeAndRenewGalleries();
            \Wallpaper::purgeHomeContent();
            \Tag::purgeTrendingTags();
            return response()->json(['success' => EDIT_GALLERY_MESSAGE, 'code' => EDIT_GALLERY_CODE], 200);
        }
    }

    /**
     * Delete existing gallery.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDeleteGallery(Request $request)
    {
        if (!$request->expectsJson())
            return null;

        Gallery::delete($request->get('gallery_id'));
        Gallery::purgeAndRenewGalleries();
        \Wallpaper::purgeHomeContent();
        \Tag::purgeTrendingTags();
        return response()->json(['success' => DELETE_GALLERY_MESSAGE, 'code' => DELETE_GALLERY_CODE], 200);
    }

}
