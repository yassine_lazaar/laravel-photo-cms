<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JSInjection;
use View;
use Carbon\Carbon;
use Sentinel;

class NotificationsController extends Controller
{

    /**
     * NotificationsController constructor.
     *
     */
    public function __construct()
    {
        //
    }


    /**
     * User notifications.
     * 
     *@param Request $request
     *@return Response
     */
    public function getUserNotifications(Request $request){

        $user = Sentinel::getUser();

        if ($request->expectsJson()){
            $page = $request->get('pagination');
            $notifications = feed()->offset($page*5)->limit(5)->pullRead($user);
            if($notifications->isEmpty())
                return response()->json([
                    'success' => NOTIFICATION_PULL_END_MESSAGE,
                    'code' => NOTIFICATION_PULL_END_CODE
                    ],
                    200);  

            $resultView =  View::make(
                    'frontend.partials.notificationsection',
                    ['notifications' => pepareNotificationData($notifications)]
                )->render();

            return response()->json([
                'success' => NOTIFICATION_PULL_MESSAGE,
                'code' => NOTIFICATION_PULL_CODE,
                'results' => $resultView
                ],
                200);  
        }

        $breadcrumb =  [
            ['a' => 'Home', 'u' => route('gethome')],
            ['a' => 'Notifications', 'u' => route('getusernotifications')]
        ];
                    
        JSInjection::addSessionModalData(true);
        JSInjection::addData([
            'infinite_scroll' => true
        ]);
        JSInjection::inject();

        $unread = feed()->pull($user);
        $outputUnread = clone $unread;      
        $view =  View::make('frontend.usernotifications')
        ->with([
            'title' => 'Notifications | '.env('APP_NAME'),
            'description' => 'Notifications | '.env('APP_NAME'),
            'breadcrumb' => $breadcrumb,
            'read' => pepareNotificationData(feed()->limit(5)->pullRead($user)),
            'unread' => pepareNotificationData($outputUnread),
            'timestamp' => Carbon::now()->getTimestamp()
        ]);

        foreach($unread as $ur){
            feed()->read($ur);
        }
        return $view;
    }
   

    /**
     * Mark notification as read.
     * 
     * @param Request $request
     * @return mixed
     *
     */
    public function postNotificationRead(Request $request){
        $user = Sentinel::getUser();
        $notification = feed()->pull($user)->where('id', $request->route('id'))->first();
        if(isset($notification)){
            feed()->read($notification);
            return response()->json(['success' => NOTIFICATION_READ_MESSAGE, 'code' => NOTIFICATION_READ_CODE], 200);  
        }
    }

}
