<?php

namespace App\Http\Controllers\Frontend;

use Gallery;
use Wallpaper;
use JSInjection;
use Settings;
use Device;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Extensions\Paginator;
use View;
use Carbon\Carbon;
use Sentinel;
use Seo;


class GalleriesController extends Controller
{

     /**
     * Gallery
     *
     * @param String $gallerySlug
     * @param String $order
     * @param String $page
     * @return \Illuminate\View\View
     */
    public function getGallery($gallerySlug, $orderSlug = null, $pageSlug = null)
    {
        $url = $gallerySlug;
        $url = ($orderSlug)? $url.'/'.$orderSlug : $url;     
        $pageNumber = 1;
        $order = $orderSlug;   
        
        if ($pageSlug != null) {
            $pageNumber = str_replace('page-','', $pageSlug);
            if ($pageNumber <= 0) 
                return abort(404);
        }
        
        try {
            $gallery = Gallery::findBySlug($gallerySlug);
            if(!$gallery->published) abort(404);
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
            $filters = [
                ['gallery_id', '=', $gallery->id],
                ['published', '=', '1']
            ];
            $deviceFilter = Device::getDeviceFilter();
            
            switch ($order) {
                case GALLERY_ORDER_VIEWS:
                case GALLERY_ORDER_DOWNLOADS:
                case GALLERY_ORDER_FAVORITES:
                    break;
                case GALLERY_ORDER_FEATURED:
                    $filters[] = ['featured', '=', '1'];
                    $order = 'created_at';   
                    break; 
                case GALLERY_ORDER_TRENDING:
                    $filters[] = ['views', '>', WALLPAPER_TRENDING_THRESHOLD];
                    $filters[] = ['downloads', '>', WALLPAPER_TRENDING_THRESHOLD];
                    $filters[] = ['created_at', '>', Carbon::now()->subWeek()];
                    $order = 'created_at';                    
                    break;
                default:
                    $order = 'created_at';     
            }

            $filterFn = function($q) use($filters, $deviceFilter)
            {
                $q->where($filters);
                if(!is_null($deviceFilter))
                    $q->where($deviceFilter);
            };

            try {
                if (strcmp($order, GALLERY_ORDER_FAVORITES) != 0) {
                    $wallpapers = Wallpaper::query(
                        $filterFn, 
                        ['id', 'title', 'slug', 'filename', 'featured', 'views', 'downloads', 'gallery_id', 'user_id'],
                        $order
                    );
                } else {
                    $wallpapers = Wallpaper::queryWithFavoritesCount(
                        $filterFn, 
                        ['id', 'title', 'slug', 'filename', 'featured', 'views', 'downloads', 'gallery_id', 'user_id']
                    );
                }       
            } catch (ModelNotFoundException $e) {
                $wallpapers = collect();
            }
            
            if ($wallpapers->count() > 0 && $pageNumber > ceil($wallpapers->count() / Settings::perPage())){
                abort(404);   
            }

            $wallpapers = new Paginator($wallpapers, Settings::perPage(), $pageNumber, url($url));
            $wallpapers = Wallpaper::assignFavorites($wallpapers);

            $breadcrumb =  [
                    ['a' => 'Home', 'u' => route('gethome')],
                    ['a' => $gallery->name, 'u' => route('getgallery', $gallery->slug)]
                ];

            $canonical = route('getgallery', $gallery->slug);
            if (!empty($orderSlug)){
                $breadcrumb[] = ['a' => $orderSlug, 'u' => route('getgallery', [$gallery->slug, $orderSlug])];
                $canonical = route('getgallery', [$gallery->slug, $orderSlug]);
            }
            if ($pageNumber > 1){
                if (!empty($orderSlug)){
                    $breadcrumb[] = ['a' => 'Page '.$pageNumber, 'u' => route('getgallery', [$gallery->slug, $orderSlug,'page-'.$pageNumber])];                
                    $canonical = route('getgallery', [$gallery->slug, $orderSlug, 'page-'.$pageNumber]);
                }
                else {
                    $breadcrumb[] = ['a' => 'Page '.$pageNumber, 'u' => route('getgallery', [$gallery->slug,'page-'.$pageNumber])];                
                    $canonical = route('getgallery', [$gallery->slug, 'page-'.$pageNumber]);
                }
            }

            JSInjection::addSessionModalData(false);
            JSInjection::inject();
            return View::make('frontend.gallery')
            ->with([
                'gallery' => $gallery,
                'order_base' => '/'.$gallery->slug.'/',
                'order_slug' => $orderSlug,
                'wallpapers' =>  $wallpapers,
                'breadcrumb' => $breadcrumb,
                'showDeviceFilter' => true,
                'title' => Seo::getGalleryData(TITLE_KEY, $gallery, $pageNumber),
                'description' => Seo::getGalleryData(DESCRIPTION_KEY, $gallery, $pageNumber),
                'canonical' => $canonical,
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
        
    }
    
}
