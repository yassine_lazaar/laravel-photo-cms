<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JSInjection;
use Wallpaper;
use Settings;
use App\Extensions\Paginator;
use Carbon\Carbon;
use View;
use Seo;

class SearchController extends Controller
{

    /**
     * SearchController constructor.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Search suggestions
     *
     * @param Request $request
     * @return mixed
     */
    public function getSuggestions(Request $request)
    {
        if ($request->expectsJson()){
            $term = $request->route('term');
            $results = Wallpaper::search($term, true);
            return response()->json([
                'success' => SEARCH_SUGGESTION_MESSAGE,
                'code' => SEARCH_SUGGESTION_CODE,
                'results' => $results
                ],
                200);
        }
    }

    /**
     * Search reults or suggestions
     *
     * @param Request $request
     * @return mixed
     */
    public function getResults(Request $request)
    {
        $term = $request->route('term');
        $url = 'search/'.$term;
        $pageSlug = $request->route('pageSlug');
        $pageNumber = 1;
        
        if ($pageSlug != null) {
            $pageNumber = str_replace('page-','', $pageSlug);
            if ($pageNumber <= 0) 
                return abort(404);
        }

        $results = collect(Wallpaper::search($term));
        if ($pageNumber > ceil($results->count() / Settings::perPage()) && $results->count() > 0){
            abort(404);   
        }
        $results = new Paginator($results, Settings::perPage(), $pageNumber, url($url));
        $results = Wallpaper::assignFavorites($results);

        $breadcrumb =  [
                ['a' => 'Home', 'u' => route('gethome')],
                ['a' => 'Search', 'u' => '']
            ];

        JSInjection::addSessionModalData(false);
        JSInjection::inject();
 
        return View::make('frontend.searchresults')
        ->with([
            'term' => $request->route('term'),
            'wallpapers' => $results,
            'breadcrumb' => $breadcrumb,
            'showDeviceFilter' => true,
            'title' => Seo::getSearchData(TITLE_KEY, $term, $pageNumber),
            'description' => Seo::getSearchData(DESCRIPTION_KEY, $term, $pageNumber),
            'timestamp' => Carbon::now()->getTimestamp()
        ]);
    }

}

   