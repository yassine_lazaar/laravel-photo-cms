<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Tag;
use JSInjection;
use Device;
use Settings;
use Wallpaper;
use App\Http\Controllers\Controller;
use View;
use App\Extensions\Paginator;
use Sentinel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;
use Seo;

class TagsController extends Controller
{
    /**
     * Tag Page
     *
     * @param String $tag
     * @param String $pageSlug
     * @return \Illuminate\View\View
     */
    public function getTag($tag = null, $pageSlug = null)
    {
        if (!$tag && !$pageSlug){
            $breadcrumb =  [
                ['a' => 'Home', 'u' => route('gethome')],
                ['a' => 'Tags', 'u' => route('gettag')]
            ];
            JSInjection::addSessionModalData(false);
            JSInjection::inject();
            return View::make('frontend.tags')
                ->with([
                    'random' => Tag::random(50),
                    'breadcrumb' => $breadcrumb,
                    'title' => 'Tags | '.env('APP_NAME'),
                    'description' => 'Tags | '.env('APP_NAME'),
                    'canonical' => route('gettag'),
                    'timestamp' => Carbon::now()->getTimestamp()                    
                ]);
        } else {
            $url = 'tags/'.$tag;
            $pageNumber = 1;

            try {
                $tag = Tag::findBySlug($tag); 
            } catch (ModelNotFoundException $e){
                abort(404);
            }  

            if ($tag->published != 1)                
                    return abort(404);
                
            $canonical = route('gettag', $tag->slug);
            if ($pageSlug != null) {
                if ($pageNumber > 1)
                    $canonical = route('gettag', [$tag->slug, 'page-'.$pageNumber]);                
                $pageNumber = str_replace('page-','', $pageSlug);
                if ($pageNumber <= 0) 
                    return abort(404);
            }

            $wallpapers = $tag->wallpapers;
            $wallpapers = $wallpapers->filter(function($w) {
                return $w->gallery->published || $w->published;
            });
            $wallpapers = Device::filterWallpapersToPickedDevice($wallpapers);

            $count = $wallpapers->count();
            if ($count > 0 && $pageNumber > ceil($wallpapers->count() / Settings::perPage())){
                abort(404);   
            }
            $wallpapers = new Paginator($wallpapers, Settings::perPage(), $pageNumber, url($url));
            $wallpapers = Wallpaper::assignFavorites($wallpapers);  

            $breadcrumb =  [
                ['a' => 'Home', 'u' => route('gethome')],
                ['a' => 'Tags', 'u' => route('gettag')],
                ['a' => $tag->name, 'u' => route('gettag', $tag->slug)]
            ];

            if ($pageNumber > 1)
                $breadcrumb[] = ['a' => 'Page '.$pageNumber, 'u' => route('gettag', [$tag->slug, 'page-'.$pageNumber])];                

            JSInjection::addSessionModalData(false);
            JSInjection::inject();
            return View::make('frontend.tags')
            ->with([
                'tag' => $tag->name,
                'count' => $count,
                'wallpapers' =>  $wallpapers,
                'breadcrumb' => $breadcrumb,
                'showDeviceFilter' => true,
                'title' => Seo::getTagData(TITLE_KEY, $tag, $pageNumber),
                'description' => Seo::getTagData(DESCRIPTION_KEY, $tag, $pageNumber),
                'canonical' => $canonical,
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
        }
    }


}
