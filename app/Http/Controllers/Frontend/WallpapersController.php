<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Wallpaper;
use Exception;
use Tag;
use Resolution;
use JSInjection;
use App\Http\Controllers\Controller;
use View;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\DownloadLimitExceededException;
use Intervention\Image\Facades\Image;
use Storage;
use Sentinel;
use Carbon\Carbon;
use Seo;

class WallpapersController extends Controller
{
    
    /**
     * Wallpaper page
     *
     * @param String $wallpaperSlug
     * @return \Illuminate\View\View
     */
    public function getWallpaper($wallpaperSlug)
    {
        $wallpaperSlug = str_replace('.htm', '', $wallpaperSlug);
        try {
            $wallpaper = Wallpaper::findBySlug($wallpaperSlug);
        } catch (ModelNotFoundException $e){
            abort(404);
        }

        if(!$wallpaper->published || !$wallpaper->gallery->published)
            abort(404);

        $wallpaper->views +=1;
        $wallpaper->save();
        
        $tags = Tag::getPublishedTagsForWallpaper($wallpaper);
        $galleryRelatedWallpapers = Wallpaper::getGalleryRelated($wallpaper);
        $userRelatedWallpapers = Wallpaper::getUserRelated($wallpaper);
        
        $favorites = [];
        $user = Sentinel::getUser();
        if ($user){
            $favorites = $user->favorites(false)->pluck('wallpaper_id')->toArray();
        }

        if (!empty($favorites)){
            if (in_array($wallpaper['id'], $favorites))
                $wallpaper['favorited'] = true;
            if (!empty($galleryRelatedWallpapers)){
                foreach($galleryRelatedWallpapers as $w){
                    if (in_array($w['id'], $favorites))
                        $w['favorited'] = true;
                }
            }
            if (!empty($userRelatedWallpapers)){
                foreach($userRelatedWallpapers as $w){
                    if (in_array($w['id'], $favorites))
                        $w['favorited'] = true;
                }
            }
        }
        
        $devices = Resolution::getWallpaperCompatibleDevices($wallpaper);
        //\Log::debug('wallpaper resolution is '.$wallpaper->resolution->xFormat());
        //\Log::debug('compatible device are '.json_encode($devices));
        
        $metaInfo = [
            'img' => $wallpaper->previewPath(),
            'width' => 750,
            'height' => floor(($wallpaper->height / $wallpaper->width) * 750),
        ];

        //\Log::debug('galleryRelatedWallpapers | '.json_encode($galleryRelatedWallpapers));
        //\Log::debug('userRelatedWallpapers | '.json_encode($userRelatedWallpapers));

        JSInjection::addSessionModalData(false);
        JSInjection::inject();
        return View::make('frontend.wallpaper')
        ->with([
            'title' => Seo::getWallpaperData(TITLE_KEY, $wallpaper),
            'description' => Seo::getWallpaperData(DESCRIPTION_KEY, $wallpaper),
            'wallpaper' => $wallpaper,
            'gallery' => $wallpaper->gallery,
            'uploader' => $wallpaper->user,
            'tags' => Tag::filterTagsToDeviceAndGalleryScope($tags),
            'devices' => $devices,
            'types' => unserialize(DEVICES_TYPES),
            'relatedWallpapers' => $galleryRelatedWallpapers,
            'userWallpapers' => $userRelatedWallpapers,
            'twitterCardImageInfo' => $metaInfo,
            'facebookCardImageInfo' => $metaInfo,
            'canonical' => route('getwallpaper', $wallpaper->slug.'.htm'),
            'timestamp' => Carbon::now()->getTimestamp()
        ]);
    }

    /**
     * Wallpaper download
     *
     * @param Request $request
     * @return mixed
     */
    public function postDownload(Request $request)
    {
        if (!$request->expectsJson())
            return;
        try {
            //\Log::debug('session downloads '+session(SESSION_DOWNLOADS));
            $filepath = Wallpaper::download(decrypt($request->route()->parameter('wallpaper')), $request->route()->parameter('device'));
            //\Log::debug('filepath '.json_encode($filepath));
            if ($filepath)
                return response()->json(['wallpaper_url' => '/'.$filepath], 200);            
        } catch (Exception $e) {
            if ($e instanceof DownloadLimitExceededException)
                return response()->json(['error' => DOWNLOAD_LIMIT_EXCEEDED_MESSAGE, 'code' => DOWNLOAD_LIMIT_EXCEEDED_CODE], 200);

            \Log::debug('Download failed | '.$e->getMessage());
            return response()->json(['error' => DOWNLOAD_ERROR_MESSAGE, 'code' => DOWNLOAD_ERROR_CODE], 400);
        }
    }

    /**
     * Favorite add/remove
     *
     * @param Request $request
     * @return mixed
     */
    public function postFavorite(Request $request)
    {
        if (!$request->expectsJson())
            return;
            
        $favorite = Wallpaper::favorite(decrypt($request->route()->parameter('wallpaper')));
        if(!is_null($favorite) and $favorite){
            return response()->json(['success' => ADDED_TO_FAVORITES_MESSAGE, 'code' => ADDED_TO_FAVORITES_CODE], 200);  
        }
        return response()->json(['success' => REMOVED_FROM_FAVORITES_MESSAGE, 'code' => REMOVED_FROM_FAVORITES_CODE], 200);
    }

}

