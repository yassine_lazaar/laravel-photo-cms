<?php

namespace App\Http\Controllers\Frontend;

use App\Managers\JSInjectionManager;
use App\Http\Controllers\Controller;
use JSInjection;
use Illuminate\Http\Request;
use Validator;
use View;
use Carbon\Carbon;
use Gallery;
use Resolution;
use Settings;
use Tag;
use Uploader;
use Mail;

class UploadController extends Controller
{

    protected $maxUploadSize;
    protected $minUploadDimension;

    /**
     * UploadController constructor.
     */
    public function __construct()
    {
        $this->maxUploadSize = Settings::maxUploadSize();
        $this->minUploadDimensions = Resolution::getLowestResolutions();
    }

    /**
     * Upload page
     * 
     * @return \Illuminate\View\View
     */
    public function getImageUpload()
    {
        $breadcrumb =  [
            ['a' => 'Home', 'u' => route('gethome')],
            ['a' => 'Upload', 'u' => route('getimageupload')]
        ];

        JSInjection::addSessionModalData(true);
        JSInjection::addData([
            'wallpaperFileSizeLimit' => $this->maxUploadSize,
            'wallpaperVerticalLimit' => $this->minUploadDimensions['vertical'],
            'wallpaperHorizontalLimit' => $this->minUploadDimensions['horizontal']
        ]);
        JSInjection::inject();
        return View::make('frontend.upload')
            ->with([
                'title' => 'Upload Your Wallpapers | '.env('APP_NAME'),
                'description' => 'Upload Your Wallpapers | '.env('APP_NAME'),
                'canonical' => route('getimageupload'),
                'breadcrumb' => $breadcrumb,
                'galleries' => Gallery::getOrderedGalleries(true),
                'startResolutions' => $this->minUploadDimensions,
                'maxUploadSize' => $this->maxUploadSize,
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
    }

    /**
     * Post wallpaper upload
     * 
     * @param Request $request
     * @return mixed
     */
    public function postImageUpload(Request $request)
    {
        //\Log::debug('$request '.json_encode($request->all()));
        $uploadedImage = $request->file('file');
        
        $uploadRules = array(
            'file' => array('required', 'image', 'mimes:jpeg,jpg,png', 'min_resolutions:'.$this->minUploadDimensions['vertical']['width'].'x'.$this->minUploadDimensions['vertical']['height'].','.$this->minUploadDimensions['horizontal']['width'].'x'.$this->minUploadDimensions['horizontal']['height'], 'max:'.$this->maxUploadSize * 1024),
            'title' => array('required', 'string', 'max:100'),
            'category' => array('required', 'integer', 'exists:galleries,id'),
            'description' => array('required', 'string', 'max:255'),
            'tags' => array('sometimes', 'nullable', 'array', 'max:5'),
            'license' => array('sometimes', 'integer', 'between:0,5'),
            'author' => array('sometimes', 'string', 'max:25'),
            'author_website' => array('sometimes', 'url', 'max:255'),
        );
        $data = array(
            'file' => $uploadedImage,
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'tags' => $request->get('tags'),
            'category' => $request->get('category'),
            'license' => $request->get('license'),
            'author' => $request->get('author'),
            'author_website' => addHttp($request->get('author_website'))
        );

        $imageValidator = Validator::make($data, $uploadRules);
        if ($imageValidator->fails()) {
            return redirect()->route('getimageupload')
                ->withInput()
                ->withErrors($imageValidator);
        }

        $result = Uploader::upload($data);
        if (isset($result)){
                try {
                $message = sprintf(
                                UPLOAD_MAIL_PLAIN,
                                $result->user->username,
                                $result->gallery->name
                            );
                Mail::send(
                    'sentinel.emails.uploadmail', 
                    array(
                        'uploader' => $result->user->username,
                        'senderMail' => env('ADMIN_MAIL'),
                        'mailMessage' => $message,
                    ),
                    function($m) use ($request, $message){
                        $m->to(env('ADMIN_MAIL'))->subject('Upload');
                        $m->from(env('ADMIN_MAIL'), env('APP_NAME'));
                        $m->addPart(
                            $message,
                            'text/plain'
                        );
                    }
                );
            } catch (Exception $e) {
                //\Log::debug('Error while notifying admin | '.$e->getMessage());
            }
            return redirect()->back()
                ->withSuccess(UPLOAD_WALLPAPER_MESSAGE);
        }
            
    }

    /**
     * Suggest tags
     *  
     * @param Request $request
     * @return mixed
     */
    public function getSuggestedTags(Request $request)
    {
        if ($request->expectsJson()) {
            $filters = Tag::getFilters($request);
            if(empty($filters))
                return response()->json([], 200);
            $tags = Tag::filter($filters);
            if ($request->has('suggest')) {
                $published = $tags->where('published',1);
                return response()->json($published, 200);
            }
        }
    }
}
