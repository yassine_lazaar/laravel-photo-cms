<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Avatar;
use JSInjection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Sentinel;
use Carbon\Carbon;
use Validator;
use Exception;
use App;
use Intervention\Image\Facades\Image;

class UserSettingsController extends Controller
{
    private $user;

    /**
     * UserSettingsController constructor.
     *
     */
    public function __construct()
    {
        $this->user = App::make('sentinel.users');
    }

    /**
     * User settings
     *
     * @return \Illuminate\View\View
     */
    public function getUserSettings()
    {
        $user = Sentinel::getUser();
        $breadcrumb =  [
                ['a' => 'Home', 'u' => route('gethome')],
                ['a' => 'Settings', 'u' => route('getusersettings')]
            ];

        JSInjection::addSessionModalData(true);
        JSInjection::addData([
            'user_avatar' => $user->avatar
        ]);
        JSInjection::inject();

        return View::make('frontend.usersettings')
        ->with([
            'title' => 'Settings | '.env('APP_NAME'),
            'description' => 'Settings | '.env('APP_NAME'),
            'breadcrumb' => $breadcrumb,
            'user' => $user,
            'countries' => unserialize(COUNTRIES),
            'timestamp' => Carbon::now()->getTimestamp()
        ]);
    }

    /**
     * Update user settings
     *
     * @return $this
     */
    public function postUserSettings(Request $request)
    {
        //\Log::debug('request '.json_encode($request->all()));
        $rules = [
            'first_name' => 'max:20',
            'last_name' => 'max:20',
            'gender' => 'in:1,2,3',
            'birthdate' => 'date_format:m-d-Y',
            'address' => 'max:255',
            'zip_code' => 'zip_code',
            'city' => 'max:20',
            'phone_number' => 'numeric|phone',
            'country' => 'country_code',
            'bio' => 'max:255',
            'website' => 'url',
            'facebook' => 'max:50',
            'twitter' => 'max:15',
            'instagram' => 'max:30',
        ];
        $v = Validator::make($request->all(), $rules);
        if ($v->fails()){
            return redirect()->back()->withErrors($v->getMessageBag());
        }

        try {
            $user = Sentinel::getUser();
            $alteredRequest = $request->toArray();
            $alteredRequest['birthdate'] = (!empty($alteredRequest['birthdate']))? \Carbon\Carbon::createFromFormat('d-m-Y', $alteredRequest['birthdate'])->format('Y-m-d') : null;
            $alteredRequest['gender'] = (isset($alteredRequest['gender']))? $alteredRequest['gender'] : null;
            $alteredRequest['address'] = setNullIfEmpty($alteredRequest['address']);
            $alteredRequest['birthdate'] = setNullIfEmpty($alteredRequest['birthdate']);
            $alteredRequest['zip_code'] = setNullIfEmpty($alteredRequest['zip_code']);
            $alteredRequest['city'] = setNullIfEmpty($alteredRequest['city']);
            $alteredRequest['phone_number'] = setNullIfEmpty($alteredRequest['phone_number']);
            $alteredRequest['bio'] = setNullIfEmpty($alteredRequest['bio']);
            $alteredRequest['website'] = setNullIfEmpty($alteredRequest['website']);
            $alteredRequest['facebook'] = setNullIfEmpty($alteredRequest['facebook']);
            $alteredRequest['twitter'] = setNullIfEmpty($alteredRequest['twitter']);
            $alteredRequest['instagram'] = setNullIfEmpty($alteredRequest['instagram']);
            
            $this->user->update($user->id, $alteredRequest);
            return redirect()->route('getusersettings')
                ->withSuccess(EDIT_USER_SETTINGS_MESSAGE);
        } catch (Exception $e){
            \Log::error('Failed to update user settings: '.$e->getMessage());
            return redirect()->route('getusersettings');
        }
    }

    /**
     * User avatar upload
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAvatarUpload(Request $request)
    {
        //\Log::debug("postAvatarUpload request: ".json_encode($request->toArray()));
        $rules = array(
            'avatar_file' => 'required|image|mimes:jpg,jpeg,png,gif|max:1024|dimensions:min_width='.DEVICE_IMAGE_SIZE_3X.',min_height='.DEVICE_IMAGE_SIZE_3X,
            'params' => 'required',
        );
        $v = Validator::make($request->all(), $rules);
        if($v->fails())
            return response()->json($v->errors(), 400);

        $isSafe = isSFW(file_get_contents($request->file('avatar_file')));
        if(!$isSafe)
            return response()->json(['results' => INAPPROPRIATE_IMAGE_MESSAGE, 'code' => UPLOAD_AVATAR_CODE], 400);

        $image = Image::make($request->file('avatar_file'));
        $cropParams = json_decode($request->get('params'), true);        
        $current_user = Sentinel::getUser();
        Avatar::saveAvatar($image, $cropParams, $current_user);
        return response()->json(['results' => UPLOAD_AVATAR_MESSAGE, 'code' => UPLOAD_AVATAR_CODE], 200);
    }


    /**
     * User avatar delete
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAvatarDelete()
    {
        $current_user = Sentinel::getUser();
        if(Avatar::deleteAvatar($current_user))
            return response()->json(['results' => DELETE_AVATAR_MESSAGE, 'code' => DELETE_AVATAR_CODE, 'avatar' => AVATAR_DEFAULT], 200);
        return response()->json(['results' => DELETE_AVATAR_FAIL_MESSAGE, 'code' => DELETE_AVATAR_FAIL_CODE], 500);
    }

    /**
     * User cover upload
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCoverUpload(Request $request)
    {
        //\Log::debug("postCoverUpload request: ".json_encode($request->all()));
        $imageData = $request->get('cover');
        $isSafe = isSFW(file_get_contents($imageData));
        if(!$isSafe)
            return response()->json(['results' => INAPPROPRIATE_IMAGE_MESSAGE, 'code' => UPLOAD_COVER_CODE], 400);

        $current_user = Sentinel::getUser();
        Avatar::saveCover($imageData, $current_user);
        return response()->json(['results' => UPLOAD_COVER_MESSAGE, 'id' => $current_user->id , 'username'=> $current_user->username, 'code' => UPLOAD_COVER_CODE], 200);
    }

    /**
     * User cover delete
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCoverDelete()
    {
        $current_user = Sentinel::getUser();
        if(Avatar::deleteCover($current_user))
            return response()->json(['results' => DELETE_COVER_MESSAGE, 'code' => DELETE_COVER_CODE], 200);
        return response()->json(['results' => DELETE_COVER_FAIL_MESSAGE, 'code' => DELETE_COVER_FAIL_CODE], 500);
    }

}
