<?php

namespace App\Http\Controllers\Frontend;

use Wallpaper;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Device;
use Settings;
use Illuminate\Support\Facades\View;
use JSInjection;
use App\Extensions\Paginator;
use Carbon\Carbon;
use Seo;
use Exception;
use Validator;
use Mail;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HomeController extends Controller
{
    /**
     * Main Page
     *
     * @param String $order
     * @param String $page
     * @return \Illuminate\View\View
     */
    public function getMain($orderSlug = null, $pageSlug = null)
    {
        $breadcrumb =  [
                ['a' => 'Home', 'u' => route('gethome')]
            ];
        $pageNumber = 1;

        if (!empty($orderSlug))
            $breadcrumb[] = ['a' => $orderSlug, 'u' => route('gethome', [$orderSlug])];
        else
            $breadcrumb[] = ['a' => 'Recent', 'u' => route('gethome')];                
        
        $filters = [
            ['published', '=', '1']
        ];
        $deviceFilter = Device::getDeviceFilter();
        if (is_null($deviceFilter) && !$orderSlug && !$pageSlug){ // Fetch cached homepage content
            $canonical = 'https://'.env('APP_DOMAIN');
            $homeContent = Wallpaper::getHomeContent();
            $wallpapers = new Paginator($homeContent['wallpapers'], Settings::perPage(), 1, '', $homeContent['count']);
            $wallpapers = Wallpaper::assignFavorites($wallpapers);
        } else {
            $url = '';
            $url = ($orderSlug)? $url.'/'.$orderSlug : $url;     
            $order = $orderSlug;   

            if ($pageSlug != null) {
                $pageNumber = str_replace('page-','', $pageSlug);
                if ($pageNumber <= 0){
                    return abort(404);
                }
            }
            switch ($order) {
                case GALLERY_ORDER_VIEWS:
                case GALLERY_ORDER_DOWNLOADS:
                case GALLERY_ORDER_FAVORITES:
                    break;
                case GALLERY_ORDER_FEATURED:
                    $filters = [
                        ['featured', '=', '1']
                    ];
                    $order = 'created_at';   
                    break; 
                case GALLERY_ORDER_TRENDING:
                    $filters = [
                        ['views', '>', WALLPAPER_TRENDING_THRESHOLD],
                        ['downloads', '>', WALLPAPER_TRENDING_THRESHOLD],
                        ['created_at', '>', Carbon::now()->subWeek()],
                    ];
                    $order = 'created_at';                    
                    break;
                default:
                    $order = 'created_at';     
            }

            $filterFn = function($q) use($filters, $deviceFilter)
            {
                $q->where($filters);
                $q->whereHas('gallery', function($query){
                    $query->where('published', 1);
                });
                if(!is_null($deviceFilter))
                    $q->where($deviceFilter);
            };

            try {
                if (strcmp($order, GALLERY_ORDER_FAVORITES) != 0) {
                    $wallpapers = Wallpaper::query(
                        $filterFn, 
                        ['id', 'title', 'slug', 'filename', 'featured', 'views', 'downloads', 'gallery_id', 'user_id'],
                        $order
                    );
                } else {
                    $wallpapers = Wallpaper::queryWithFavoritesCount(
                        $filterFn, 
                        ['id', 'title', 'slug', 'filename', 'featured', 'views', 'downloads', 'gallery_id', 'user_id']
                    );
                }
            } catch (ModelNotFoundException $e){
                $wallpapers = collect();
            }

            if ($wallpapers->count() > 0 && $pageNumber > ceil($wallpapers->count() / Settings::perPage())){
                abort(404);
            }  
            
            $wallpapers = new Paginator($wallpapers, Settings::perPage(), $pageNumber, url($url));
            $wallpapers = Wallpaper::assignFavorites($wallpapers);
            //\Log::debug('wallpapers '.json_encode($wallpapers));
            $canonical = route('gethome', $orderSlug);
            if ($pageNumber > 1){
                if (!empty($orderSlug)){
                    $breadcrumb[] = ['a' => 'Page '.$pageNumber, 'u' => route('gethome', [ $orderSlug,'page-'.$pageNumber])];                
                    $canonical = route('gethome', [$orderSlug, 'page-'.$pageNumber]);
                } else {
                    $breadcrumb[] = ['a' => 'Page '.$pageNumber, 'u' => route('gethome', ['page-'.$pageNumber])];                
                    $canonical = route('gethome', 'page-'.$pageNumber);
                }
            }
            
        }
        JSInjection::addSessionModalData(false);
        JSInjection::inject();

        return View::make('frontend.home')
        ->with([
            'order_base' => '/',
            'order_slug' => $orderSlug,
            'wallpapers' =>  $wallpapers,
            'breadcrumb' => $breadcrumb,
            'showDeviceFilter' => true,
            'title' => Seo::getHomePageData(TITLE_KEY, $pageNumber),
            'description' => Seo::getHomePageData(DESCRIPTION_KEY, $pageNumber),
            'canonical' => $canonical,
            'timestamp' => Carbon::now()->getTimestamp()        
        ]);
    }

    /**
     * Tos Page
     *
     * @return \Illuminate\View\View
     */
    public function getTerms()
    {
        $breadcrumb =  [
                ['a' => 'Home', 'u' => route('gethome')],
                ['a' => 'Terms & Conditions', 'u' => route('getterms')]
            ];

        JSInjection::addSessionModalData(false);
        JSInjection::inject();
        return View::make('frontend.tos')
        ->with([
            'title' => 'Terms & Conditions | '.env('APP_NAME'),
            'description' => 'Terms & Conditions | '.env('APP_NAME'),
            'breadcrumb' => $breadcrumb,
            'canonical' => route('getterms'),
            'privacy' => \Settings::getSetting(SETTING_SITE_TOS),
            'timestamp' => Carbon::now()->getTimestamp()        
        ]);
    }

    /**
     * Privacy Page
     *
     * @return \Illuminate\View\View
     */
    public function getPrivacy()
    {
        $breadcrumb =  [
            ['a' => 'Home', 'u' => route('gethome')],
            ['a' => 'Privacy Policy', 'u' => route('getprivacy')]
        ];

        JSInjection::addSessionModalData(false);
        JSInjection::inject();
        return View::make('frontend.privacy')
        ->with([
            'title' => 'Privacy Policy | '.env('APP_NAME'),
            'description' => 'Privacy Policy | '.env('APP_NAME'),
            'canonical' => route('getprivacy'),
            'breadcrumb' => $breadcrumb,
            'privacy' => \Settings::getSetting(SETTING_SITE_PRIVACY_POLICY),
            'timestamp' => Carbon::now()->getTimestamp() 
        ]);
            
    }

    /**
     * Copyright Page
     *
     * @return \Illuminate\View\View
     */
    public function getCopyright()
    {
        $breadcrumb =  [
            ['a' => 'Home', 'u' => route('gethome')],
            ['a' => 'Copyright Policy', 'u' => route('getcopyright')]
        ];

        JSInjection::addSessionModalData(false);
        JSInjection::inject();
        return View::make('frontend.copyright')
        ->with([
            'title' => 'Copyright Policy | '.env('APP_NAME'),
            'description' => 'Copyright Policy | '.env('APP_NAME'),
            'canonical' => route('getcopyright'),
            'breadcrumb' => $breadcrumb,
            'copyright' => \Settings::getSetting(SETTING_SITE_COPYRIGHT_POLICY),
            'timestamp' => Carbon::now()->getTimestamp() 
        ]);
            
    }

    /**
     * FAQ Page
     *
     * @return \Illuminate\View\View
     */
    public function getFaq()
    {
        $breadcrumb =  [
            ['a' => 'Home', 'u' => route('gethome')],
            ['a' => 'Frequently Asked Questions', 'u' => route('getfaq')]
        ];

        JSInjection::addSessionModalData(false);
        JSInjection::inject();
        return View::make('frontend.faq')
        ->with([
            'title' => 'Frequently Asked Questions | '.env('APP_NAME'),
            'description' => 'Frequently Asked Questions | '.env('APP_NAME'),
            'canonical' => route('getfaq'),
            'breadcrumb' => $breadcrumb,
            'faq' => \Settings::getSetting(SETTING_SITE_FAQ),
            'timestamp' => Carbon::now()->getTimestamp()        
        ]);
    }

    /**
     * Contact Page
     *
     * @return \Illuminate\View\View
     */
    public function getContact()
    {
        $breadcrumb =  [
            ['a' => 'Home', 'u' => route('gethome')],
            ['a' => 'Contact', 'u' => route('getcontact')]
        ];

        JSInjection::addSessionModalData(false);
        JSInjection::inject();
        return View::make('frontend.contact')
        ->with([
            'title' => 'Contact Us | '.env('APP_NAME'),
            'description' => 'Contact Us | '.env('APP_NAME'),
            'canonical' => route('getcontact'),
            'breadcrumb' => $breadcrumb,
            'timestamp' => Carbon::now()->getTimestamp()        
        ]);
    }


    /**
     * Contact Form
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function postContact(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'email' => 'required|email',
            'email_subject' => 'required',
            'email_message' => 'required',
            'g-recaptcha-response' => 'captcha|required'
        );

        $message = [
            'g-recaptcha-response.captcha' => 'Invalid captcha.',
            'g-recaptcha-response.required' => 'Missing captcha.'
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return redirect()->route('getcontact')
                ->withInput()
                ->withErrors($validator);
        }

        try {
            Mail::send(
                'sentinel.emails.usermail', 
                array(
                    'sender' => $request->get('name'),
                    'senderMail' => $request->get('email'),
                    'mailSubject' => $request->get('email_subject'),
                    'mailMessage' => $request->get('email_message'),
                ),
                function($m) use ($request) {
                    $m->to(env('CONTACT_MAIL'))->subject($request->get('email_subject'));
                    $m->from(env('CONTACT_MAIL'), $request->get('name'));
                    $m->addPart(
                        sprintf(
                            USER_MAIL_PLAIN,
                            $request->get('name'),
                            $request->get('email'),
                            $request->get('email_subject'),
                            $request->get('email_message')
                        ),
                        'text/plain'
                    );
                }
            );
            return redirect()->route('getcontact')
                ->withSuccess('Message successfully sent. We will back to you shortly.');

        } catch (Exception $e) {
            //\Log::debug('Error while sending user message | '.$e->getMessage());
            return redirect()->route('getcontact')
                    ->withInput()
                    ->withErrors('Message was not sent. Please try again later.');
        }
    }
}
