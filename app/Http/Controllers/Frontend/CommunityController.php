<?php

namespace App\Http\Controllers\Frontend;

use App;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use JSInjection;
use Wallpaper;
use Settings;
use Device;
use App\Extensions\Paginator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class CommunityController extends Controller
{
    private $user;

    /**
     * CommunityController constructor.
     *
     */
    public function __construct()
    {
        $this->user = App::make('sentinel.users');
    }


    /**
     * Community page
     *
     * @return \Illuminate\View\View
     */
    public function getCommunity()
    {
        
    }


    /**
     * User page
     *
     * @param String $username
     * @return \Illuminate\View\View
     */
    public function getUser($username)
    {
        try {
            $user = $this->user->findByUsername($username);
        } catch(ModelNotFoundException $e){
            abort(404);
        }
        $breadcrumb =  [
            ['a' => 'Home', 'u' => route('gethome')],
            ['a' => $user->username, 'u' => route('getuser', $user->username)]
        ];

        $deviceFilter = Device::getDeviceFilter();
        $favorites = $user->favorites(false)->get()->map(function($f) use($deviceFilter){
            try {
                $filters = [
                    ['id', '=', $f['wallpaper_id']],
                    ['published', '=', '1']
                ];
                $filterFn = function($q) use($filters, $deviceFilter){
                    $q->where($filters);
                    $q->whereHas('gallery', function($query){
                        $query->where('published', 1);
                    });
                    if(!is_null($deviceFilter))
                        $q->where($deviceFilter);
                };
                $w = Wallpaper::query($filterFn, array('*'))->first();
                $w['created_at'] = $f['updated_at'];
                $w['markAsLike'] = true;
                return $w;
            } catch (ModelNotFoundException $e){
                //
            }
        })->filter(function ($item, $key) {
            return $item != null;
        })->take(6)->values();
        //\Log::debug('favorites '.json_encode($favorites));
        
        $filters = [
                ['user_id', '=', $user->id],
                ['published', '=', '1']
            ];
        $filterFn = function($q) use($filters, $deviceFilter){
            $q->where($filters);
            $q->whereHas('gallery', function($query){
                $query->where('published', 1);
            });
            if(!is_null($deviceFilter))
                $q->where($deviceFilter);
        };

        try {        
            $wallpapers = Wallpaper::query($filterFn, array('*'))->take(6);
        } catch(ModelNotFoundException $e){
            $wallpapers = collect();
        }
        //\Log::debug('wallpapers '.json_encode($wallpapers));
        
        $favorites->each(function ($item, $key) use ($wallpapers) {
            $wallpapers->push($item);
        });
        $activities = $wallpapers->sortByDesc('created_at');
        //\Log::debug('union '.json_encode($wallpapers));
        
        $isProfileOwner = (\Sentinel::check())?(\Sentinel::check()->id == $user->id):false;
        JSInjection::addSessionModalData($isProfileOwner);
        JSInjection::inject();
        
        return View::make('frontend.user')
        ->with([
            'title' => $user->username.'\'s Profile Activity | '.env('APP_NAME'),
            'description' => 'Check out '.$user->username.'\'s Profile Activity | '.env('APP_NAME'),
            'canonical' => route('getuser', $username),
            'user' => $user,
            'isProfileOwner' => $isProfileOwner,
            'cover' => $user->getCover(),
            'socialTitle' => 'Profile',
            'activities' => $activities,
            'breadcrumb' => $breadcrumb,
            'timestamp' => Carbon::now()->getTimestamp()
        ]);
    }


    /**
     * User favorites
     *
     * @param String $username
     * @param String $pageSlug
     * @return \Illuminate\View\View
     */
    public function getUserFavorites($username, $pageSlug = null)
    {
        try {
            $user = $this->user->findByUsername($username);
        } catch(ModelNotFoundException $e){
            abort(404);
        }
            $url = 'user/'.$username.'/favorites';
            $url = ($pageSlug)? $url.'/'.$pageSlug : $url; 
            $pageNumber = 1;

            if ($pageSlug != null) {
                $pageNumber = str_replace('page-','', $pageSlug);
                if ($pageNumber <= 0) 
                    return abort(404);
            }

            $deviceFilter = Device::getDeviceFilter();
            $wallpapers = $user->favorites(false)->get()->map(function($f) use($deviceFilter){
                    try {
                        $filters = [
                            ['id', '=', $f['wallpaper_id']],
                            ['published', '=', '1']
                        ];
                        $filterFn = function($q) use($filters, $deviceFilter){
                            $q->where($filters);
                            $q->whereHas('gallery', function($query){
                                $query->where('published', 1);
                            });
                            if(!is_null($deviceFilter))
                                $q->where($deviceFilter);
                        };
                        $w = Wallpaper::query($filterFn, array('*'))->first();
                        return $w;
                    } catch (ModelNotFoundException $e){
                        //
                    }
                })->filter(function ($item, $key) {
                    return $item != null;
                })->values();
                
            if ($wallpapers->count() > 0 && $pageNumber > ceil($wallpapers->count() / Settings::perPage())){
                abort(404);   
            }
            $wallpapers = new Paginator($wallpapers, Settings::perPage(), $pageNumber, route('getuserfavorites', $user->username));
            $wallpapers = Wallpaper::assignFavorites($wallpapers);

            $breadcrumb =  [
                ['a' => 'Home', 'u' => route('gethome')],
                ['a' => $user->username, 'u' => route('getuser', $user->username)],
                ['a' => 'Favorites', 'u' => route('getuserfavorites', $user->username)]
            ];

            $title = $user->username.'\'s Favorite Photos - '.env('APP_NAME');
            $description = 'Check out '.$user->username.'\'s Favorite Photos - '.env('APP_NAME');
            $canonical = route('getuserfavorites', $user->username);
            if ($pageNumber > 1){
                $title = $user->username.'\'s Favorite Photos - Page '.$pageNumber.' - '.env('APP_NAME');
                $description = 'Check out '.$user->username.'\'s Favorite Photos - Page '.$pageNumber.' - '.env('APP_NAME');
                $canonical = route('getuserfavorites', [$user->username, 'page-'.$pageNumber]);
                $breadcrumb[] = ['a' => 'Page '.$pageNumber, 'u' => route('getuserfavorites', [$user->username, 'page-'.$pageNumber])];
            }

            $isProfileOwner = (\Sentinel::check())?(\Sentinel::check()->id == $user->id):false;
            JSInjection::addSessionModalData($isProfileOwner);
            JSInjection::inject();
            
            return View::make('frontend.userfavorites')
            ->with([
                'title' => $title,
                'description' => $description,
                'canonical' => $canonical,
                'user' => $user,
                'wallpapers' => $wallpapers,
                'breadcrumb' => $breadcrumb,
                'cover' => $user->getCover(),
                'socialTitle' => 'Favotites',
                'isProfileOwner' => $isProfileOwner,
                'showDeviceFilter' => true,
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
        
    }

    /**
     * User uploads
     *
     * @param String $username
     * @param String $pageSlug
     * @return \Illuminate\View\View
     */
    public function getUserUploads($username, $pageSlug = null)
    {
        try{
            $user = $this->user->findByUsername($username);
            } catch(ModelNotFoundException $e){
            abort(404);
        }
            $url = 'user/'.$username.'/uploads';
            $url = ($pageSlug)? $url.'/'.$pageSlug : $url; 
            $pageNumber = 1;

            if ($pageSlug != null) {
                $pageNumber = str_replace('page-','', $pageSlug);
                if ($pageNumber <= 0) 
                    return abort(404);
            }

            $filters = [
                ['user_id', '=', $user->id],
                ['published', '=', '1']
            ];
            $deviceFilter = Device::getDeviceFilter();
            $filterFn = function($q) use($filters, $deviceFilter){
                $q->where($filters);
                $q->whereHas('gallery', function($query){
                    $query->where('published', 1);
                });
                if(!is_null($deviceFilter))
                    $q->where($deviceFilter);
            };

            try {
                $wallpapers = Wallpaper::query($filterFn, array('*'));
            } catch(ModelNotFoundException $e){
                $wallpapers = collect();
            }

            if ($wallpapers->count() > 0 && $pageNumber > ceil($wallpapers->count() / Settings::perPage())){
                abort(404);   
            }
            $wallpapers = new Paginator($wallpapers, Settings::perPage(), $pageNumber, route('getuseruploads', $user->username));
            $wallpapers = Wallpaper::assignFavorites($wallpapers);

            $breadcrumb =  [
                ['a' => 'Home', 'u' => route('gethome')],
                ['a' => $user->username, 'u' => route('getuser', $user->username)],
                ['a' => 'Uploads', 'u' => route('getuseruploads', $user->username)]
            ];

            $title = $user->username.'\'s Uploads - '.env('APP_NAME');
            $description = 'Check '.$user->username.'\'s Uploaded Photos - '.env('APP_NAME');
            $canonical = route('getuseruploads', $user->username);
            if ($pageNumber > 1){
                $title = $user->username.'\'s Uploaded Photos - Page '.$pageNumber.' - '.env('APP_NAME');
                $description = 'Check '.$user->username.'\'s Uploaded Photos - Page '.$pageNumber.' - '.env('APP_NAME');
                $canonical = route('getuseruploads', [$user->username, 'page-'.$pageNumber]);
                $breadcrumb[] = ['a' => 'Page '.$pageNumber, 'u' => route('getuseruploads', [$user->username, 'page-'.$pageNumber])];
            }

            $isProfileOwner = (\Sentinel::check())?(\Sentinel::check()->id == $user->id):false;
            JSInjection::addSessionModalData($isProfileOwner);
            JSInjection::inject();
            
            return View::make('frontend.useruploads')
            ->with([
                'title' => $title,
                'description' => $description,
                'canonical' => $canonical,
                'user' => $user,
                'wallpapers' => $wallpapers,
                'breadcrumb' => $breadcrumb,
                'cover' => $user->getCover(),
                'socialTitle' => 'Uploads',
                'isProfileOwner' => $isProfileOwner,
                'showDeviceFilter' => true,
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
        
    }
}
