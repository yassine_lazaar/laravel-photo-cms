<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JSInjection;
use App\Repositories\DevicesRepository;
use App\Repositories\ResolutionsRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Sentinel;
use Device;
use Seo;
use Wallpaper;
use Resolution;
use Settings;
use App\Extensions\Paginator;
use View;
use Exception;
use Carbon\Carbon;



class DeviceController extends Controller
{
    private $device;
    private $resolution;

    /*
    * DeviceController constructor.
    *
    * @param DevicesRepository $device
    * @param ResolutionsRepository $resolution
    */
    public function __construct(DevicesRepository $device, ResolutionsRepository $resolution)
    {
        $this->device = $device;
        $this->resolution = $resolution;

    }

/**
     * Main Page
     *
     * @param String $deviceSlug
     * @param String $order
     * @param String $page
     * @return \Illuminate\View\View
     */
    public function getDevice($deviceSlug, $orderSlug = null, $pageSlug = null)
    {
        $url = $deviceSlug.'-wallpapers';
        $url = ($orderSlug)? $url.'/'.$orderSlug : $url;     
        $pageNumber = 1;
        $order = $orderSlug;   
        
        if ($pageSlug != null) {
            $pageNumber = str_replace('page-','', $pageSlug);
            if ($pageNumber <= 0) 
                return abort(404);
        }
        
        try {
            $device = $this->device
                    ->where([
                        ['slug', '=', $deviceSlug],
                        ['excluded', '=', 0]
                    ])->first();
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
            $filters = [
                ['published', '=', '1']
            ];
            $deviceFilter = Device::getDeviceFilter();
            
            switch ($order) {
                case GALLERY_ORDER_VIEWS:
                case GALLERY_ORDER_DOWNLOADS:
                case GALLERY_ORDER_FAVORITES:
                    break;
                case GALLERY_ORDER_FEATURED:
                    $filters[] = ['featured', '=', '1'];
                    $order = 'created_at';   
                    break; 
                case GALLERY_ORDER_TRENDING:
                    $filters[] = ['views', '>', WALLPAPER_TRENDING_THRESHOLD];
                    $filters[] = ['downloads', '>', WALLPAPER_TRENDING_THRESHOLD];
                    $filters[] = ['created_at', '>', Carbon::now()->subWeek()];
                    $order = 'created_at';                    
                    break;
                default:
                    $order = 'created_at';     
            }

            $filterFn = function($q) use($filters, $deviceFilter)
            {
                $q->where($filters);
                $q->whereHas('gallery', function($query){
                    $query->where('published', 1);
                });
                if(!is_null($deviceFilter))
                    $q->where($deviceFilter);
            };

            try {
                if (strcmp($order, GALLERY_ORDER_FAVORITES) != 0) {
                    $wallpapers = Wallpaper::query(
                        $filterFn, 
                        ['id', 'title', 'slug', 'filename', 'featured', 'views', 'downloads', 'gallery_id', 'user_id'],
                        $order
                    );
                } else {
                    $wallpapers = Wallpaper::queryWithFavoritesCount(
                        $filterFn, 
                        ['id', 'title', 'slug', 'filename', 'featured', 'views', 'downloads', 'gallery_id', 'user_id']
                    );
                }       
            } catch (ModelNotFoundException $e) {
                $wallpapers = collect();
            }
            
            if ($wallpapers->count() > 0 && $pageNumber > ceil($wallpapers->count() / Settings::perPage())){
                abort(404);   
            }

            $wallpapers = new Paginator($wallpapers, Settings::perPage(), $pageNumber, url($url));
            $wallpapers = Wallpaper::assignFavorites($wallpapers);

            $breadcrumb =  [
                    ['a' => 'Home', 'u' => route('gethome')],
                    ['a' => (strcmp($device->maker, 'N/A') == 0)? $device->name : $device->maker.' '.$device->name, 'u' => route('getdevice', $device->slug)]
                ];

            $canonical = route('getdevice', $device->slug);
            if (!empty($orderSlug)){
                $breadcrumb[] = ['a' => $orderSlug, 'u' => route('getdevice', [$device->slug, $orderSlug])];
                $canonical = route('getdevice', [$device->slug, $orderSlug]);
            }
            if ($pageNumber > 1){
                if (!empty($orderSlug)){
                    $breadcrumb[] = ['a' => 'Page '.$pageNumber, 'u' => route('getdevice', [$device->slug, $orderSlug,'page-'.$pageNumber])];                
                    $canonical = route('getdevice', [$device->slug, $orderSlug, 'page-'.$pageNumber]);
                }
                else{
                    $breadcrumb[] = ['a' => 'Page '.$pageNumber, 'u' => route('getdevice', [$device->slug,'page-'.$pageNumber])];                
                    $canonical = route('getdevice', [$device->slug, 'page-'.$pageNumber]);
                }
            }

            JSInjection::addSessionModalData(false);
            JSInjection::inject();
            return View::make('frontend.device')
            ->with([
                'device' => $device,
                'order_base' => '/'.$device->slug.'-wallpapers/',
                'order_slug' => $orderSlug,
                'wallpapers' =>  $wallpapers,
                'breadcrumb' => $breadcrumb,
                'title' => Seo::getDeviceData(TITLE_KEY, $device, $pageNumber),
                'description' => Seo::getDeviceData(DESCRIPTION_KEY, $device, $pageNumber),
                'canonical' => $canonical,
                'timestamp' => Carbon::now()->getTimestamp()
            ]);
    }

    /*
    * Device Picker
    *
    * @return \Illuminate\View\View
    */
    public function getDevicePicker()
    {
        $breadcrumb =  [
            ['a' => 'Home', 'u' => route('gethome')],
            ['a' => 'Pick Your Device', 'u' => route('getdevicepicker')]
        ];

        $current_user = Sentinel::getUser();
        JSInjection::addSessionModalData(false);
        JSInjection::inject();
        return View::make('frontend.devicepicker')
        ->with([
            'title' => 'Pick Your Device | '.env('APP_NAME'),
            'description' => 'Pick A Device To Filter Content | '.env('APP_NAME'),
            'canonical' => route('getdevicepicker'),
            'breadcrumb' => $breadcrumb,
            'user' => $current_user,
            'timestamp' => Carbon::now()->getTimestamp()
        ]);
    }

    /*
    * Device Picker Search
    *
    * @return
    */
    public function searchDevices(Request $request)
    {
        if ($request->expectsJson())
        {
            try {
                $filters = [];
                foreach(explode(' ', $request->get('search_term')) as $t){
                    $filters[] = ['name', 'like', "{$t}%"];
                    $filters[] = ['maker', 'like', "{$t}%"];
                }
                $results = $this->device->makeModel()
                    ->where(function ($q) use ($filters){
                        foreach($filters as $f){
                            $q->orWhere($f[0], $f[1], $f[2]);
                        }
                    })
                    ->where('excluded', 0)
                    ->get();
            } catch (ModelNotFoundException $e){
                $results = [];
            }
            return response()
                ->json([
                    'results' => $results
                ], 200);
        }
    }

    /*
    * Device Pick
    *
    * @return
    */
    public function pickDevice($id)
    {
        $device = $this->device->find($id);
        if($device->excluded == 1) 
            return redirect()->back()->withErrors('Sorry! You cannot pick that device.');
        
        session([SESSION_DEVICE_ID_KEY => $id]);
        session([SESSION_DEVICE_NAME_KEY => $device->name]);  
        session([SESSION_DEVICE_MAKER_KEY => $device->maker]);  
        session([SESSION_DEVICE_SLUG_KEY => $device->slug]);  
        session([SESSION_DEVICE_IMAGE_KEY => $device->featured_image]);  

        return redirect()->to(route('getdevice', $device->slug))->withSuccess(sprintf(DEVICE_PICKER_MESSAGE, (strcmp($device->maker, 'N/A') == 0)? $device->name : $device->maker.' '.$device->name));
    }

    
    /*
    * Device clear
    *
    * @return
    */
    public function clearDevice()
    {
        if(session()->has(SESSION_DEVICE_ID_KEY)){
            $pickedDevice = Device::getPickedDevice();
            if(isset($pickedDevice)){
                session()->forget(SESSION_DEVICE_ID_KEY);
                session()->forget(SESSION_DEVICE_NAME_KEY);
                session()->forget(SESSION_DEVICE_MAKER_KEY);
                session()->forget(SESSION_DEVICE_SLUG_KEY);
                session()->forget(SESSION_DEVICE_IMAGE_KEY);
            }
        }
        return redirect()->route('gethome');
    }
}
