<?php

namespace App\Http\Controllers\Auth;

use App;
use Avatar;
use JSInjection;
use Cartalyst\Sentinel\Addons\Social\AccessMissingException;
use Cartalyst\Sentinel\Addons\Social\Models\LinkInterface;
use Cartalyst\Sentinel\Addons\Social\Repositories\LinkRepository;
use Cartalyst\Sentinel\Users\UserInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Torann\GeoIP\Facades\GeoIP;
use Sentinel;
use Social;
use Session;
use Guzzle\Service\Client as GuzzleClient;

class SocialAuthController extends Controller
{

    protected $links;
    private $sessionHandler;
    private $user;
    private $role;

    /**
     * SocialAuthController constructor.
     *
     * @param LinkRepository $links
     *
     */
    public function __construct(LinkRepository $links)
    {
        $this->middleware('guest', ['only' => 'providerLogin']);
        $this->links = $links;
        $this->sessionHandler = Session::getHandler();
        $this->user = App::make('sentinel.users');
        $this->role = App::make('sentinel.roles');
    }

    /**
     * Social authorization
     *
     * @param mixed $provider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function providerAuthorize($provider) // need to redirect back with proper error if failed to reach provider
    {
        if($user = Sentinel::check()) { // Linking provider
            if($user->isProviderLinked($provider)){
                return redirect()->back()
                    ->withErrors('Your '.ucfirst($provider).' account is already linked.');

            }
        }
        session()->put('invoker', \URL::previous());
        $callback = route('providercallback', $provider);
        $url = Social::getAuthorizationUrl($provider, $callback);
        return redirect()->to($url);
    }

    /**
     * Social callback
     *
     * @param Request $request
     * @param $provider
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function providerCallback(Request $request, $provider)
    {
        // Callback is required for providers such as Facebook and a few others (it's required
        // by the spec, but some providers omit this).
        $callback = URL::current();

        Social::registered(function(LinkInterface $link, $provider, $token, $slug)
        {
            try {
                // User is newly registered
                $user = $link->getUser();
                
                $role = $this->role->findByName('User');
                $role->users()->attach($user);
                $providerUser = Social::getUserDetails($provider, $token);
                //$this->user->update($user->getUserId(), ['social_providers' => $slug]);

                //Save user location
                $user->country = GeoIP::getLocation(\Request::ip())['iso_code'];
                $user->save();
                
                // Suggest a username to be saved
                if ($provider instanceof \League\OAuth1\Client\Server\Server){
                    $username = $providerUser->name;
                } else  if ($provider instanceof \League\OAuth2\Client\Provider\AbstractProvider){
                    $username = ($providerUser->getName()) ? $providerUser->getName() : $providerUser->getFirstName() . $providerUser->getLastName();
                }
                $username = $this->cleanUsername($username);
                session(array('suggested_username' => $username));
                
                // Fetch Avatar
                if ($provider instanceof \League\OAuth1\Client\Server\Server){
                    if (!$providerUser->isDefaultPicture)
                        $this->fetchSocialAvatar($user, $providerUser->imageUrl);
                } else  if ($provider instanceof \League\OAuth2\Client\Provider\AbstractProvider){
                    if (!$providerUser->isDefaultPicture())
                        $this->fetchSocialAvatar($user, $providerUser->getPictureUrl());
                }
            } catch (\Exception $e){
                \Log::debug('Exception while registering social user |'.$e->getMessage());
            }
        });

/*
        Social::existing(function(LinkInterface $link, $provider, $token, $slug)
        {
            // User already exists
            
        });
*/

        try {
            $user = Social::authenticate($provider, $callback, function (LinkInterface $link, $provider, $token, $slug)
            {
                // Check if already logged in
                try {
                    $user= $link->getUser();
                    if ($user)
                    {
                        if($this->sessionHandler->isUserLoggedIn($user->id)){
                            Sentinel::logout($user);
                        }
                    }
                } catch (ModelNotFoundException $e) {
                    //
                }
            });
        } catch (AccessMissingException $e) {
            // Missing OAuth parameters from the query string.
            // Either the person rejected the app, or the URL has been manually
            // accessed.
            if ($request->get('error') || $request->get('denied'))
                return redirect()->to(session('invoker'))->withErrors('Access to your ' . ucfirst($provider) . ' account was denied.');
            return redirect()->to(session('invoker'))
                ->withErrors('There seem to be a problem. Could not login using ' . ucfirst($provider) . '.');
        
        } catch (\Exception $e) { // Catch bad responses and any other exception
            \Log::debug('Exception while authenticating social user |'.$e->getMessage());
            return redirect()->to(session('invoker'))
                ->withErrors('There seem to be a problem. Could not login using ' . ucfirst($provider) . '.');
        }
        return redirect()->to('/');
    }

    /**
     *
     * Fetch Social Avatar
     *
     * @param $user
     * @param $url
     *
     */
    private function fetchSocialAvatar($user, $url){

        $client = new GuzzleClient();
        $request = $client->get(stripcslashes($url));
        $response = $request->send();

        if(strcmp($user->avatar, AVATAR_DEFAULT) == 0)
            Avatar::saveSocialAvatar(
                \Image::make($response->getBody()->getStream()),
                $user
            );
    }


    private function cleanUsername($username) {
        // Additional Swedish filters
        $username = str_replace(array("ä", "Ä"), "a", $username);
        $username = str_replace(array("å", "Å"), "a", $username);
        $username = str_replace(array("ö", "Ö"), "o", $username);

        // Remove any character that is not alphanumeric, white-space, or a hyphen 
        $username = preg_replace("/[^a-z0-9\s\-]/i", "", $username);
        // Replace multiple instances of white-space with a single space
        $username = preg_replace("/\s\s+/", " ", $username);
        // Replace all spaces with hyphens
        $username = preg_replace("/\s/", "-", $username);
        // Replace multiple hyphens with a single hyphen
        $username = preg_replace("/\-\-+/", "-", $username);
        // Remove leading and trailing hyphens
        $username = trim($username, "-");
        // Lowercase the URL
        $username = strtolower($username);
        // Max of 15 chars
        $username= substr($username, 0, 15);

        return $username;
    }

}
