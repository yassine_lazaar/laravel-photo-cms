<?php

namespace App\Http\Controllers\Auth;

use Activation;
use App;
use JSInjection;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class ActivationController extends Controller
{
    private $user;

    /**
     * ActivationController constructor.
     *
     */
    public function __construct()
    {
        $this->user = App::make('sentinel.users');
        $this->middleware('guest');
    }

    /**
     * Handles user activation.
     *
     * @param  int  $id
     * @param  string  $code
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function getActivate($id,$code)
    {
        $user = $this->user->findById($id);
        if ($user && $activation = Activation::exists($user)){
                if (Activation::complete($user,$code)){
                    return redirect()->route('getlogin')
                        ->withSuccess('Your account is now activated. You may login now.');
                }
                return redirect()->route('getlogin')
                    ->withErrors('Could not complete activation process.');
        } else {
            return redirect()->route('getlogin')
                ->withErrors('Invalid activation parameters.');
        }
    }

    /**
     * Show the form for resending activation link.
     *
     * @return \Illuminate\View\View
     */
    public function getReactivate()
    {
        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('frontend.reactivate')
            ->with([
                'title' => 'Activate Your Account | '.env('APP_NAME'),
                'description' => 'Get your activation details | '.env('APP_NAME'),
            ]);
    }

    /**
     * Handles activation link request.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postReactivate(Request $request)
    {
        $rules = [
            'email'    => 'required|email',
            'g-recaptcha-response'  => 'captcha|required'
        ];

        $message = [
            'g-recaptcha-response.captcha' => 'Invalid captcha.',
            'g-recaptcha-response.required' => 'Missing captcha.'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails())
        {
            return redirect()->route('getreactivate')
                ->withErrors($validator);
        }

        try {
            $user = $this->user->findByCredentials($request->only('email'));
        } catch (ModelNotFoundException $e) {
            return redirect()->route('getreactivate')
                    ->withErrors('There is no registered account with the provided email.');
        }
        if (!($activation = Activation::exists($user))) { // Activation not found or expired or user already registered
            if ($activation = Activation::completed($user)) {
                // User has completed the activation process
                return redirect()->route('getreactivate')
                    ->withErrors('This account is already registered.');
            } else { // Activation not found or expired so create a new one
                $activation = Activation::create($user);
            }
        }
        $code = $activation->code;
        $sent = $this->sendActivationLink($user, $code);
        if ($sent === 0) {
            return redirect()->route('getreactivate')
                ->withErrors('Failed to send activation email.');
        }
        return redirect()->route('getlogin')
            ->withSuccess('Check your email for the activation link.');
    }


    /**
     * Sends activation link.
     *
     * @param User $user
     * @param string|string $code
     * @return bool
     */
    private function sendActivationLink(User $user, $code)
    {
        return Mail::send(
            'sentinel.emails.activate', 
            compact('user', 'code'), 
            function($m) use ($user, $code) {
                $m->to($user->email)->subject('Activate Your Account');
                $m->from(env('ADMIN_MAIL'), env('MAIL_SENDER'));
                $m->addPart(
                    sprintf(
                        ACTIVATION_MAIL_PLAIN,
                        $user->username,
                        env('APP_NAME'),
                        route('getactivate', array('id' => $user->getUserId(), 'code' => $code)),
                        route('getactivate', array('id' => $user->getUserId(), 'code' => $code)),
                        \Config::get('cartalyst.sentinel.activations.expires')/3600,
                        env('SUPPORT_MAIL'),
                        env('SUPPORT_MAIL'),
                        env('MAIL_SENDER')
                    ),
                    'text/plain'
                );
            }
        );
    }
}
