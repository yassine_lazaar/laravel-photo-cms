<?php

namespace App\Http\Controllers\Auth;

use App;
use JSInjection;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Mail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Reminder;

class PasswordController extends Controller
{
    private $user;

    /**
     * PasswordController constructor.
     *
     */
    public function __construct()
    {
        $this->user = App::make('sentinel.users');
        $this->middleware('guest');
    }

    /**
     * Show password mail form
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function getResetMail()
    {
        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('frontend.passwordmail')
            ->with([
                'title' => 'Password Reset | '.env('APP_NAME'),
                'description' => 'Get your password reset details | '.env('APP_NAME'),
                'canonical' => route('getreset')
            ]);
    }

    /**
     * Show password reset form
     *
     * @param  int  $id
     * @param  string  $code
     * @return mixed
     */
    public function getReset($id,$code)
    {
        $user = $this->user->findById($id);
        if ($user && $reminder = Reminder::exists($user)) {
            if (str_is($reminder->code, $code)) {
                JSInjection::addSessionModalData(true);
                JSInjection::inject();
                return View::make('frontend.passwordreset')
                    ->with([
                        'id' => $id,
                        'code' => $code,
                        'title' => 'Password Reset | '.env('APP_NAME'),
                        'description' => 'Reset your password | '.env('APP_NAME'),
                    ]);
            }
        }
        return redirect()->route('getlogin')
            ->withErrors('Invalid reset parameters.');
    }

    /**
     * Handles reset link (via Mail) requests
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postResetMail(Request $request)
    {
        $rules = [
            'email'    => 'required|email',
            'g-recaptcha-response'  => 'captcha|required'

        ];
        $message = [
            'g-recaptcha-response.captcha' => 'Invalid captcha.',
            'g-recaptcha-response.required' => 'Missing captcha.'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return redirect()->route('getreset')
                ->withErrors($validator);
        }

        try {
            $user = $this->user->findByCredentials(['email' => $request->only('email')]);
        } catch (ModelNotFoundException $e){
            return redirect()->route('getreset')
                ->withErrors('There is no registered account with the provided email.');
        }

        if(!($reminder = Reminder::exists($user))){ // Activation not found or expired
            $reminder = Reminder::create($user);
        }
        $code = $reminder->code;
        $sent = $this->sendReminderLink($user,$code);
        if ($sent === 0) {
            return redirect()->route('getreset')
                ->withErrors('Failed to send password reset email.');
        }
        return redirect()->route('getlogin')
            ->withSuccess('Check your email to reset your password.');
    }


    /**
     * Handles password reset requests
     *
     * @param Request $request
     * @param  int  $id
     * @param  string  $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postReset(Request $request,$id,$code)
    {
        $rules = [
            'password' => 'required|confirmed|min:6',
        ];
        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return redirect()->route('getpassreset')
                ->withErrors($validator);
        }

        $user = $this->user->findById($id);

        if ($user && $reminder = Reminder::exists($user)){
            if (str_is($reminder->code, $code)){
                if (Reminder::complete($user,$code,$request->get('password'))){
                    // Reminder was successfull
                    return redirect()->route('getlogin')
                        ->withSuccess('Your password has been reset. You may login now.');
                } else {
                    return redirect()->route('getlogin')
                        ->withErrors('Could not complete password reset.');
                }
            }
        }
        return redirect()->route('getlogin')
            ->withErrors('Invalid reset parameters.');
    }


    /**
     * Mail password reset links
     *
     * @param User $user
     * @param $code
     * @return boolean
     */
    private function sendReminderLink(User $user, $code)
    {
        $sent = Mail::send(
                'sentinel.emails.reminder', 
                compact('user', 'code'), 
                function($m) use ($user, $code) {
                    $m->to($user->email)->subject('Password reset');
                    $m->from(env('ADMIN_MAIL'), env('MAIL_SENDER'));
                    $m->addPart(
                        sprintf(
                            REMINDER_MAIL_PLAIN,
                            $user->username,
                            env('APP_NAME'),
                            route('getpassreset', array('id' => $user->getUserId(), 'code' => $code)),
                            route('getpassreset', array('id' => $user->getUserId(), 'code' => $code)),
                            \Config::get('cartalyst.sentinel.reminders.expires')/3600,
                            env('SUPPORT_MAIL'),
                            env('SUPPORT_MAIL'),
                            env('MAIL_SENDER')
                        ),
                        'text/plain'
                    );
                }
            );
        return $sent;
    }

}
