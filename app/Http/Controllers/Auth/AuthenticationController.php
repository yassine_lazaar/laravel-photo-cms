<?php

namespace App\Http\Controllers\Auth;

use Activation;
use JSInjection;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Torann\GeoIP\Facades\GeoIP;
use App;
use Mail;
use Sentinel;
use Validator;
use Exception;
use App\Models\User as User;

class AuthenticationController extends Controller
{

    private $role;
    private $sessionHandler;

    private $loginRules = [
        'email'    => 'required|email',
        'password' => 'required',
    ];

    /**
     * AuthenticationController constructor.
     *
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout','sessionCheck', 'getUsername', 'postUsername']]);
        $this->role = App::make('sentinel.roles');
        $this->sessionHandler = Session::getHandler();
    }

    /**
     * Show the form for logging the user in.
     *
     * @return \Illuminate\View\View
     */
    public function getLogin()
    {
        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('frontend.login')
            ->with([
                'title' => 'Login | '.env('APP_NAME'),
                'description' => 'Login | '.env('APP_NAME'),
                'canonical' => route('getlogin')
            ]);
    }


    /**
     * Show the form for the user registration.
     *
     * @return \Illuminate\View\View
     */
    public function getRegister()
    {
        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('frontend.register')
            ->with([
                'title' => 'Register | '.env('APP_NAME'),
                'description' => 'Register | '.env('APP_NAME'),
                'canonical' => route('getregister')
            ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:15|alpha_dash|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'g-recaptcha-response'  => 'captcha|required'
        ]);
    }

    /**
     * Handle posting of the form for the user registration.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails())
        {
            return redirect()->route('getregister')
                ->withInput()
                ->withErrors($validator);
        }
        if ($user = Sentinel::register($request->all()))
        {
            $activation = Activation::create($user);
            $code = $activation->code;
            
            Mail::send(
                'sentinel.emails.activate', 
                compact('user', 'code'), 
                function($m) use ($user, $code) {
                    $m->to($user->email)->subject('Activate Your Account');
                    $m->from(env('ADMIN_MAIL'), env('MAIL_SENDER'));
                    $m->addPart(
                        sprintf(
                            ACTIVATION_MAIL_PLAIN,
                            $user->username,
                            env('APP_NAME'),
                            route('getactivate', array('id' => $user->getUserId(), 'code' => $code)),
                            route('getactivate', array('id' => $user->getUserId(), 'code' => $code)),
                            \Config::get('cartalyst.sentinel.activations.expires')/3600,
                            env('SUPPORT_MAIL'),
                            env('SUPPORT_MAIL'),
                            env('MAIL_SENDER')
                        ),
                        'text/plain'
                    );
                }
            );

            //Setting User Role
            $role = $this->role->findByName('User');
            $role->users()->attach($user->id);

            //Save user location and default avatar
            $user->avatar = AVATAR_DEFAULT;
            $user->country = GeoIP::getLocation(\Request::ip())['iso_code'];
            $user->save();

            return redirect()->route('getlogin')
                ->withSuccess("Your account was successfully created. Check your email to activate your account.")
                ->with('userId', $user->id);
        }
        return redirect()->route('getregister')
            ->withInput()
            ->withErrors('Failed to register.');
    }


    /**
     * Handle posting of the form for the user login.
     *
     * @param $request
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function postLogin(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), $this->loginRules);

            if ($validator->fails())
            {
                return redirect()->route('getlogin')
                    ->withInput($request->except('password'))
                    ->withErrors($validator);
            }

            // Check if already logged in
            try {
                $user = Sentinel::stateless($request->all());
                if ($user)
                {
                    if($this->sessionHandler->isUserLoggedIn($user->id)){
                        Sentinel::logout($user);
                    }
                }
            } catch (ModelNotFoundException $e) {
                //
            }

            try {
                $remember = (bool) $request->input('remember',false);
                //\Log::debug('remember '.$remember);
                if (Sentinel::authenticate($request->all(), $remember))
                {
                    session()->forget(SESSION_DOWNLOADS);                
                    return redirect()->intended('/');
                }
            } catch (ModelNotFoundException $e) {
                //
            }
            $errors = 'These credentials do not match our records.';
        }
        catch (NotActivatedException $e)
        {
            $errors = 'Your account has not been activated yet.';
            $info = [
                'info' => 'Didn\'t receive activation mail?',
                'linktext' => 'Resend',
                'link' => route('getreactivate')
                ];
            return redirect()->route('getlogin')
                ->withErrors($errors)
                ->with($info);
        }
        catch (ThrottlingException $e)
        {
            $delay = $e->getDelay();
            $errors = 'Too many login attempts. You can try again in '.\Carbon\Carbon::now()->subSeconds($delay)->diffForHumans(null, true).'.';
        }
        return redirect()->route('getlogin')
            ->withInput()
            ->withErrors($errors);
    }

    /**
     * Logout currently logged user
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function getLogout()
    {
        if(Sentinel::check())
            Sentinel::logout();
        return redirect()->route('getlogin');
    }


    /**
     * Request a valid username
     *
     * @return \Illuminate\View\View
     */
    public function getUsername()
    {
        JSInjection::addSessionModalData(true);
        JSInjection::inject();
        return View::make('frontend.usernamerequest')
            ->with([
                'title' => 'Select Username | '.env('APP_NAME'),
                'description' => 'Select a username for your profile | '.env('APP_NAME'),
            ]);
    }


    /**
     * Providing a valid username
     *
     * @param $request
     * @param $provider
     * @return mixed
     */
    public function postUsername(Request $request)
    {
        $user = \Sentinel::check();
       
        $username = $request->get('username');
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:15|alpha_dash|unique:users'
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withInput()
                ->withErrors($validator);
        }

        $user->username = $username;
        $user->save();
        session()->forget('suggested_username');
        return redirect()->to('/');
    }

    /**
     * An Ajax call made each time the window goes on focus
     * to make sure it passes the CSRF middleware
     * and validate the session (no timeout)
     */
    public function sessionCheck(Request $request)
    {
        //\Log::debug('sessionCheck '.json_encode($request->all()));
        if ($request->expectsJson()){
            if ($this->sessionHandler->exists()){
                $data = [
                    'logged' => (\Sentinel::check() instanceof User)? true : false,
                    'last_activity' => $this->sessionHandler->userLastActivity(),
                    'server_timestamp' => round(microtime(true) * 1000)
                ];
                return response()->json($data, 200);
            }
        }
    }

}
