<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Sentinel;


class AdminAccess
{

    /**
     * The Sentinel implementation.
     *
     * @var Sentinel
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Sentinel  $auth
     * @return void
     */
    public function __construct(Sentinel $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()){
            if ($this->auth->inRole('admin')) {
                return $next($request);
            } else {
                if ($request->ajax()) {
                    return response()->json('Unauthorized.', 401);
                } 
            }
        }
        return redirect()->to('/');
    }
}
