<?php

namespace App\Http\Middleware;

use Closure;

class ServerPush
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $response->header('Link', '</storage/css/frontend.min.css>; rel=preload; as=style', false);
        $response->header('Link', '</storage/js/frontend.min.js>; rel=preload; as=script', false);

        return $response;
    }
}