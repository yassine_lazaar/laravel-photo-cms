<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Sentinel;


class ValidUsername
{
    /**
     * The Sentinel implementation.
     *
     * @var Sentinel
     */
    protected $auth;
    

    /**
     * Create a new filter instance.
     *
     * @param  Sentinel  $auth
     * @return void
     */
    public function __construct(Sentinel $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()){
            $user = $this->auth->check();
            if (empty($user->username) && \Route::currentRouteName() != 'getusername' && \Route::currentRouteName() != 'postusername'){
                return redirect()->route('getusername');
            }
        }            
        return $next($request);
    }
}
