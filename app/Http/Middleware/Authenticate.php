<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Sentinel;


class Authenticate
{
    /**
     * The Sentinel implementation.
     *
     * @var Sentinel
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Sentinel  $auth
     * @return void
     */
    public function __construct(Sentinel $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Sentinel
        if (!$this->auth->check()) {
            if ($request->ajax()) {
                return response()->json('Unauthorized.', 401);
            } else {
                return redirect()->route('getlogin');
            }
        }
        return $next($request);
    }
}
