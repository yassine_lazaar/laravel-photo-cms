<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Sentinel;

class RedirectIfAuthenticated
{

  /**
  * The Sentinel implementation.
  *
  * @var Sentinel
  */
  protected $auth;

  /**
  * Create a new filter instance.
  *
  * @param  Sentinel $auth
  */
  public function __construct(Sentinel $auth)
  {
    $this->auth = $auth;
  }

  /**
  * Handle an incoming request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \Closure  $next
  * @param  string|null  $guard
  * @return mixed
  */

  public function handle($request, Closure $next, $guard = null)
  {
    // Sentinel auth check
    if ($this->auth->check()) {
       return redirect()->to('/');
    }
    return $next($request);
  }
}
