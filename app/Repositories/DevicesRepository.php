<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 30/03/2016
 * Time: 11:28
 */

namespace App\Repositories;

class DevicesRepository extends Repository
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Device';
    }
}