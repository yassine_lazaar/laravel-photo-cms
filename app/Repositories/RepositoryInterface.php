<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 29/04/2016
 * Time: 21:30
 */

namespace App\Repositories;

interface RepositoryInterface {

    public function all($columns = array('*'));

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function find($id, $columns = array('*'));

    public function findBy($field, $value, $columns = array('*'));

}