<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 30/03/2016
 * Time: 11:28
 */

namespace App\Repositories;

use Cartalyst\Sentinel\Roles\IlluminateRoleRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RolesRepository extends IlluminateRoleRepository
{
    public function all($columns = array('*'))
    {
        return $this->model->get($columns);
    }

    /**
     * {@inheritDoc}
     */
    public function findById($id)
    {
        $out = parent::findById($id);
        if(!isset($out))
            throw new ModelNotFoundException();
        return $out;
    }

    /**
     * {@inheritDoc}
     */
    public function findBySlug($slug)
    {
        $out = parent::findBySlug($slug);
        if(!isset($out))
            throw new ModelNotFoundException();
        return $out;
    }

    /**
     * {@inheritDoc}
     */
    public function findByName($name)
    {
        $out = parent::findByName($name);
        if(!isset($out))
            throw new ModelNotFoundException();
        return $out;
    }
}