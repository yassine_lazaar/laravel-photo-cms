<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 29/04/2016
 * Time: 21:24
 */

namespace App\Repositories;

use App\Exceptions\ModelCreateException;
use App\Exceptions\ModelDeleteException;
use App\Exceptions\ModelEditException;
use Exception;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class Repository implements RepositoryInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var
     */
    protected $model;

    /**
     * @param Container $container
     * @throws Exception
     */
    public function __construct(Container $container) {
        $this->container = $container;
        $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract function model();

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*')) {
        return $this->model->get($columns);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = array('*')) {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ModelCreateException
     */
    public function create(array $data) {
        $output = $this->model->create($data);
        if(!isset($output))
            throw new ModelCreateException();
        return $output;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     * @throws ModelEditException
     */
    public function update(array $data, $id) {
        $output = $this->model->where('id', '=', $id)->update($data);
        if(!isset($output))
            throw new ModelEditException();
        return $output;
    }

    /**
     * @param $id
     * @return mixed
     * @throws ModelDeleteException
     */
    public function delete($id) {
        $output = $this->model->destroy($id);
        if(!isset($output))
            throw new ModelDeleteException();
        return $output;
    }

    /**
     * @param $id
     * @param array $columns
     * @param array $eager
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function find($id, $columns = array('*'), $eager = []) {
        $output = $this->makeModel()->with($eager)->find($id, $columns);
        if(!isset($output))
            throw new ModelNotFoundException();
        return $output;
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @param array $eager
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*'), $eager = []) {
        $output = $this->makeModel()->with($eager)->where($attribute, $value)->first($columns);
        if(!isset($output))
            throw new ModelNotFoundException();
        return $output;
    }

    /**
     * @param array $filters
     * @param array $columns
     * @param mixed $orderBy
     * @param array $eager
     * @return mixed
     * @throws Exception
     */
    public function where($filters, $columns = array('*'), $orderBy = 'id', $eager = []) {
        if(is_array($orderBy)){
            $placeholders = implode(',',array_fill(0, count($orderBy), '?'));
            $output = $this->makeModel()->with($eager)->where($filters)->orderByRaw("field(id,{$placeholders})", $orderBy)->get($columns);
        }
        else
            $output = $this->makeModel()->with($eager)->where($filters)->orderBy($orderBy, 'desc')->get($columns);
        if($output->isEmpty())
            throw new ModelNotFoundException();
        return $output;
    }

    /**
     * @param array $filters
     * @param array $columns
     * @return mixed
     * @throws Exception
     */
    public function whereFirst($filters, $columns = array('*')) {
        $output = $this->makeModel()->where($filters)->first($columns);
        if(!isset($output))
            throw new ModelNotFoundException();
        return $output;
    }

    /**
     * @param array $filters
     * @param array $columns
     * @param integer $take
     * @param array $eager
     * @return mixed
     * @throws Exception
     */
    public function random($filters = null, $columns = array('*'), $take = 6, $eager = ['user', 'gallery', 'favorites']) {
        if($filters)
            $output = $this->makeModel()->with($eager)->where($filters)->inRandomOrder()->take($take)->get($columns);
        else {
            $output = $this->makeModel()->with($eager)->inRandomOrder()->take($take)->get($columns);           
        }
        if($output->isEmpty())
            throw new ModelNotFoundException();
        return $output;
    }

    /**
     * Get model count
     * 
     * @return integer
     */
    public function count() {
        return $this->makeModel()->count();
    }

    /**
     * @return Model
     * @throws \Exception
     */
    public function makeModel() {
        $model = $this->container->make($this->model());
        if(!$model instanceof Model)
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        return $this->model = $model;
    }
}