<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 04/11/2016
 * Time: 14:43
 */

namespace App\Repositories;


class TagsRepository extends Repository
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Tag';
    }


}