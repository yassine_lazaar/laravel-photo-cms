<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 30/03/2016
 * Time: 11:28
 */

namespace App\Repositories;

use app\Exceptions\ModelEditException;
use Cartalyst\Sentinel\Users\IlluminateUserRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UsersRepository extends IlluminateUserRepository
{
    /**
     * {@inheritDoc}
     */
    public function findByUsername($username)
    {
         $out = $this->createModel()
            ->where('username', $username)
            ->first();
        if(!isset($out))
            throw new ModelNotFoundException();
        return $out;
    }

        /**
     * {@inheritDoc}
     */
    public function findById($id)
    {
        $out = parent::findById($id);
        if(!isset($out))
            throw new ModelNotFoundException();
        return $out;
    }

    /**
     * {@inheritDoc}
     */
    public function findByCredentials(array $credentials)
    {
        $out = parent::findByCredentials($credentials);
        if(!isset($out))
            throw new ModelNotFoundException();
        return $out;
    }

    /**
     * @param array $filters
     * @return mixed
     */
    /**
     * @param array $filters
     * @return mixed
     */
    public function where($filters) {
        $output = $this->createModel()
            ->newQuery()
            ->where($filters)
            ->get();
        if($output->isEmpty())
            throw new ModelNotFoundException();
        return $output;
    }

    /**
     * {@inheritDoc}
     */
    public function update($user, array $credentials)
    {
        $out = parent::update($user, $credentials);
        if(!isset($out))
            throw new ModelEditException();
        return $out;
    }
}