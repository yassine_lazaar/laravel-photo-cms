<?php
/**
 * Created by PhpStorm.
 * User: Yassine
 * Date: 30/03/2016
 * Time: 12:24
 */

namespace App\Repositories;

class GalleriesRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Gallery';
    }

}

