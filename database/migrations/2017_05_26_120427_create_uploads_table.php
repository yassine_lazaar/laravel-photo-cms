<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->increments('id')->unique()->index();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('tags')->nullable();
            $table->string('filename')->unique();
            $table->smallinteger('width');
            $table->smallinteger('height');
            $table->smallinteger('license')->default(0);
            $table->string('author')->nullable();
            $table->text('author_link')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('gallery_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('uploads');
    }
}