<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAddInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($t)
        {
            $t->boolean('banned')->default(0)->after('last_name');
            $t->string('avatar')->default(AVATAR_DEFAULT)->after('banned');
            $t->integer('gender')->nullable()->after('avatar');
            $t->date('birthdate')->nullable()->after('gender');
            $t->string('address')->nullable()->after('birthdate');
            $t->string('zip_code')->nullable()->after('address');
            $t->string('city')->nullable()->after('zip_code');
            $t->string('country')->nullable()->after('city');
            $t->string('phone_number')->nullable()->after('city');
            $t->string('bio')->nullable()->after('phone_number');
            //$t->string('social_providers')->nullable()->after('phone_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($t) {
            $t->dropColumn(['avatar', 'gender', 'birthdate', 'address', 'zip_code', 'city', 'country', 'phone_number', 'bio']);
        });
    }
}
