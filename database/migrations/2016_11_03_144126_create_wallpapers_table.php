<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWallpapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallpapers', function (Blueprint $table) {
            $table->increments('id')->unique()->index();
            $table->string('title')->unique();
            $table->string('slug')->unique();
            $table->text('description')->nullable();
            $table->smallinteger('width');
            $table->smallinteger('height');
            $table->string('filename')->unique();
            $table->smallinteger('license')->default(0);
            $table->string('author')->nullable();
            $table->text('author_link')->nullable();
            $table->boolean('published')->default(0);
            $table->boolean('featured')->default(0);
            $table->integer('views')->default(0);
            $table->integer('downloads')->default(0);
            $table->integer('user_id')->unsigned();
            $table->integer('gallery_id')->unsigned();
            $table->string('thumbnail_params');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wallpapers');
    }
}