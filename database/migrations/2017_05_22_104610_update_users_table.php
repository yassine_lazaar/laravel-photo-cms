<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->string('website')->nullable()->after('country');
            $table->string('twitter',15)->nullable()->after('website');
            $table->string('facebook',50)->nullable()->after('twitter');
            $table->string('instagram',30)->nullable()->after('facebook');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('website'); 
            $table->dropColumn('twitter'); 
            $table->dropColumn('facebook'); 
            $table->dropColumn('instagram'); 
        });
    }
}