<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(GalleriesSeeder::class);
        $this->call(DevicesSeeder::class);
        $this->call(ResolutionsSeeder::class);
        $this->call(TagsSeeder::class);
        $this->call(SettingsSeeder::class);
    }
}
