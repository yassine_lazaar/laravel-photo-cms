<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('site_settings')
            ->insert(array(
                array('setting' => SETTING_SITE_NAME, 'setting_value' => 'Site Name'),
                array('setting' => SETTING_SITE_DESCRIPTION, 'setting_value' => 'Some Description'),
                array('setting' => SETTING_SITE_TOS, 'setting_value' => 'Add tos here'),
                array('setting' => SETTING_SITE_PRIVACY_POLICY, 'setting_value' => 'Add privacy policy here'),
                array('setting' => SETTING_SITE_COPYRIGHT_POLICY, 'setting_value' => 'Add copyright policy here'),
                array('setting' => SETTING_SITE_FAQ, 'setting_value' => 'Add faq here'),
                array('setting' => SETTING_SITE_ABOUT, 'setting_value' => 'Add about us here'),

                array('setting' => SETTING_WALLPAPERS_PER_PAGE, 'setting_value' => '12'),
                array('setting' => SETTING_GUEST_DOWNLOAD_LIMIT, 'setting_value' => '10'),
                array('setting' => SETTING_MAX_UPLOAD_SIZE, 'setting_value' => '4'),
                //array('setting' => SETTING_AUTO_APPROVE_UPLOADS, 'setting_value' => '0'),

                array('setting' => SETTING_SITEMAP_INCLUDE_GALLERIES, 'setting_value' => '0'),
                array('setting' => SETTING_SITEMAP_INCLUDE_TAGS, 'setting_value' => '0'),
                array('setting' => SETTING_SITEMAP_INCLUDE_IMAGES, 'setting_value' => '0'),
                array('setting' => SETTING_SITEMAP_INCLUDE_DEVICES, 'setting_value' => '0'),

                array('setting' => SETTING_ANALYTICS_CODE, 'setting_value' => 'analytics'),

                array('setting' => SETTING_SEO_HOMEPAGE_TITLE_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_HOMEPAGE_DESCRIPTION_TEMPLATE, 'setting_value' => 'template'),
                //array('setting' => SETTING_SEO_HOMEPAGE_KEYWORDS_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_GALLERY_TITLE_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_GALLERY_DESCRIPTION_TEMPLATE, 'setting_value' => 'template'),
                //array('setting' => SETTING_SEO_GALLERY_KEYWORDS_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_WALLPAPER_TITLE_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_WALLPAPER_DESCRIPTION_TEMPLATE, 'setting_value' => 'template'),
                //array('setting' => SETTING_SEO_WALLPAPER_KEYWORDS_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_TAG_TITLE_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_TAG_DESCRIPTION_TEMPLATE, 'setting_value' => 'template'),
                //array('setting' => SETTING_SEO_TAG_KEYWORDS_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_DEVICE_TITLE_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_DEVICE_DESCRIPTION_TEMPLATE, 'setting_value' => 'template'),
                //array('setting' => SETTING_SEO_DEVICE_KEYWORDS_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_SEARCH_TITLE_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_SEARCH_DESCRIPTION_TEMPLATE, 'setting_value' => 'template'),
                //array('setting' => SETTING_SEO_SEARCH_KEYWORDS_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_404_TITLE_TEMPLATE, 'setting_value' => 'template'),
                array('setting' => SETTING_SEO_404_DESCRIPTION_TEMPLATE, 'setting_value' => 'template'),
                //array('setting' => SETTING_SEO_404_KEYWORDS_TEMPLATE, 'setting_value' => 'template')
            ));
    }
}
