<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DevicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('devices')
            ->insert([
                array(
                    'name' => 'iPhone 6',
                    'slug' => 'iphone-6',
					'filename' => 'iphone-6',
                    'maker' => 'Apple',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2014-09-14'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPhone 6 Plus',
                    'slug' => 'iphone-6-plus',
					'filename' => 'iphone-6-plus',
                    'maker' => 'Apple',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2014-09-14'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPhone 6S',
                    'slug' => 'iphone-6s',
					'filename' => 'iphone-6s',
                    'maker' => 'Apple',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2015-09-25'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPhone 6S Plus',
                    'slug' => 'iphone-6s-plus',
					'filename' => 'iphone-6s-plus',
                    'maker' => 'Apple',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2015-09-25'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPhone 7',
                    'slug' => 'iphone-7',
					'filename' => 'iphone-7',
                    'maker' => 'Apple',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2016-09-16'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPhone 7 Plus',
                    'slug' => 'iphone-7-plus',
					'filename' => 'iphone-7-plus',
                    'maker' => 'Apple',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2016-09-16'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPhone 8',
                    'slug' => 'iphone-8',
					'filename' => 'iphone-8',
                    'maker' => 'Apple',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2017-09-12'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPhone 8 Plus',
                    'slug' => 'iphone-8-plus',
					'filename' => 'iphone-8-plus',
                    'maker' => 'Apple',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2017-09-12'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPhone X',
                    'slug' => 'iphone-x',
					'filename' => 'iphone-x',
                    'maker' => 'Apple',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2017-09-12'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad 3',
                    'slug' => 'ipad-3',
					'filename' => 'ipad-3',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2012-03-16'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad 4',
                    'slug' => 'ipad-4',
					'filename' => 'ipad-4',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2012-11-02'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad Air',
                    'slug' => 'ipad-air',
					'filename' => 'ipad-air',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2013-11-01'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad Air 2',
                    'slug' => 'ipad-ai-2',
					'filename' => 'ipad-ai-2',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2014-10-22'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad Mini',
                    'slug' => 'ipad-mini',
					'filename' => 'ipad-mini',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2012-11-02'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad Mini 2',
                    'slug' => 'ipad-mini-2',
					'filename' => 'ipad-mini-2',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2013-11-12'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad Mini 3',
                    'slug' => 'ipad-mini-3',
					'filename' => 'ipad-mini-3',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2014-10-22'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad Mini 4',
                    'slug' => 'ipad-mini-4',
					'filename' => 'ipad-mini-4',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2015-09-09'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad Pro 9.7″',
                    'slug' => 'ipad-pro-97',
					'filename' => 'ipad-pro-97',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2015-03-31'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad Pro 10.5″',
                    'slug' => 'ipad-pro-105',
					'filename' => 'ipad-pro-105',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2017-06-13'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'iPad Pro 12.9″',
                    'slug' => 'ipad-pro-129',
					'filename' => 'ipad-pro-129',
                    'maker' => 'Apple',
                    'type' => 1,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2017-06-13'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Galaxy S5',
                    'slug' => 'galaxy-s5',
					'filename' => 'galaxy-s5',
                    'maker' => 'Samsung',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2014-02-24'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Galaxy S6',
                    'slug' => 'galaxy-s6',
					'filename' => 'galaxy-s6',
                    'maker' => 'Samsung',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2015-03-01'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Galaxy S6 Edge',
                    'slug' => 'galaxy-s6-edge',
					'filename' => 'galaxy-s6-edge',
                    'maker' => 'Samsung',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2015-03-01'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Galaxy S7',
                    'slug' => 'galaxy-s7',
					'filename' => 'galaxy-s7',
                    'maker' => 'Samsung',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2016-02-21'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Galaxy S7 Edge',
                    'slug' => 'galaxy-s7-edge',
					'filename' => 'galaxy-s7-edge',
                    'maker' => 'Samsung',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2016-02-21'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Galaxy S8',
                    'slug' => 'galaxy-s8',
					'filename' => 'galaxy-s8',
                    'maker' => 'Samsung',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2017-03-29'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Galaxy S8+',
                    'slug' => 'galaxy-s8-plus',
					'filename' => 'galaxy-s8-plus',
                    'maker' => 'Samsung',
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2017-03-29'),
                    'excluded'=> false,
                    'type' => 0,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Galaxy Note 5',
                    'slug' => 'galaxy-note-5',
					'filename' => 'galaxy-note-5',
                    'maker' => 'Samsung',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2015-08-21'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Galaxy Note 8',
                    'slug' => 'galaxy-note-8',
					'filename' => 'galaxy-note-8',
                    'maker' => 'Samsung',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2017-08-23'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Pixel',
                    'slug' => 'pixel',
					'filename' => 'pixel',
                    'maker' => 'Google',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2016-10-20'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'name' => 'Pixel XL',
                    'slug' => 'pixel-xl',
					'filename' => 'pixel-xl',
                    'maker' => 'Google',
                    'type' => 0,
                    'release_date' => Carbon::createFromFormat('Y-m-d', '2016-10-20'),
                    'excluded'=> false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                )
            ]);

    }
}
