<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GalleriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('galleries')
            ->insert(
                array(
                    'name' => 'Uncategorized',
                    'slug' => 'uncategorized',
                    'folder' => 'uncategorized',
                    'published' => false,
                    'protected'=> true,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                )
            );

    }
}
