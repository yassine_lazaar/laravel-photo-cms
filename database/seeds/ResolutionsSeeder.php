<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResolutionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resolutions')
            ->insert([
                array(
                    'width' => 750,
                    'height' => 1134,
                    'aspect_ratio' => '16:9',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 852,
                    'height' => 1608,
                    'aspect_ratio' => '16:9',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 1080,
                    'height' => 1920,
                    'aspect_ratio' => '16:9',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 2662,
                    'height' => 2662,
                    'aspect_ratio' => '1:1',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 1125,
                    'height' => 2436,
                    'aspect_ratio' => '2.17:1',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 1536,
                    'height' => 2048,
                    'aspect_ratio' => '4:3',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 1262,
                    'height' => 1262,
                    'aspect_ratio' => '1:1',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 2542,
                    'height' => 2542,
                    'aspect_ratio' => '1:1',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 1668,
                    'height' => 2224,
                    'aspect_ratio' => '4:3',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 2048,
                    'height' => 2732,
                    'aspect_ratio' => '4:3',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 1440,
                    'height' => 2560,
                    'aspect_ratio' => '16:9',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 1440,
                    'height' => 2880,
                    'aspect_ratio' => '18:9',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 1440,
                    'height' => 2960,
                    'aspect_ratio' => '18.5:9',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 1080,
                    'height' => 2220,
                    'aspect_ratio' => '18.5:9',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ),
                array(
                    'width' => 3840,
                    'height' => 2160,
                    'aspect_ratio' => '16:9',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                )
            ]);
            
    }
}
