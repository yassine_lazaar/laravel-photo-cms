<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    $person = new Faker\Provider\en_US\Person($faker);
    $address = new Faker\Provider\en_US\Address($faker);
    $misc = new Faker\Provider\Miscellaneous($faker);
    $phoneNumber = new Faker\Provider\en_US\PhoneNumber($faker);

    return [
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'last_name' => $person->lastName(),
        'first_name' => $person->firstName(),
        'banned' => 0,
        'avatar' => AVATAR_DEFAULT,
        'birthdate' => $faker->dateTimeThisCentury,
        'address' => $address->address(),
        'zip_code' => $address->postcode(),
        'city' => $address->city(),
        'country' => $misc->countryCode()
        //'phone_number' => $phoneNumber->e164PhoneNumber()
    ];
});
