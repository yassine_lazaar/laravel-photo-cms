@extends('sentinel.emails.index')

@section('content')
    <tr>
        <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-bottom: 15px; border-bottom: 3px solid #eeeeee;">
            <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                Message from {{ $sender.' ('.$senderMail.')' }}
            </p>
            <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                {{ 'Subject: '.$mailSubject }}
            </p>
            <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                {{ $mailMessage }}
            </p>
        </td>
    </tr>
@endsection


