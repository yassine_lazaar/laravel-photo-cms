@extends('sentinel.emails.index')

@section('content')
    <tr>
        <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-bottom: 15px; border-bottom: 3px solid #eeeeee;">
            <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                Hello {{ $user->username }},
            </p>
            <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                Welcome to {{ env('APP_DOMAIN') }} and thanks for signing up! You're one step closer to accessing the finest wallpapers on the web.
            </p>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;">
            <h2 style="font-size: 24px; font-weight: 800; line-height: 24px; color: #333333;">NEXT STEP</h2>
            <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                Please confirm your
                <br>email address to get started.
            </p>
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 0 25px 0;">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" style="border-radius: 5px;" bgcolor="#6FC171">
                        <a href="{{ route('getactivate', array('id' => $user->getUserId(), 'code' => $code)) }}" target="_blank" style="font-size: 18px; font-family: Open Sans, Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; border-radius: 5px; background-color: #6FC171; padding: 15px 30px; border: 1px solid #6FC171; display: block;">Confirm account</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;">
            <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                The link is will stay valid for the next {{ Config::get('cartalyst.sentinel.activations.expires')/3600 }} hours.</br>
                <br>Please let us know if you have any questions by writing to <a href="mailto:{{ env('SUPPORT_MAIL')}}" target="_top">{{ env('SUPPORT_MAIL')}}</a>.
            </p>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px;">
            <p style="font-size: 14px; font-weight: 400; line-height: 20px; color: #777777;">
                If you didn't create an account using this email address, please ignore this email.
            </p>
        </td>
    </tr>
@endsection


