<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        img {
            -ms-interpolation-mode: bicubic;
        }
        /* RESET STYLES */
        
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }
        table {
            border-collapse: collapse !important;
        }
        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }
        /* iOS BLUE LINKS */
        
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
        /* MEDIA QUERIES */
        
        @media screen and (max-width: 480px) {
            .mobile-hide {
                display: none !important;
            }
            .mobile-center {
                text-align: center !important;
            }
        }
        /* ANDROID CENTER FIX */
        
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
    
    <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                    <!--[if (gte mso 9)|(IE)]>
               <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                  <tr>
                     <td align="center" valign="top" width="600">
                        <![endif]-->
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                        <tr>
                            <td align="center" valign="top" style="font-size:0; padding: 35px;" bgcolor="#350B3A">
                                <!--[if (gte mso 9)|(IE)]>
                                 <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                                    <tr>
                                       <td align="left" valign="top" width="300">
                                          <![endif]-->
                                <div style="display:inline-block; min-width:78px; vertical-align:top; width:100%;">
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:300px;">
                                        <tr>
                                            <td align="left" valign="top" class="mobile-center">
                                               <img src="https://static3.wllppr.co/image/logo_white_2x.png" width="78" height="22" style="display: block; border: 0px;" alt="Wllppr">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                <!--[if (gte mso 9)|(IE)]>
                                 <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                                    <tr>
                                       <td align="center" valign="top" width="600">
                                          <![endif]-->
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                    @yield('content')
                                </table>
                                <!--[if (gte mso 9)|(IE)]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style=" padding: 35px; background-color: #ED185D; border-bottom: 20px solid #c0134b;" bgcolor="#ED185D">
                                <!--[if (gte mso 9)|(IE)]>
                                 <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                                    <tr>
                                       <td align="center" valign="top" width="600">
                                          <![endif]-->
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                    <tr>
                                        <td align="center">
                                            <table>
                                                <tr>
                                                    <td style="padding: 0 10px;">
                                                        <a href="https://www.facebook.com/wllppr.co" target="_blank"><img src="https://static3.wllppr.co/image/icon-facebook.png" width="32" height="32" style="display: block; border: 0px;" alt="facebook"/>
                                                        </a>
                                                    </td>
                                                    <td style="padding: 0 10px;">
                                                        <a href="https://twitter.com/wllpprco" target="_blank"><img src="https://static3.wllppr.co/image/icon-twitter.png" width="32" height="32" style="display: block; border: 0px;" alt="twitter"/>
                                                        </a>
                                                    </td>
                                                    <td style="padding: 0 10px;">
                                                        <a href="https://www.instagram.com/wllppr.co" target="_blank"><img src="https://static3.wllppr.co/image/icon-instagram.png" width="32" height="32" style="display: block; border: 0px;" alt="instagram"/>
                                                        </a>
                                                    </td>
                                                    <td style="padding: 0 10px;">
                                                        <a href="https://www.pinterest.com/wllpprco" target="_blank"><img src="https://static3.wllppr.co/image/icon-pinterest.png" width="32" height="32" style="display: block; border: 0px;" alt="pinterest"/>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <!--[if (gte mso 9)|(IE)]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                     </td>
                  </tr>
               </table>
               <![endif]-->
                </td>
            </tr>
        </table>
    </body>
</html>