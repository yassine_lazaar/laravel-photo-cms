<!-- START MODAL -->
<div class="modal fade slide-up disable-scroll" id="session-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content text-center">
                <div class="modal-header clearfix ">
                    <h5>Session timeout</h5>
                </div>
                <div class="modal-body">
                    <span>
                        Your session has expired. Please refresh the page.
                    </span>
                    <div class="m-t-20">
                        <button type="button" class="btn btn-primary btn-cons btn-lg session-modal-btn">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
