<div class="container-fluid container-fixed-lg full-width sm-p-l-20 sm-p-r-20 hidden-xs">
    <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb pull-left p-l-0" itemscope itemtype="http://schema.org/BreadcrumbList">
            @for ($i = 0; $i < count($breadcrumb); $i++)
            <li  itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                @if ($i == count($breadcrumb) - 1)
                    @if(isset($breadcrumb[$i]['u']))
                    <a href="{{ $breadcrumb[$i]['u'] }}" class="active" itemprop="item"><span itemprop="name">{{ $breadcrumb[$i]['a'] }}</span></a></a>
                    @else
                    <span>{{ $breadcrumb[$i]['a'] }}</span>
                    @endif
                @else
                    @if(isset($breadcrumb[$i]['u']))
                    <a href="{{ $breadcrumb[$i]['u'] }}" itemprop="item"><span itemprop="name">{{ $breadcrumb[$i]['a'] }}</span></a>
                    @else
                        <span>{{ $breadcrumb[$i]['a'] }}</span>
                    @endif
                @endif
                </li>
            @endfor
        </ul>
        @if (session()->has(SESSION_DEVICE_NAME_KEY) && isset($showDeviceFilter))
        @if (strcmp(session(SESSION_DEVICE_MAKER_KEY), 'N/A') != 0)
        <span class="label inline pull-right light bg-white text-uppercase fs-10 font-montserrat m-l-5 m-t-15 padding-10"><i class="mdi mdi-filter-variant m-r-5"></i>Filtered for {{ session(SESSION_DEVICE_MAKER_KEY).' '.session(SESSION_DEVICE_NAME_KEY) }}</span>
        @else
        <span class="label inline pull-right light bg-white text-uppercase fs-10 font-montserrat m-l-5 m-t-15 padding-10"><i class="mdi mdi-filter-variant m-r-5"></i>Filtered for {{ session(SESSION_DEVICE_NAME_KEY) }}</span>
        @endif
        @endif
        <!-- END BREADCRUMB -->
    </div>
</div>