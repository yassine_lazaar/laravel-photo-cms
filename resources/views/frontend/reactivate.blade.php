@extends('frontend.master.extra')

@section('criticalCSS')

    @include('css::criticalfrontendauth')

@endsection

@section('content')
<div class="activation-container full-height sm-p-t-30">
    <div class="container-sm-height full-height">
        <div class="row row-sm-height">
            <div class="col-sm-12 col-sm-height col-middle">
                <a href="{{ route('gethome') }}"><img src="//static3.{{ env('APP_DOMAIN') }}/image/logo.png" alt="Wllppr" data-src="//static3.{{ env('APP_DOMAIN') }}/image/logo.png" data-src-retina="//static3.{{ env('APP_DOMAIN') }}/image/logo_2x.png" width="78" height="22"></a>
                <h3>Send activation link</h3>
                <p>
                    <small>
                        Please enter your registered e-mail address here so that we can resend you the activation link.
                    </small>
                </p>
                @include('frontend.partials.formalerts')
                {!! Form::open(['route'=>'postreactivate', 'method' => 'post', 'role' => 'form', 'class' => 'p-t-15', 'id' => 'form-register']) !!}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-default">
                            <label>Email</label>
                            <input type="email" name="email" placeholder="Registered email address" class="form-control" value="{{ old('email') }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        {!! app('captcha')->display() !!}
                    </div>
                </div>
                {!! Form::submit('Send activation code', array('class' => 'btn btn-block btn-primary btn-cons btn-lg m-t-10'))!!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div class="full-width">
        <div class="register-container padding-15">
            <div>
                <p class="hinted-text small">No part of this website or any of its contents may be reproduced, copied, modified or adapted, without the prior written consent of the author, unless otherwise indicated for stand-alone materials.</p>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('common.sessionmodal')
@endsection