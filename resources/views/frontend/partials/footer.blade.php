        <!-- START FOOTER -->
        <div class="footer no-padding bg-primary">
            <div class="container-fluid">
                <div class="col-md-5 col-sm-12 hidden-xs">
                    <div class="panel panel-transparent text-white">
                        <div class="panel-heading">
                            <div class="panel-title">About US</div>
                        </div>
                        <div class="panel-body">
                            <p class="small">{!! \Settings::getSetting(SETTING_SITE_ABOUT) !!}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 p-r-0">
                        <div class="panel panel-transparent text-white">
                        <div class="panel-heading">
                            <div class="panel-title">More</div>
                        </div>
                        <div class="panel-body p-r-0">
                            <ul class="">
                                <li class="small"><a href="{{ route('getimageupload') }}"><i class="mdi mdi-cloud-upload m-r-10"></i>Submit A Photo</a></li>
                                <li class="small"><a href="{{ route('getfaq') }}"><i class="mdi mdi-comment-question-outline m-r-10"></i>Frequently Asked Qusetions</a></li>
                                <li class="small"><a href="{{ route('getcontact') }}"><i class="mdi mdi-mailbox m-r-10"></i>Contact Us</a></li>
                            </ul>    
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="panel panel-transparent text-white">
                        <div class="panel-heading">
                            <div class="panel-title">Trending Tags</div>
                        </div>
                        <div class="panel-body">
                            @foreach($trendingTags as $tt)
                                <a href="{{ route('gettag', $tt->slug) }}" class="btn btn-primary b-white bg-transparent btn-xs m-b-5">{{  $tt->name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer no-padding bg-primary-dark">
            <div class="container-fluid">
                <div class="col-xs-12">
                    <ul class="text-center text-white pull-left padding-15">
                        <li class="inline fs-16"><a href="https://www.facebook.com/wllppr.co" rel="me nofollow"><i class="mdi mdi-facebook m-r-10"></i></a></li>
                        <li class="inline fs-16"><a href="https://twitter.com/wllpprco" rel="me nofollow"><i class="mdi mdi-twitter m-r-10"></i></a></li>
                        <li class="inline fs-16"><a href="https://www.instagram.com/wllppr.co" rel="me nofollow"><i class="mdi mdi-instagram m-r-10"></i></a></li>
                        <li class="inline fs-16"><a href="https://www.pinterest.com/wllpprc" rel="me nofollow"><i class="mdi mdi-pinterest m-r-10"></i></a></li>
                    </ul>
                    <ul class="text-center pull-right padding-15">
                        <li class="inline fs-11 text-white bold m-r-10"><a href="{{ route('gethome') }}">&#169; {{ env('APP_NAME').' '.\Carbon\Carbon::now()->year }}</a></li>
                        <li class="inline fs-11 text-white m-l-10 m-r-10"><a href="{{ route('getterms') }}">Terms &#x26; Conditions</a></li>
                        <li class="inline fs-11 text-white m-l-10 m-r-10"><a href="{{ route('getcopyright') }}">Copyright Policy</a></li>
                        <li class="inline fs-11 text-white m-l-10 m-r-10"><a href="{{ route('getprivacy') }}">Privacy Policy</a></li>
                        <li class="inline fs-11 text-white m-l-10 m-r-10"><a id="scrollToTop" href="javascript:;">Back To Top</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @if(!isset($_COOKIE['cookie-consent']))
        @endif
        <!-- END FOOTER -->
