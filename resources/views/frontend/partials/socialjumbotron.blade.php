<!-- START JUMBOTRON -->
<div class="jumbotron relative" data-pages="parallax" data-social="cover">
    @include('common.breadcrumb')
    @if(isset($cover))
    @if($isProfileOwner)
    <div class="cover-photo" style="background:url({{ '"'.$cover.'?'.$timestamp.'"' }}) center / cover no-repeat"></div>
    @else
    <div class="cover-photo" style="background:url({{ '"'.$cover.'"' }}) center / cover no-repeat"></div>
    @endif
    @else
    <div class="cover-photo"></div>
    @endif
    @if($isProfileOwner)
    <div class="full-width full-height" style="position:absolute; top:0">
        {!! Form::open(['route'=>'postusercoverupload', 'id' => 'cover-upload-form', 'method' => 'post', 'class' => 'full-height', 'files' => true]) !!}
            <div class="row relative full-height">
                <div class="hide" id="image-cropper">
                    <div class="cropit-preview"></div>
                </div>
                <div class="pull-up full-width full-height bg-semitransparent hide" id="cover-loader" style="z-index: 100;">
                    <div class="progress">
                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                    </div>
                </div>
                <input type="file" name="cover_file" class="cropit-image-input hide" id="cover-upload-input" accept="image/jpg,image/jpeg" readonly>
                <div class="pull-bottom bottom-right m-b-40 m-r-40 hidden-xs hidden-sm">
                    <a href="javascript:;" class="btn btn-xs no-border" id="cover-upload-btn" style="z-index:2">
                        <i class="mdi mdi-cloud-upload m-r-5"></i>
                        <span>Update cover</span>
                    </a>
                    @if(isset($cover))
                    <a href="javascript:;" class="btn btn-xs no-border" id="cover-delete-btn" style="z-index:2">
                    @else
                    <a href="javascript:;" class="btn btn-xs no-border hide" id="cover-delete-btn" style="z-index:2">
                    @endif
                        <i class="mdi mdi-delete"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs no-border hide" id="cover-upload-submit-btn" style="z-index:2">
                        <i class="mdi mdi-content-save m-r-5"></i>
                        <span>Save</span>
                    </a>
                    <a href="javascript:;" class="btn btn-xs no-border hide" id="cover-upload-clear-btn" style="z-index:2">
                        <i class="mdi mdi-close m-r-5"></i>
                        <span>Clear</span>
                    </a>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
    @endif
    <div class="cover-gradient"></div> 
    <div class="container-fluid sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <div class="pull-bottom bottom-left m-b-40 sm-p-l-15">
                <h5 class="text-white no-margin">{{ $user->username }}'s</h5>
                <h1 class="text-white no-margin"><span class="semi-bold">{{ $socialTitle }}</span></h1>
            </div>
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->