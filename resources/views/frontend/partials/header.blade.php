    <!-- START HEADER -->
    <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
            <!-- LEFT SIDE -->
            <div class="pull-left full-height visible-sm visible-xs">
                <!-- START ACTION BAR -->
                <div class="header-inner">
                @if (Sentinel::check() && $notifications->count() > 0)
                    <a href="javascript:;" class="cbutton cbutton--effect-radomir btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-15" data-toggle="sidebar">
                        <span class="cbutton__icon mdi mdi-menu fs-16"></span>
                    </a>
                @else
                    <a href="javascript:;" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-15" data-toggle="sidebar">
                        <span class="mdi mdi-menu fs-16"></span>
                    </a>
                @endif
                </div>
                <!-- END ACTION BAR -->
            </div>
            <div class="pull-center hidden-md hidden-lg">
                <div class="header-inner">
                    <div class="brand inline">
                        <a href="{{ route('gethome') }}"><img src="//static3.{{ env('APP_DOMAIN') }}/image/logo_center.png" alt="Wllppr" data-src="//static3.{{ env('APP_DOMAIN') }}/image/logo_center_2x.png" width="78" height="22"></a>
                    </div>
                </div>
            </div>
            <!-- RIGHT SIDE -->
            <div class="pull-right full-height visible-sm visible-xs">
                <!-- START ACTION BAR -->
                <div class="header-inner">
                    <a href="javascript:;" class="btn-link visible-sm-inline-block visible-xs-inline-block padding-15"  data-toggle="search">
                        <span class="mdi mdi-magnify fs-16"></span>
                    </a>
                </div>
                <!-- END ACTION BAR -->
            </div>
        </div>
        <!-- END MOBILE CONTROLS -->
        <!-- START DESKTOP CONTROLS -->
        <div class="pull-left sm-table hidden-xs hidden-sm">
            <div class="header-inner">
                <div class="brand">
                    <a href="{{ route('gethome') }}"><img src="//static3.{{ env('APP_DOMAIN') }}/image/logo.png" alt="Wllppr" data-src="//static3.{{ env('APP_DOMAIN') }}/image/logo.png" data-src-retina="//static3.{{ env('APP_DOMAIN') }}/image/logo_2x.png" width="78" height="22"></a>
                </div>
            </div>
        </div>
        <div class="flex hidden-xs hidden-sm">
            <div class="sm-table display-table full-width">
                <div class="header-inner">
                    <div class="no-margin no-style pull-left">
                        <ul class="notification-list">
                            <li class="inline p-r-10 b-grey b-r" data-toggle="search">
                                <a href="javascript:;" class="padding-15">
                                    <i class="p-r-5 mdi mdi-magnify"></i>
                                    <span class="">Type to search</span>
                                </a>
                            </li>
                            @if (Sentinel::check())
                            @if ($notifications->count() == 0)
                            <li class="inline p-l-0">
                                <a href="{{ route('getusernotifications') }}" class="padding-15">
                                    <i class="inline p-r-5 mdi mdi-bell"></i>
                                    <span class="">
                                        Notifications
                                    </span>
                                </a>
                            </li>
                            @else
                            <li class="inline p-l-0">
                                <div class="dropdown">
                                    @if ($notifications->count() > 0)
                                    <a href="javascript:;" class="padding-15 " id="notification-center" data-toggle="dropdown">
                                    @else
                                    <a href="javascript:;" class="padding-15" id="notification-center" data-toggle="dropdown">
                                    @endif
                                        <i class="inline p-r-5 mdi mdi-bell"></i>
                                        <span class="">
                                            @if ($notifications->count() > 0)
                                            Notifications ({{ $notifications->count() }})
                                            @else
                                            Notifications
                                            @endif
                                        </span>
                                    </a>
                                    <!-- START Notification Dropdown -->
                                    <div class="dropdown-menu notification-toggle" role="menu" aria-labelledby="notification-center">
                                        <!-- START Notification -->
                                        <div class="notification-panel">
                                            <!-- START Notification Body-->
                                            <div class="notification-body scrollable">
                                                @foreach ($notifications as $notification)
                                                <!-- START Notification Item-->
                                                @if($notification->read == 0)
                                                <div class="notification-item unread clearfix">
                                                @else
                                                <div class="notification-item clearfix">
                                                @endif
                                                    @if($notification->type == NOTIFICATION_TYPE_MODERATION_PUBLISH)
                                                    <div class="heading p-r-30">
                                                        <a href="{{ route('getwallpaper', $notification->link.'.htm') }}" class="text-complete pull-left">
                                                            <i class="mdi mdi-check-circle fs-16 m-r-10"></i>
                                                            <span class="bold">Upload approved</span>
                                                        </a>
                                                        <div class="pull-right">
                                                            <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details" data-id={{ $notification->id }}>
                                                                <div><i class="mdi mdi-chevron-left inline"></i>
                                                                </div>
                                                            </div>
                                                            <span class=" time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->timestamp->date))->diffForHumans() }}</span>
                                                        </div>
                                                        <div class="more-details">
                                                            <div class="more-details-inner inline">
                                                                <div class="inline thumbnail-wrapper img-rounded"><a href="{{ route('getwallpaper', $notification->link.'.htm') }}"><img src="{{ $notification->thumb }}_1x.jpg"></a></div>
                                                                <div class="inline m-l-10 notification-item-details">
                                                                    <div class="semi-bold fs-16 wallpaper-title">{{ $notification->title }} is now published.</div>
                                                                    <div class="small hint-text fs-11">
                                                                        <a class="text-black" href="{{ route('getgallery', $notification->gallery_slug) }}">Under {{ $notification->gallery }}</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="option">
                                                    </div>
                                                    @elseif($notification->type == NOTIFICATION_TYPE_MODERATION_DISCARD)
                                                    <div class="heading p-r-30">
                                                        <span class="text-danger pull-left">
                                                            <i class="mdi mdi-minus-circle fs-16 m-r-10"></i>
                                                            <span class="bold">Upload declined</span>
                                                        </span>
                                                        <div class="pull-right">
                                                            <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details" data-id={{ $notification->id }}>
                                                                <div><i class="mdi mdi-chevron-left inline"></i>
                                                                </div>
                                                            </div>
                                                            <span class=" time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->timestamp->date))->diffForHumans() }}</span>
                                                        </div>
                                                        <div class="more-details">
                                                            <div class="more-details-inner inline">
                                                                <div class="inline m-l-10 notification-item-details">
                                                                    <div class="semi-bold fs-16 wallpaper-title">'{{ $notification->title }}' was not approved.</div>
                                                                    @if($notification->discard_reason > 0)
                                                                    <div class="small hint-text fs-11">
                                                                        {{ unserialize(UPLOAD_DISCARD_REASONS)[$notification->discard_reason - 1]->msg }}
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="option">
                                                    </div>
                                                    @elseif($notification->type == NOTIFICATION_TYPE_FAVORITE)
                                                    <div class="heading p-r-30">
                                                        <a href="{{ route('getwallpaper', $notification->link.'.htm') }}" class="pull-left">
                                                            <i class="mdi mdi-heart fs-16 m-r-10 text-primary"></i>
                                                            <span class="bold">New favorite!</span>
                                                        </a>
                                                        <div class="pull-right">
                                                            <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details" data-id={{ $notification->id }}>
                                                                <div><i class="mdi mdi-chevron-left inline"></i>
                                                                </div>
                                                            </div>
                                                            <span class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->timestamp->date))->diffForHumans() }}</span>
                                                        </div>
                                                        <div class="more-details">
                                                            <div class="more-details-inner inline">
                                                                <div class="inline thumbnail-wrapper img-rounded"><a href="{{ route('getwallpaper', $notification->link.'.htm') }}"><img src="{{ $notification->thumb }}_1x.jpg"></a></div>
                                                                <div class="inline m-l-10 notification-item-details">
                                                                    <div class="semi-bold fs-16 wallpaper-title">{{ $notification->username }} added '{{$notification->title}}' to his favorites.</div>
                                                                    <div class="small hint-text fs-11">
                                                                        <a class="text-black" href="{{ route('getuser', $notification->username) }}">View {{ $notification->username }}'s profile</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="option">
                                                    </div>
                                                    @endif
                                                </div>
                                                <!-- END Notification Item-->
                                                @endforeach
                                            </div>
                                            <!-- END Notification Body-->
                                            <!-- START Notification Footer-->
                                            <div class="notification-footer text-center">
                                                <a href="/user/notifications" class="">Read all notifications</a>
                                            </div>
                                            <!-- END Notification Footer-->
                                        </div>
                                        <!-- END Notification -->
                                    </div>
                                    <!-- END Notification Dropdown -->
                                </div>
                            </li>
                            @endif
                            @endif
                        </ul>
                    </div>
                    <div class="no-margin no-style pull-right">
                        <ul class="">
                            @if (\Sentinel::check())
                            <!-- START User Info-->
                            <li class="inline">
                                <span class="m-r-15">{{ $user->username }}</span>
                            </li>
                            <!-- END User Info-->
                            @else
                                <li class="inline b-grey b-r">
                                    <a href="{{ route('getlogin') }}" class="padding-15">
                                        <i class="p-r-5 mdi mdi-login-variant"></i>
                                        <span class="">Login</span>
                                    </a>
                                </li>
                                <li class="inline dropdown">
                                    <a href="{{ route('getregister') }}" class="padding-15">
                                        <i class="p-r-5 mdi mdi-account-plus"></i>
                                        <span class="">Register</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
                @if (\Sentinel::check())
                    <div class="header-inner dropdown pull-right">
                        <button class="profile-dropdown-toggle full-height" type="button" data-toggle="dropdown">
                            <span class="thumbnail-wrapper d32 circular bordered b-grey">
                                <img src="{{ $user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src="{{ $user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src-retina="{{ $user->avatarPath() }}_2x.jpg?{{ $timestamp }}" width="32" height="32">
                            </span>
                        </button>
                        <ul class="dropdown-menu profile-dropdown" role="menu">
                            <li><a href="{{ route('getuser', ['username' => $user->username]) }}"><i class="p-r-5 mdi mdi-account-circle fs-16"></i> My Profile</a>
                            </li>
                            <li><a href="{{ route('getuseruploads', ['username' => $user->username]) }}"><i class="p-r-5 mdi mdi-image-multiple fs-16"></i> My Uploads</a>
                            </li>
                            <li><a href="{{ route('getuserfavorites', ['username' => $user->username]) }}"><i class="p-r-5 mdi mdi-heart fs-16"></i> My Favorites</a>
                            </li>
                            <li><a href="{{ route('getusersettings') }}"><i class="p-r-5 mdi mdi-settings fs-16"></i> Settings</a>
                            </li>
                            <li class="bg-master-lightest"><a href="{{ route('getlogout') }}"><i class="p-r-5 mdi mdi-logout-variant fs-16"></i> Logout</a>
                            </li>
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <!-- END DESKTOP CONTROLS -->
    </div>
    <!-- END HEADER -->
