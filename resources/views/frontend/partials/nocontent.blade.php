<div class="m-t-40">
    <h3 class="text-center">Sorry!</h3>
    <div class="text-center full-width m-t-20">
        <img class="" src="//static3.{{ env('APP_DOMAIN') }}/image/no-content.png" width="150" height="150">					
        <h4>There is no content here...Yet.</h4>
    </div>
</div>