<div class="pgn-wrapper" data-position="top">
    <div class="pgn push-on-sidebar-open pgn-bar">
@if(Session::has('success'))
        <div class="alert hide" type="success">
            <button type="button" class="close" data-dismiss="alert"></button>
            <span>{{ Session::get('success') }}</span>
        </div>
@endif
@if(Session::has('errors'))
        <div class="alert hide" type="danger">
            <button type="button" class="close" data-dismiss="alert"></button>
            <span>{{ Session::get('errors')->first() }}</span>
        </div>
@endif
@if(Session::has('warning'))
        <div class="alert hide" type="warning">
            <button type="button" class="close" data-dismiss="alert"></button>
            <span>{{ Session::get('warning')->first() }}</span>
        </div>
@endif
@if(Session::has('default'))
        <div class="alert hide" type="default">
            <button type="button" class="close" data-dismiss="alert"></button>
            <span>{{ Session::get('default') }}</span>
        </div>
@endif
@if(Session::has('info'))
        <div class="alert hide" type="info">
            <button type="button" class="close" data-dismiss="alert"></button>
            <p class="pull-left">{{ Session::get('info') }}</p>
            @if(Session::has('link'))
                <p class="pull-right bold no-margin">
                    <a class="alert-link"href="{{ Session::get('link') }}">{{ Session::get('linktext') }}</a>
                </p>
            @endif
            <div class="clearfix"></div>
        </div>
@endif
    </div>
</div>