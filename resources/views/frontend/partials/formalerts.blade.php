    @if(Session::has('success'))
        <div class="alert m-b-5 hide" type="success">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('success') }}
        </div>
    @endif
    @if(Session::has('errors'))
        <div class="alert m-b-5 hide" type="error">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('errors')->first() }}
        </div>
    @endif
    @if(Session::has('warning'))
        <div class="alert m-b-5 hide" type="warning">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('warning') }}
        </div>
    @endif
    @if(Session::has('default'))
        <div class="alert m-b-5 hide" type="default">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('default') }}
        </div>
    @endif
    @if(Session::has('info'))
        <div class="alert m-b-5 hide" type="info">
            <p class="pull-left">{{ Session::get('info') }}</p>
            <button class="close" data-dismiss="alert"></button>
            @if(Session::has('link'))
            <p class="pull-right bold no-margin">
                <a class="alert-link" href="{{ Session::get('link') }}">{{ Session::get('linktext') }}</a>
            </p>
            @endif
            <div class="clearfix"></div>
        </div>
    @endif