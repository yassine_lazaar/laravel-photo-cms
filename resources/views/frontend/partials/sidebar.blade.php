<!-- BEGIN SIDEBPANEL-->
<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR HEADER -->
    <div class="sidebar-header">
        <a href="{{ route('gethome') }}"><img src="//static3.{{ env('APP_DOMAIN') }}/image/logo_white.png" alt="Wllppr" class="brand" data-src="//static3.{{ env('APP_DOMAIN') }}/image/logo_white.png" data-src-retina="//static3.{{ env('APP_DOMAIN') }}/image/logo_white_2x.png" width="78" height="22"></a>
    </div>
    <!-- END SIDEBAR HEADER -->
    <!-- BEGIN SIDEBAR MENU -->
    <div class="sidebar-menu">
        <ul class="menu-items p-t-10">
            @if (\Sentinel::check())
            <li class="visible-sm visible-xs m-t-20 m-b-20">
                <a class="detailed" href="javascript:;" style="width:87%">
                    <span class="thumbnail-wrapper d40 circular m-r-10 bordered " style="line-height: 32px;">
                        <img src="{{ $user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src="{{ $user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src-retina="{{ $user->avatarPath() }}_2x.jpg?{{ $timestamp }}" width="36" height="36" alt="Wllppr">
                    </span>
                    <span class="inline">
                        <span class="title fs-14">{{ $user->username }}</span>
                        <span class="details fs-11">{{ $user->wallpapers->count() }} Uploads</span>
                    </span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="">
                        <a href="{{ route('getuser', ['username' => $user->username]) }}">My Profile</a>
                        <span class="icon-thumbnail"><i class="mdi text-white mdi-account-circle"></i></span>
                    </li>
                    <li class="">
                        <a href="{{ route('getuseruploads', ['username' => $user->username]) }}">My Uploads</a>
                        <span class="icon-thumbnail"><i class="mdi text-white mdi-image-multiple"></i></span>
                    </li>
                    <li class="">
                        <a href="{{ route('getuserfavorites', ['username' => $user->username]) }}">My Favorites</a>
                        <span class="icon-thumbnail"><i class="mdi text-white mdi-heart"></i></span>
                    </li>
                    <li class="">
                        <a href="{{ route('getusersettings') }}">Settings</a>
                        <span class="icon-thumbnail"><i class="mdi text-white mdi-settings"></i></span>
                    </li>
                    <li class="">
                        <a href="{{ route('getlogout') }}">Logout</a>
                        <span class="icon-thumbnail"><i class="mdi text-white mdi-logout-variant"></i></span>
                    </li>
                </ul>
            </li>
            <li class="visible-sm visible-xs">
                <a href="{{ route('getusernotifications') }}" class="detailed">
                    <span class="title">Notifications</span>
                    <span class="details fs-11">New ({{ $notifications->count() }})</span>
                </a>
                @if ($notifications->count() > 0)
                <span class="icon-thumbnail bg-warning"><i class="mdi text-white mdi-bell"></i></span>
                @else
                <span class="icon-thumbnail"><i class="mdi text-white mdi-bell"></i></span>
                @endif
            </li>
            @endif
            @if(!empty($galleries))
            <li class="">
                <a href="javascript:;">
                    <span class="title">Catergories</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="mdi text-white mdi-image-album"></i></span>
                <ul class="sub-menu" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
                    @foreach($galleries as $g)
                        <li itemprop="name">
                            <a href="/{{ $g->slug }}" itemprop="url">{{ $g->name }}</a>
                            @if(\File::exists(public_path().\Storage::url('public/'.GALLERY_FEATURES_FOLDER.$g->slug.'.jpg')))
                                <span class="icon-thumbnail" style="background-image:url({!! '//static3.'.env('APP_DOMAIN').'/'.GALLERY_FEATURES_FOLDER.$g->slug.'.jpg' !!})"></span>
                            @else
                                <span class="icon-thumbnail">{{ ucfirst($g->name[0]) }}</span>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </li>
            @endif
            @if (!empty(session(SESSION_DEVICE_NAME_KEY)))
            <li class="">
                <a class="detailed" href="{{ route('getdevice',  session(SESSION_DEVICE_SLUG_KEY)) }}">
                    @if (strcmp(session(SESSION_DEVICE_MAKER_KEY), 'N/A') != 0)
                    <span class="title device-name">{{ session(SESSION_DEVICE_MAKER_KEY).' '.session(SESSION_DEVICE_NAME_KEY) }}</span>
                    @else
                    <span class="title device-name">{{ session(SESSION_DEVICE_NAME_KEY) }}</span>
                    @endif
                    <span class="details fs-11">View all wallpapers</span>
                </a>
                <span class="icon-thumbnail"><i class="mdi text-white mdi-cellphone-iphone"></i></span>
            </li>
            @endif
            <li class="">
                    @if (!empty(session(SESSION_DEVICE_NAME_KEY)))
                    <a class="detailed" href="{{ route('postdevicepickerclear') }}">
                        <span class="title">Clear device</span>
                        @if (strcmp(session(SESSION_DEVICE_MAKER_KEY), 'N/A') != 0)
                        <span class="details fs-11">{{ session(SESSION_DEVICE_MAKER_KEY).' '.session(SESSION_DEVICE_NAME_KEY) }}</span>
                        @else
                        <span class="details fs-11">{{ session(SESSION_DEVICE_NAME_KEY) }}</span>
                        @endif
                    </a>
                        <span class="icon-thumbnail"><i class="mdi text-white mdi-filter-remove"></i></span>
                    @else
                    <a class="detailed" href="{{ route('getdevicepicker') }}">
                        <span class="title">Pick a device</span>
                        <span class="details fs-11">No device selected</span>
                    </a>
                        <span class="icon-thumbnail"><i class="mdi text-white mdi-cellphone-iphone"></i></span>
                    @endif         
            </li>
            <li class="">
                <a href="{{ route('getimageupload') }}">
                    <span class="title">Upload</span>
                </a>
                <span class="icon-thumbnail"><i class="mdi text-white mdi-cloud-upload"></i></span>
            </li>
            @if (!\Sentinel::check())
            <li class="visible-sm visible-xs">
                <a href="{{ route('getlogin') }}">
                    <span class="title">Login</span>
                </a>
                <span class="icon-thumbnail"><i class="mdi text-white mdi-login-variant"></i></span>
            </li>
            <li class="visible-sm visible-xs">
                <a href="{{ route('getregister') }}">
                    <span class="title">Register</span>
                </a>
                <span class="icon-thumbnail"><i class="mdi text-white mdi-account-plus"></i></span>
            </li>
            @endif
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>
<!-- END SIDEBPANEL-->
