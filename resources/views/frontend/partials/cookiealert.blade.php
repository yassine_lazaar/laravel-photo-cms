<div class="bg-success" id="cookie-alert">
    <div class="col-xs-12">
        <div class="container-fluid relative">
            <span class="pull-left p-r-70 p-t-15 p-b-15 text-white">{{ env('APP_NAME') }} uses cookies to give you the best possible experience. By using our site you agree to our <a class="text-white" href="{{ route('getprivacy') }}" rel="nofollow"><u>cookie policy</u></a>.
            </span>
            <span href="javascript:;" id="cookie-alert-close-btn" class="btn btn-info btn-xs pull-top top-right m-t-15 m-r-15">Ok</span>
        </div>
    </div>
</div>
