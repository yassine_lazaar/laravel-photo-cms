<meta charset="utf-8"/>
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>{!! $title !!}</title>
    <meta name="description" content="{!! $description !!}"/>
    <meta name="p:domain_verify" content="66e45370a62a4b7ab39e8b43d570b3bb"/>
@if(isset($canonical))
    <link rel="canonical" href="{{ $canonical }}" />
@endif
@if(isset($wallpapers))
@if(!empty($wallpapers->previousPageUrl()))
    <link rel="prev" href="{{ $wallpapers->previousPageUrl() }}" />
@endif
@if(!empty($wallpapers->nextPageUrl()))
    <link rel="next" href="{{ $wallpapers->nextPageUrl() }}" />
@endif
@endif
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@wllpprco">
    <meta name="twitter:title" content="{!! $title !!}">
    <meta name="twitter:description" content="{!! $description !!}">
    <meta name="twitter:url" content="{{ Request::url() }}" />
@if(isset($twitterCardImageInfo))
    <meta name="twitter:image" content="{{ $twitterCardImageInfo['img'] }}">
    <meta name="twitter:image:width" content="{{ $twitterCardImageInfo['width'].'px' }}" />
    <meta name="twitter:image:height" content="{{ $twitterCardImageInfo['height'].'px' }}" />
@else
    @include('frontend.partials.metatwitter')
@endif
    <meta property="fb:app_id" content="1495679177164371">
    <meta property="og:title" content="{!! $title !!}" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ Request::url() }}" />
@if(isset($facebookCardImageInfo))
    <meta property="og:image" content="{{ $facebookCardImageInfo['img'] }}" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="{{ $facebookCardImageInfo['width'] }}" />
    <meta property="og:image:height" content="{{ $facebookCardImageInfo['height'] }}" />
@else
    @include('frontend.partials.metafacebook')
@endif
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
