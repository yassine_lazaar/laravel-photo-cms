@if($notifications->count() > 0)
@foreach($notifications as $notification)
<!-- START Notification Item-->
@if($notification->read == 0)
<div class="notification b-b b-grey p-b-15 p-t-15 unread relative clearfix">
@else
<div class="notification b-b b-grey p-b-15 p-t-15 relative clearfix">
@endif
    @if($notification->type == NOTIFICATION_TYPE_MODERATION_PUBLISH)
    <div class="heading p-r-30">
        <a href="{{ route('getwallpaper', $notification->link.'.htm') }}" class="text-complete pull-left">
            <i class="mdi mdi-check-circle fs-16 m-r-10"></i>
            <span class="bold">Upload approved</span>
        </a>
        <div class="pull-right">
            <span class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->timestamp->date))->diffForHumans() }}</span>
        </div>
        <div class="clearfix"></div>
        <div class="inline more-details full-width m-t-15">
            <div class="more-details-inner relative ">
                <div class="inline thumbnail-wrapper img-rounded"><a href="{{ route('getwallpaper', $notification->link.'.htm') }}"><img src="{{ $notification->thumb }}_1x.jpg"></a></div>
                <div class="inline pull-top top-left p-l-90">
                    <div class="semi-bold fs-16 wallpaper-title">{{ $notification->title }} in now published.</div>
                    <div class="small hint-text fs-11">
                        <a class="text-black" href="{{ route('getgallery', $notification->gallery_slug) }}">Under {{ $notification->gallery }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif($notification->type == NOTIFICATION_TYPE_MODERATION_DISCARD)
    <div class="heading p-r-30">
        <span class="text-danger pull-left">
            <i class="mdi mdi-minus-circle fs-16 m-r-10"></i>
            <span class="bold">Upload declined</span>
        </span>
        <div class="pull-right">
            <span class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->timestamp->date))->diffForHumans() }}</span>
        </div>
        <div class="clearfix"></div>
        <div class="inline more-details full-width m-t-15">
            <div class="more-details-inner relative">
                <div class="inline m-l-10">
                    <div class="semi-bold fs-16 wallpaper-title">'{{ $notification->title }}' was not approved.</div>
                    @if($notification->discard_reason > 0)
                    <div class="small hint-text fs-11">
                        {{ unserialize(UPLOAD_DISCARD_REASONS)[$notification->discard_reason - 1]->msg }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @elseif($notification->type == NOTIFICATION_TYPE_FAVORITE)
    <div class="heading p-r-30">
        <a href="{{ route('getwallpaper', $notification->link.'.htm') }}" class="pull-left">
            <i class="mdi mdi-heart fs-16 m-r-10 text-primary"></i>
            <span class="bold">New favorite!</span>
        </a>
        <div class="pull-right">
            <span class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->timestamp->date))->diffForHumans() }}</span>
        </div>
        <div class="clearfix"></div>
        <div class="inline more-details full-width m-t-15">
            <div class="more-details-inner relative">
                <div class="inline thumbnail-wrapper img-rounded m-r-10"><a href="{{ route('getwallpaper', $notification->link.'.htm') }}"><img src="{{ $notification->thumb }}_1x.jpg"></a></div>
                <div class="inline pull-top top-left p-l-90">
                    <div class="semi-bold fs-16 wallpaper-title">{{ $notification->username }} added '{{ $notification->title }}' to his favorites.</div>
                    <div class="small hint-text fs-11">
                        <a class="text-black" href="{{ route('getuser', $notification->username) }}">View {{ $notification->username }}'s profile</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($notification->read == 0)
    <div class="option bg-complete-lighter">
    @else
    <div class="option bg-master-lightest">
    @endif
    </div>
</div>
<!-- END Notification Item-->
@endforeach
@else
No notifications
@endif