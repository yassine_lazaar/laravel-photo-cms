<div class="gallery-filters pull-right text-center p-t-15 p-b-15">
    <div class="btn-group inline" data-toggle="buttons">
    @if(empty($order_slug))
        <label class="btn btn-xs btn-info bg-info-light" onclick="document.location.href='{{ $order_base }}';">
    @else
        <label class="btn btn-xs btn-info" onclick="document.location.href='{{ $order_base }}';">
    @endif
            <i class="mdi m-r-5 xs-m-r-0 mdi-clock-fast"></i>
            <input type="radio">
            <span class="hide-xs">Recent</span>
        </label>
    @if(strcmp($order_slug,GALLERY_ORDER_TRENDING) == 0)
        <label class="btn btn-xs btn-info bg-info-light" onclick="document.location.href='{{ $order_base }}trending';">
    @else
        <label class="btn btn-xs btn-info" onclick="document.location.href='{{ $order_base }}trending';">
    @endif
            <i class="mdi m-r-5 xs-m-r-0 mdi-fire"></i>
            <input type="radio">
            <span class="hide-xs">Trending</span>
        </label>
    @if(strcmp($order_slug,GALLERY_ORDER_FEATURED) == 0)
        <label class="btn btn-xs btn-info bg-info-light" onclick="document.location.href='{{ $order_base }}featured';">
    @else
        <label class="btn btn-xs btn-info" onclick="document.location.href='{{ $order_base }}featured';">
    @endif
            <i class="mdi m-r-5 xs-m-r-0 mdi-star"></i>
            <input type="radio">
            <span class="hide-xs">Featured</span>
        </label>
    @if(strcmp($order_slug,GALLERY_ORDER_VIEWS) == 0)
        <label class="btn btn-xs btn-info bg-info-light" onclick="document.location.href='{{ $order_base }}views';">
    @else
        <label class="btn btn-xs btn-info" onclick="document.location.href='{{ $order_base }}views';">
    @endif
            <i class="mdi m-r-5 xs-m-r-0 mdi-eye"></i>
            <input type="radio">
            <span class="hide-xs">Most Views</span>
        </label>
    @if(strcmp($order_slug,GALLERY_ORDER_DOWNLOADS) == 0)
        <label class="btn btn-xs btn-info bg-info-light" onclick="document.location.href='{{ $order_base }}downloads';">
    @else
        <label class="btn btn-xs btn-info" onclick="document.location.href='{{ $order_base }}downloads';">
    @endif
            <i class="mdi m-r-5 xs-m-r-0 mdi-download"></i>
            <input type="radio">
            <span class="hide-xs">Most Downloads</span>
        </label>
    @if(strcmp($order_slug,GALLERY_ORDER_FAVORITES) == 0)
        <label class="btn btn-xs btn-info bg-info-light" onclick="document.location.href='{{ $order_base }}favorites';">
    @else
        <label class="btn btn-xs btn-info" onclick="document.location.href='{{ $order_base }}favorites';">
    @endif
            <i class="mdi m-r-5 xs-m-r-0 mdi-heart"></i>
            <input type="radio">
            <span class="hide-xs">Most Favored</span>
        </label>
    </div>
</div>