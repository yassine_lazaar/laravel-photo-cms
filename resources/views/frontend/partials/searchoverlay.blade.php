<!-- START OVERLAY -->
<div class="overlay no-padding bg-primary" style="display: none" data-pages="search">
    <!-- BEGIN Overlay Content !-->
    <div class="overlay-content has-results m-t-30">
        <!-- BEGIN Overlay Input !-->
        <div class="container text-center m-b-10">
            <input id="overlay-search" class="no-border text-center text-white overlay-search bg-transparent" placeholder="Type to search" autocomplete="off" spellcheck="false">
        </div>
        <!-- END Overlay Input !-->
        <!-- BEGIN Overlay Controls !-->
        <div class="container font-monserrat m-t-15 text-center">
            <div class="btn-group m-b-15">
                <a class="btn btn-info btn-lg fs-16 search-submit" type="button"><i class="mdi mdi-magnify m-r-5"></i>Search</a>
                <a class="btn btn-info btn-lg fs-16 overlay-close" type="button"><i class="mdi mdi-close m-r-5"></i>Close</a>
            </div>
            <div id="overlay-suggestions"></div>
        </div>
        <!-- END Overlay Controls !-->
        </div>
    <!-- END Overlay Content !-->
</div>
<!-- END OVERLAY -->
