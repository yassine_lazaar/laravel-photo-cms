@extends('frontend.master.index')
          
@section('criticalCSS')

    @include('css::criticalfrontenduserfavorites')

@endsection

@section('content')
<!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
            <div class="social-wrapper">
                <div class="social" data-pages="social">
                    @include('frontend.partials.socialjumbotron')
                    <div class="container-fluid container-fixed-lg no-padding sm-p-l-10 sm-p-r-10">
                        <div class="no-border bg-transparent full-width">
                            <!-- START CONTAINER FLUID -->
                            <div class="container container-fluid p-t-10 p-b-30 p-r-0 p-l-0">
                                <div class="row text-center">
                                    <div class="col-xs-12">
                                        <div class="m-x-auto p-b-10">
                                            <img class="img-circle" data-src-retina="{{ $user->avatarPath() }}_3x.jpg?{{ $timestamp }}" data-src="{{ $user->avatarPath() }}_2x.jpg?{{ $timestamp }}" src="{{ $user->avatarPath() }}_2x.jpg?{{ $timestamp }}" width="64" height="64" alt="{{ $user->username }}">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <h2 class="no-margin" style="text-overflow: ellipsis;white-space: nowrap; overflow: hidden;">{{ $user->username }}</h2>
                                        <p class="hint-text no-margin small"><i class="mdi mdi-map-marker-radius"></i> {{ unserialize(COUNTRIES)[$user->country] }}</p>
                                    </div>
                                </div>
                                @if($user->bio)
                                <div class="row text-center">
                                    <div class="col-xs-12">
                                        <blockquote class="p-t-15 p-b-15">
                                        <p class="hint-text">{{ $user->bio }}</p>
                                        </blockquote>
                                    </div>
                                </div>
                                @endif
                                <div class="row text-center m-t-15">
                                    <a class="btn btn-info btn-xs m-b-5" href="{{ route('getuseruploads', $user->username) }}"><i class="mdi mdi-heart text-white p-r-10"></i>View Uploads</a>
                                    @if($isProfileOwner)
                                    <a class="btn btn-info btn-xs m-b-5" href="{{ route('getusersettings') }}"><i class="mdi mdi-settings text-white p-r-10"></i>Settings</a>
                                    @endif
                                @if($user->facebook || $user->twitter || $user->website)
                                    <div class="inline">
                                        @if($user->facebook)
                                        <a class="btn btn-facebook btn-xs m-b-5" href="https://www.facebook.com/{{ $user->facebook }}"><i class="mdi mdi-facebook text-white p-l-15 p-r-15"></i></a>
                                        @endif
                                        @if($user->twitter)
                                        <a class="btn btn-twitter btn-xs m-b-5" href="https://www.twitter.com/{{ $user->twitter }}"><i class="mdi mdi-twitter text-white  p-l-15 p-r-15"></i></a>
                                        @endif
                                        @if($user->instagram)
                                        <a class="btn btn-instagram btn-xs m-b-5" href="https://www.instagram.com/{{ $user->twitter }}"><i class="mdi mdi-instagram text-white  p-l-15 p-r-15"></i></a>
                                        @endif
                                        @if($user->website)
                                        <a class="btn btn-primary btn-xs m-b-5" href="{{ $user->website }}"><i class="mdi mdi-web text-white  p-l-15 p-r-15"></i></a>
                                        @endif
                                    </div>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="no-border bg-transparent full-width" data-social="item">
                            <div class="panel bg-transparent sm-p-l-10 sm-p-r-10">
                                @if($wallpapers->count() !=0)
                                <div class="gallery clearfix">
                                    @for ($i = 0; $i < $wallpapers->count(); $i++)
                                    <!-- START GALLERY ITEM -->
                                    @if($i == 9 &&  $wallpapers->count() > 11)
                                    <div class="gallery-item pull-left" data-width="2" data-height="2">
                                        <a href="{{ $wallpapers[$i]->link() }}">
                                            <img class="lazy" src="//static3.{{ env('APP_DOMAIN') }}/image/thumb-placeholder.jpg" data-src="{{ $wallpapers[$i]->thumbnailPath() }}_big.jpg" data-src-sm="{{ $wallpapers[$i]->thumbnailPath() }}_big.jpg" alt="{{ $wallpapers[$i]->title.' Wallpaper' }}">						
                                            <div class="fade"></div>
                                            <div class="top-right p-t-20 p-r-20">
                                                <a class="fs-10" href="{{ route('getgallery', $wallpapers[$i]->gallery->slug) }}">
                                                    <span class="label font-montserrat bg-alpha text-master">{{ $wallpapers[$i]->gallery->name }}</span>
                                                </a>
                                                @if($wallpapers[$i]->featured)									
                                                <a class="fs-10 m-l-5"><span class="label  font-montserrat bg-featured text-white">&#9733;</span></a>
                                                @endif	
                                            </div>
                                    @else
                                    <div class="gallery-item pull-left" data-width="1" data-height="1">
                                        <a href="{{ $wallpapers[$i]->link() }}">									
                                            <img class="lazy" src="//static3.{{ env('APP_DOMAIN') }}/image/thumb-placeholder.jpg" data-src="{{ $wallpapers[$i]->thumbnailPath() }}_3x.jpg" data-src-sm="{{ $wallpapers[$i]->thumbnailPath() }}_big.jpg" alt="{{ $wallpapers[$i]->title.' Wallpaper' }}">							
                                            <div class="fade"></div>
                                            <div class="top-right p-t-20 p-r-20">
                                                <a class="fs-10" href="{{ route('getgallery', $wallpapers[$i]->gallery->slug) }}">
                                                    <span class="label font-montserrat bg-alpha text-master">{{ $wallpapers[$i]->gallery->name }}</span>
                                                </a>
                                                @if($wallpapers[$i]->featured)									
                                                <a class="fs-10 m-l-5"><span class="label  font-montserrat bg-featured text-white">&#9733;</span></a>
                                                @endif	
                                            </div>
                                    @endif
                                            <!-- START ITEM OVERLAY DESCRIPTION -->
                                            <div class="overlayer bottom-left full-width">
                                                <div class="overlayer-wrapper item-info gradient-grey p-t-50">
                                                    <div class="wallpaper-title full-width p-r-20 p-l-20 m-b-10">
                                                        <p class="title full-width font-montserrat text-white p-r-50 p-b-10 m-b-0">{{ $wallpapers[$i]->title }}</p>
                                                    </div>
                                                    <div class="uploader-stats p-l-20 p-b-20">
                                                        <div class="thumbnail-wrapper d32 circular m-t-10">
                                                            <img width="32" height="32" src="{{ $wallpapers[$i]->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src="{{ $wallpapers[$i]->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src-retina="{{ $wallpapers[$i]->user->avatarPath() }}_2x.jpg?{{ $timestamp }}" alt="{{ $wallpapers[$i]->user->username }}">
                                                        </div>
                                                        <div class="inline m-l-10 m-t-10">
                                                            <p class="no-margin text-white fs-11">By {{ $wallpapers[$i]->user->username }}</p>
                                                            <p class="stats text-white fs-11 m-b-0">
                                                                <span><i class="mdi mdi-eye"></i></span>
                                                                <span>{{ numberAbbreviation($wallpapers[$i]->views) }}</span>
                                                                <span><i class="mdi mdi-download m-l-5"></i></span>
                                                                <span>{{ numberAbbreviation($wallpapers[$i]->downloads) }}</span>
                                                                <span><i class="mdi mdi-heart m-l-5"></i></span>
                                                                <span>{{numberAbbreviation($wallpapers[$i]->favorites->count()) }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ITEM OVERLAY DESCRIPTION -->
                                        </a>
                                        <div class="btn-favorite-container bottom-right p-r-20 p-b-20">
                                            <button class="btn btn-md bg-semitransparent fs-14 no-border pull-right p-r-10 p-l-10 btn-favorite" type="button" data-id="{{ encrypt($wallpapers[$i]->id) }}" data-slug="{{ $wallpapers[$i]->slug }}">
                                            @if($wallpapers[$i]->favorited)
                                                <i class="mdi fs-16 text-primary mdi-heart"></i>
                                            @else  
                                                <i class="mdi fs-16 text-primary mdi-heart-outline"></i>
                                            @endif
                                            </button>
                                        </div>						
                                    </div>
                                    <!-- END GALLERY ITEM -->
                                    @endfor
                                </div>
                                @else
                                <div class="full-width p-t-20 p-b-10">
                                    <h4 class="text-center hint-text">No Favorites :(</h4>
                                </div>
                                @endif
                                <div class="text-center full-width clearfix">
                                    @include('frontend.partials.pagination.link_limit', ['paginator' => $wallpapers])						
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
        <!-- END PAGE CONTENT -->
        @include('frontend.partials.footer')
    </div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @if(Sentinel::check())
    	@include('common.sessionmodal')
	@endif
@endsection

