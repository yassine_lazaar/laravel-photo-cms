@extends('frontend.master.index')
          
@section('criticalCSS')

    @include('css::criticalfrontend')

@endsection

@section('content')
<!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
			<div class="tag-page">
				@include('common.breadcrumb')
                @if(empty($random))
                <div class="container-fluid container-fixed-lg full-width sm-p-l-15 sm-p-r-15 title-order">
					<div class="pull-left text-center">
                    @if($count > 1)
					    <h1 class="text-master m-t-15 m-b-15">{{ $count.' '.$tag. ' Wallpapers'}}</h1>
                    @else
					    <h1 class="text-master m-t-15 m-b-15">{{ $count.' '.$tag. ' Wallpaper'}}</h1>
                    @endif
					</div>
					<div class="clearfix">
					</div>
				</div>
                @endif
				<!-- START CATEGORY -->
                @if(empty($random))
				<div class="panel bg-transparent sm-p-l-10 sm-p-r-10">
                    <div class="gallery clearfix">
                        {{ $adRendered = false }}
						@for ($i = 0; $i < $wallpapers->count(); $i++)
						<!-- START GALLERY ITEM -->
						@if ($i == 11 && !$adRendered)
						<div class="gallery-item pull-left" data-width="1" data-height="1">
							@include('frontend.ads.galleryad')
						</div>
						{{ $adRendered = true }}
						{{ $i-- }}
						<?php continue ?>
						@endif
                        @if($i == 8 &&  $wallpapers->count() > 11)
                        <div class="gallery-item pull-left" data-width="2" data-height="2">
                            <a href="{{ $wallpapers[$i]->link() }}">
                                <img class="lazy" src="//static3.{{ env('APP_DOMAIN') }}/image/thumb-placeholder.jpg" data-src="{{ $wallpapers[$i]->thumbnailPath() }}_big.jpg" data-src-sm="{{ $wallpapers[$i]->thumbnailPath() }}_big.jpg" alt="{{ $wallpapers[$i]->title.' Wallpaper' }}">						
                                <div class="fade"></div>
                                <div class="top-right p-t-20 p-r-20">
									<a class="fs-10" href="{{ route('getgallery', $wallpapers[$i]->gallery->slug) }}">
										<span class="label font-montserrat bg-alpha text-master">{{ $wallpapers[$i]->gallery->name }}</span>
									</a>
									@if($wallpapers[$i]->featured)									
									<a class="fs-10 m-l-5"><span class="label  font-montserrat bg-featured text-white">&#9733;</span></a>
									@endif	
								</div>
                        @else
                        <div class="gallery-item pull-left" data-width="1" data-height="1">
                            <a href="{{ $wallpapers[$i]->link() }}">									
                                <img class="lazy" src"//static3.{{ env('APP_DOMAIN') }}/image/thumb-placeholder.jpg" data-src="{{ $wallpapers[$i]->thumbnailPath() }}_3x.jpg" data-src-sm="{{ $wallpapers[$i]->thumbnailPath() }}_big.jpg" alt="{{ $wallpapers[$i]->title.' Wallpaper' }}">							
                                <div class="fade"></div>
                                <div class="top-right p-t-20 p-r-20">
									<a class="fs-10" href="{{ route('getgallery', $wallpapers[$i]->gallery->slug) }}">
										<span class="label font-montserrat bg-alpha text-master">{{ $wallpapers[$i]->gallery->name }}</span>
									</a>
									@if($wallpapers[$i]->featured)									
									<a class="fs-10 m-l-5"><span class="label  font-montserrat bg-featured text-white">&#9733;</span></a>
									@endif	
								</div>
                        @endif
                                <!-- START ITEM OVERLAY DESCRIPTION -->
                                <div class="overlayer bottom-left full-width">
                                    <div class="overlayer-wrapper item-info gradient-grey p-t-50">
                                        <div class="wallpaper-title full-width p-r-20 p-l-20 m-b-10">
                                            <p class="title full-width font-montserrat text-white p-r-50 p-b-10 m-b-0">{{ $wallpapers[$i]->title }}</p>
                                        </div>
                                        <div class="uploader-stats p-l-20 p-b-20">
                                            <div class="thumbnail-wrapper d32 circular m-t-10">
                                                <img width="32" height="32" src="{{ $wallpapers[$i]->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src="{{ $wallpapers[$i]->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src-retina="{{ $wallpapers[$i]->user->avatarPath() }}_2x.jpg?{{ $timestamp }}" alt="{{ $wallpapers[$i]->user->username }}">
                                            </div>
                                            <div class="inline m-l-10 m-t-10">
                                                <p class="no-margin text-white fs-11">By {{ $wallpapers[$i]->user->username }}</p>
                                                <p class="stats text-white fs-11 m-b-0">
                                                    <span><i class="mdi mdi-eye"></i></span>
                                                    <span>{{ numberAbbreviation($wallpapers[$i]->views) }}</span>
                                                    <span><i class="mdi mdi-download m-l-5"></i></span>
                                                    <span>{{ numberAbbreviation($wallpapers[$i]->downloads) }}</span>
                                                    <span><i class="mdi mdi-heart m-l-5"></i></span>
                                                    <span>{{numberAbbreviation($wallpapers[$i]->favorites->count()) }}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END ITEM OVERLAY DESCRIPTION -->
                            </a>
                            <div class="btn-favorite-container bottom-right p-r-20 p-b-20">
                                <button class="btn btn-md bg-semitransparent fs-14 no-border pull-right p-r-10 p-l-10 btn-favorite" type="button" data-id="{{ encrypt($wallpapers[$i]->id) }}" data-slug="{{ $wallpapers[$i]->slug }}">
                                @if($wallpapers[$i]->favorited)
                                    <i class="mdi fs-16 text-primary mdi-heart"></i>
                                @else  
                                    <i class="mdi fs-16 text-primary mdi-heart-outline"></i>
                                @endif
                                </button>
                            </div>						
                        </div>
                        <!-- END GALLERY ITEM -->
                        @endfor
                    </div>
                    <div class="text-center full-width clearfix">
                        @include('frontend.partials.pagination.link_limit', ['paginator' => $wallpapers])						
                    </div>
				</div>
                @else
                <div class="container-fluid container-fixed-lg full-width sm-p-l-15 sm-p-r-15">
                    <div class="panel bg-transparent sm-p-l-10 sm-p-r-10">
                        <h1>Tags</h1>                        
                        @foreach ($random as $tag)
                        <a class="label bg-primary m-b-10 m-r-10 padding-0 inline text-white" href="/tags/{{ $tag->slug }}">
                            <span class="p-t-10 p-b-10 p-l-10 p-r-5 inline">{{ $tag->name }}</span>
                            <span class="padding-10 inline tag-b-rad-sm bg-primary-dark"> @if($tag['validCount']) {{ $tag['validCount'] }} @else {{ $tag->wallpapers->count() }} @endif</span>
                        </a> 
                        @endforeach  
                    </div> 
                </div> 
                @endif
				<!-- END CATEGORY -->
			</div>
		</div>
        <!-- END PAGE CONTENT -->
        @include('frontend.partials.footer')
    </div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @if(Sentinel::check())
    	@include('common.sessionmodal')
	@endif
@endsection

