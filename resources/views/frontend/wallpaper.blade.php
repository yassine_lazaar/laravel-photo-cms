@extends('frontend.master.index')
          
@section('criticalCSS')

    @include('css::criticalfrontendwallpaper')

@endsection

@section('schema')
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebPage",
        "name": "{{ $title }}",
        "url": "{{ route('getwallpaper', $wallpaper->slug).'.htm' }}",
        "description": "{{ $description }}",
        "mainEntity": {
            "@type": "ImageObject",
            "author": "{{ ($wallpaper->author)? $wallpaper->author : $wallpaper->user->username }}",
            "contentUrl": "{{ $wallpaper->previewPath() }}",
            "thumbnailUrl": "{{ $wallpaper->thumbnailPath().'_3x.jpg' }}
            "datePublished": {{ $wallpaper->created_at }},
            "description": "{{ ($wallpaper->description)? $wallpaper->description : $wallpaper->title.' '.collect($devices)->pluck('name')->implode(', ') }}",
            "name": "{{ $wallpaper->title }}"
        }
    }
    </script>
@endsection               
@section('content')
<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid">
            <!-- BEGIN PLACE PAGE CONTENT HERE -->
            <div class="row">
                <div class="col-lg-8 col-md-7 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <h1 id="wallpaper-preview-title">{{ $wallpaper->title }}</h1>
                            <div class="full-width bg-master-lightest">
                                <figure class="relative" style="max-width:750px; margin:auto">
                                    <img class="" id="wallpaper-preview" src="{{ $wallpaper->previewPath() }}" alt="{{ $wallpaper->title.' Wallpaper' }}">                            
                                    <div class="pull-bottom bottom-right btn-group m-r-10 m-b-10">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ env('APP_DOMAIN').$wallpaper->link() }}" class="btn btn-social btn-sm btn-facebook  p-l-10 p-r-10" target="_blank" rel="nofollow">
                                            <span class="mdi mdi-facebook"></span>
                                        </a>
                                        <a href="https://twitter.com/home?status={{ $wallpaper->title.' '.env('APP_DOMAIN').$wallpaper->link() }}" class="btn btn-social btn-sm btn-twitter p-l-10 p-r-10" target="_blank" rel="nofollow">
                                            <span class="mdi mdi-twitter"></span>
                                        </a>
                                        <a href="https://pinterest.com/pin/create/button/?url={{ env('APP_DOMAIN').$wallpaper->link() }}&media={{ $wallpaper->previewPath() }}&description={{ $description }}" class="btn btn-social btn-sm btn-pinterest p-l-10 p-r-10" target="_blank" rel="nofollow">
                                            <span class="mdi mdi-pinterest"></span>
                                        </a>
                                    </div>
                                </figure>
                            </div>
                            @if(count($tags) >= 1)
                            <div class="m-t-5">
                                @foreach ($tags as $tag)
                                <a class="label bg-master-lighter text-black m-t-5 m-r-5 padding-0 inline" href="/tags/{{ $tag->slug }}">
                                    <span class="p-t-10 p-b-10 p-l-10 p-r-5 inline">{{ $tag->name }}</span>
                                    <span class="padding-10 inline tag-b-rad-sm bg-master-light"> @if($tag['validCount']) {{ $tag['validCount'] }} @else {{ $tag->wallpapers->count() }} @endif</span>
                                </a> 
                                @endforeach                                       
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="">
                    </div>
                </div>
                <aside class="col-lg-4 col-md-5 col-xs-12">                
                    <div class="panel">
                        <div class="panel-body p-t-20">
                            <div class="col-xs-12 no-padding">
                                <div class="row m-r-0 m-l-0 m-b-5">
                                    <div class="col-xs-4 p-r-5 p-l-0">
                                        <div class="label bg-master-lightest block hint-text fs-12 p-t-10 p-b-10">
                                            <i class="mdi mdi-eye m-r-5"></i>
                                            {{ numberAbbreviation($wallpaper->views) }}
                                        </div>
                                    </div>
                                    <div class="col-xs-4 p-r-5 p-l-5">
                                        <div class="label bg-master-lightest block hint-text fs-12 p-t-10 p-b-10">
                                            <i class="mdi mdi-download m-r-5"></i>
                                            {{ numberAbbreviation($wallpaper->downloads) }}
                                        </div>
                                    </div>
                                    <div class="col-xs-4 p-r-0 p-l-5">
                                        <div class="label bg-master-lightest block hint-text fs-12 p-t-10 p-b-10">
                                            <i class="mdi mdi-heart m-r-5"></i>
                                            {{ numberAbbreviation($wallpaper->favorites->count()) }}
                                        </div>
                                    </div>
                                </div>
                                @include('frontend.ads.wallpaperad')
                                <div class="inline full-width b-rad-sm p-t-10 p-b-10 p-r-15 p-l-15 m-b-10 m-t-10 bg-master-lightest" id="wallpaper-details">
                                    <div class="row m-x-auto m-t-5 m-b-5">                        
                                        <div class="col-xs-8 no-padding">
                                            <a class="text-master" href="{{ route('getuser', $wallpaper->user->username) }}">
                                                <div class="pull-left">
                                                    <span class="thumbnail-wrapper d36 circular bordered b-white"><img src="{{ $wallpaper->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src="{{ $wallpaper->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src-retina="{{ $wallpaper->user->avatarPath() }}_2x.jpg?{{ $timestamp }}" alt="{{ $wallpaper->user->usrename }}"></span>
                                                </div>
                                                <div class="pull-left p-l-10">
                                                    <div class="fs-11 bold" style="line-height:18px">{{ $uploader->username }}</div>
                                                    <div class="fs-10" style="line-height:16px"><span class="hidden-md">Uploaded </span>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($wallpaper->created_at))->diffForHumans() }}</div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 no-padding">
                                            <button class="btn btn-md bg-master-lighter no-border pull-right p-r-15 p-l-15 btn-favorite" type="button" data-id="{{ encrypt($wallpaper->id) }}" data-slug="{{ $wallpaper->slug }}">
                                            @if($wallpaper->favorited)
                                                <i class="mdi fs-16 mdi-heart text-primary"></i>
                                            @else  
                                                <i class="mdi fs-16 mdi-heart-outline text-primary"></i>
                                            @endif
                                            </button>
                                        </div>
                                    </div>
                                    @if($wallpaper->description)                                    
                                    <div class="clearfix m-t-10 p-l-5 p-r-5">                                    
                                        <p class="pull-left fs-10" id="description">{{ $wallpaper->description }}</p>
                                        <div class="clearfix"></div>         
                                    </div>
                                    @endif                                                                        
                                    <div class="p-l-5 p-r-5">
                                        <p class="pull-left">Category</p>
                                        <p class="pull-right"><a href="{{ $wallpaper->gallery->slug }}">{{ $gallery->name }}</a></p>
                                        <div class="clearfix"></div>                                            
                                    </div>
                                    @if($wallpaper->author)
                                    <div class="p-l-5 p-r-5">
                                        <p class="pull-left">Creator</p>
                                        @if($wallpaper->author_link)
                                            <p class="pull-right"><a href="{{ $wallpaper->author_link }}" rel="nofollow">{{ $wallpaper->author }}</a></p>
                                        @else
                                            <p class="pull-right">{{ $wallpaper->author }}</p>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                    @endif                                    
                                    <div class="p-l-5 p-r-5">
                                        <p class="pull-left">License</p>
                                        <p class="pull-right">{{ $wallpaper->getLicense() }}</p>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="">
                                </div>
                                <div class="">
                                    @if (isset($devices))
                                        @if (strcmp($devices->get(0)['maker'], 'N/A') == 0)
                                        @php $devices->get(0)['maker'] = ''; @endphp
                                        @else
                                        @php $devices->get(0)['maker'] = $devices->get(0)['maker'].' '; @endphp
                                        @endif
                                        <button class="btn btn-download btn-lg btn-block no-border btn-primary" data-slug="{{ encrypt($wallpaper->slug) }}" data-id="{{ $devices->get(0)['id'] }}">Download For {{ $devices->get(0)['maker'].$devices->get(0)['name'] }}</button> 
                                        <?php $devices->shift() ?>                                        
                                    @endif
                                    @if (count($devices) >= 1)
                                    <div class="full-width m-t-15" id="download-dropdown">
                                        <input type="checkbox" id="checkbox-toggle">
                                        <label class="btn btn-lg btn-block no-border btn-primary" for="checkbox-toggle"><span class="mdi mdi-chevron-down inline pull-right"></span>Other Devices<div class="clearfix"></div></label>
                                        <ul class="hide" id="download-dropdown-list">
                                        @foreach($devices as $device)
                                            @if (strcmp($device->maker, 'N/A') == 0)
                                            @php $device->maker = ''; @endphp
                                            @else
                                            @php $device->maker = $device->maker.' '; @endphp
                                            @endif
                                            @if ($loop->index == 0)
                                            <li class="bg-primary-dark fs-10 text-center list-header">{{ $types[$device->type] }}</li>
                                            <li class="btn btn-download btn-lg btn-block no-margin no-border fs-11 btn-primary" href="javascript:;" data-slug="{{ encrypt($wallpaper->slug) }}" data-id="{{ $device->id }}" value="{{ $device->name }}">{{ $device->maker.$device->name }}</li>
                                            @else
                                                @if($devices[$loop->index - 1]->type != $device->type)
                                                <li class="bg-primary-dark fs-10 text-center list-header">{{ $types[$device->type] }}</li>
                                                <li class="btn btn-download btn-lg btn-block no-margin no-border fs-11 btn-primary" href="javascript:;" data-slug="{{ encrypt($wallpaper->slug) }}" data-id="{{ $device->id }}" value="{{ $device->name }}">{{  $device->maker.$device->name }}</li>
                                                @else
                                                <li class="btn btn-download btn-lg btn-block no-margin no-border fs-11 btn-primary" href="javascript:;" data-slug="{{ encrypt($wallpaper->slug) }}" data-id="{{ $device->id }}" value="{{ $device->name }}">{{ $device->maker.$device->name }}</li>
                                                @endif
                                            @endif
                                        @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
            @if($relatedWallpapers->count() > 0)
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="full-width">
                                <h5 class="pull-left m-b-0">More In {{ $gallery->name }}</h5>
                                <a href="{{ route('getgallery', $wallpaper->gallery->slug) }}" class="inline btn btn-info btn-xs pull-right m-t-10 m-b-10">View All</a>
                                <div class="clearfix"></div>                                
                                <div class="owl-carousel owl-theme">
                                    @for ($i = 0; $i < $relatedWallpapers->count(); $i++)
                                    <div class="gallery-item gallery-owl-item" data-width="1" data-height="1">
                                        <a class="full-height" href="{{ $relatedWallpapers[$i]->link() }}">									
                                            <img class="lazy" src="//static3.{{ env('APP_DOMAIN') }}/image/thumb-placeholder.jpg" data-owl-src-1x="{{ $relatedWallpapers[$i]->thumbnailPath() }}_3x.jpg" data-owl-src-2x="{{ $relatedWallpapers[$i]->thumbnailPath() }}_big.jpg" alt="{{ $relatedWallpapers[$i]->title.' Wallpaper'}}">							
                                            <div class="fade"></div>
                                            @if($relatedWallpapers[$i]->featured)									
                                            <div class="top-right p-t-20 p-r-20">
                                                <span class="label bg-featured inline fs-10 p-t-5 p-b-5"><i class="mdi text-white mdi-star text-bold"></i></span>
                                            </div>
                                            @endif								
                                            <div class="overlayer bottom-left full-width">
                                                <div class="overlayer-wrapper item-info gradient-grey p-t-50">
                                                    <div class="wallpaper-title full-width p-r-20 p-l-20 m-b-10">
                                                        <p class="title full-width font-montserrat text-white p-r-50 p-b-10 m-b-0">{{ $relatedWallpapers[$i]->title }}</p>
                                                    </div>
                                                    <div class="uploader-stats p-l-20 p-b-20">
                                                        <div class="thumbnail-wrapper d32 circular m-t-10">
                                                            <img width="32" height="32" src="{{ $relatedWallpapers[$i]->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src="{{ $relatedWallpapers[$i]->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src-retina="{{ $relatedWallpapers[$i]->user->avatarPath() }}_2x.jpg?{{ $timestamp }}" alt="{{ $relatedWallpapers[$i]->user->username }}">
                                                        </div>
                                                        <div class="inline m-l-10 m-t-10">
                                                            <p class="no-margin text-white fs-11">By {{ $relatedWallpapers[$i]->user->username }}</p>
                                                            <p class="stats text-white fs-11 m-b-0">
                                                                <span><i class="mdi mdi-eye"></i></span>
                                                                <span>{{ numberAbbreviation($relatedWallpapers[$i]->views) }}</span>
                                                                <span><i class="mdi mdi-download m-l-5"></i></span>
                                                                <span>{{ numberAbbreviation($relatedWallpapers[$i]->downloads) }}</span>
                                                                <span><i class="mdi mdi-heart m-l-5"></i></span>
                                                                <span>{{ numberAbbreviation($relatedWallpapers[$i]->favorites->count()) }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="btn-favorite-container bottom-right p-r-20 p-b-20">
                                            <button class="btn btn-md bg-semitransparent fs-14 no-border pull-right p-r-10 p-l-10 btn-favorite" type="button" data-id="{{ encrypt($relatedWallpapers[$i]->id) }}" data-slug="{{ $relatedWallpapers[$i]->slug }}">
                                            @if($relatedWallpapers[$i]->favorited)
                                                <i class="mdi fs-16 text-primary mdi-heart"></i>
                                            @else  
                                                <i class="mdi fs-16 text-primary mdi-heart-outline"></i>
                                            @endif
                                            </button>
                                        </div>						
                                    </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if($userWallpapers->count() > 0)
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="full-width">
                                <h5 class="pull-left m-b-0">More From {{ $uploader->username }}</h5>
                                <a href="{{ route('getuseruploads', $wallpaper->user->username) }}" class="inline btn btn-info btn-xs pull-right m-t-10 m-b-10">View All</a>
                                <div class="clearfix"></div>                            
                                <div class="owl-carousel owl-theme">
                                    @for ($i = 0; $i < $userWallpapers->count(); $i++)
                                    <div class="gallery-item gallery-owl-item" data-width="1" data-height="1">
                                        <a class="full-height" href="{{ $userWallpapers[$i]->link() }}">									
                                            <img class="lazy" src="//static3.{{ env('APP_DOMAIN') }}/image/thumb-placeholder.jpg" data-owl-src-1x="{{ $userWallpapers[$i]->thumbnailPath() }}_3x.jpg" data-owl-src-2x="{{ $userWallpapers[$i]->thumbnailPath() }}_big.jpg" alt="{{ $userWallpapers[$i]->title.' Wallpaper' }}">							
                                            <div class="fade"></div>
                                            @if($userWallpapers[$i]->featured)									
                                            <div class="top-right p-t-20 p-r-20">
                                                <span class="label bg-featured inline fs-10 p-t-5 p-b-5"><i class="mdi text-white mdi-star text-bold"></i></span>
                                            </div>
                                            @endif								
                                            <div class="overlayer bottom-left full-width">
                                                <div class="overlayer-wrapper item-info gradient-grey p-t-50">
                                                    <div class="wallpaper-title full-width p-r-20 p-l-20 m-b-10">
                                                        <p class="title full-width font-montserrat text-white p-r-50 p-b-10 m-b-0">{{ $userWallpapers[$i]->title }}</p>
                                                    </div>
                                                    <div class="uploader-stats p-l-20 p-b-20">
                                                        <div class="thumbnail-wrapper d32 circular m-t-10">
                                                            <img width="32" height="32" src="{{ $userWallpapers[$i]->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src="{{ $userWallpapers[$i]->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src-retina="{{ $userWallpapers[$i]->user->avatarPath() }}_2x.jpg?{{ $timestamp }}" alt="{{ $userWallpapers[$i]->user->username }}">
                                                        </div>
                                                        <div class="inline m-l-10 m-t-10">
                                                            <p class="no-margin text-white fs-11">By {{ $uploader->username }}</p>
                                                            <p class="stats text-white fs-11 m-b-0">
                                                                <span><i class="mdi mdi-eye"></i></span>
                                                                <span>{{ numberAbbreviation($userWallpapers[$i]->views) }}</span>
                                                                <span><i class="mdi mdi-download m-l-5"></i></span>
                                                                <span>{{ numberAbbreviation($userWallpapers[$i]->downloads) }}</span>
                                                                <span><i class="mdi mdi-heart m-l-5"></i></span>
                                                                <span>{{ numberAbbreviation($userWallpapers[$i]->favorites->count()) }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="btn-favorite-container bottom-right p-r-20 p-b-20">
                                            <button class="btn btn-md bg-semitransparent fs-14 no-border pull-right p-r-10 p-l-10 btn-favorite" type="button" data-id="{{ encrypt($userWallpapers[$i]->id) }}" data-slug="{{ $userWallpapers[$i]->slug }}">
                                            @if($userWallpapers[$i]->favorited)
                                                <i class="mdi fs-16 text-primary mdi-heart"></i>
                                            @else  
                                                <i class="mdi fs-16 text-primary mdi-heart-outline"></i>
                                            @endif
                                            </button>
                                        </div>						
                                    </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>
    <!-- END PAGE CONTENT -->
    @include('frontend.partials.footer')
</div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @if(Sentinel::check())
        @include('common.sessionmodal')
    @endif
@endsection
