@extends('frontend.master.index')
          
@section('criticalCSS')

    @include('css::criticalfrontendnotifications')

@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content">
        @include('common.breadcrumb')
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid">
            <!-- BEGIN PLACE PAGE CONTENT HERE -->
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2">
                    <div class="panel">
                        <div class="panel-heading top-left top-right ">
                            <div class="panel-title">
                                <span class="font-montserrat fs-11 all-caps">
                                    Unread
                                </span>
                            </div>
                        </div>
                        <div class="panel-body p-t-50">
                            @include('frontend.partials.notificationsection',['notifications' => $unread])
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading top-left top-right ">
                            <div class="panel-title">
                                <span class="font-montserrat fs-11 all-caps">
                                    Read
                                </span>
                            </div>
                        </div>
                        <div class="panel-body p-t-50" id="unread-notifications">
                            @include('frontend.partials.notificationsection',['notifications' => $read])
                            <div class="progress-circle-indeterminate progress-circle-primary m-t-20 hide" id="pull-notifications-loader"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>
    <!-- END PAGE CONTENT -->
    @include('frontend.partials.footer')
</div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @include('common.sessionmodal')
@endsection
