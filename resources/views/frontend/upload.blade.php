@extends('frontend.master.index')
          
@section('criticalCSS')

    @include('css::criticalfrontendupload')

@endsection

@section('content')
<!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
            @include('common.breadcrumb')
            <!-- START CONTAINER FLUID -->
            <div class="container-fluid">
                <!-- BEGIN PLACE PAGE CONTENT HERE -->
                <div class="row">
                    {!! Form::open(['route'=>'postimageupload', 'id' => 'image-upload-details', 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) !!}
                    <div class="col-md-8">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="container-fluid">
                                    <div class="form-group">
                                        <div class=" bg-master-lightest m-t-15 m-r-15 m-b-15 m-l-15 hide" id="wallpaper-upload-wrapper">
                                            <img id="wallpaper-upload-preview" src="">
                                        </div>
                                        <label for="file" class="col-sm-3 control-label">File *</label>
                                        <div class="col-sm-9">
                                            <div class="form-group-default input-group no-focus m-b-15" id="wallpaper-upload-form-group">
                                                <label>Select a file</label>
                                                <input type="file" name="file" class="hide" id="wallpaper-upload-input" accept="image/jpeg" readonly>
                                                <span class="input-group-addon">
                                                    <i class="mdi mdi-file mdi-20px"></i>
                                                </span>
                                            </div>
                                            <p class="small hint-text">
                                                * Only jpg and png files are accepted.
                                                <br>* We recommand image dimensions superior to {{ $startResolutions['vertical']['width'].'x'.$startResolutions['vertical']['height'] }} or {{ $startResolutions['horizontal']['width'].'x'.$startResolutions['horizontal']['height'] }}.
                                                <br>* {{ $maxUploadSize }}MB file-size limit apply.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="title" class="col-sm-3 control-label">Title *</label>
                                        <div class="col-sm-9">
                                            {{ Form::text('title', "", array('class'=>'form-control')) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="description" class="col-sm-3 control-label">Description *</label>
                                        <div class="col-sm-9">
                                            {{ Form::textarea('description', "", array('class'=>'form-control', 'height'=>'200')) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="gallery" class="col-sm-3 control-label">Category *</label>
                                        <div class="select2 with-border col-sm-9">
                                            <select class="full-width hide select2-input" name="category" data-init-plugin="select2" data-disable-search="true">
                                                <option disabled selected></option>
                                                @foreach ($galleries as $g)
                                                <option value="{{ $g->id }}">{{ $g->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tags" class="col-sm-3 control-label">Tags (Separate with commas)</label>
                                        <div class="col-sm-9">
                                            <div class="" id="upload-wallpaper-tags">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="creative_license" class="col-sm-3 control-label">Creative License</label>
                                        <div class="select2 with-border col-sm-9">
                                            <select class="full-width hide select2-input" name="license" data-init-plugin="select2" data-disable-search="true">
                                                <option value="0" selected>Attribution</option>
                                                <option value="1">Attribution-ShareAlike</option>
                                                <option value="2">Attribution-NoDerivs</option>
                                                <option value="3">Attribution-NonCommercial</option>
                                                <option value="4">Attribution-NonCommercial-ShareAlike</option>
                                                <option value="5">Attribution-NonCommercial-NoDerivs</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="author" class="col-sm-3 control-label">Author</label>
                                        <div class="col-sm-9">
                                            {{ Form::text('author', '', array('class'=>'form-control', 'placeholder' => 'Johnny Appleseed')) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="author_website" class="col-sm-3 control-label">Author's Website</label>
                                        <div class="col-sm-9">
                                            {{ Form::text('author_website', '', array('class'=>'form-control', 'placeholder' => 'johnnyappleseed.com')) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-transparent" id="sticky-container">
                            <div class="panel-body p-t-0" id="sticky">
                                {!! Form::submit('Upload', array('type' => 'submit','class' => 'btn btn-info btn-lg btn-larger btn-cons btn-block'))!!}
                                {!! Form::reset('Clear', array('type' => 'clear','class' => 'btn btn-lg btn-larger btn-cons btn-block', 'id' => 'upload-clear-button'))!!}
                                {!! Form::close() !!}
                                <p class="small hint-text m-t-10">
                                    By clicking the above Upload button to publish on iphonehdwallpapers.net, you are agreeing to the website's Terms of Service. Please only upload images that you own the rights to.<br>(*) Required fields
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PLACE PAGE CONTENT HERE -->
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
        @include('frontend.partials.footer')
    </div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @include('common.sessionmodal')
@endsection
