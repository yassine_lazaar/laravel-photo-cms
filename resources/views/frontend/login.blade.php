@extends('frontend.master.extra')
          
@section('criticalCSS')

    @include('css::criticalfrontendauthlogin')

@endsection

@section('content')
<!-- START PAGE-CONTAINER -->
<div class="login-wrapper" style="background:url('//static3.{{ env('APP_DOMAIN') }}/image/login-bg.jpg') no-repeat; background-size: cover;">
    <!-- START Login Left Container-->
    <div class="login-container bg-white full-height">
        <div class="p-l-20 m-l-20 p-r-20 m-r-20 p-t-50 m-t-30 m-b-30 sm-p-l-0 sm-p-r-0 sm-p-t-40">
            <a href="{{ route('gethome') }}"><img src="//static3.{{ env('APP_DOMAIN') }}/image/logo.png" alt="Wllppr" data-src="//static3.{{ env('APP_DOMAIN') }}/image/logo.png" data-src-retina="//static3.{{ env('APP_DOMAIN') }}/image/logo_2x.png" width="78" height="22"></a>
            <p class="m-t-35">Sign into your account. If you don't have an account yet, you can <a href="{{ route('getregister') }}" class="text-info">register here</a>.</p>
            <!-- START Login Form -->
            @include('frontend.partials.formalerts')
            {!! Form::open(['route'=>'postlogin','id' => 'form-login', 'method' => 'post', 'role' => 'form', 'class' => 'm-t-15']) !!}
                <!-- START Form Control-->
                <div class="form-group form-group-default">
                    <label>Login</label>
                    <div class="controls">
                        <input type="text" name="email" placeholder="Email" class="form-control" value="{{ old('email') }}">
                    </div>
                </div>
                <!-- END Form Control-->
                <!-- START Form Control-->
                <div class="form-group form-group-default">
                    <label>Password</label>
                    <div class="controls">
                        <input type="password" class="form-control" name="password" placeholder="Credentials">
                    </div>
                </div>
                <!-- START Form Control-->
                <div class="row no-margin">
                    <div class="col-xs-6 no-padding">
                        <div class="m-t-10 m-b-10">
                            <label class="fs-12"><input class="switchery hide p-r-10" type="checkbox" id="checkbox-remember" name="remember" data-init-plugin="switchery" data-size="small" data-color="success"><span class="m-l-10">Remember me</span></label>
                        </div>
                    </div>
                    <div class="col-xs-6 text-right no-padding p-t-10">
                        <a href="{{ route('getreset') }}" class="text-info fs-12">Forgot password?</a>
                    </div>
                </div>
                <!-- END Form Control-->
                {!! Form::submit('Sign in', array('type' => 'submit','class' => 'btn btn-block btn-primary btn-cons btn-lg m-t-10'))!!}
            {!! Form::close() !!}
            <!--END Login Form-->
            <hr>
            <p>Sign in with Facebook or Twitter</p>
            <div class="btn-group btn-group-justified m-t-15">
                <a href="{{ route('providerauth','facebook') }}" class="btn btn-social btn-facebook btn-lg" rel="nofollow">
                    <span class="mdi mdi-facebook"></span>
                </a>
                <a href="{{ route('providerauth','twitter') }}" class="btn btn-social btn-twitter btn-lg" rel="nofollow">
                    <span class="mdi mdi-twitter"></span>
                </a>
            </div>
            <div class="no-padding m-t-20">
                <p>
                    <small>
                            * We will not share anything on your social accounts without your permission.
                    </small>
                </p>
            </div>
        </div>
    </div>
    <!-- END Login Left Container-->
</div>
<!-- END PAGE CONTAINER -->
@endsection

@section('modals')
    @include('common.sessionmodal')
@endsection
