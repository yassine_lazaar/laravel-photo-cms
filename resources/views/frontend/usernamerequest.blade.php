@extends('frontend.master.extra')

@section('criticalCSS')

    @include('css::criticalfrontendauth')

@endsection

@section('content')
    <div class="provider-username-container full-height sm-p-t-30">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    <img src="//static3.{{ env('APP_DOMAIN') }}/image/logo.png" alt="logo" data-src="//static3.{{ env('APP_DOMAIN') }}/image/logo.png" data-src-retina="//static3.{{ env('APP_DOMAIN') }}/image/logo_2x.png" width="78" height="22">
                    <h3>One more step</h3>
                    <p>
                        <small>
                            Choose a username and you're all set.
                        </small>
                    </p>
                    @include('frontend.partials.formalerts')
                    {!! Form::open(['route'=> 'postusername', 'method' => 'post', 'role' => 'form', 'class' => 'p-t-15', 'id' => 'form-register']) !!}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>User name</label>
                                <input type="text" name="username" placeholder="Username has to be unique. It can not be altered later." class="form-control" value="{{ old('username')? old('username'): session('suggested_username') }}">
                            </div>
                        </div>
                    </div>
                    {!! Form::submit('Continue', array('class' => 'btn btn-block btn-primary btn-cons btn-lg m-t-10'))!!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="full-width">
        <div class="register-container padding-15">
            <div>
                <p class="hinted-text small">No part of this website or any of its contents may be reproduced, copied, modified or adapted, without the prior written consent of the author, unless otherwise indicated for stand-alone materials.</p>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('common.sessionmodal')
@endsection