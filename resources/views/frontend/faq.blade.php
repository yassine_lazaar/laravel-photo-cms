@extends('frontend.master.index')
          
@section('criticalCSS')

    @include('css::criticalfrontendmisc')

@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content">
        @include('common.breadcrumb')
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid">
            <!-- BEGIN PLACE PAGE CONTENT HERE -->
            <div class="row">
                <div class="col-sm-12 col-md-10 col-md-offset-1">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3>Frequently Asked Questions</h3>
                        </div>
                        <div class="panel-body">
                            {!! $faq !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>
    <!-- END PAGE CONTENT -->
    @include('frontend.partials.footer')
</div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @if(Sentinel::check())
    	@include('common.sessionmodal')
	@endif
@endsection
