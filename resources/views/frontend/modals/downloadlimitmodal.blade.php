<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="download-limit-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5>Download limit</h5>
                </div>
                <div class="modal-body">
                    <p class="no-margin">
                        You reached the maximum amount of download allowed daily.</br>
                        Login for limitless downloads.
                    </p>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6 p-r-5">
                            <button type="button" class="btn btn-primary btn-block btn-lg" OnClick="location.href='{{ route('getlogin') }}'">Login</button>
                        </div>
                        <div class="col-xs-6 p-l-5">
                            <button type="button" class="btn btn-default btn-block btn-lg no-margin" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
