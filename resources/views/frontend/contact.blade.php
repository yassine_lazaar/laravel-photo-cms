@extends('frontend.master.index')

@section('criticalCSS')

    @include('css::criticalfrontendcontact')

@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content">
        @include('common.breadcrumb')
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid">
            <!-- BEGIN PLACE PAGE CONTENT HERE -->
            <div class="row">
                <div class="col-sm-12 col-md-6 col-md-offset-3">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3>Send us a message</h3>
                            <p>
                            We kindly request for your patience while your request is checked and surely a support team member will get back to you.
                            </p>
                        </div>
                        <div class="panel-body">
                            {!! Form::open(['route'=>'postcontact', 'method' => 'post', 'role' => 'form', 'class' => 'p-t-15', 'id' => 'form-contact']) !!}
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Sender's Name*</label>
                                            <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Email*</label>
                                            <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Email Subject*</label>
                                            <input type="text" name="email_subject" class="form-control" value="{{ old('email_subject') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Message*</label>
                                            {{ Form::textarea('email_message', '', array('class' => 'form-control', 'cols' => 50, 'rows' => 10, 'style' => 'height:auto')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        {!! app('captcha')->display() !!}
                                    </div>
                                </div>
                                {!! Form::submit('Send Message', array('class' => 'btn btn-block btn-info btn-cons btn-lg m-t-10')) !!}
                                <p class="small hint-text m-t-10">
                                    (*) Required fields
                                </p>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>
    <!-- END PAGE CONTENT -->
    @include('frontend.partials.footer')
</div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @include('common.sessionmodal')
@endsection
