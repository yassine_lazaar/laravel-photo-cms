@extends('frontend.master.index')
          
@section('criticalCSS')

    @include('css::criticalfrontenduser')

@endsection

@section('content')
<!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
            <div class="social-wrapper">
                <div class="social" data-pages="social">
                    @include('frontend.partials.socialjumbotron')
                    <div class="container-fluid container-fixed-lg sm-p-l-10 sm-p-r-10">
                        <div class="feed">
                            <!-- START DAY -->
                            <div class="day" data-social="day">
                                <!-- START ITEM -->
                                <div class="no-border bg-transparent full-width">
                                    <!-- START CONTAINER FLUID -->
                                    <div class="container-fluid p-t-10 p-b-30 p-r-0 p-l-0">
                                        <div class="row text-center">
                                            <div class="col-xs-12">
                                                <div class="m-x-auto p-b-10">
                                                    <img class="img-circle" data-src-retina="{{ $user->avatarPath() }}_3x.jpg?{{ $timestamp }}" data-src="{{ $user->avatarPath() }}_2x.jpg?{{ $timestamp }}" src="{{ $user->avatarPath() }}_2x.jpg?{{ $timestamp }}" width="64" height="64" alt="{{ $user->username }}">
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <h2 class="no-margin" style="text-overflow: ellipsis;white-space: nowrap; overflow: hidden;">{{ $user->username }}</h2>
                                                <p class="hint-text no-margin small"><i class="mdi mdi-map-marker-radius"></i> {{ unserialize(COUNTRIES)[$user->country] }}</p>
                                            </div>
                                        </div>
                                        @if($user->bio)
                                        <div class="row text-center">
                                            <div class="col-xs-12">
                                                <blockquote class="p-t-15 p-b-15">
                                                <p class="hint-text">{{ $user->bio }}</p>
                                                </blockquote>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="row text-center m-t-15">
                                            <a class="btn btn-info btn-xs m-b-5" href="{{ route('getuseruploads', $user->username) }}"><i class="mdi mdi-cloud-upload text-white p-r-10"></i>View All Uploads</a>
                                            <a class="btn btn-info btn-xs m-b-5" href="{{ route('getuserfavorites', $user->username) }}"><i class="mdi mdi-heart text-white p-r-10"></i>View All Favorites</a>
                                            @if($isProfileOwner)
                                            <a class="btn btn-info btn-xs m-b-5" href="{{ route('getusersettings') }}"><i class="mdi mdi-settings text-white p-r-10"></i>Settings</a>
                                            @endif
                                        @if($user->facebook || $user->twitter || $user->website)
                                            <div class="inline">
                                                @if($user->facebook)
                                                <a class="btn btn-facebook btn-xs m-b-5" href="https://www.facebook.com/{{ $user->facebook }}"><i class="mdi mdi-facebook text-white p-l-15 p-r-15"></i></a>
                                                @endif
                                                @if($user->twitter)
                                                <a class="btn btn-twitter btn-xs m-b-5" href="https://www.twitter.com/{{ $user->twitter }}"><i class="mdi mdi-twitter text-white  p-l-15 p-r-15"></i></a>
                                                @endif
                                                @if($user->instagram)
                                                <a class="btn btn-instagram btn-xs m-b-5" href="https://www.instagram.com/{{ $user->twitter }}"><i class="mdi mdi-instagram text-white  p-l-15 p-r-15"></i></a>
                                                @endif
                                                @if($user->website)
                                                <a class="btn btn-primary btn-xs m-b-5" href="{{ $user->website }}"><i class="mdi mdi-web text-white  p-l-15 p-r-15"></i></a>
                                                @endif
                                            </div>
                                        @endif
                                        </div>
                                    </div>
                                    <!-- END CONTAINER FLUID -->
                                </div>
                                <!-- END ITEM -->
                                @if($activities->count() !=0)
                                @foreach($activities as $activity)
                                <!-- START ITEM -->
                                <div class="card share share-other col1" data-social="item">
                                    <div class="card-content">
                                        <a href="{{ $activity->link() }}">
                                            <img class="lazy" src="//static3.{{ env('APP_DOMAIN') }}/image/thumb-placeholder.jpg" data-src="{{ $activity->thumbnailPath() }}_3x.jpg" data-src-retina="{{ $activity->thumbnailPath() }}_3x.jpg" data-src-sm="{{ $activity->thumbnailPath() }}_big.jpg"alt="{{ $activity->title }}" width="300" height="250" alt="{{ $activity->title.' activity' }}">							
                                        </a>
                                    </div>
                                    <div class="card-header no-padding p-l-15 p-r-5 clearfix">
                                        @if($activity->markAsLike)
                                        <div class="container-xs-height">
                                            <div class="row-xs-height">
                                                <div class="col-xs-height v-align-middle" style="width:55px">
                                                    <label class="label label-lg bg-master-lightest padding-15"><i class="mdi mdi-heart text-primary fs-16"></i></label>
                                                </div>
                                                <div class="col-xs-height padding-5 p-t-10">
                                                    <div class="">
                                                        <div class="clearfix">
                                                            <p class="fs-12 pull-left no-margin text-black bold">{{ $user->username }}</p>
                                                        </div>
                                                        <div class="clearfix">
                                                            <p class="fs-11 pull-left no-margin hint-text">Liked {{ $activity->user->username }}'s activity</p>
                                                        </div>
                                                        <div class="clearfix">
                                                            <p class="fs-10 pull-right no-margin hint-text">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($activity->created_at))->diffForHumans() }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                        <div class="container-xs-height">
                                            <div class="row-xs-height">
                                                <div class="col-xs-height v-align-middle" style="width:55px">
                                                    <label class="label label-lg bg-master-lightest padding-15"><i class="mdi mdi-cloud-upload text-primary fs-16"></i></label>
                                                </div>
                                                <div class="col-xs-height padding-5 p-t-10">
                                                    <div class="">
                                                        <div class="clearfix">
                                                            <p class="fs-12 pull-left no-margin text-black bold">{{ $user->username }}</p>
                                                        </div>
                                                        <div class="clearfix">
                                                            <p class="fs-11 pull-left no-margin hint-text">Shared a activity on {{ $activity->gallery->name }}</p>
                                                        </div>
                                                        <div class="clearfix">
                                                            <p class="fs-10 pull-right no-margin hint-text">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($activity->created_at))->diffForHumans() }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <!-- END ITEM -->
                                @endforeach
                                @else
                                <div class="full-width p-t-20 p-b-10">
                                    <h4 class="text-center hint-text">No Activity :(</h4>
                                </div>
                                @endif
                            </div>
                            <!-- END DAY -->
                        </div>
                        <!-- END FEED -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT -->
        @include('frontend.partials.footer')
    </div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @if(Sentinel::check())
    	@include('common.sessionmodal')
	@endif
@endsection

