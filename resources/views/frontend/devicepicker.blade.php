@extends('frontend.master.index')
          
@section('criticalCSS')

    @include('css::criticalfrontenddevicepicker')

@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content">
        @include('common.breadcrumb')
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid">
            <!-- BEGIN PLACE PAGE CONTENT HERE -->
            <div class="row">
                <div class="col-xs-12 full-height m-t-50" id="device-picker">
                    <div class="col-xs-12 col-sm-offset-3 col-sm-6">
                        <div class="text-right p-r-5 p-t-5 p-b-5 hint-text">
                            <span class="inline"><i class="mdi mdi-cellphone-iphone"></i></span>
                            <span class="inline"><i class="mdi mdi-tablet-ipad"></i></span>
                            <span class="inline"><i class="mdi mdi-laptop-mac"></i></span>
                            <span class="inline"><i class="mdi mdi-monitor"></i></span>
                        </div>
                        <div class="input-group device-picker-field-group text-center">
                            <input type="text" id="device-picker-field" placeholder="Search devices" autocomplete="off" spellcheck="false">
                            <span class="input-group-addon">
                                <i class="mdi mdi-18px mdi-magnify p-l-10 p-r-10"></i>
                            </span>
                        </div>
                        <p class="text-center fs-12 p-t-5">Select your phone to make sure that you only get the content that is compatible with your device.</br>E.g. Desktop 1920x1080, Samsung Galaxy S7, iPhone 8 Plus</p>                                                    
                    </div>
                    <div class="col-xs-12 col-sm-offset-2 col-sm-8 m-t-20 m-b-20" id="device-picker-results">
                    </div>
                </div>
            </div>
            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>
    <!-- END PAGE CONTENT -->
    @include('frontend.partials.footer')
</div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @if(Sentinel::check())
    	@include('common.sessionmodal')
	@endif
@endsection
