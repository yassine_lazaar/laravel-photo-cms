@extends('frontend.master.index')
              
@section('criticalCSS')

    @include('css::criticalfrontendsettings')

@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content">
        @include('common.breadcrumb')
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid">
            <!-- BEGIN PLACE PAGE CONTENT HERE -->
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2">
                    <div class="panel">
                        <div class="panel-heading top-left top-right ">
                            <div class="panel-title">
                                <span class="font-montserrat fs-11 all-caps">
                                    Avatar
                                </span>
                            </div>
                        </div>
                        <div class="panel-body p-t-50">
                            {!! Form::open(['route'=>'postuseravatarupload', 'id' => 'avatar-upload-form', 'method' => 'post', 'class' => '', 'files' => true]) !!}
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 bg-master-lightest">
                                        <div id="image-cropper">
                                            <div class="cropit-preview relative m-x-auto">
                                                <div class="slider-wrapper pull-bottom bottom-right bottom-left bg-semitransparent padding-20">
                                                    <input type="range" class="cropit-image-zoom-input hide" min="0" max="1" step="0.01">
                                                </div>
                                                <div class="full-width full-height bg-semitransparent hide" id="avatar-loader" style="z-index: 100;">
                                                    <div class="progress">
                                                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 sm-padding-5">
                                        <div class="container-fluid  sm-m-t-15 sm-m-r-15 sm-m-l-15">
                                            <div class="form-group form-group-default input-group no-focus" id="avatar-upload-input-group">
                                                <label>Select a file</label>
                                                <input type="file" name="avatar_file" class="cropit-image-input hide" id="avatar-upload-input" accept="image/jpg,image/jpeg,image/png" readonly>
                                                <span class="input-group-addon" id="avatar-input-addon">
                                                    <i class="mdi mdi-file mdi-20px"></i>
                                                </span>
                                            </div>
                                            <a href="javascript:;" class="btn btn-info btn-block btn-lg m-b-5 disabled" id="avatar-upload-btn">
                                                <i class="mdi mdi-cloud-upload m-r-5"></i>
                                                <span>Upload</span>
                                            </a>
                                            <a href="javascript:;" class="btn delete-primary btn-block btn-lg inline m-b-10" id="avatar-delete-btn">
                                                <i class="mdi mdi-delete m-r-5"></i>
                                                <span>Delete</span>
                                            </a>
                                            <p class="small hint-text">
                                                * Only jpg, png and gif files are accepted.
                                                <br>* Upload images should be of at least 256x256 pixels.
                                                <br>* 1MB file-size limit apply.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading top-left top-right ">
                            <div class="panel-title">
                                <span class="font-montserrat fs-11 all-caps">
                                    Profile
                                </span>
                            </div>
                        </div>
                        <div class="panel-body p-t-50">
                            {!! Form::open(['route'=>'postusersettings', 'id' => 'user-settings-edit', 'method' => 'post', 'class' => '']) !!}
                            <div class="form-group-attached">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group form-group-default input-group">
                                            <span class="input-group-addon">
                                                <i class="mdi mdi-account-card-details mdi-20px"></i>
                                            </span>
                                            <label>First name</label>
                                            <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group form-group-default input-group full-width">
                                            <span class="input-group-addon hidden-sm hidden-md hidden-lg">
                                            </span>
                                            <label>Last name</label>
                                            <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group form-group-default form-group-default-select2 select2 input-group">
                                            <span class="input-group-addon">
                                                <i class="mdi mdi-gender-male-female mdi-20px"></i>
                                            </span>
                                            <label>Gender</label>
                                            <select class="full-width hide" name="gender" data-init-plugin="select2" data-disable-search="true">
                                                <option disabled selected></option>
                                                <option value="1" @if($user->gender == 1) selected="selected" @endif>Prefer not to disclose</option>
                                                <option value="2" @if($user->gender == 2) selected="selected" @endif>Male</option>
                                                <option value="3" @if($user->gender == 3) selected="selected" @endif>Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group form-group-default input-group date datepicker">
                                            <span class="input-group-addon">
                                                <i class="mdi mdi-calendar mdi-20px"></i>
                                            </span>
                                            <label>Birth date</label>
                                            <input type="text" class="form-control" name="birthdate" value="{{ $user->birthdate }}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-group-default input-group">
                                    <span class="input-group-addon">
                                        <i class="mdi mdi-home-map-marker mdi-20px"></i>
                                    </span>
                                    <label>Address</label>
                                    <input type="text" class="form-control" name="address" value="{{ $user->address }}">
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group form-group-default input-group">
                                            <span class="input-group-addon">
                                                <i class="mdi mdi-map-marker-radius mdi-20px"></i>
                                            </span>
                                            <label>Zip code</label>
                                            <input type="number" class="form-control" name="zip_code" value="{{ $user->zip_code }}" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group form-group-default input-group">
                                            <span class="input-group-addon">
                                                <i class="mdi mdi-city mdi-20px"></i>
                                            </span>
                                            <label>City</label>
                                            <input type="text" class="form-control" name="city" value="{{ $user->city }}">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group form-group-default form-group-default-select2 select2 input-group">
                                            <span class="input-group-addon">
                                                <i class="mdi mdi-earth mdi-20px"></i>
                                            </span>
                                            <label>Country</label>
                                            <select class="full-width hide" name="country" data-init-plugin="select2" data-disable-search="true">
                                                <option disabled selected></option>
                                                @foreach($countries as $key => $value)
                                                <option value="{{ $key }}" @if($user->country == $key) selected="selected" @endif>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-group-default input-group">
                                    <span class="input-group-addon">
                                        <i class="mdi mdi-phone mdi-20px"></i>
                                    </span>
                                    <label>Phone number</label>
                                    <input type="tel" class="form-control" name="phone_number" value="{{ $user->phone_number }}" placeholder="">
                                </div>
                                <div class="form-group form-group-default input-group">
                                    <span class="input-group-addon">
                                        <i class="mdi mdi-format-quote mdi-20px"></i>
                                    </span>
                                    <label>Bio</label>
                                    <input type="text" class="form-control" name="bio"  value="{{ $user->bio }}">
                                </div>
                                <div class="form-group form-group-default input-group">
                                    <span class="input-group-addon">
                                        <i class="mdi mdi-facebook mdi-20px"></i>
                                    </span>
                                    <label>Facebook handle</label>
                                    <input type="text" class="form-control" name="facebook"  value="{{ $user->facebook }}" placeholder="">
                                </div>
                                <div class="form-group form-group-default input-group">
                                    <span class="input-group-addon">
                                        <i class="mdi mdi-twitter mdi-20px"></i>
                                    </span>
                                    <label>Twitter handle</label>
                                    <input type="text" class="form-control" name="twitter"  value="{{ $user->twitter }}" placeholder="">
                                </div>
                                <div class="form-group form-group-default input-group">
                                    <span class="input-group-addon">
                                        <i class="mdi mdi-instagram mdi-20px"></i>
                                    </span>
                                    <label>Instagram handle</label>
                                    <input type="text" class="form-control" name="instagram"  value="{{ $user->instagram }}" placeholder="">
                                </div>
                                <div class="form-group form-group-default input-group">
                                    <span class="input-group-addon">
                                        <i class="mdi mdi-web mdi-20px"></i>
                                    </span>
                                    <label>Website</label>
                                    <input type="text" class="form-control" name="website"  value="{{ $user->website }}" placeholder="http://www.website.com">
                                </div>
                            </div>
                            {!! Form::submit('Save', array('type' => 'submit','class' => 'btn btn-info btn-cons full-mobile btn-lg pull-right m-t-15 xs-m-r-0'))!!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>
    <!-- END PAGE CONTENT -->
    @include('frontend.partials.footer')
</div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @include('common.sessionmodal')
@endsection
