@extends('frontend.master.index')
          
@section('criticalCSS')

    @include('css::criticalfrontend')

@endsection

@section('content')
<!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
			<div class="gallery-page">
				@include('common.breadcrumb')
				<div class="container-fluid container-fixed-lg full-width sm-p-l-15 sm-p-r-15 title-order">
					<div class="pull-left text-center">
						<h1 class="text-master m-t-15 m-b-15">{{ $gallery->name }}</h1>
					</div>
					@include('frontend.partials.galleryordering')					
					<div class="clearfix">
					</div>
				</div>
				<!-- START CATEGORY -->
				<div class="panel bg-transparent sm-p-l-10 sm-p-r-10">
					@if(!$wallpapers->isEmpty())
					<div class="gallery clearfix">
						{{ $adRendered = false }}
						@for ($i = 0; $i < $wallpapers->count(); $i++)
						<!-- START GALLERY ITEM -->
						@if ($i == 11 && !$adRendered)
						<div class="gallery-item pull-left" data-width="1" data-height="1">
							@include('frontend.ads.galleryad')
						</div>
						{{ $adRendered = true }}
						{{ $i-- }}
						<?php continue ?>
						@endif
						@if($i == 8 && $wallpapers->count() > 11)
						<div class="gallery-item pull-left" data-width="2" data-height="2">
							<a href="{{ $wallpapers[$i]->link() }}">
								<img class="lazy" src="//static3.{{ env('APP_DOMAIN') }}/image/thumb-placeholder.jpg" data-src="{{ $wallpapers[$i]->thumbnailPath() }}_big.jpg" data-src-sm="{{ $wallpapers[$i]->thumbnailPath() }}_big.jpg" alt="{{ $wallpapers[$i]->title.' Wallpaper' }}">						
								<div class="fade"></div>
								@if($wallpapers[$i]->featured)
								<div class="top-right p-t-20 p-r-20">
									<a class="fs-10 m-l-5"><span class="label  font-montserrat bg-featured text-white">&#9733;</span></a>
								</div>
								@endif									
						@else
						<div class="gallery-item pull-left" data-width="1" data-height="1">
							<a href="{{ $wallpapers[$i]->link() }}">									
								<img class="lazy" src="//static3.{{ env('APP_DOMAIN') }}/image/thumb-placeholder.jpg" data-src="{{ $wallpapers[$i]->thumbnailPath() }}_3x.jpg" data-src-sm="{{ $wallpapers[$i]->thumbnailPath() }}_big.jpg" alt="{{ $wallpapers[$i]->title.' Wallpaper' }}">							
								<div class="fade"></div>
								@if($wallpapers[$i]->featured)									
								<div class="top-right p-t-20 p-r-20">
									<a class="fs-10 m-l-5"><span class="label  font-montserrat bg-featured text-white">&#9733;</span></a>
								</div>
								@endif																		
						@endif
								<!-- START ITEM OVERLAY DESCRIPTION -->
								<div class="overlayer bottom-left full-width">
									<div class="overlayer-wrapper item-info gradient-grey p-t-50">
										<div class="wallpaper-title full-width p-r-20 p-l-20 m-b-10">
											<p class="title full-width font-montserrat text-white p-r-50 p-b-10 m-b-0">{{ $wallpapers[$i]->title }}</p>
										</div>
										<div class="uploader-stats p-l-20 p-b-20">
											<div class="thumbnail-wrapper d32 circular m-t-10">
												<img width="32" height="32" src="{{ $wallpapers[$i]->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src="{{ $wallpapers[$i]->user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src-retina="{{ $wallpapers[$i]->user->avatarPath() }}_2x.jpg?{{ $timestamp }}" alt="{{ $wallpapers[$i]->user->username }}">
											</div>
											<div class="inline m-l-10 m-t-10">
												<p class="no-margin text-white fs-11">By {{ $wallpapers[$i]->user->username }}</p>
												<p class="stats text-white fs-11 m-b-0">
													<span><i class="mdi mdi-eye"></i></span>
													<span>{{ numberAbbreviation($wallpapers[$i]->views) }}</span>
													<span><i class="mdi mdi-download m-l-5"></i></span>
													<span>{{ numberAbbreviation($wallpapers[$i]->downloads) }}</span>
													<span><i class="mdi mdi-heart m-l-5"></i></span>
													<span>{{ numberAbbreviation($wallpapers[$i]->favorites->count()) }}</span>
												</p>
											</div>
										</div>
									</div>
								</div>
								<!-- END ITEM OVERLAY DESCRIPTION -->
							</a>
							<div class="btn-favorite-container bottom-right p-r-20 p-b-20">
								<button class="btn btn-md bg-semitransparent fs-14 no-border pull-right p-r-10 p-l-10 btn-favorite" type="button" data-id="{{ encrypt($wallpapers[$i]->id) }}" data-slug="{{ $wallpapers[$i]->slug }}">
								@if($wallpapers[$i]->favorited)
									<i class="mdi fs-16 text-primary mdi-heart"></i>
								@else  
									<i class="mdi fs-16 text-primary mdi-heart-outline"></i>
								@endif
								</button>
							</div>						
						</div>
						<!-- END GALLERY ITEM -->
						@endfor
					</div>
                    <div class="text-center full-width clearfix">
                    	@include('frontend.partials.pagination.link_limit', ['paginator' => $wallpapers])						
					</div>
					@else
					@include('frontend.partials.nocontent')
					@endif
				</div>
				<!-- END CATEGORY -->
			</div>
		</div>
        <!-- END PAGE CONTENT -->
        @include('frontend.partials.footer')
    </div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
	@if(Sentinel::check())
    	@include('common.sessionmodal')
	@endif
@endsection
