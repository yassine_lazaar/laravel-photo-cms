@extends('backend.master.index')

@section('content')
<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20 hidden-xs">
    <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <a href="{{ route('getimageupload') }}">Dashboard</a>
            </li>
            <li><a href="{{ route('getusers') }}" class="active">Users</a>
            </li>
        </ul>
        <!-- END BREADCRUMB -->
    </div>
</div>
<!-- START CONTAINER FLUID -->
<div class="container-fluid">
    <!-- BEGIN PLACE PAGE CONTENT HERE -->
    <div class="panel">
        <div class="panel-heading">
            <h3>Users</h3>
            <p>Currently registered users.</p>
        </div>
        <div class="panel-body">
            <table id="usersTable" class="table responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Avatar</th>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Joined In</th>
                    <th>Role</th>
                    <th>Banned</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- END PLACE PAGE CONTENT HERE -->
</div>
<!-- END CONTAINER FLUID -->
@endsection

@section('modals')
    @include('common.sessionmodal')
    @include('backend.modals.userdeletemodal')
    @include('backend.modals.usereditmodal')
@endsection
