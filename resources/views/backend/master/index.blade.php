<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>{{ $title }}</title>
    <meta name="description" content="{{ $description }}"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <!-- BEGIN CSS-->
    {!! Html::style("//static3.".env('APP_DOMAIN')."/css/backend.min.css") !!}
    <!-- END CSS -->
</head>
<body class="fixed-header" id="notification-holder">
@include('backend.partials.sidebar')
<!-- START PAGE-CONTAINER -->
<div class="page-container">
    <!-- START PAGE HEADER WRAPPER -->
    @include('backend.partials.header')
    <!-- END PAGE HEADER WRAPPER -->
    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
            @yield('content')
        </div>
        <!-- END PAGE CONTENT -->
        @include('backend.partials.footer')
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->
@yield('modals')
@include('backend.partials.pushnotifications')
@include('common.passedJSVars')
<!-- BEGIN JS -->
{!! Html::script("//static3.".env('APP_DOMAIN')."/js/backend.min.js", array("defer")) !!}
<!-- END JS -->
</body>
</html>
