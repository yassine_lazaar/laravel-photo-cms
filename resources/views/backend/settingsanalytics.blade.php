<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        {!! Form::open(['route'=>'postanalyticssettings', 'id' => 'site-settings-analytics-edit', 'method' => 'post', 'class' => 'form-horizontal', 'autocomplete' => 'off']) !!}
        <div class="form-group">
           <label for="{{SETTING_ANALYTICS_CODE}}" class="col-sm-4 control-label">Analytics code</label>
           <div class="col-sm-8 editor">
               {{ Form::textarea(SETTING_ANALYTICS_CODE, $settings[SETTING_ANALYTICS_CODE], array('class'=>'form-control markdown')) }}
           </div>
        </div>
        <br>
        {!! Form::submit('Save', array('type' => 'submit','class' => 'btn btn-success btn-cons btn-lg pull-right m-r-0'))!!}
        {!! Form::close() !!}
    </div>
</div>