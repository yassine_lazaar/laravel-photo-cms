@extends('backend.master.index')

@section('content')
<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20 hidden-xs">
    <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <a href="{{ route('getdashboard') }}">Dashboard</a>
            </li>
            <li><a href="{{ route('gettags') }}" class="active">Tags</a>
            </li>
        </ul>
        <!-- END BREADCRUMB -->
    </div>
</div>
<!-- START CONTAINER FLUID -->
<div class="container-fluid">
    <!-- BEGIN PLACE PAGE CONTENT HERE -->
    <div class="panel">
        <div class="panel-heading">
            <h3>Tags</h3>
            <p>Currently used tags.</p>
        </div>
        <div class="panel-body">
            <table id="tagsTable" class="table responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Created by</th>
                    <th>Count</th>
                    <th>Published</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- END PLACE PAGE CONTENT HERE -->
</div>
<!-- END CONTAINER FLUID -->
@endsection

@section('modals')
    @include('common.sessionmodal')
    @include('backend.modals.tagdeletemodal')
    @include('backend.modals.tageditmodal')
@endsection
