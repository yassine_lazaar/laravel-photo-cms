<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="edit-device-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5>Edit Device</h5>
                    <div class="progress progress-small" id="edit-device-progress">
                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                    </div>
                </div>
                {!! Form::open(['route'=>'posteditdevice', 'id' => 'edit-device-form', 'method' => 'post', 'autocomplete' => 'off', 'class' => '', 'files' => true]) !!}
                <div class="modal-body" id="edit-device-modal-body">
                    <div class="form-group-attached">
                        {{ Form::text('device_id', '', array('class'=>'form-control hidden', 'id'=>'edit-device-id-input')) }}
                        <div class="form-group form-group-default">
                            <label>Device name</label>
                            {{Form::text('device_name', null, ['class' => 'form-control', 'id' => 'edit-device-name-input', 'placeholder' => 'e.g. \'iPhone SE\''])}}
                        </div>
                        <div class="form-group form-group-default">
                            <label>Device Slug (URL)</label>
                            {{ Form::text('device_slug', '', array('class'=>'form-control', 'id' => 'edit-device-slug-input', 'placeholder' => 'English characters only, space will be separated by a dash.')) }}
                        </div>
                        <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                            <label>Type</label>
                            <select class="full-width" data-init-plugin="select2" data-disable-search="true" id="edit-device-type-select2">
                                @foreach (unserialize(DEVICES_TYPES) as $k => $type)
                                    <option value="{{ $k }}">{{ $type }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                            <label>Maker</label>
                            <select class="full-width" data-init-plugin="select2" data-disable-search="true" id="edit-device-maker-select2">
                                @foreach (unserialize(MAKERS) as $maker)
                                    <option value="{{ $maker }}">{{ $maker }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                            <label>Resolution</label>
                            <select class="full-width" data-init-plugin="select2" data-disable-search="true" id="edit-device-resolution-select2"  multiple="multiple">
                                <option disabled selected></option>
                                @foreach ($resolutions as $r)
                                    <option value="{{ $r->id }}">{{ $r->width.'x'.$r->height }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group form-group-default input-group date datepicker">
                            <label>Release date</label>
                            <input type="text" class="form-control" name="edit_device_release_date" id="edit-device-release-date-input" readonly>
                            <span class="input-group-addon">
                                <i class="mdi mdi-calendar mdi-20px"></i>
                            </span>
                        </div>
                        <div class="form-group form-group-default input-group">
                            <label>Feature image</label>
                            <input type="file" class="form-control" id="edit-device-image-input" accept="image/jpg,image/jpeg,image/png" readonly>
                            <span class="input-group-addon">
                                <i class="mdi mdi-file mdi-20px"></i>
                            </span>
                        </div>
                        <div class="form-group form-group-default input-group">
                            <label class="inline" for="device-exclude">Exclude device</label>
                            <span class="input-group-addon bg-transparent">
                            <input type="checkbox" name="excluded" class="switchery hide" id="edit-device-exclude-switch" data-init-plugin="switchery" data-size="small"/>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6 p-r-5">
                            <button type="button" class="btn btn-primary btn-block btn-lg" id="edit-device-submit-button">Update</button>
                        </div>
                        <div class="col-xs-6 p-l-5">
                            <button type="button" class="btn btn-default btn-block btn-lg" id="edit-device-close-button" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
