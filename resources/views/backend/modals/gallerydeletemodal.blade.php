<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="delete-gallery-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5>Delete gallery</h5>
                    <div class="progress progress-small" id="delete-gallery-progress">
                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                    </div>
                </div>
                <div class="modal-body" id="delete-gallery-modal-body">
                    <p class="no-margin">
                        Are you sure you want to proceed?</br>
                        All data related to this gallery will be lost.
                    </p>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6 p-r-5">
                            <button type="button" class="btn btn-primary btn-block btn-lg" id="delete-gallery-submit-button">Continue</button>
                        </div>
                        <div class="col-xs-6 p-l-5">
                            <button type="button" class="btn btn-default btn-block btn-lg no-margin" id="delete-gallery-close-button" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
