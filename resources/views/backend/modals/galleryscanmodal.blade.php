<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="scan-gallery-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5>Scan for new content</h5>
                    <div class="progress progress-small" id="scan-gallery-progress">
                        <div class="progress-bar-determinate progress-bar-primary"></div>
                    </div>
                </div>
                {!! Form::open(['route'=>'postgalleryscan', 'id' => 'scan-gallery-form', 'method' => 'post', 'autocomplete' => 'off', 'class' => '']) !!}
                <div class="modal-body" id="scan-gallery-modal-body">
                    <div class="form-group form-group-default form-group-default-select2 select2 input-group">
                        <span class="input-group-addon">
                            <i class="mdi mdi-image-album mdi-20px"></i>
                        </span>
                        <label>Gallery</label>
                        <select class="full-width hide" data-init-plugin="select2" data-disable-search="true" id="scan-gallery-select2">
                            @foreach ($galleries as $g)
                                @if ($g->previous == -1){
                                    <option value="{{ $g->id }}" selected>{{ $g->name }}</option>
                                } @else {
                                    <option value="{{ $g->id }}">{{ $g->name }}</option>
                                }
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <p class="small hint-text  m-t-10">
                        * Only jpg and png files will be processed.
                        <br>* Only Images of large dimesions will be scaned ({{ $startResolutions['vertical']['width'].'x'.$startResolutions['vertical']['height'] }} or {{ $startResolutions['horizontal']['width'].'x'.$startResolutions['horizontal']['height'] }} pixels).
                        <br>* {{ $maxUploadSize }}MB file-size limit apply.
                    </p>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6 p-r-5">
                            <button type="button" class="btn btn-primary btn-block btn-lg" id="scan-gallery-submit-button">Scan</button>
                        </div>
                        <div class="col-xs-6 p-l-5">
                            <button type="button" class="btn btn-default btn-block btn-lg" id="scan-gallery--close-button" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
