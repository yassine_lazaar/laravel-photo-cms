<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="edit-gallery-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5 class="pull-left inline">Edit Gallery</h5>
                    <div class="progress progress-small pull-left inline full-width" id="edit-gallery-progress">
                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                    </div>
                </div>
                {!! Form::open(['route'=>'posteditgallery', 'id' => 'edit-gallery-form', 'method' => 'post', 'autocomplete' => 'off']) !!}
                <div class="modal-body" id="edit-gallery-modal-body">
                    <div class="form-group-attached">
                        {{ Form::text('gallery_id', '', array('class'=>'form-control hidden', 'id'=>'gallery-id-input')) }}
                        <div class="form-group form-group-default">
                            <label for="gallery_name">Gallery Name</label>
                            {{ Form::text('gallery_name', '', array('class'=>'form-control', 'id'=>'edit-gallery-name-input')) }}
                        </div>
                        <div class="form-group form-group-default">
                            <label for="gallery_slug">Gallery Slug (URL)</label>
                            {{ Form::text('gallery_slug', '', array('class'=>'form-control', 'id'=>'edit-gallery-slug-input', 'placeholder' => 'English characters only, space will be separated by a dash. e.g. \'apple\'')) }}
                        </div>
                        <div class="form-group form-group-default">
                            <label for="gallery_description">Description</label>
                            {{ Form::text('gallery_description', '', array('class'=>'form-control', 'id'=>'edit-gallery-description-input', 'placeholder' => '100 Characters max.')) }}
                        </div>
                        <div class="form-group form-group-default">
                            <label for="gallery_folder">Gallery Folder</label>
                            {{ Form::text('gallery_folder', '', array('class'=>'form-control', 'id'=>'edit-gallery-folder-input', 'placeholder' => 'English characters only, space will be separated by a dash. e.g. \'Apple\'')) }}
                        </div>
                        <div class="form-group form-group-default input-group">
                            <label class="inline" for="gallery-published">Published</label>
                            <span class="input-group-addon bg-transparent">
                            <input type="checkbox" name="gallery_published" class="switchery hide" id="edit-gallery-published-switch" data-init-plugin="switchery" data-size="small"/>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6 p-r-5">
                            <button type="button" class="btn btn-primary btn-block btn-lg" id="edit-gallery-submit-button">Update</button>
                        </div>
                        <div class="col-xs-6 p-l-5">
                            <button type="button" class="btn btn-default btn-block btn-lg" id="edit-gallery-close-button" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
