<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="edit-upload-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5 class="pull-left inline">Edit upload</h5>
                    <div class="progress progress-small pull-left inline full-width" id="edit-upload-progress">
                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                    </div>
                </div>
                {!! Form::open(['route'=>'postuploadedit', 'id' => 'edit-upload-form', 'method' => 'post', 'autocomplete' => 'off']) !!}
                <div class="modal-body flex" id="edit-upload-modal-body">
                    <div class="m-x-auto" id="upload-image-cropper">
                        <div class="m-x-auto" id="upload-cropit-preview"></div>
                        <div class="slider-wrapper p-t-20 p-b-20 p-r-5 p-l-5">
                            <input type="range" class="cropit-image-zoom-input" min="0" max="1" step="0.01">
                        </div>
                    </div>
                    <div class="xs-m-0 m-l-15 xs-m-l-0">
                        <div class="form-group-attached">
                            {{ Form::text('upload_id', '', array('class'=>'form-control hidden', 'id'=>'upload-id-input')) }}
                            <div class="form-group form-group-default">
                                <label for="upload_title">Title</label>
                                {{ Form::text('upload_title', '', array('class'=>'form-control', 'id'=>'edit-upload-title-input')) }}
                            </div>
                            <div class="form-group form-group-default">
                                <label for="upload_slug">Slug (URL)</label>
                                {{ Form::text('upload_slug', '', array('class'=>'form-control', 'id'=>'edit-upload-slug-input', 'placeholder' => 'English characters only, space will be separated by a dash. e.g. \'apple\'')) }}
                            </div>
                            <div class="form-group form-group-default">
                                <label for="upload_description">Description</label>
                                {{ Form::textarea('upload_description', '', array('class'=>'form-control', 'id'=>'edit-upload-description-input', 'placeholder' => '')) }}
                            </div>
                            <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                                <label for="upload_gallery">Gallery</label>
                                <select class="full-width hide" data-init-plugin="select2" data-disable-search="true" id="edit-upload-gallery-select2">
                                    <option disabled selected></option>
                                    @foreach ($galleries as $g)
                                        <option value="{{ $g->id }}">{{ $g->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-group-default" id="edit-upload-tags">
                                <label for="upload_tags">Tags</label>
                                <!-- {{ Form::text('upload_tags', '', array('class'=>'form-control', 'id'=>'edit-upload-tags-input', 'placeholder' => '')) }} -->
                            </div>
                            <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                                <label for="upload_license">License</label>
                                <select class="full-width hide" data-init-plugin="select2" data-disable-search="true" id="edit-upload-license-select2">
                                    <option disabled selected></option>
                                    @for ($i = 0; $i < count($licenses); $i++)
                                        <option value="{{ $i }}">{{ $licenses[$i] }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label for="upload_author">Author</label>
                                {{ Form::text('upload_author', '', array('class'=>'form-control', 'id'=>'edit-upload-author-input', 'placeholder' => 'e.g. \'Johnny Appleseed\'')) }}
                            </div>
                            <div class="form-group form-group-default">
                                <label for="upload_author_link">Author's Link</label>
                                {{ Form::text('upload_author_link', '', array('class'=>'form-control', 'id'=>'edit-upload-author-link-input', 'placeholder' => 'e.g. \'http://www.johnnyappleseed.com\'')) }}
                            </div>
                            <div class="form-group form-group-default input-group">
                                <label class="inline" for="upload-featured">Featured</label>
                                <span class="input-group-addon bg-transparent">
                                    <input type="checkbox" name="upload_featured" class="switchery hide" id="edit-upload-featured-switch" data-init-plugin="switchery" data-size="small"/>
                                </span>
                            </div>
                        </div>
                        <div class="m-t-30">
                            <div class="col-xs-6 p-r-5 p-l-0">
                                <button type="button" class="btn btn-primary btn-block btn-lg" id="edit-upload-submit-button">Publish</button>
                            </div>
                            <div class="col-xs-6 p-l-5 p-r-0">
                                <button type="button" class="btn btn-default btn-block btn-lg" id="edit-upload-close-button" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
