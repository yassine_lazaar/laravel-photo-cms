<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="discard-upload-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5>Discard upload</h5>
                    <div class="progress progress-small" id="discard-upload-progress">
                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                    </div>
                </div>
                {{ Form::text('upload_id', '', array('class'=>'form-control hidden', 'id'=>'discard-upload-id-input')) }}
                <div class="modal-body" id="discard-upload-modal-body">
                    <p class="no-margin p-b-15">
                        Are you sure you want to proceed?</br>
                        This upload will be discarded.
                    </p>
                    <div class="form-group-attached">
                        <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                            <label>Reason</label>
                            <select class="full-width" data-init-plugin="select2" data-disable-search="true" id="discard-upload-reason-select2">
                                <option disabled selected></option>
                                @foreach ($discard_reasons as $r)
                                    <option value="{{ $r->k }}">{{ $r->v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6 p-r-5">
                            <button type="button" class="btn btn-primary btn-block btn-lg" id="discard-upload-submit-button">Continue</button>
                        </div>
                        <div class="col-xs-6 p-l-5">
                            <button type="button" class="btn btn-default btn-block btn-lg no-margin" id="discard-upload-close-button" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
