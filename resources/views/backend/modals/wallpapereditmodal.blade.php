<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="edit-wallpaper-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5 class="pull-left inline">Edit wallpaper</h5>
                    <div class="progress progress-small pull-left inline full-width" id="edit-wallpaper-progress">
                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                    </div>
                </div>
                {!! Form::open(['route'=>'postwallpaperedit', 'id' => 'edit-wallpaper-form', 'method' => 'post', 'autocomplete' => 'off']) !!}
                <div class="modal-body flex" id="edit-wallpaper-modal-body">
                    <div class="m-x-auto" id="wallpaper-image-cropper">
                        <div class="m-x-auto" id="wallpaper-cropit-preview"></div>
                        <div class="slider-wrapper p-t-20 p-b-20 p-r-5 p-l-5">
                            <input type="range" class="cropit-image-zoom-input" min="0" max="1" step="0.01">
                        </div>
                    </div>
                    <div class="xs-m-0 m-l-15 xs-m-l-0">
                        <div class="form-group-attached">
                            {{ Form::text('wallpaper_id', '', array('class'=>'form-control hidden', 'id'=>'wallpaper-id-input')) }}
                            <div class="form-group form-group-default">
                                <label for="wallpaper_title">Title</label>
                                {{ Form::text('wallpaper_title', '', array('class'=>'form-control', 'id'=>'edit-wallpaper-title-input')) }}
                            </div>
                            <div class="form-group form-group-default">
                                <label for="wallpaper_slug">Slug (URL)</label>
                                {{ Form::text('wallpaper_slug', '', array('class'=>'form-control', 'id'=>'edit-wallpaper-slug-input', 'placeholder' => 'English characters only, space will be separated by a dash. e.g. \'apple\'')) }}
                            </div>
                            <div class="form-group form-group-default">
                                <label for="wallpaper_description">Description</label>
                                {{ Form::textarea('wallpaper_description', '', array('class'=>'form-control', 'id'=>'edit-wallpaper-description-input', 'placeholder' => '')) }}
                            </div>
                            <div class="form-group form-group-default" id="edit-wallpaper-tags">
                                <label for="wallpaper_tags">Tags</label>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                                <label for="wallpaper_license">License</label>
                                <select class="full-width hide" data-init-plugin="select2" data-disable-search="true" id="edit-wallpaper-license-select2">
                                    <option disabled selected></option>
                                    @for ($i = 0; $i < count($licenses); $i++)
                                        <option value="{{ $i }}">{{ $licenses[$i] }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label for="wallpaper_author">Author</label>
                                        {{ Form::text('wallpaper_author', '', array('class'=>'form-control', 'id'=>'edit-wallpaper-author-input', 'placeholder' => 'e.g. \'Johnny Appleseed\'')) }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label for="wallpaper_description">Author's Link</label>
                                        {{ Form::text('wallpaper_author_link', '', array('class'=>'form-control', 'id'=>'edit-wallpaper-author-link-input', 'placeholder' => 'e.g. \'http://www.johnnyappleseed.com\'')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-default input-group">
                                <label class="inline" for="wallpaper-published">Published</label>
                                <span class="input-group-addon bg-transparent">
                                    <input type="checkbox" name="wallpaper_published" class="switchery hide" id="edit-wallpaper-published-switch" data-init-plugin="switchery" data-size="small"/>
                                </span>
                            </div>
                            <div class="form-group form-group-default input-group">
                                <label class="inline" for="wallpaper-featured">Featured</label>
                                <span class="input-group-addon bg-transparent">
                                    <input type="checkbox" name="wallpaper_featured" class="switchery hide" id="edit-wallpaper-featured-switch" data-init-plugin="switchery" data-size="small"/>
                                </span>
                            </div>
                        </div>
                        <div class="m-t-30">
                            <div class="col-xs-6 p-r-5 p-l-0">
                                <button type="button" class="btn btn-primary btn-block btn-lg" id="edit-wallpaper-submit-button">Update</button>
                            </div>
                            <div class="col-xs-6 p-l-5 p-r-0">
                                <button type="button" class="btn btn-default btn-block btn-lg" id="edit-wallpaper-close-button" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
