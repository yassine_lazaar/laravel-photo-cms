<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="edit-user-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5>Edit User</h5>
                    <div class="progress progress-small" id="edit-user-progress">
                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                    </div>
                </div>
                {!! Form::open(['route'=>'postedituser', 'id' => 'edit-user-form', 'method' => 'post', 'autocomplete' => 'off', 'class' => '']) !!}
                <div class="modal-body" id="edit-user-modal-body">
                    <div class="row m-l-0 m-r-0 m-b-15 padding-15 bg-master-lightest text-center">
                        <div class="inline vertical-center m-r-10" id="edit-user-info-avatar">
                        </div>
                        <div class="inline vertical-center text-left p-t-0 p-l-5 sm-p-l-0 sm-p-t-5">
                            <div class="row row-sm-height">
                                <div class="col-sm-12 col-sm-height col-top">
                                    Username: <span class="hint-text small" id="edit-user-info-username"></span>
                                </div>
                            </div>
                            <div class="row row-sm-height">
                                <div class="col-sm-12 col-sm-height col-middle">
                                    Email: <span class="hint-text small" id="edit-user-info-email"></span>
                                </div>
                            </div>
                            <div class="row row-sm-height">
                                <div class="col-sm-12 col-sm-height col-bottom">
                                    Member since: <span class="hint-text small" id="edit-user-info-created-at"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-attached">
                        <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                            <label for="user-role">Role</label>
                            <select class="full-width hide" data-init-plugin="select2" data-disable-search="true" id="edit-user-role-select2">
                                <option disabled selected></option>
                                <option value="User">User</option>
                                <option value="Admin">Admin</option>
                            </select>
                        </div>
                        <div class="form-group form-group-default input-group">
                            <label class="inline" for="user-delete-avatar">Delete Avatar</label>
                            <span class="input-group-addon bg-transparent">
                            <input type="checkbox" name="user-delete-avatar" class="switchery hide" id="edit-user-delete-avatar-switch" data-init-plugin="switchery" data-size="small"/>
                            </span>
                        </div>
                        <div class="form-group form-group-default input-group">
                            <label class="inline" for="user-banned">Banned</label>
                            <span class="input-group-addon bg-transparent">
                            <input type="checkbox" name="user-banned" class="switchery hide" id="edit-user-banned-switch" data-init-plugin="switchery" data-size="small"/>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6 p-r-5">
                            <button type="button" class="btn btn-primary btn-block btn-lg" id="edit-user-submit-button">Update</button>
                        </div>
                        <div class="col-xs-6 p-r-5">
                            <button type="button" class="btn btn-default btn-block btn-lg" id="edit-user-close-button" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
