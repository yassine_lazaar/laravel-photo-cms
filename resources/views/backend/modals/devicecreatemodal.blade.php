<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="create-device-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5>Add Device</h5>
                    <div class="progress progress-small" id="create-device-progress">
                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                    </div>
                </div>
                {!! Form::open(['route'=>'postcreatedevice', 'id' => 'create-device-form', 'method' => 'post', 'autocomplete' => 'off', 'class' => '']) !!}
                <div class="modal-body" id="create-device-modal-body">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default">
                            <label>Device name</label>
                            {{Form::text('device_name', null, ['class' => 'form-control', 'id' => 'create-device-name-input', 'placeholder' => 'e.g. \'iPhone SE\''])}}
                        </div>
                        <div class="form-group form-group-default">
                            <label>Device slug</label>
                            {{Form::text('device_slug', null, ['class' => 'form-control', 'id' => 'create-device-slug-input', 'placeholder' => 'e.g. \'iphone-se\''])}}
                        </div>
                        <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                            <label>Type</label>
                            <select class="full-width" data-init-plugin="select2" data-disable-search="true" id="create-device-type-select2">
                                <option disabled selected></option>
                                @foreach (unserialize(DEVICES_TYPES) as $k => $type)
                                    <option value="{{ $k }}">{{ $type }}</option>
                                @endforeach
                            </select>
                        </div>
                         <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                            <label>Maker</label>
                            <select class="full-width" data-init-plugin="select2" data-disable-search="true" id="create-device-maker-select2">
                                <option disabled selected></option>
                                @foreach (unserialize(MAKERS) as $maker)
                                    <option value="{{ $maker }}">{{ $maker }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group form-group-default form-group-default-select2 select2 input-group full-width">
                            <label>Resolutions</label>
                            <select class="full-width" data-init-plugin="select2" data-disable-search="true" id="create-device-resolution-select2" multiple="multiple">
                                <option disabled selected></option>
                                @foreach ($resolutions as $r)
                                    <option value="{{ $r->id }}">{{ $r->width.'x'.$r->height }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group form-group-default input-group date datepicker">
                            <label>Release date</label>
                            <input type="text" class="form-control" name="create_device_release_date" id="create-device-release-date-input" readonly>
                            <span class="input-group-addon">
                                <i class="mdi mdi-calendar mdi-20px"></i>
                            </span>
                        </div>
                        <div class="form-group form-group-default input-group">
                            <label>Feature image</label>
                            <input type="file" class="form-control" id="create-device-image-input" accept="image/jpg,image/jpeg,image/png" readonly>
                            <span class="input-group-addon">
                                <i class="mdi mdi-file mdi-20px"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6 p-r-5">
                            <button type="button" class="btn btn-primary btn-block btn-lg" id="create-device-submit-button">Add device</button>
                        </div>
                        <div class="col-xs-6 p-l-5">
                            <button type="button" class="btn btn-default btn-block btn-lg" id="create-device-close-button" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
