<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="move-wallpaper-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5>Move wallpapers to a new gallery</h5>
                    <div class="progress progress-small" id="move-wallpaper-progress">
                        <div class="progress-bar-determinate progress-bar-primary"></div>
                    </div>
                </div>
                {!! Form::open(['route'=>'postwallpapermove', 'id' => 'move-wallpaper-form', 'method' => 'post', 'autocomplete' => 'off', 'class' => '']) !!}
                <div class="modal-body" id="move-wallpaper-modal-body">
                    <div class="form-group form-group-default form-group-default-select2 select2 input-group">
                        <span class="input-group-addon">
                            <i class="mdi mdi-image-album mdi-20px"></i>
                        </span>
                        <label>Gallery</label>
                        <select class="full-width hide" data-init-plugin="select2" data-disable-search="true" id="move-wallpaper-select2">
                            @foreach ($galleries as $g)
                                @if ($g->previous == -1){
                                    <option value="{{ $g->id }}" selected>{{ $g->name }}</option>
                                } @else {
                                    <option value="{{ $g->id }}">{{ $g->name }}</option>
                                }
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6 p-r-5">
                            <button type="button" class="btn btn-primary btn-block btn-lg" id="move-wallpaper-submit-button">Move</button>
                        </div>
                        <div class="col-xs-6 p-l-5">
                            <button type="button" class="btn btn-default btn-block btn-lg" id="move-wallpaper-close-button" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
