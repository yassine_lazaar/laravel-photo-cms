<!-- START MODAL -->
<div class="modal fade stick-up disable-scroll" id="edit-tag-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5 class="pull-left inline">Edit tag</h5>
                    <div class="progress progress-small pull-left inline full-width" id="edit-tag-progress">
                        <div class="progress-bar-indeterminate progress-bar-primary"></div>
                    </div>
                </div>
                {!! Form::open(['route'=>'posttagedit', 'id' => 'edit-tag-form', 'method' => 'post', 'autocomplete' => 'off']) !!}
                <div class="modal-body" id="edit-tag-modal-body">
                    <div class="form-group-attached">
                        {{ Form::text('tag_id', '', array('class'=>'form-control hidden', 'id'=>'tag-id-input')) }}
                        <div class="form-group form-group-default">
                            <label for="tag_name">Tag name</label>
                            {{ Form::text('tag_name', '', array('class'=>'form-control', 'id'=>'edit-tag-name-input')) }}
                        </div>
                        <div class="form-group form-group-default input-group">
                            <label class="inline" for="tag-published">Published</label>
                                <span class="input-group-addon bg-transparent">
                                <input type="checkbox" name="tag_published" class="switchery hide" id="edit-tag-published-switch" data-init-plugin="switchery" data-size="small"/>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-xs-6 p-r-5 p-l-0">
                        <button type="button" class="btn btn-primary btn-block btn-lg" id="edit-tag-submit-button">Update</button>
                    </div>
                    <div class="col-xs-6 p-l-5 p-r-0">
                        <button type="button" class="btn btn-default btn-block btn-lg" id="edit-tag-close-button" data-dismiss="modal">Close</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
