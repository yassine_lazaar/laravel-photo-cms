<div class="row">
    <div class="col-md-12">
        {!! Form::open(['route'=>'postgallerysettings', 'id' => 'site-settings-general-edit', 'method' => 'post', 'class' => 'form-horizontal', 'autocomplete' => 'off']) !!}
        <div class="form-group select2 with-border">
            <label for="{{SETTING_WALLPAPERS_PER_PAGE}}" class="col-sm-4 control-label">Number of images per page</label>
            <div class="col-sm-8">
                <select name="{{SETTING_WALLPAPERS_PER_PAGE}}" class="full-width hide" data-init-plugin="select2" data-disable-search="true">
                    <option value="12" {{($settings[SETTING_WALLPAPERS_PER_PAGE]== '12')?'selected':''}}>12</option>
                    <option value="16" {{($settings[SETTING_WALLPAPERS_PER_PAGE]== '16')?'selected':''}}>16</option>
                    <option value="20" {{($settings[SETTING_WALLPAPERS_PER_PAGE]== '20')?'selected':''}}>20</option>
                    <option value="24" {{($settings[SETTING_WALLPAPERS_PER_PAGE]== '24')?'selected':''}}>24</option>
                </select>
            </div>
        </div>
        <div class="form-group select2 with-border">
            <label for="{{SETTING_MAX_UPLOAD_SIZE}}" class="col-sm-4 control-label">File size upload limit (Mb)</label>
            <div class="col-sm-8">
                <select name="{{SETTING_MAX_UPLOAD_SIZE}}" class="full-width hide" data-init-plugin="select2" data-disable-search="true">
                    <option value="2" {{($settings[SETTING_MAX_UPLOAD_SIZE]== '2')?'selected':''}}>2</option>
                    <option value="3" {{($settings[SETTING_MAX_UPLOAD_SIZE]== '3')?'selected':''}}>3</option>
                    <option value="4" {{($settings[SETTING_MAX_UPLOAD_SIZE]== '4')?'selected':''}}>4</option>
                    <option value="5" {{($settings[SETTING_MAX_UPLOAD_SIZE]== '5')?'selected':''}}>5</option>
                    <option value="6" {{($settings[SETTING_MAX_UPLOAD_SIZE]== '6')?'selected':''}}>6</option>
                    <option value="7" {{($settings[SETTING_MAX_UPLOAD_SIZE]== '7')?'selected':''}}>7</option>
                    <option value="8" {{($settings[SETTING_MAX_UPLOAD_SIZE]== '8')?'selected':''}}>8</option>
                    <option value="9" {{($settings[SETTING_MAX_UPLOAD_SIZE]== '9')?'selected':''}}>9</option>
                    <option value="10" {{($settings[SETTING_MAX_UPLOAD_SIZE]== '10')?'selected':''}}>10</option>
                </select>
            </div>
        </div>
        <div class="form-group select2 with-border">
            <label for="{{SETTING_GUEST_DOWNLOAD_LIMIT}}" class="col-sm-4 control-label">Guest download limit</label>
            <div class="col-sm-8">
                <select name="{{SETTING_GUEST_DOWNLOAD_LIMIT}}" class="full-width hide" data-init-plugin="select2" data-disable-search="true">
                    <option value="10" {{($settings[SETTING_GUEST_DOWNLOAD_LIMIT]== '10')?'selected':''}}>10</option>
                    <option value="20" {{($settings[SETTING_GUEST_DOWNLOAD_LIMIT]== '20')?'selected':''}}>20</option>
                    <option value="30" {{($settings[SETTING_GUEST_DOWNLOAD_LIMIT]== '30')?'selected':''}}>30</option>
                    <option value="40" {{($settings[SETTING_GUEST_DOWNLOAD_LIMIT]== '40')?'selected':''}}>40</option>
                </select>
            </div>
        </div>
        <br>
        {!! Form::submit('Save', array('type' => 'submit','class' => 'btn btn-success btn-cons btn-lg pull-right m-r-0'))!!}
        {!! Form::close() !!}
    </div>
</div>