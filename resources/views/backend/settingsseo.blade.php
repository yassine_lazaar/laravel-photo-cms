<div class="row">
    <div class="col-md-12">
        {!! Form::open(['route'=>'postseosettings', 'id' => 'site-settings-seo-edit', 'method' => 'post', 'class' => 'form-horizontal', 'autocomplete' => 'off']) !!}
        <div class="alert alert-info">You can set a fixed title, description and meta keywords for individual pages. Define their templates here using adequate placeholders.</div>
        <div class="panel panel-transparent b-b b-grey">
            <div class="panel-heading p-l-0 p-r-0">
                <div class="panel-title">Home page</div>
            </div>
            <div class="panel-body p-l-0 p-r-0">
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_HOMEPAGE_TITLE_TEMPLATE}}" class="col-sm-4 control-label">Title template</label>
                    <div class="col-sm-8">
                        {{ Form::text(SETTING_SEO_HOMEPAGE_TITLE_TEMPLATE, $settings[SETTING_SEO_HOMEPAGE_TITLE_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_HOMEPAGE_DESCRIPTION_TEMPLATE}}" class="col-sm-4 control-label">Meta description template</label>
                    <div class="col-sm-8">
                        {{ Form::textarea(SETTING_SEO_HOMEPAGE_DESCRIPTION_TEMPLATE, $settings[SETTING_SEO_HOMEPAGE_DESCRIPTION_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-transparent b-b b-grey">
            <div class="panel-heading p-l-0 p-r-0">
                <div class="panel-title">Wallpaper</div>
            </div>
            <div class="panel-body p-l-0 p-r-0">
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_WALLPAPER_TITLE_TEMPLATE}}" class="col-sm-4 control-label">Title template</label>
                    <div class="col-sm-8">
                        {{ Form::text(SETTING_SEO_WALLPAPER_TITLE_TEMPLATE, $settings[SETTING_SEO_WALLPAPER_TITLE_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_WALLPAPER_DESCRIPTION_TEMPLATE}}" class="col-sm-4 control-label">Meta description template</label>
                    <div class="col-sm-8">
                        {{ Form::textarea(SETTING_SEO_WALLPAPER_DESCRIPTION_TEMPLATE, $settings[SETTING_SEO_WALLPAPER_DESCRIPTION_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-transparent b-b b-grey">
            <div class="panel-heading p-l-0 p-r-0">
                <div class="panel-title">Gallery</div>
            </div>
            <div class="panel-body p-l-0 p-r-0">
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_GALLERY_TITLE_TEMPLATE}}" class="col-sm-4 control-label">Title template</label>
                    <div class="col-sm-8">
                        {{ Form::text(SETTING_SEO_GALLERY_TITLE_TEMPLATE, $settings[SETTING_SEO_GALLERY_TITLE_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_GALLERY_DESCRIPTION_TEMPLATE}}" class="col-sm-4 control-label">Meta description template</label>
                    <div class="col-sm-8">
                        {{ Form::textarea(SETTING_SEO_GALLERY_DESCRIPTION_TEMPLATE, $settings[SETTING_SEO_GALLERY_DESCRIPTION_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-transparent b-b b-grey">
            <div class="panel-heading p-l-0 p-r-0">
                <div class="panel-title">Device</div>
            </div>
            <div class="panel-body p-l-0 p-r-0">
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_DEVICE_TITLE_TEMPLATE}}" class="col-sm-4 control-label">Title template</label>
                    <div class="col-sm-8">
                        {{ Form::text(SETTING_SEO_DEVICE_TITLE_TEMPLATE, $settings[SETTING_SEO_DEVICE_TITLE_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_DEVICE_DESCRIPTION_TEMPLATE}}" class="col-sm-4 control-label">Meta description template</label>
                    <div class="col-sm-8">
                        {{ Form::textarea(SETTING_SEO_DEVICE_DESCRIPTION_TEMPLATE, $settings[SETTING_SEO_DEVICE_DESCRIPTION_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-transparent b-b b-grey">
            <div class="panel-heading p-l-0 p-r-0">
                <div class="panel-title">Tag</div>
            </div>
            <div class="panel-body p-l-0 p-r-0">
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_TAG_TITLE_TEMPLATE}}" class="col-sm-4 control-label">Title template</label>
                    <div class="col-sm-8">
                        {{ Form::text(SETTING_SEO_TAG_TITLE_TEMPLATE, $settings[SETTING_SEO_TAG_TITLE_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_TAG_DESCRIPTION_TEMPLATE}}" class="col-sm-4 control-label">Meta description template</label>
                    <div class="col-sm-8">
                        {{ Form::textarea(SETTING_SEO_TAG_DESCRIPTION_TEMPLATE, $settings[SETTING_SEO_TAG_DESCRIPTION_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-transparent b-b b-grey">
            <div class="panel-heading p-l-0 p-r-0">
                <div class="panel-title">Search</div>
            </div>
            <div class="panel-body p-l-0 p-r-0">
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_SEARCH_TITLE_TEMPLATE}}" class="col-sm-4 control-label">Title template</label>
                    <div class="col-sm-8">
                        {{ Form::text(SETTING_SEO_SEARCH_TITLE_TEMPLATE, $settings[SETTING_SEO_SEARCH_TITLE_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_SEARCH_DESCRIPTION_TEMPLATE}}" class="col-sm-4 control-label">Meta description template</label>
                    <div class="col-sm-8">
                        {{ Form::textarea(SETTING_SEO_SEARCH_DESCRIPTION_TEMPLATE, $settings[SETTING_SEO_SEARCH_DESCRIPTION_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-transparent b-b b-grey">
            <div class="panel-heading p-l-0 p-r-0">
                <div class="panel-title">404 Not found</div>
            </div>
            <div class="panel-body p-l-0 p-r-0">
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_404_TITLE_TEMPLATE}}" class="col-sm-4 control-label">Title template</label>
                    <div class="col-sm-8">
                        {{ Form::text(SETTING_SEO_404_TITLE_TEMPLATE, $settings[SETTING_SEO_404_TITLE_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group no-border p-t-10 p-b-10">
                    <label for="{{SETTING_SEO_404_DESCRIPTION_TEMPLATE}}" class="col-sm-4 control-label">Meta description template</label>
                    <div class="col-sm-8">
                        {{ Form::textarea(SETTING_SEO_404_DESCRIPTION_TEMPLATE, $settings[SETTING_SEO_404_DESCRIPTION_TEMPLATE], array('class'=>'form-control')) }}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::submit('Save', array('type' => 'submit','class' => 'btn btn-success btn-cons btn-lg pull-right m-r-0'))!!}
        {!! Form::close() !!}
    </div>
</div>