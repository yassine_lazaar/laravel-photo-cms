@extends('backend.master.index')

@section('content')
<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20 hidden-xs">
    <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <a href="{{ route('getdashboard') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('getdevices') }}" class="active">Devices</a>
            </li>
        </ul>
        <!-- END BREADCRUMB -->
    </div>
</div>
<!-- START CONTAINER FLUID -->
<div class="container-fluid">
    <!-- BEGIN PLACE PAGE CONTENT HERE -->
    <div class="panel">
        <div class="panel-heading">
            <h3>Devices</h3>
            <p>A list of all supported devices.</p>
        </div>
        <div class="panel-body">
            <table id="devicesTable" class="table responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Maker</th>
                    <th>Slug</th>
                    <th>Resolutions</th>
                    <th>Release Date</th>
                    <th>Excluded</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- END PLACE PAGE CONTENT HERE -->
</div>
<!-- END CONTAINER FLUID -->
@endsection

@section('modals')
    @include('common.sessionmodal')
    @include('backend.modals.devicecreatemodal')
    @include('backend.modals.deviceeditmodal')
    @include('backend.modals.devicedeletemodal')
@endsection
