@extends('backend.master.index')

@section('content')
<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20 hidden-xs">
    <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <a href="{{ route('getdashboard') }}">Dashboard</a>
            </li>
        </ul>
        <!-- END BREADCRUMB -->
    </div>
</div>
<!-- START CONTAINER FLUID -->
<div class="container-fluid">
    <!-- BEGIN PLACE PAGE CONTENT HERE -->
    <div class="row">
        <div class="col-xs-12 col-sm-6 m-b-10">
            
        </div>
        <div class="col-xs-12 col-sm-6 m-b-10">
            
        </div>
    </div>
    <!-- END PLACE PAGE CONTENT HERE -->
</div>
<!-- END CONTAINER FLUID -->
@endsection

@section('modals')
    @include('common.sessionmodal')
@endsection