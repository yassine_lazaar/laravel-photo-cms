@extends('backend.master.index')

@section('content')
<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20 hidden-xs">
    <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <a href="{{ route('getdashboard') }}">Dashboard</a>
            </li>
            <li><a href="{{ route('getsettings') }}" class="active">Site Settings</a>
            </li>
        </ul>
        <!-- END BREADCRUMB -->
    </div>
</div>
<!-- START CONTAINER FLUID -->
<div class="container-fluid">
    <!-- BEGIN PLACE PAGE CONTENT HERE -->
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <div class="panel">
                <div class="panel-heading">
                    <h3>Settings</h3>
                    <p>Edit settings.</p>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs nav-tabs-simple" data-init-reponsive-tabs="collapse">
                        <li class="active">
                            <a data-toggle="tab" href="#general">General</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#gallery">Gallery</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#seo">SEO</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#sitemap">Sitemap</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#analytics">Analytics</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active fade in" id="general">
                            @include('backend.settingsgeneral')
                        </div>
                        <div class="tab-pane fade" id="gallery">
                            @include('backend.settingsgallery')
                        </div>
                        <div class="tab-pane fade" id="seo">
                            @include('backend.settingsseo')
                        </div>
                        <div class="tab-pane fade" id="sitemap">
                            @include('backend.settingssitemap')
                        </div>
                        <div class="tab-pane fade" id="analytics">
                            @include('backend.settingsanalytics')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden-xs hidden-sm col-md-4 full-height">
            <div class="panel full-height">
                <div class="panel-heading top-left top-right">
                    <div class="panel-title">
                        <span class="font-montserrat fs-11 all-caps">Seo template guide</span>
                    </div>
                </div>
                <div class="panel-body p-t-50">
                    <div class="seo-guide row-fluid">
                        <p>These placeholders can be included in the templates and will be replaced when a page is displayed.</p>
                        <div class="m-b-10">
                            <p><span class="semi-bold p-b-0">Home page placeholders:</span></p>
                            <span class="label">%%site_name%%</span>
                            <span class="label">%%site_description%%</span>
                            <span class="label">%%page_number%%</span>
                        </div>
                        <div class="m-b-10">
                            <p><span class="semi-bold p-b-0">Wallpaper placeholders:</span></p>
                            <span class="label">%%site_name%%</span>
                            <span class="label">%%site_description%%</span>
                            <span class="label">%%wallpaper_title%%</span>
                            <span class="label">%%wallpaper_gallery%%</span>
                            <span class="label">%%wallpaper_description%%</span>
                            <span class="label">%%wallpaper_resolution%%</span>
                            <span class="label">%%wallpaper_uploader%%</span>
                            <span class="label">%%wallpaper_author%%</span>
                        </div>
                        <div class="m-b-10">
                            <p><span class="semi-bold p-b-0">Gallery placeholders:</span></p>
                            <span class="label">%%site_name%%</span>
                            <span class="label">%%site_description%%</span>
                            <span class="label">%%gallery_name%%</span>
                            <span class="label">%%gallery_description%%</span>
                            <span class="label">%%page_number%%</span>
                        </div>
                        <div class="m-b-10">
                            <p><span class="semi-bold p-b-0">Tag placeholders:</span></p>
                            <span class="label">%%site_name%%</span>
                            <span class="label">%%site_description%%</span>
                            <span class="label">%%tag_name%%</span>
                            <span class="label">%%page_number%%</span>
                        </div>
                        <div class="m-b-10">
                            <p><span class="semi-bold p-b-0">Device placeholders:</span></p>
                            <span class="label">%%site_name%%</span>
                            <span class="label">%%site_description%%</span>
                            <span class="label">%%device_name%%</span>
                            <span class="label">%%device_maker%%</span>
                            <span class="label">%%page_number%%</span>
                        </div>
                        <div class="m-b-10">
                            <p><span class="semi-bold p-b-0">Search page placeholders:</span></p>
                            <span class="label">%%site_name%%</span>
                            <span class="label">%%site_description%%</span>
                            <span class="label">%%search_term%%</span>
                            <span class="label">%%page_number%%</span>
                        </div>
                        <div class="m-b-10">
                            <p><span class="semi-bold p-b-0">404 page placeholders:</span></p>
                            <span class="label">%%site_name%%</span>
                            <span class="label">%%site_description%%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- END PLACE PAGE CONTENT HERE -->
    </div>
</div>
<!-- END CONTAINER FLUID -->
@endsection

@section('modals')
    @include('common.sessionmodal')
@endsection
