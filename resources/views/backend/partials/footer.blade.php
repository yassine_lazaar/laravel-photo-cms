        <!-- START FOOTER -->
        <div class="container-fluid container-fixed-lg footer no-padding bg-master-light">
            <div class="padding-20">
                <ul class="text-right">
                    <li class="inline fs-11 bold m-r-10"><a href="{{ route('gethome') }}" class="text-info">&#169; {{ env('APP_NAME').' '.\Carbon\Carbon::now()->year }}</a></li>
                    <li class="inline fs-11 m-l-10 m-r-10"><a href="{{ route('getterms') }}" class="text-info">Terms &#x26; Conditions</a></li>
                    <li class="inline fs-11 m-l-10 m-r-10"><a href="{{ route('getcopyright') }}" class="text-info">Copyright Policy</a></li>
                    <li class="inline fs-11 m-l-10 m-r-10"><a href="{{ route('getprivacy') }}" class="text-info">Privacy Policy</a></li>
                    <li class="inline fs-11 m-l-10 m-r-10"><a id="scrollToTop" href="javascript:;" class="text-info">Back To Top</a></li>
                </ul>
            </div>
        </div>
        <!-- END FOOTER -->
