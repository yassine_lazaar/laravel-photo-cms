    <!-- START HEADER -->
    <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
            <!-- LEFT SIDE -->
            <div class="pull-left full-height visible-sm visible-xs">
                <!-- START ACTION BAR -->
                <div class="header-inner">
                    <a href="javascript:;" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-15" data-toggle="sidebar">
                        <span class="mdi mdi-menu fs-16"></span>
                    </a>
                </div>
                <!-- END ACTION BAR -->
            </div>
            <div class="pull-center hidden-md hidden-lg">
                <div class="header-inner">
                    <div class="brand inline">
                        <a href="{{ route('gethome') }}"><img src="//static3.{{ env('APP_DOMAIN') }}/image/logo_center.png" alt="wllppr" data-src="//static3.{{ env('APP_DOMAIN') }}/image/logo_center_2x.png" width="78" height="22" alt="Wllppr"></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MOBILE CONTROLS -->
        <!-- START DESKTOP CONTROLS -->
        <div class="pull-left sm-table hidden-xs hidden-sm">
            <div class="header-inner">
                <div class="brand">
                    <a href="{{ route('gethome') }}"><img src="//static3.{{ env('APP_DOMAIN') }}/image/logo_center.png" alt="wllppr" data-src="//static3.{{ env('APP_DOMAIN') }}/image/logo_center_2x.png" width="78" height="22" alt="Wllppr"></a>
                </div>
            </div>
        </div>
        <div class="flex hidden-xs hidden-sm">
            <div class="sm-table display-table full-width">
                <div class="header-inner">
                    <div class="no-margin no-style pull-left">
                        <ul class="">
                            
                        </ul>
                    </div>
                    <div class="no-margin no-style pull-right">
                        <ul class="">
                            <!-- START User Info-->
                            <li class="inline">
                                <span class="m-r-15">{{ $user->username }}</span>
                            </li>
                            <!-- END User Info-->
                        </ul>
                    </div>
                </div>
                <div class="header-inner dropdown pull-right">
                    <button class="profile-dropdown-toggle full-height" type="button" data-toggle="dropdown">
                        <span class="thumbnail-wrapper d32 circular bordered b-grey">
                            <img src="{{ $user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src="{{ $user->avatarPath() }}_1x.jpg?{{ $timestamp }}" data-src-retina="{{ $user->avatarPath() }}_2x.jpg?{{ $timestamp }}" width="32" height="32" alt="{{ $user->username }}">
                        </span>
                    </button>
                    <ul class="dropdown-menu profile-dropdown" role="menu">
                        <li><a href="{{ route('getuser', ['username' => $user->username]) }}"><i class="p-r-5 mdi mdi-account-circle fs-16"></i> My Profile</a>
                        </li>
                        <li><a href="{{ route('getuseruploads', ['username' => $user->username]) }}"><i class="p-r-5 mdi mdi-image-multiple fs-16"></i> My Uploads</a>
                        </li>
                        <li><a href="{{ route('getuserfavorites', ['username' => $user->username]) }}"><i class="p-r-5 mdi mdi-heart fs-16"></i> My Favorites</a>
                        </li>
                        <li><a href="{{ route('getusersettings') }}"><i class="p-r-5 mdi mdi-settings fs-16"></i> Settings</a>
                        </li>
                        <li class="bg-master-lightest"><a href="{{ route('getlogout') }}"><i class="p-r-5 mdi mdi-logout-variant fs-16"></i> Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END DESKTOP CONTROLS -->
    </div>
    <!-- END HEADER -->
