@if(Session::has('success'))
    <div class="pgn-wrapper" data-position="top">
        <div class="pgn pgn-bar">
            <div class="alert alert-success m-b-5">
                <button class="close" data-dismiss="alert"></button>
                {{ Session::get('success') }}
            </div>
        </div>
    </div>
@endif
@if(Session::has('errors'))
    <div class="pgn-wrapper" data-position="top">
        <div class="pgn pgn-bar">
            <div class="alert alert-danger m-b-5">
                <button class="close" data-dismiss="alert"></button>
                {{ Session::get('errors')->first() }}
            </div>
        </div>
    </div>
@endif
@if(Session::has('warning'))
    <div class="pgn-wrapper" data-position="top">
        <div class="pgn pgn-bar">
            <div class="alert alert-warning m-b-5">
                <button class="close" data-dismiss="alert"></button>
                {{ Session::get('warning') }}
            </div>
        </div>
    </div>
@endif
@if(Session::has('default'))
    <div class="pgn-wrapper" data-position="top">
        <div class="pgn pgn-bar">
            <div class="alert alert-default m-b-5">
                <button class="close" data-dismiss="alert"></button>
                {{ Session::get('default') }}
            </div>
        </div>
    </div>
@endif
@if(Session::has('info'))
    <div class="pgn-wrapper" data-position="top">
        <div class="pgn pgn-bar">
            <div class="alert alert-info m-b-5">
                <p class="pull-left">{{ Session::get('info') }}</p>
                <button class="close" data-dismiss="alert"></button>
                @if(Session::has('link'))
                    <p class="pull-right bold no-margin">
                        <a class="alert-link" href="{{ Session::get('link') }}">{{ Session::get('linktext') }}</a>
                    </p>
                @endif
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endif