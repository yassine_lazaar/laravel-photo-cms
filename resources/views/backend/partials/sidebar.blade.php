 <!-- BEGIN SIDEPANEL-->
<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR HEADER -->
    <div class="sidebar-header">
        <a href="{{ route('gethome') }}"><img src="//static3.{{ env('APP_DOMAIN') }}/image/logo_white.png" alt="logo" class="brand" data-src="//static3.{{ env('APP_DOMAIN') }}/image/logo_white.png" data-src-retina="//static3.{{ env('APP_DOMAIN') }}/image/logo_white_2x.png" width="78" height="22" alt="Wllppr"></a>
    </div>
    <!-- END SIDEBAR HEADER -->
    <!-- BEGIN SIDEBAR MENU -->
    <div class="sidebar-menu">
        <ul class="menu-items">
            <li class="m-t-30">
                <a href="{{ route('getdashboard') }}">
                    <span class="title">Dashboard</span>
                </a>
                <span class="icon-thumbnail"><i class="mdi mdi-view-dashboard"></i></span>
            </li>
            <li class="">
                <a href="{{ route('getusers') }}">
                    <span class="title">Users</span>
                </a>
                <span class="icon-thumbnail"><i class="mdi mdi-account-multiple"></i></span>
            </li>
            <li class="">
                <a href="{{ route('getuploads') }}">
                    <span class="title">Uploads</span>
                </a>
                <span class="icon-thumbnail"><i class="mdi mdi-folder-upload"></i></span>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Settings</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail "><i class="mdi mdi-settings-box"></i></span>
                <ul class="sub-menu">
                    <li class="sub-menu-to-tab">
                        <a href="{{ route('getsettings') }}#general">General</a>
                        <span class="icon-thumbnail">Ge</span>
                    </li>
                    <li class="sub-menu-to-tab">
                        <a href="{{ route('getsettings') }}#gallery">Gallery</a>
                        <span class="icon-thumbnail">Ga</span>
                    </li>
                    <li class="sub-menu-to-tab">
                        <a href="{{ route('getsettings') }}#seo">Search Engine Opt.</a>
                        <span class="icon-thumbnail">Se</span>
                    </li>
                    <li class="sub-menu-to-tab">
                        <a href="{{ route('getsettings') }}#sitemap">Sitemap</a>
                        <span class="icon-thumbnail">Si</span>
                    </li>
                    <li class="sub-menu-to-tab">
                        <a href="{{ route('getsettings') }}#analytics">Analytics</a>
                        <span class="icon-thumbnail">An</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="{{ route('getgalleries') }}">
                    <span class="title">Galleries</span>
                </a>
                <span class="icon-thumbnail "><i class="mdi mdi-image-album"></i></span>
            </li>
            <li class="">
                <a href="{{ route('getwallpapers') }}">
                    <span class="title">Wallpapers</span>
                </a>
                <span class="icon-thumbnail "><i class="mdi mdi-image"></i></span>
            </li>
            <li class="">
                <a href="{{ route('gettags') }}">
                    <span class="title">Tags</span>
                </a>
                <span class="icon-thumbnail "><i class="mdi mdi-tag-multiple"></i></span>
            </li>
            <li class="">
                <a href="{{ route('getdevices') }}">
                    <span class="title">Devices</span>
                </a>
                <span class="icon-thumbnail "><i class="mdi mdi-cellphone-iphone"></i></span>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>
<!-- END SIDEPANEL-->