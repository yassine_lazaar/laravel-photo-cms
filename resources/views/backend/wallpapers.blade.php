@extends('backend.master.index')

@section('content')
<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20 hidden-xs">
    <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <a href="{{ route('getdashboard') }}">Dashboard</a>
            </li>
            <li><a href="{{ route('getwallpapers') }}" class="active">Wallpapers</a>
            </li>
        </ul>
        <!-- END BREADCRUMB -->
    </div>
</div>
<!-- START CONTAINER FLUID -->
<div class="container-fluid">
    <!-- BEGIN PLACE PAGE CONTENT HERE -->
    <div class="panel">
        <div class="panel-heading">
            <h3>Wallpapers</h3>
        </div>
        <div class="panel-body">
            <div class="row m-b-5">
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group form-group-default input-group">
                        <span class="input-group-addon">
                            <i class="mdi mdi-magnify mdi-20px"></i>
                        </span>
                        <label>Title</label>
                        <input type="text" class="form-control" name="filter_title" id="wallpapers-filter-title">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group form-group-default input-group">
                        <span class="input-group-addon">
                            <i class="mdi mdi-account-search mdi-20px"></i>
                        </span>
                        <label>User</label>
                        <input type="text" class="form-control" name="filter_user" id="wallpapers-filter-user">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group form-group-default form-group-default-select2 select2 input-group">
                        <span class="input-group-addon">
                            <i class="mdi mdi-image-album mdi-20px"></i>
                        </span>
                        <label>Gallery</label>
                        <select class="full-width hide" data-init-plugin="select2" data-disable-search="true" id="wallpapers-filter-gallery-select2">
                            <option disabled selected></option>
                            @foreach ($galleries as $g)
                                <option value="{{ $g->id }}">{{ $g->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group form-group-default form-group-default-select2 select2 input-group">
                        <span class="input-group-addon">
                            <i class="mdi mdi-move-resize mdi-20px"></i>
                        </span>
                        <label>Resolution</label>
                        <select class="full-width hide" data-init-plugin="select2" data-disable-search="true" id="wallpapers-filter-resolution-select2">
                            <option disabled selected></option>
                            @foreach ($resolutions as $r)
                                <option value="{{ $r->id }}">{{ $r->width.'x'.$r->height }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <table id="wallpapersTable" class="table responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Thumbnail</th>
                    <th>Title</th>
                    <th>Slug</th>
                    <th>Uploader</th>
                    <th>Gallery</th>
                    <th>Resolution</th>
                    <th>Devices</th>
                    <th>Tags</th>
                    <th>Published</th>
                    <th>Featured</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- END PLACE PAGE CONTENT HERE -->
</div>
<!-- END CONTAINER FLUID -->
@endsection

@section('modals')
    @include('common.sessionmodal')
    @include('backend.modals.galleryscanmodal')
    @include('backend.modals.wallpapermovemodal')
    @include('backend.modals.wallpaperdeletemodal')
    @include('backend.modals.wallpapereditmodal')
    @include('backend.modals.wallpaperpublishmodal')
@endsection
