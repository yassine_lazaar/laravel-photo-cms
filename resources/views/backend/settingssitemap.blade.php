<div class="row">
    <div class="col-md-12">
        {!! Form::open(['route'=>'postsitemapsettings', 'id' => 'site-settings-edit', 'method' => 'post', 'class' => 'form-horizontal', 'autocomplete' => 'off']) !!}
        <div class="form-group">
            <label for="{{SETTING_SITEMAP_INCLUDE_IMAGES}}" class="col-sm-4 control-label p-t-0">Include preview images</label>
            <div class="col-sm-8">
                <div class="pull-right switch">
                    <input name="{{SETTING_SITEMAP_INCLUDE_IMAGES}}" type="checkbox" class="switchery hide" data-init-plugin="switchery" data-size="small" {{(strcmp($settings[SETTING_SITEMAP_INCLUDE_IMAGES] , 'on') == 0)?'checked':''}}/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="{{SETTING_SITEMAP_INCLUDE_GALLERIES}}" class="col-sm-4 control-label p-t-0">Include galleries</label>
            <div class="col-sm-8">
                <div class="pull-right switch">
                    <input name="{{SETTING_SITEMAP_INCLUDE_GALLERIES}}" type="checkbox" class="switchery hide" data-init-plugin="switchery" data-size="small" {{(strcmp($settings[SETTING_SITEMAP_INCLUDE_GALLERIES], 'on') == 0)?'checked':''}} />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-4 control-label p-t-0">Include devices</label>
            <div class="col-sm-8">
                <div class="pull-right switch">
                    <input name="{{SETTING_SITEMAP_INCLUDE_DEVICES}}" type="checkbox" class="switchery hide" data-init-plugin="switchery" data-size="small" {{(strcmp($settings[SETTING_SITEMAP_INCLUDE_DEVICES], 'on') == 0)?'checked':''}} />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="{{SETTING_SITEMAP_INCLUDE_TAGS}}" class="col-sm-4 control-label p-t-0">Include tags</label>
            <div class="col-sm-8">
                <div class="pull-right switch">
                    <input name="{{SETTING_SITEMAP_INCLUDE_TAGS}}" type="checkbox" class="switchery hide" data-init-plugin="switchery" data-size="small" {{(strcmp($settings[SETTING_SITEMAP_INCLUDE_TAGS], 'on') == 0)?'checked':''}} />
                </div>
            </div>
        </div>
        <br>
        {!! Form::submit('Generate', array('type' => 'submit','class' => 'btn btn-success btn-cons btn-lg pull-right m-r-0'))!!}
        {!! Form::close() !!}
    </div>
</div>