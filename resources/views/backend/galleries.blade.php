@extends('backend.master.index')

@section('content')
    <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20 hidden-xs">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('getdashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="{{ route('getgalleries') }}">Galleries</a>
                </li>
            </ul>
            <!-- END BREADCRUMB -->
        </div>
    </div>
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid">
        <!-- BEGIN PLACE PAGE CONTENT HERE -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h3>Galleries</h3>
                        <p>You can rearrange existing galleries by dragging and dropping below.</p>
                    </div>
                    <div class="panel-body">
                        <ul class="" id="galleries-list">
                            @if (isset($galleries))
                                @foreach ($galleries as $g)
                                    <li class="b-a b-grey b-rad-sm m-t-5" data-id="{{ $g->id }}">
                                        <span class="mdi mdi-reorder-horizontal mdi-24px handle m-r-10 m-l-5"></span>
                                    <span class="list-item gallery-name">
                                        <a href="javascript:;" class="edit-gallery-button" data-toggle="modal" data-target="@if(!$g->protected) #edit-gallery-modal @endif" data-id="{{ $g->id }}" data-name="{{ $g->name }}" data-slug="{{ $g->slug }}" data-description="{{ $g->description }}" data-folder="{{ $g->folder }}" data-published="{{ $g->published }}">
                                            {{ $g->name }}@if(!$g->published) (unpublished) @endif
                                        </a>
                                    </span>
                                    <span class="pull-right p-r-10 p-l-10">
                                        <a href="javascript:;" class="delete-gallery-button @if($g->protected) hide @endif" data-toggle="modal" data-target="#delete-gallery-modal" data-id="{{ $g->id }}" data-name="{{ $g->name }}">
                                            ✖
                                        </a>
                                    </span>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <p>Add a new gallery</p>
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['route'=>'postcreategallery', 'id' => 'create-gallery-form', 'method' => 'post', 'autocomplete' => 'off', 'class' => '']) !!}
                        <div class="form-group-attached">
                            <div class="form-group form-group-default">
                                <label for="gallery_name">Gallery name</label>
                                {{ Form::text('gallery_name', null, ['class' => 'form-control', 'id' => 'create-gallery-name-input', 'placeholder' => 'e.g. \'Apple\'']) }}
                            </div>
                            <div class="form-group form-group-default">
                                <label for="gallery_slug">Gallery Slug (URL)</label>
                                {{ Form::text('gallery_slug', '', ['class'=>'form-control', 'id'=>'create-gallery-slug-input', 'placeholder' => 'English characters only, space will be separated by a dash. e.g. \'apple\'']) }}
                            </div>
                            <div class="form-group form-group-default">
                                <label for="gallery_description">Gallery Description</label>
                                {{ Form::text('gallery_description', '', ['class'=>'form-control', 'id'=>'create-gallery-description-input', 'placeholder' => '100 Characters max.']) }}
                            </div>
                            <div class="form-group form-group-default input-group">
                                <label for="gallery_folder">Gallery Folder</label>
                                {{ Form::text('gallery_folder', null, ['class' => 'form-control', 'id' => 'create-gallery-folder-input', 'placeholder' => 'English characters only, space will be separated by a dash. e.g. \'Apple\'']) }}
                                <span class="input-group-addon">
                                <i class="mdi mdi-folder mdi-20px"></i>
                            </span>
                            </div>
                        </div>
                        <br>
                        {!! Form::submit('Add A New Gallery', array('type' => 'submit','class' => 'btn btn-primary btn-lg btn-block', 'id' => 'create-gallery-submit-button')) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
@endsection

@section('modals')
    @include('common.sessionmodal')
    @include('backend.modals.galleryeditmodal')
    @include('backend.modals.gallerydeletemodal')
@endsection
