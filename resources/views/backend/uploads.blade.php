@extends('backend.master.index')

@section('content')
<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20 hidden-xs">
    <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <a href="{{ route('getdashboard') }}">Dashboard</a>
            </li>
            <li><a href="{{ route('getuploads') }}" class="active">Uploads</a>
            </li>
        </ul>
        <!-- END BREADCRUMB -->
    </div>
</div>
<!-- START CONTAINER FLUID -->
<div class="container-fluid">
    <!-- BEGIN PLACE PAGE CONTENT HERE -->
    <div class="panel">
        <div class="panel-heading">
            <h3>Uploads</h3>
        </div>
        <div class="panel-body">
            <table id="uploadsTable" class="table responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Thumbnail</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Uploader</th>
                    <th>Gallery</th>
                    <th>Tags</th>
                    <th>Resolution</th>
                    <th>Compatible Resolutions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- END PLACE PAGE CONTENT HERE -->
</div>
<!-- END CONTAINER FLUID -->
@endsection

@section('modals')
    @include('common.sessionmodal')
    @include('backend.modals.uploadeditmodal')
    @include('backend.modals.uploaddiscardmodal')
@endsection
