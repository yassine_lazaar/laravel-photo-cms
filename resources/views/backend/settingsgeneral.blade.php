<div class="row">
    <div class="col-md-12">
        {!! Form::open(['route'=>'postgeneralsettings', 'id' => 'site-settings-general-edit', 'method' => 'post', 'class' => 'form-horizontal', 'autocomplete' => 'off']) !!}
        <div class="form-group">
            <label for="{{SETTING_SITE_NAME}}" class="col-sm-3 control-label">Site Name</label>
            <div class="col-sm-9">
                {{ Form::text(SETTING_SITE_NAME, $settings[SETTING_SITE_NAME], array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
            <label for="{{SETTING_SITE_DESCRIPTION}}" class="col-sm-3 control-label">Site description</label>
            <div class="col-sm-9">
                {{ Form::text(SETTING_SITE_DESCRIPTION, $settings[SETTING_SITE_DESCRIPTION], array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
            <label for="{{SETTING_SITE_TOS}}" class="col-sm-3 control-label">Terms of services</label>
            <div class="col-sm-9 editor">
                <ul class="nav nav-tabs nav-tabs-simple">
                    <li class="active pull-right">
                        <a data-toggle="tab" href="#tab2toshtml">Html</a>
                    </li>
                    <li class="pull-right">
                        <a data-toggle="tab" href="#tab2tospreview">Preview</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2toshtml">
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::textarea(SETTING_SITE_TOS, $settings[SETTING_SITE_TOS], array('class'=>'form-control markdown')) }}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2tospreview">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="{{SETTING_SITE_PRIVACY_POLICY}}" class="col-sm-3 control-label">Privacy</label>
            <div class="col-sm-9 editor">
                <ul class="nav nav-tabs nav-tabs-simple">
                    <li class="active pull-right">
                        <a data-toggle="tab" href="#tab2privacyhtml">Html</a>
                    </li>
                    <li class="pull-right">
                        <a data-toggle="tab" href="#tab2privacypreview">Preview</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2privacyhtml">
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::textarea(SETTING_SITE_PRIVACY_POLICY, $settings[SETTING_SITE_PRIVACY_POLICY], array('class'=>'form-control markdown')) }}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2privacypreview">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="{{SETTING_SITE_COPYRIGHT_POLICY}}" class="col-sm-3 control-label">Copyright Policy</label>
            <div class="col-sm-9 editor">
                <ul class="nav nav-tabs nav-tabs-simple">
                    <li class="active pull-right">
                        <a data-toggle="tab" href="#tab2copyrighthtml">Html</a>
                    </li>
                    <li class="pull-right">
                        <a data-toggle="tab" href="#tab2copyrightpreview">Preview</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2copyrighthtml">
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::textarea(SETTING_SITE_COPYRIGHT_POLICY, $settings[SETTING_SITE_COPYRIGHT_POLICY], array('class'=>'form-control markdown')) }}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2copyrightpreview">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="{{SETTING_SITE_FAQ}}" class="col-sm-3 control-label">About</label>
            <div class="col-sm-9 editor">
                <ul class="nav nav-tabs nav-tabs-simple">
                    <li class="active pull-right">
                        <a data-toggle="tab" href="#tab2abouthtml">Html</a>
                    </li>
                    <li class="pull-right">
                        <a data-toggle="tab" href="#tab2aboutpreview">Preview</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2abouthtml">
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::textarea(SETTING_SITE_ABOUT, $settings[SETTING_SITE_ABOUT], array('class'=>'form-control markdown')) }}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2aboutpreview">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="{{SETTING_SITE_ABOUT}}" class="col-sm-3 control-label">FAQ</label>
            <div class="col-sm-9 editor">
                <ul class="nav nav-tabs nav-tabs-simple">
                    <li class="active pull-right">
                        <a data-toggle="tab" href="#tab2faqhtml">Html</a>
                    </li>
                    <li class="pull-right">
                        <a data-toggle="tab" href="#tab2faqpreview">Preview</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2faqhtml">
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::textarea(SETTING_SITE_FAQ, $settings[SETTING_SITE_FAQ], array('class'=>'form-control markdown')) }}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2faqpreview">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        {!! Form::submit('Save', array('type' => 'submit','class' => 'btn btn-success btn-cons btn-lg pull-right m-r-0'))!!}
        {!! Form::close() !!}
    </div>
</div>