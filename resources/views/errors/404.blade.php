@extends('frontend.master.index')

@section('criticalCSS')

    @include('css::criticalfrontend')

@endsection

@section('content')
<!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
			@include('common.breadcrumb')
			<div class="container-fluid">
				<div class="panel bg-transparent">
					<h3 class="text-center">Oops!</h3>
					<div class="text-center full-width m-t-20">
                    	<img class="" src="//static3.{{ env('APP_DOMAIN') }}/image/404-not-found.png" width="150" height="150">					
						<h4>Sorry! We can’t find that page.</h4>
						<p><em>The page you are looking for was either not found or does not exist.</br>Try the search feature instead.</em></p>
					</div>
				</div>
			</div>
		</div>
        <!-- END PAGE CONTENT -->
        @include('frontend.partials.footer')
    </div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('modals')
    @include('common.sessionmodal')
@endsection
