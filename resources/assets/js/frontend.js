$(document).ready(function() {

    // Ajax setup
    window.ajaxEnabled = true;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function() {
            return window.ajaxEnabled;
        }
    });

    var avatarFileSizeLimit = 1;
    var avatarWidthLimit = 256;
    var avatarHeightLimit = 256;

    // Init
    var $notificationsHolder = $('body');
    var $select2 = $('.select2');
    var $datepicker = $('.datepicker');

    showHiddenElements();

    // Datepicker
    $datepicker.datepicker({
            autoclose: true,
            format: 'mm-dd-yyyy',
            clearBtn: true
        })
        .on('hide', function() {
            $(this).removeClass('focused');
        })
        .on('changeDate', function() {
            $(this).find('label').addClass('fade');
        });

    $select2.on('select2-open', function() { // hide when using select2
        $datepicker.datepicker('hide');
        $('.datepicker input').blur();
    });

    // Select2
    $select2.on('select2-selecting', function() {
            $(this).addClass('option-selected');
            $(this).find('label').addClass('fade');
        })
        .on('select2-opening', function() {
            $(':focus').blur();
            $(this).addClass('focused');
        })
        .on('select2-close', function() {
            $_this = $(this);
            setTimeout(function() {
                $_this.removeClass('focused');
                $_this.find(':focus').blur();
            }, 1);
        })
        .on('click touchend', function() {
            $(this).find('select').select2('open');
        });

    $('.select2 input').prop('readonly', true); // dirty fix for mobile keyboard


    // Hidden Inputs
    function showHiddenElements() {
        $('.select2 select').removeClass('hide');
        $('.switchery input[type=checkbox]').removeClass('hide');
    }

    // Download Dropdown
    $devicesList = $('#download-dropdown-list');
    $devicesList.scrollbar({
        ignoreMobile: false,
        onInit: function() {
            $('#download-dropdown .scroll-wrapper, #download-dropdown .scroll-content').removeClass('hide');
        }
    });


    // Search overlay
    // Initializes search overlay plugin.
    // Replace onSearchSubmit() and onKeyEnter() with
    // your logic to perform a search and display results
    var searchRequests = {};
    var index = 1;
    $('[data-pages="search"]').search({
        searchField: '#overlay-search',
        submitButton: '.search-submit',
        closeButton: '.overlay-close',
        suggestions: '#overlay-suggestions',
        brand: '.brand',
        onSearchSubmit: function(searchString) {
            if (searchString.length == 0)
                return;
            if (searchRequests.hasOwnProperty('searchRequest' + (index - 1))) {
                searchRequests['searchRequest' + (index - 1)].abort();
                delete searchRequests['searchRequest' + (index - 1)];
            }
            window.location = '/search/' + searchString;
        },
        onKeyEnter: function(searchString) {
            $suggestions = $('#overlay-suggestions');
            if (searchString.length > 2) {
                searchRequests['searchRequest' + index] = $.ajax({
                        type: 'get',
                        dataType: 'json',
                        url: '/search-suggest/' + searchString,
                        beforeSend: function() {
                            if (searchRequests.hasOwnProperty('searchRequest' + (index - 1))) {
                                searchRequests['searchRequest' + (index - 1)].abort();
                                delete searchRequests['searchRequest' + (index - 1)];
                            }
                            index++;
                        },
                        success: function(data) {
                            console.log(data);
                            $suggestions.empty();
                            if (data.results.length > 0) {
                                $suggestions.empty();
                                i = 1;
                                data.results.forEach(function(e) {
                                    suggestion = e['title'];
                                    suggestionUrl = '/' + e['slug'] + '.htm';
                                    $suggestions.append('<a class="btn btn-sm btn-info m-b-10 m-r-5 m-l-5" href="' + suggestionUrl + '">' + suggestion + '</a>').hide().fadeIn(300 * i);
                                    i++;
                                });
                            }
                        },
                        error: function(data) {},
                        complete: function() {
                            refreshSessionModal();
                        }
                    },
                    'json');
            } else {
                //console.log(searchRequests);
                if (searchRequests.hasOwnProperty('searchRequest' + (index - 1))) {
                    searchRequests['searchRequest' + (index - 1)].abort();
                    delete searchRequests['searchRequest' + (index - 1)];
                }
                $suggestions.empty();
            }
        },
        onCloseOverlay: function() {
            if (searchRequests.hasOwnProperty('searchRequest' + (index - 1))) {
                searchRequests['searchRequest' + (index - 1)].abort();
                delete searchRequests['searchRequest' + (index - 1)];
            }
            $suggestions.empty();
        }
    });

    // Avatar upload
    var $avatarUploadForm = $('#avatar-upload-form');
    var $avatarDeleteBtn = $avatarUploadForm.find('#avatar-delete-btn');
    var $avatarUploadBtn = $avatarUploadForm.find('#avatar-upload-btn');
    var $avatarUploadGroupInput = $avatarUploadForm.find('#avatar-upload-input-group');
    var $avatarUploadInput = $avatarUploadGroupInput.find('#avatar-upload-input');
    var $imageCropper = $avatarUploadForm.find('#image-cropper');
    var $imageCropperPreview = $imageCropper.find('.cropit-preview');
    var $avatarUploadLoader = $imageCropper.find('#avatar-loader');

    $imageCropper.cropit({
        $preview: $imageCropperPreview,
        width: avatarWidthLimit,
        height: avatarHeightLimit,
        initialZoom: 0,
    });

    // init
    initAvatarUploadStyle();
    enableAvatarUploadInput();
    disableAvatarCropit();

    $avatarUploadBtn.on('click touchend', function(event) {
        event.preventDefault();
        abortSessionCheck();
        $avatarUploadBtn.addClass('disabled');
        $avatarDeleteBtn.addClass('disabled');
        showAvatarUploadLoader();
        disableAvatarCropit();
        disableAvatarUploadInput();
        uploadAvatar();
    });

    $avatarDeleteBtn.on('click touchend', function(event) {
        event.preventDefault();

    });

    $avatarUploadInput
        .removeClass('hide')
        .click(function(e) {
            e.stopPropagation();
            $avatarUploadInput.val('');
        })
        .filestyle({
            icon: false,
            buttonText: ''
        });

    $avatarUploadInput.on('change', function() {
        if (this.files && this.files[0]) {
            // File size
            var file = this.files[0];
            if ((file.size / (1024 * 1024)) > avatarFileSizeLimit) {
                showNotification('warning', 'Uploaded file must not exceed ' + avatarFileSizeLimit + ' MB');
                return;
            }

            // Wrong file type
            var fileType = file["type"];
            var validImageTypes = ["image/jpg", "image/jpeg", "image/png"];
            if ($.inArray(fileType, validImageTypes) < 0) {
                showNotification('warning', 'Selection is not a valid image file.');
                return;
            }

            // Reading error
            var reader = new FileReader();
            reader.onerror = function(e) {
                showNotification('warning', 'Cound not read the selected file.');
            };

            reader.onload = function(e) {
                // Dimensions
                var image = new Image();
                image.src = e.target.result;

                image.onload = function() {
                    if (this.width < avatarWidthLimit || this.height < avatarHeightLimit) {
                        showNotification('warning', 'Please use an image that\'s at least ' + avatarWidthLimit + 'px in width and ' + avatarHeightLimit + 'px in height.');
                        return;
                    }

                    $avatarDeleteBtn.removeClass('disabled');
                    $avatarUploadBtn.removeClass('disabled');
                    //console.log(reader.result);
                    $imageCropper.cropit('imageSrc', reader.result);
                    enableAvatarCropit();
                };
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    function uploadAvatar() {
        var params = $imageCropper.cropit('offset');
        params['zoom'] = $imageCropper.cropit('zoom');
        var formData = new FormData($('#avatar-upload-form')[0]);
        formData.append('avatar_file', $avatarUploadInput[0].files[0]);
        formData.append('params', JSON.stringify(params));
        return $.ajax({
            type: 'post',
            processData: false,
            contentType: false,
            url: '/avatarupload',
            data: formData,
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                return xhr;
            },
            success: function(data) {
                console.log(data);
                showNotification('success', data.results);
            },
            error: function(data) {
                console.log(data);
                $avatarUploadBtn.removeClass('disabled');
                showNotification('error', data.responseJSON.results);
            },
            complete: function() {
                $avatarDeleteBtn.removeClass('disabled');
                hideAvatarUploadLoader();
                enableAvatarUploadInput();
                refreshSessionModal();
            }
        }, 'json');
    }

    function initAvatarUploadStyle() {
        $('.cropit-image-zoom-input').removeClass('hide');
        if (typeof(user_avatar) !== 'undefined') {
            $imageCropperPreview.css({
                'background-image': 'url("//static3.' + window.location.hostname + '/avatar/' + user_avatar + '_4x.jpg?' + Date.now() + '")'
            });
        }
    }

    function disableAvatarCropit() {
        $imageCropper.cropit('disable');
        $imageCropper.find('.slider-wrapper').hide();
    }

    function enableAvatarCropit() {
        $imageCropper.cropit('reenable');
        $imageCropper.find('.slider-wrapper').show();
    }

    function enableAvatarUploadInput() {
        $avatarUploadGroupInput.on('click touchstart', function(e) {
            $avatarUploadInput.click();
        });
    }

    function disableAvatarUploadInput() {
        $avatarUploadGroupInput.off('click touchstart');
    }

    function showAvatarUploadLoader() {
        $avatarUploadLoader.removeClass('hide');
    }

    function hideAvatarUploadLoader() {
        $avatarUploadLoader.addClass('hide');
    }

    // Delete avatar
    $avatarDeleteBtn.on('click touchend', function(event) {
        event.preventDefault();
        abortSessionCheck();
        $avatarUploadBtn.addClass('disabled');
        $avatarDeleteBtn.addClass('disabled');
        showAvatarUploadLoader();
        disableAvatarCropit();
        disableAvatarUploadInput();
        deleteAvatar();
    });

    function deleteAvatar() {
        return deleteAvatarRequest = $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/avatardelete',
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                return xhr;
            },

            success: function(data) {
                console.log(data);
                console.log(data.avatar);
                $avatarUploadInput.filestyle('clear');
                $imageCropperPreview.find('.cropit-preview-image').removeAttr('src'); //remove background image
                $imageCropperPreview.css({
                    'background-image': 'url("//static3.' + window.location.hostname + '/avatar/' + data.avatar + '_4x.jpg")',
                    'background-size': avatarWidthLimit,
                    'background-position': '0'
                });
                showNotification('success', data.results);
            },
            error: function(data) {
                $avatarDeleteBtn.removeClass('disabled');
                showNotification('error', data.responseJSON.results);
            },
            complete: function() {
                hideAvatarUploadLoader();
                enableAvatarUploadInput();
                refreshSessionModal();
            }
        }, 'json');
    }

    // Cover upload
    var $coverImageContainer = $('.cover-photo');
    var $coverUploadForm = $('#cover-upload-form');
    var $coverDeleteBtn = $coverUploadForm.find('#cover-delete-btn');
    var $coverUploadBtn = $coverUploadForm.find('#cover-upload-btn');
    var $coverUploadSubmitBtn = $coverUploadForm.find('#cover-upload-submit-btn');
    var $coverUploadClearBtn = $coverUploadForm.find('#cover-upload-clear-btn');
    var $coverDeleteBtn = $coverUploadForm.find('#cover-delete-btn');
    var $coverUploadInput = $coverUploadForm.find('#cover-upload-input');
    var $coverImageCropper = $coverUploadForm.find('#image-cropper');
    var $coverImageCropperPreview = $coverImageCropper.find('.cropit-preview');
    var $coverImageCropperLoader = $coverUploadForm.find('#cover-loader');
    var $coverImageCropperPreviewImage = $coverImageCropperPreview.find('.cropit-preview-image')
    $coverImageCropper.cropit({
        $preview: $coverImageCropperPreview,
        height: 400,
        width: 1440,
        smallImage: 'allow',
        onImageLoaded: function() {
            $coverImageCropper.cropit('maxZoom', zoom);
            $coverImageCropper.cropit('zoom', zoom);
        },
        onImageError: function(e) {
            showNotification('error', 'Something went wrong while reading the image.');
        },
        onFileReaderError: function() {
            showNotification('error', 'Something went wrong while loading the file.');
        }
    });
    var zoom;
    $coverUploadInput.on('change', function() {
        if (this.files && this.files[0]) {
            // File size
            var file = this.files[0];
            if ((file.size / (1024 * 1024)) > 3) {
                showNotification('error', 'Cover image file must not exceed ' + 3 + ' MB');
                return;
            }
            // Wrong file type
            var fileType = file["type"];
            var validImageTypes = ["image/jpg", "image/jpeg"];
            if ($.inArray(fileType, validImageTypes) < 0) {
                showNotification('error', 'Selection is not a valid image file.');
                return;
            }
            // Reading error
            var reader = new FileReader();
            reader.onerror = function(e) {
                showNotification('warning', 'Cound not read the selected file.');
                return;
            };

            reader.onload = function(e) {
                // Dimensions
                var image = new Image();
                image.src = e.target.result;
                image.onload = function() {
                    if (this.width < 1440 || this.height < 400) {
                        showNotification('warning', 'The selected image is small and may appear blurry.');
                    }
                    //console.log(reader.result);
                    zoom = $coverImageCropperPreview.width() / this.width;
                    $coverImageCropper.cropit('imageSrc', reader.result);
                    $coverImageCropper.removeClass('hide');
                    $coverUploadBtn.addClass('hide');
                    $coverDeleteBtn.addClass('hide');
                    $coverUploadSubmitBtn.removeClass('hide');
                    $coverUploadClearBtn.removeClass('hide');
                };
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    function clearCropper() {
        //console.log($coverImageCropperPreviewImage);

        $coverImageCropperPreviewImage.removeAttr('src');
        $coverImageCropper.addClass('hide');
        $coverUploadSubmitBtn.addClass('hide');
        $coverUploadClearBtn.addClass('hide');
        $coverUploadBtn.removeClass('hide');
    }

    $coverUploadBtn.on('click touchend', function(event) {
        event.preventDefault();
        $coverUploadInput.val('');
        $coverUploadInput.click();
    });

    $coverUploadClearBtn.on('click touchend', function(event) {
        event.preventDefault();
        $coverUploadInput.val('');
        clearCropper();
    });

    $coverUploadSubmitBtn.on('click touchend', function(event) {
        event.preventDefault();
        $coverImageCropperLoader.removeClass('hide');
        var imageData = $coverImageCropper.cropit('export', {
            type: 'image/jpeg',
            quality: 1
        });
        return $.ajax({
                type: 'post',
                url: '/coverupload',
                data: {
                    cover: imageData
                },
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    return xhr;
                },
                success: function(data) {
                    console.log(data);
                    $coverImageCropper.css('background-image', ''); //remove preview-image
                    $coverImageCropperPreviewImage.removeAttr('src'); //remove background image
                    $coverImageContainer.css({
                        'background': 'url(' + imageData + ') center / cover no-repeat'
                    });
                    showNotification('success', data.results);
                    $coverDeleteBtn.removeClass('hide');
                    clearCropper();
                },
                error: function(data) {
                    console.log(data);
                    showNotification('error', data.responseJSON.results);
                },
                complete: function() {
                    $coverImageCropperLoader.addClass('hide');
                    refreshSessionModal();
                }
            },
            'json');
    });

    // Cover delete
    $coverDeleteBtn.on('click touchend', function(event) {
        event.preventDefault();
        $coverImageCropperLoader.removeClass('hide');
        return $.ajax({
                type: 'post',
                url: '/coverdelete',
                success: function(data) {
                    console.log(data);
                    $coverImageContainer.css({
                        'background': 'none'
                    });
                    showNotification('success', data.results);
                    $coverDeleteBtn.addClass('hide');
                },
                error: function(data) {
                    console.log(data);
                    showNotification('error', data.responseJSON.results);
                },
                complete: function() {
                    $coverImageCropperLoader.addClass('hide');
                    refreshSessionModal();
                }
            },
            'json');
    });

    // Wallpaper upload
    var $wallpaperUploadGroupInput = $('#wallpaper-upload-form-group');
    var $wallpaperUploadInput = $('#wallpaper-upload-input');
    var $wallpaperUploadPreview = $('#wallpaper-upload-preview');
    var $wallpaperUploadWrapper = $('#wallpaper-upload-wrapper');

    $wallpaperUploadInput
        .removeClass('hide')
        .click(function(e) {
            e.stopPropagation();
        })
        .filestyle({
            icon: false,
            buttonText: ''
        });

    enableWallpaperUploadInput();

    function enableWallpaperUploadInput() {
        $wallpaperUploadGroupInput.on('click touchstart', function(e) {
            $wallpaperUploadInput.click();
        });
    }

    $wallpaperUploadInput.on('change', function() {
        if (this.files && this.files[0]) {
            // File size
            var file = this.files[0];
            if ((file.size / (1024 * 1024)) > wallpaperFileSizeLimit) {
                showNotification('warning', 'Uploaded file must not exceed ' + wallpaperFileSizeLimit + ' MB');
                return;
            }
            // Wrong file type
            var fileType = file["type"];
            var validImageTypes = ["image/jpg", "image/jpeg", "image/png"];
            if ($.inArray(fileType, validImageTypes) < 0) {
                showNotification('warning', 'Selection is not a valid image file.');
                return;
            }
            // Reading error
            var reader = new FileReader();
            reader.onerror = function(e) {
                showNotification('warning', 'Cound not read the selected file.');
            };
            reader.onload = function(e) {
                // Dimensions
                var image = new Image();
                image.src = e.target.result;
                image.onload = function() {
                    if (this.width <= this.height) {
                        if (this.width < wallpaperVerticalLimit.width || this.height < wallpaperVerticalLimit.height) {
                            showNotification('warning', 'Selected image is too small');
                            return;
                        }
                    } else {
                        if (this.width < wallpaperHorizontalLimit.width || this.height < wallpaperHorizontalLimit.height) {
                            showNotification('warning', 'Selected image is too small');
                            return;
                        }
                    }
                    $wallpaperUploadWrapper.removeClass('hide');
                    $wallpaperUploadPreview.attr('src', reader.result);
                };
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    // Sticky buttons in upload.. need to fix this
    var $stickyContainer = $('#sticky-container');
    if ($stickyContainer.length) {
        $sticky = $stickyContainer.find('#sticky');
        var initialOffset = ($stickyContainer.offset().top < $(window).height()) ? $stickyContainer.offset().top : 0;
        $(window).scroll(function() {
            var diff = $(window).scrollTop() + initialOffset - $stickyContainer.offset().top;
            if (diff > 0) {
                $sticky.css({
                    'margin-top': diff + 'px'
                });
            } else {
                $sticky.css({
                    'margin-top': '0px'
                });
            }
        });
    }

    // Clear upload fields
    $('#upload-clear-button').on('click touchend', function(e) {
        $('.select2-input').select2('val', '');
        $wallpaperUploadPreview.attr('src', '');
        $wallpaperUploadTags.magicSuggest().clear();
    });

    // Device picker
    var $devicePickerFeild = $('#device-picker-field');
    $devicePickerFeild.on('input', function() {
        var val = $(this).val();
        if (val.length != 0 /* && val.length % 2 == 0*/ ) {
            lookForDevice(val)
        } else if (val.length < 1) {
            if (typeof $searchDeviceRequest !== 'undefined')
                $searchDeviceRequest.abort();
            $('#device-picker-results').empty();
        }
    });

    var domain = (function() {
        var i = 0,
            domain = document.domain,
            p = domain.split('.'),
            s = '_gd' + (new Date()).getTime();
        while (i < (p.length - 1) && document.cookie.indexOf(s + '=' + s) == -1) {
            domain = p.slice(-1 - (++i)).join('.');
            document.cookie = s + "=" + s + ";domain=" + domain + ";";
        }
        document.cookie = s + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;domain=" + domain + ";";
        return domain;
    })();

    function lookForDevice(term) {
        $searchDeviceRequest = $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/devicepicker/search',
            data: {
                search_term: term
            },
            success: function(data) {
                console.log(data);
                $('#device-picker-results').empty();
                if (data.results.length > 0) {
                    data.results.forEach(function(e) {
                        if (e['maker'].localeCompare('N/A') == 0) {
                            e['maker'] = '';
                        }
                        var deviceSerachTemplate = [
                            '<div class="device-picker-result bordered b-rad-md bg-white inline m-b-10 m-l-5 m-r-5 xs-m-r-0 xs-m-l-0 full-mobile">',
                            '<a href="devicepicker/set/' + e['id'] + '" class="">',
                            '<div class="device-picker-result-image padding-15 bg-warning b-white bordered b-t b-r b-l b-rad-sm text-center">',
                            '<img class="" src="//static3.' + domain + '/device/' + e['featured_image'] + '_2x.png" alt="" data-src="//static3.' + window.location.hostname + '/device/' + e['featured_image'] + '_2x.png" data-src-retina="//static3.' + window.location.hostname + '/device/' + e['featured_image'] + '_3x.png" width="128" heigh="128">',
                            '</div>',
                            '<div class="padding-10">',
                            '<div class="clearfix">',
                            '<p class="fs-10 v-align-middle pull-left no-margin text-black">' + e['name'] + '</p>',
                            '<p class="fs-10 text-right v-align-middle pull-right no-margin text-black">' + e['maker'] + '</p>',
                            '</div>',
                            '</div>',
                            '</a>',
                            '</div>'
                        ].join("\n");
                        $('#device-picker-results').append(deviceSerachTemplate);
                    });
                }
            },
            error: function(data) {
                console.log(data);
                $('#device-picker-results').empty();
            },
            complete: function() {
                refreshSessionModal();
            }
        }, 'json');
    }

    // Wallpaper Ops
    $unauthenticatedModal = $('#unauthenticated-modal');
    $downloadLimitModal = $('#download-limit-modal');

    // iOS
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window.HTMLElement) || (function(p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;

    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    $('.btn-download').click(function(e) {
        e.preventDefault();
        var device_id = $(this).attr("data-id");
        var slug = $(this).attr("data-slug")
        return downloadRequest = $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/download/' + slug + '/' + device_id,
            success: function(data) {
                //console.log(data);
                if (data['code'] == 1007) {
                    $downloadLimitModal.modal('show');
                    return;
                }

                if (isSafari || iOS) {
                    location.href = data['wallpaper_url'];
                    return;
                }

                if (isFirefox || isIE) {
                    window.open(data['wallpaper_url'], '_blank')
                    return;
                }

                var file_path = data['wallpaper_url'];
                var a = document.createElement('A');
                a.href = file_path;
                a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            },
            error: function(data) {
                console.log(data);
            },
            complete: function() {
                refreshSessionModal();
            }
        }, 'json');
    });

    inProgress = false;

    $('.btn-favorite').click(function(e) {
        e.preventDefault();
        if (!logged) {
            $unauthenticatedModal.modal('show');
            return;
        }
        if (inProgress) {
            console.log('There is already a pending favorite request.');
            return false;
        }

        icon = $(this).find('i');
        id = $(this).data('id');
        slug = $(this).data('slug');
        return favoriteRequest = $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/favorite/' + id,
            beforeSend: function() {
                inProgress = true;
            },
            success: function(data) {
                console.log(data);
                if (data['code'] == 1008) {
                    $('.btn-favorite[data-slug="' + slug + '"]  i').removeClass("mdi-heart-outline")
                        .addClass("mdi-heart");
                    return;
                }
                if (data['code'] == 1009) {
                    $('.btn-favorite[data-slug="' + slug + '"]  i').removeClass("mdi-heart")
                        .addClass("mdi-heart-outline");
                }
            },
            error: function(data) {
                console.log(data);
                if (data.status = 401) {
                    $unauthenticatedModal.modal('show');
                }
            },
            complete: function() {
                inProgress = false;
                refreshSessionModal();
            }
        }, 'json');
    });

    // Upload
    var $wallpaperUploadTags = $('#upload-wallpaper-tags');
    $wallpaperUploadTags.magicSuggest({
        name: 'tags',
        method: 'get',
        dataUrlParams: { 'suggest': true },
        data: '/suggest/tags',
        queryParam: 'filter_n',
        displayField: 'name',
        valueField: 'name',
        vregex: /^[A-Za-z0-9-\s]{1,15}$/,
        useCommaKey: true,
        hideTrigger: true,
        selectFirst: true,
        allowDuplicates: false,
        minChars: 2,
        highlight: false,
        maxSelection: 5,
        placeholder: '',
        selectionCls: 'tag label label-info pull-left'
    });


    // Notifications
    $('.toggle-more-details').on('click touchend', function() {
        var id = $(this).attr('data-id');
        $parent = $(this).closest('.notification-item');
        if ($parent.hasClass('unread')) {
            $parent.removeClass('unread');
            return markReadRequest = $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/user/notification/mark/' + id,
                success: function(data) {
                    console.log(data);
                },
                error: function(data) {
                    console.log(data);
                },
                complete: function() {
                    refreshSessionModal();
                }
            }, 'json');
        }
    });

    if (typeof(infinite_scroll) !== 'undefined' && infinite_scroll) {
        var $win = $(window);
        var pull = true;
        var pagination = 2;
        var $readNotificationsHolder = $('#unread-notifications');
        var $loader = $('#pull-notifications-loader');
        $win = $(window);
        $win.scroll(function() {
            if ($win.height() + $win.scrollTop() == $(document).height() && pull) {
                $loader.removeClass('hide');
                $.ajax({
                        type: 'get',
                        dataType: 'json',
                        url: '/user/notifications',
                        data: {
                            pagination: pagination
                        },
                        success: function(data) {
                            pagination++;
                            console.log(data);
                            if (data.code == 1034) {
                                $(data.results).insertBefore($loader);
                            } else if (data.code == 1035) {
                                pull = false;
                                $loader.addClass('hide');
                            }
                        },
                        error: function(data) {
                            console.log(data);
                        },
                        complete: function() {
                            refreshSessionModal();
                            $loader.addClass('hide');
                        }
                    },
                    'json');
            }
        });
    }

    // Cookie altert
    var cookieAlert = $("#cookie-alert");
    $("#cookie-alert-close-btn").on('click touchend', function() {
        cookieAlert.addClass('hide');
        $.cookie('cookie-consent', 'agreed', {
            expires: 365
        });
    });

    // Push Notifications
    function showNotification(type, msg) {
        $notificationsHolder.pgNotification({
            style: 'bar',
            position: 'top',
            type: type,
            timeout: 4000,
            message: msg
        }).show();
    }

    $(window).bind("load", function() {
        $('.alert').each(function() {
            $(this).removeClass('hide')
                .addClass('alert-' + $(this).attr('type'));
        });
    });

    // Back to top
    $('#scrollToTop').on('click touchend', function() {
        $('html, body').animate({ scrollTop: 0 }, 800);
        return false;
    });

    // Show/hide button
    $('.collapseToggle').click(function() {
        var $this = $(this);
        $this.toggleClass('collapseToggle');
        if ($this.hasClass('collapseToggle')) {
            $this.html('show');
        } else {
            $this.html('hide');
        }
    });

});


// Helpers
function slugify(text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
}