$(document).ready(function() {

    // Ajax setup
    window.ajaxEnabled = true;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function() {
            return window.ajaxEnabled;
        }
    });

    /* Init */
    var $notificationsHolder = $('#notification-holder');
    var $select2 = $('.select2');
    var $datepicker = $('.datepicker');
    $('input[type="file"]').filestyle({
        icon: false,
        buttonText: ''
    });
    showHiddenElements();

    // Datepicker
    $datepicker.datepicker({
            autoclose: true,
            format: 'mm-dd-yyyy',
            clearBtn: true
        })
        .on('hide', function() {
            $(this).removeClass('focused');
        })
        .on('changeDate', function(e) {
            $(this).find('label').addClass('fade');
        });

    $select2.on('select2-open', function() { // hide when using select2
        $datepicker.datepicker('hide');
        $('.datepicker input').blur();
    });

    $('.modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

    $('.table').on('xhr.dt', function() {
        refreshSessionModal();
    });

    // Initializes search overlay plugin.
    // Replace onSearchSubmit() and onKeyEnter() with
    // your logic to perform a search and display results
    $('[data-pages="search"]').search({
        searchField: '#overlay-search',
        closeButton: '.overlay-close',
        suggestions: '#overlay-suggestions',
        brand: '.brand',
        onSearchSubmit: function(searchString) {
            console.log("Search for: " + searchString);
        },
        onKeyEnter: function(searchString) {
            console.log("Live search for: " + searchString);
            var searchField = $('#overlay-search');
            var searchResults = $('.search-results');
            clearTimeout($.data(this, 'timer'));
            searchResults.fadeOut("fast");
            var wait = setTimeout(function() {
                searchResults.find('.result-name').each(function() {
                    if (searchField.val().length != 0) {
                        $(this).html(searchField.val());
                        searchResults.fadeIn("fast");
                    }
                });
            }, 500);
            $(this).data('timer', wait);
        }
    });

    // Select2
    $select2.on('select2-selecting', function() {
            $(this).addClass('option-selected');
            $(this).find('label').addClass('fade');
        })
        .on('select2-opening', function() {
            $(':focus').blur();
            $(this).addClass('focused');
        })
        .on('select2-close', function() {
            $_this = $(this);
            setTimeout(function() {
                $_this.removeClass('focused');
                $_this.find(':focus').blur();
            }, 1);
        })
        .on('click touchend', function() {
            $(this).find('select').select2('open');
        });

    $('.select2 input').prop('readonly', true); // dirty fix for mobile keyboard


    // Tabs
    $(function() {
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');
        hash && $('ul.sub-menu .sub-menu-to-tab a[href="' + hash + '"]').tab('show'); //notworking
        $('.nav-tabs a, .sub-menu-to-tab a').click(function(e) {
            $(this).tab('show');
            var scrollmem = $('body').scrollTop() || $('html').scrollTop();
            window.location.hash = this.hash;
            $('html,body').scrollTop(scrollmem);
        });
    });

    // Hidden Inputs
    function showHiddenElements() {
        $('.select2 select').removeClass('hide');
        //$('.switchery input[type=checkbox]').removeClass('hide');
    }

    // Switches
    var initSwitch = function(_switch, value) {
        if (Boolean(value)) {
            if (!_switch.is(':checked')) {
                _switch.click();
                _switch.prop('checked', true);
            }
        } else {
            if (_switch.is(':checked')) {
                _switch.click();
                _switch.prop('checked', false);
            }
        }
    };

    function getSwitchValue(_switch) {
        if (_switch.is(':checked'))
            return 1;
        return 0;
    }

    // Markdown textareas
    $(function() {
        var $editors = $('.editor'),
            convert = new Markdown.getSanitizingConverter().makeHtml;
        $editors.each(function() {
            var _this = $(this),
                $markdown = _this.find('textarea.markdown'),
                $preview = _this.find('.preview');
            $preview.html(convert($markdown.val()));
            $markdown.on('keyup', function() {
                $preview.html(convert($markdown.val()));
            });
        })
    });

    // User datatable
    var $usersTable = $('#usersTable').DataTable({
        dom: 'Bfrtip',
        serverSide: true,
        ajax: '/dashboard/users',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'avatar', name: 'avatar', orderable: false, searchable: false },
            { data: 'username', name: 'username', orderable: true, searchable: true },
            { data: 'first_name', name: 'first_name', orderable: true, searchable: true },
            { data: 'last_name', name: 'last_name', orderable: true, searchable: true },
            { data: 'created_at', name: 'created_at' },
            { data: 'role', name: 'role', orderable: false, searchable: false },
            { data: 'banned', name: 'banned', orderable: true, searchable: false }
        ],
        select: {
            style: 'os'
        },
        buttons: [
            'selectAll',
            'selectNone',
            {
                extend: 'selectedSingle',
                text: 'Edit',
                action: function(e, dt, node, config) {
                    $userEditModal.modal('show');
                }
            },
            {
                extend: 'selected',
                text: 'Delete',
                action: function(e, dt, node, config) {
                    $userDeleteModal.modal('show');
                }
            }
        ],
        displayLength: 10
    });

    // Delete User
    var $userDeleteModal = $('#delete-user-modal');
    var $userDeleteSubmitButton = $userDeleteModal.find('#delete-user-submit-button');
    var $userDeleteCloseButton = $userDeleteModal.find('#delete-user-close-button');
    var $userDeleteModalProgress = $userDeleteModal.find('#delete-user-progress');

    $userDeleteModal.on('show.bs.modal', function() {
        $userDeleteModalProgress.css('visibility', 'hidden');
        $userDeleteSubmitButton.removeAttr('disabled');
        $userDeleteCloseButton.removeAttr('disabled');
    });

    $userDeleteModal.on('hidden.bs.modal', function() {
        $userDeleteSubmitButton.removeAttr('data-id');
    });

    $userDeleteSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $userDeleteCloseButton.attr('disabled', 'disabled');
        $userDeleteModalProgress.css('visibility', 'visible');
        userDeleteRequest(e);
    });

    function userDeleteRequest() {
        var data = $usersTable.rows({
            selected: true
        }).data();
        var usersCount = data.count();
        var users = [];
        for (var i = 0; i < usersCount; i++) {
            users[i] = data[i]['id'];
        }
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/user/delete',
            data: {
                users: users
            },
            success: function(data) {
                showNotification($notificationsHolder, 'success', data.success);
                $usersTable.rows().deselect();
                $usersTable.draw(false);
            },
            error: function(data) {
                handleError(data, $notificationsHolder);
            },
            complete: function() {
                $userDeleteModal.modal('hide');
                refreshSessionModal();
            }
        }, 'json');
    }

    // User edit
    var $userEditModal = $('#edit-user-modal');
    var $userEditSubmitButton = $userEditModal.find('#edit-user-submit-button');
    var $userEditCloseModalButton = $userEditModal.find('#edit-user-close-button');
    var $userEditProgress = $userEditModal.find('#edit-user-progress');
    var $userEditModalBody = $userEditModal.find('#edit-user-modal-body');
    var $userRole = $userEditModal.find('#edit-user-role-select2');
    var $userDeleteAvatar = $userEditModal.find('#edit-user-delete-avatar-switch');
    var $userBanned = $userEditModal.find('#edit-user-banned-switch');

    $userEditModal.on('show.bs.modal', function() {
        $userEditProgress.css('visibility', 'hidden');
        var $userEditAvatar = $userEditModal.find('#edit-user-info-avatar');
        var $userEditUsernameInfo = $userEditModal.find('#edit-user-info-username');
        var $userEditEmailInfo = $userEditModal.find('#edit-user-info-email');
        var $userEditCreatedAt = $userEditModal.find('#edit-user-info-created-at');
        var data = $usersTable.row({
            selected: true
        }).data();
        $userEditAvatar.html(data['avatar']);
        $userEditUsernameInfo.html(data['username']);
        $userEditEmailInfo.html(data['email']);
        $userEditCreatedAt.html(data['created_at']);

        $userRole.select2('val', data['role']);
        initSwitch($userDeleteAvatar, 0);
        initSwitch($userBanned, (data['banned'] == 'Yes') ? 1 : 0);
    });


    function editUserRequest() {
        var data = $usersTable.row({
            selected: true
        }).data();
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/user/edit',
            data: {
                user: data['id'],
                role: $userRole.val(),
                delete_avatar: getSwitchValue($userDeleteAvatar),
                banned: getSwitchValue($userBanned)
            },
            success: function(data) {
                showNotification($notificationsHolder, 'success', data.success);
                $userEditModal.modal('hide');
                $usersTable.rows().deselect();
                $usersTable.draw(false);
            },
            error: function(data) {
                handleError(data, $userEditModalBody);
            },
            complete: function() {
                $userEditProgress.css('visibility', 'hidden');
                $userEditSubmitButton.removeAttr('disabled');
                $userEditCloseModalButton.removeAttr('disabled');
                refreshSessionModal();
            }
        }, 'json');
    }

    $userEditSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $userEditCloseModalButton.attr('disabled', 'disabled');
        $userEditProgress.css('visibility', 'visible');
        editUserRequest();
    });

    // Sortable galleries list
    var reorderRequests = {};
    var index = 1;
    var galleriesList = document.getElementById('galleries-list');
    if (galleriesList) {
        Sortable.create(
            galleriesList, {
                animation: 150,
                onUpdate: function(e) {
                    var changes = [];
                    if (e.oldIndex != e.newIndex) {
                        changes = {
                            'gallery_id': parseInt(e.item.getAttribute('data-id')),
                            'old': e.oldIndex,
                            'new': e.newIndex
                        }
                    }
                    submitGalleriesReorder(changes);
                }
            }
        );
    }

    function submitGalleriesReorder(changes) {
        console.log(reorderRequests);
        if (!jQuery.isEmptyObject(changes)) {
            reorderRequests[index] = $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/dashboard/galleries/reorder',
                data: {
                    index: index,
                    changes: changes
                },
                success: function(data) {
                    console.log(data);
                    console.log(data.index);
                    if (data.code == 1038) {
                        delete reorderRequests[data.index];
                        $.ajax(reorderRequests[data.index + 1]);
                    } else if (data.code == 1039) {
                        $.ajax(reorderRequests[data.index]);
                    }
                },
                error: function(data) {},
                complete: function(data) {
                    refreshSessionModal();
                }
            }, 'json');
            if (Object.keys(reorderRequests).length == 1) {
                $.ajax(reorderRequests[index]);
            }
            index++;
        }
        return false;
    }

    // Create gallery
    var $galleryCreateForm = $('#create-gallery-form');
    var $galleryCreateSubmitButton = $galleryCreateForm.find('#create-gallery-submit-button');

    $galleryCreateSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        submitNewGallery();
        return false;
    });

    function submitNewGallery() {
        var newGalleryName = $galleryCreateForm.find('#create-gallery-name-input').val();
        var newGallerySlug = $galleryCreateForm.find('#create-gallery-slug-input').val();
        var newGalleryDescription = $galleryCreateForm.find('#create-gallery-description-input').val();
        var newGalleryFolder = $galleryCreateForm.find('#create-gallery-folder-input').val();
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/galleries/create',
            data: {
                gallery_name: newGalleryName,
                gallery_slug: newGallerySlug,
                gallery_description: newGalleryDescription,
                gallery_folder: newGalleryFolder,
            },
            success: function(data) {
                console.log('data :' + data);
                showNotification($notificationsHolder, 'success', data.results);
                var clone = document.createElement('li');
                clone.className += ' b-a b-grey b-rad-sm m-t-5';
                clone.innerHTML = '<span class="mdi mdi-reorder-horizontal mdi-24px handle m-r-10 m-l-5"></span><span class="list-item gallery-name"><a href="#" class="edit-gallery-button" data-toggle="modal" data-target="#edit-gallery-modal"></a></span><span class="pull-right p-r-10 p-l-10"><a href="#" class="delete-gallery-button" data-toggle="modal" data-target="#delete-gallery-modal">✖</a></span>';
                galleriesList.appendChild(clone);
                clone.setAttribute('data-id', data.gallery_id);
                var $clone = $(clone);
                $clone.find('.list-item.gallery-name a').html(data.gallery_name + ' (unpublished)');
                var $cloneEdit = $clone.find('.edit-gallery-button');
                $cloneEdit.attr({
                    'data-id': data.gallery_id,
                    'data-name': data.gallery_name,
                    'data-slug': data.gallery_slug,
                    'data-description': data.gallery_description,
                    'data-folder': data.gallery_folder,
                    'data-published': data.gallery_published
                });
                var $cloneDelete = $clone.find('.delete-gallery-button');
                $cloneDelete.attr({
                    'data-id': data.gallery_id
                });
            },
            error: function(data) {
                handleError(data, $notificationsHolder);
            },
            complete: function() {
                refreshSessionModal();
            }
        }, 'json');
    }

    // Edit gallery
    var $galleryEditModal = $('#edit-gallery-modal');
    var $galleryEditModalProgress = $galleryEditModal.find('#edit-gallery-progress');
    var $galleryEditId = $galleryEditModal.find('#gallery-id-input');
    var $galleryEditName = $galleryEditModal.find('#edit-gallery-name-input');
    var $galleryEditSlug = $galleryEditModal.find('#edit-gallery-slug-input');
    var $galleryEditDescription = $galleryEditModal.find('#edit-gallery-description-input');
    var $galleryEditFolder = $galleryEditModal.find('#edit-gallery-folder-input');
    var $galleryEditPublished = $galleryEditModal.find('#edit-gallery-published-switch');
    var $galleryEditSubmitButton = $galleryEditModal.find('#edit-gallery-submit-button');
    var $galleryEditCloseButton = $galleryEditModal.find('#edit-gallery-close-button');
    var $galleryEditModalBody = $galleryEditModal.find('#edit-gallery-modal-body');

    $galleryEditModal.on('show.bs.modal', function(e) {
        $galleryEditModalProgress.css('visibility', 'hidden');
        var galleryListItem = $(e.relatedTarget);
        $galleryEditId.val(galleryListItem.attr('data-id'));
        $galleryEditName.val(galleryListItem.attr('data-name'));
        $galleryEditDescription.val(galleryListItem.attr('data-description'));
        $galleryEditSlug.val(galleryListItem.attr('data-slug'));
        $galleryEditFolder.val(galleryListItem.attr('data-folder'));
        initSwitch($galleryEditPublished, parseInt(galleryListItem.attr('data-published')));
    });

    $galleryEditSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $galleryEditCloseButton.attr('disabled', 'disabled');
        $galleryEditModalProgress.css('visibility', 'visible');
        editGallery();
        return false;
    });

    function editGallery() {
        var galleryId = $galleryEditId.val();
        var galleryName = $galleryEditName.val();
        var gallerySlug = $galleryEditSlug.val();
        var galleryDescription = $galleryEditDescription.val();
        var galleryFolder = $galleryEditFolder.val();
        var galleryPublished = getSwitchValue($galleryEditPublished);
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/galleries/edit',
            data: {
                gallery_id: galleryId,
                gallery_name: galleryName,
                gallery_slug: gallerySlug,
                gallery_description: galleryDescription,
                gallery_folder: galleryFolder,
                gallery_published: galleryPublished
            },
            success: function(data) {
                showNotification($galleryEditModalBody, 'success', data.success);
            },
            error: function(data) {
                handleError(data, $galleryEditModalBody);
            },
            complete: function() {
                $galleryEditSubmitButton.removeAttr('disabled');
                $galleryEditCloseButton.removeAttr('disabled');
                $galleryEditModalProgress.css('visibility', 'hidden');
                refreshSessionModal();
            }
        }, 'json');
    }

    // Delete gallery
    var $galleryDeleteModal = $('#delete-gallery-modal');
    var $galleryDeleteModalProgress = $galleryDeleteModal.find('#delete-gallery-progress');
    var $galleryDeleteSubmitButton = $galleryDeleteModal.find('#delete-gallery-submit-button');
    var $galleryDeleteCloseButton = $galleryDeleteModal.find('#delete-gallery-close-button');
    var $galleryDeleteModalBody = $galleryDeleteModal.find('#delete-gallery-modal-body');

    $galleryDeleteModal.on('show.bs.modal', function(e) {
        $galleryDeleteModalProgress.css('visibility', 'hidden');
        $galleryDeleteCloseButton.removeAttr('disabled');
        $galleryDeleteSubmitButton.removeAttr('disabled');
        var $galleryListItem = $(e.relatedTarget);
        $galleryDeleteSubmitButton.attr("data-id", $galleryListItem.attr('data-id'));
    });

    $galleryDeleteModal.on('hidden.bs.modal', function() {
        $galleryDeleteSubmitButton.removeAttr('data-id');
    });

    $galleryDeleteSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $galleryDeleteCloseButton.attr('disabled', 'disabled');
        $galleryDeleteModalProgress.css('visibility', 'visible');
        deleteGallery();
        return false;
    });

    function deleteGallery() {
        var id = $galleryDeleteSubmitButton.attr('data-id');
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/galleries/delete',
            data: {
                gallery_id: id
            },
            success: function(data) {
                console.log('success ' + JSON.stringify(data));
                $(galleriesList).find("[data-id=" + id + "]").remove();
                showNotification($notificationsHolder, 'success', data.success);
            },
            error: function(data) {
                console.log('error ' + JSON.stringify(data));
                handleError(data, $notificationsHolder);
            },
            complete: function() {
                $galleryDeleteModal.modal('hide');
                refreshSessionModal();
            }
        }, 'json');
    }

    // Devices datatable
    var $devicesTable = $('#devicesTable').DataTable({
        dom: 'Bfrtip',
        serverSide: true,
        ajax: '/dashboard/devices',
        columns: [
            { data: 'id', name: 'id', orderable: true, searchable: false },
            { data: 'name', name: 'name', orderable: true, searchable: true },
            { data: 'image', name: 'image', orderable: false, searchable: false },
            { data: 'maker', name: 'maker', orderable: true, searchable: true },
            { data: 'slug', name: 'slug', orderable: false, searchable: true },
            { data: 'resolutions', name: 'resolutions', orderable: false, searchable: false },
            { data: 'release_date', name: 'release_date', orderable: true, searchable: true },
            { data: 'excluded', name: 'excluded', orderable: true, searchable: false },
        ],
        select: {
            style: 'os'
        },
        buttons: [
            'selectAll',
            'selectNone',
            {
                text: 'New',
                action: function(e, dt, node, config) {
                    $deviceCreateModal.modal('show');
                }
            },
            {
                extend: 'selectedSingle',
                text: 'Edit',
                action: function(e, dt, node, config) {
                    $deviceEditModal.modal('show');
                }
            },
            {
                extend: 'selected',
                text: 'Delete',
                action: function(e, dt, node, config) {
                    $deviceDeleteModal.modal('show');
                }
            }
        ],
        "columnDefs": [{
            "targets": 5,
            "data": "resolutions",
            "render": function(data) {
                var output = '<div class="table-tags table-cell-maxed-width">';
                if (data != null) {
                    var parsed = JSON.parse(data);
                    parsed.forEach(function(item) {
                        output += '<span class="label">' + item.width + 'x' + item.height + '</span> ';
                    });
                } else {
                    output += 'None';
                }
                output += '</div>';
                return output;
            }
        }],
        displayLength: 10
    });

    // Edit device
    var $deviceEditModal = $('#edit-device-modal');
    var $deviceEditProgress = $deviceEditModal.find('#edit-device-progress');
    var $deviceEditSubmitButton = $deviceEditModal.find('#edit-device-submit-button');
    var $deviceEditCloseButton = $deviceEditModal.find('#edit-device-close-button');

    var $deviceEditId = $deviceEditModal.find('#edit-device-id-input');
    var $deviceEditName = $deviceEditModal.find('#edit-device-name-input');
    var $deviceEditSlug = $deviceEditModal.find('#edit-device-slug-input');
    var $deviceEditType = $deviceEditModal.find('#edit-device-type-select2');
    var $deviceEditMaker = $deviceEditModal.find('#edit-device-maker-select2');
    var $deviceEditResolution = $deviceEditModal.find('#edit-device-resolution-select2');
    var $deviceEditReleaseDate = $deviceEditModal.find('#edit-device-release-date-input');
    var $deviceEditImage = $deviceEditModal.find('#edit-device-image-input');
    var $deviceEditExcludeSwitch = $deviceEditModal.find('#edit-device-exclude-switch');
    var $deviceEditModalBody = $deviceEditModal.find('#edit-device-modal-body');

    $deviceEditImage
        .removeClass('hide')
        .click(function(e) {
            e.stopPropagation();
            $deviceEditImage.val('');
        })
        .closest('.input-group').on('click touchstart', function(e) {
            $deviceEditImage.click();
        });

    var reader = new FileReader();
    var reading = false;
    $deviceEditModal.on('show.bs.modal', function() {
        reader.abort();
        $deviceEditProgress.css('visibility', 'hidden');
        var data = $devicesTable.row({
            selected: true
        }).data();

        var resolutionValues = [];
        JSON.parse(data['resolutions']).forEach(function(element) {
            resolutionValues.unshift(element.id);
        });

        $deviceEditId.val(data['id']);
        $deviceEditName.val(data['name']);
        $deviceEditSlug.val(data['slug']);
        $deviceEditType.select2('val', data['type']);
        $deviceEditMaker.select2('val', data['maker']);
        $deviceEditResolution.select2('val', resolutionValues);
        $deviceEditReleaseDate.val(data['release_date']);
        $deviceEditImage.val('');
        initSwitch($deviceEditExcludeSwitch, (data['excluded'] == 'Yes') ? 1 : 0);
    });

    $deviceEditSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $deviceEditCloseButton.attr('disabled', 'disabled');
        $deviceEditProgress.css('visibility', 'visible');
        editDeviceRequest();
    });

    $deviceEditImage.on('change', function() {
        //TODO: Device image client validation
        reader.onloadend = function(e) {
            reading = false;
        };
        reading = true;
        reader.readAsDataURL(this.files[0]);
    });

    function editDeviceRequest() {
        if (reader.readyState == FileReader.LOADING) {
            editDeviceRequest();
        }
        var data = $devicesTable.row({
            selected: true
        }).data();
        var formData = new FormData($('#edit-device-form')[0]);
        if ($deviceEditImage[0].files.length != 0)
            formData.append('device_image', $deviceEditImage[0].files[0]);
        formData.append('device_type', $deviceEditType.val());
        formData.append('device_maker', $deviceEditMaker.val());
        formData.append('device_resolution', ($deviceEditResolution.val()) ? $deviceEditResolution.val() : '');
        formData.append('device_release_date', $deviceEditReleaseDate.val());
        formData.append('device_excluded', getSwitchValue($deviceEditExcludeSwitch));
        return $.ajax({
            type: 'post',
            processData: false,
            contentType: false,
            url: '/device/edit',
            data: formData,
            success: function(data) {
                showNotification($notificationsHolder, 'success', data.success);
                $deviceEditModal.modal('hide');
                $devicesTable.rows().deselect();
                $devicesTable.draw(false);
            },
            error: function(data) {
                handleError(data, $deviceEditModalBody);
            },
            complete: function() {
                $deviceEditProgress.css('visibility', 'hidden');
                $deviceEditSubmitButton.removeAttr('disabled');
                $deviceEditCloseButton.removeAttr('disabled');
                refreshSessionModal();
            }
        }, 'json');
    }

    // Create device
    var $deviceCreateModal = $('#create-device-modal');
    var $deviceCreateProgress = $deviceCreateModal.find('#create-device-progress');
    var $deviceCreateSubmitButton = $deviceCreateModal.find('#create-device-submit-button');
    var $deviceCreateCloseButton = $deviceCreateModal.find('#create-device-close-button');
    var $deviceCreateModalBody = $deviceCreateModal.find('#create-device-modal-body');
    var $deviceCreateName = $deviceCreateModal.find('#create-device-name-input');
    var $deviceCreateSlug = $deviceCreateModal.find('#create-device-slug-input');
    var $deviceCreateType = $deviceCreateModal.find('#create-device-type-select2');
    var $deviceCreateMaker = $deviceCreateModal.find('#create-device-maker-select2');
    var $deviceCreateResolution = $deviceCreateModal.find('#create-device-resolution-select2');
    var $deviceCreateReleaseDate = $deviceCreateModal.find('#create-device-release-date-input');
    var $deviceCreateImage = $deviceCreateModal.find('#create-device-image-input');

    $deviceCreateImage
        .removeClass('hide')
        .click(function(e) {
            e.stopPropagation();
            $deviceCreateImage.val('');
        })
        .closest('.input-group').on('click touchstart', function(e) {
            $deviceCreateImage.click();
        });

    $deviceCreateSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $deviceCreateCloseButton.attr('disabled', 'disabled');
        $deviceCreateProgress.css('visibility', 'visible');
        submitNewDevice();
        return false;
    });

    $deviceCreateModal.on('show.bs.modal', function() {
        reader.abort();
        $deviceCreateProgress.css('visibility', 'hidden');
    });

    $deviceCreateImage.on('change', function() {
        //TODO: Device image client validation
        reader.onloadend = function(e) {
            reading = false;
        };
        reading = true;
        reader.readAsDataURL(this.files[0]);
    });

    function submitNewDevice() {
        if (reader.readyState == FileReader.LOADING) {
            submitNewDevice();
        }
        var formData = new FormData($('#create-device-form')[0]);
        if ($deviceCreateImage[0].files.length != 0)
            formData.append('device_image', $deviceCreateImage[0].files[0]);
        formData.append('device_type', $deviceCreateType.val());
        formData.append('device_maker', $deviceCreateMaker.val());
        formData.append('device_resolution', $deviceCreateResolution.val());
        formData.append('device_release_date', $deviceCreateReleaseDate.val());
        return $.ajax({
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            url: '/device/create',
            data: formData,
            success: function(data) {
                showNotification($notificationsHolder, 'success', data.success);
                $deviceCreateModal.modal('hide');
                $devicesTable.rows().deselect();
                $devicesTable.draw(false);
                $.each($(this).find('input'), function(k, v) {
                    v.value = '';
                });
                $deviceCreateResolution.select2('val', '');
            },
            error: function(data) {
                handleError(data, $deviceCreateModalBody);
            },
            complete: function() {
                $deviceCreateProgress.css('visibility', 'hidden');
                $deviceCreateSubmitButton.removeAttr('disabled');
                $deviceCreateCloseButton.removeAttr('disabled');
                refreshSessionModal();
            }
        }, 'json');
    }

    // Delete device
    var $deviceDeleteModal = $('#delete-device-modal');
    var $deviceDeleteProgress = $deviceDeleteModal.find('#delete-device-progress');
    var $deviceDeleteSubmitButton = $deviceDeleteModal.find('#delete-device-submit-button');
    var $deviceDeleteCloseButton = $deviceDeleteModal.find('#delete-device-close-button');

    $deviceDeleteModal.on('show.bs.modal', function() {
        $deviceDeleteProgress.css('visibility', 'hidden');
    });

    $deviceDeleteSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $deviceDeleteCloseButton.attr('disabled', 'disabled');
        $deviceDeleteProgress.css('visibility', 'visible');
        submitDeleteDevices();
        return false;
    });

    function submitDeleteDevices() {
        var data = $devicesTable.rows({
            selected: true
        }).data();
        var devicesCount = data.count();
        var devices = [];
        for (var i = 0; i < devicesCount; i++) {
            devices[i] = data[i]['id'];
        }
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/device/delete',
            data: {
                devices: devices
            },
            success: function(data) {
                showNotification($notificationsHolder, 'success', data.success);
                $devicesTable.rows().deselect();
                $devicesTable.draw(false);
            },
            error: function(data) {
                handleError(data, $notificationsHolder);
            },
            complete: function() {
                $deviceDeleteSubmitButton.removeAttr('disabled');
                $deviceDeleteCloseButton.removeAttr('disabled');
                $deviceDeleteModal.modal('hide');
                refreshSessionModal();
            }
        }, 'json');
    }

    // Wallpaper filters
    var $filterByTitle = $('#wallpapers-filter-title');
    var $filterByUser = $('#wallpapers-filter-user');
    var $filterByGallery = $('#wallpapers-filter-gallery-select2');
    var $filterByResolution = $('#wallpapers-filter-resolution-select2');

    $filterByTitle.on('change', function(e) { $wallpapersTable.draw(false); });
    $filterByUser.on('change', function(e) { $wallpapersTable.draw(false); });
    $filterByGallery.on('change select2:unselect', function(e) { $wallpapersTable.draw(false); });
    $filterByResolution.on('change select2:unselect', function(e) { $wallpapersTable.draw(false); });

    function clearFilters() {
        $filterByTitle.val('');
        $filterByUser.val('');
        $filterByGallery.select2('val', '');
        $filterByResolution.select2('val', '');
    }

    // Wallpapers datatable
    var $wallpapersTable = $('#wallpapersTable').DataTable({
        dom: 'Brtip',
        serverSide: true,
        bFilter: false,
        ajax: {
            url: '/dashboard/wallpapers',
            data: function(d) {
                d.filter_t = $filterByTitle.val();
                d.filter_u = $filterByUser.val();
                d.filter_g = $filterByGallery.val();
                d.filter_r = $filterByResolution.val();
            }
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'thumbnail', name: 'thumbnail', orderable: false },
            { data: 'title', name: 'title', orderable: true },
            { data: 'slug', name: 'slug', orderable: false },
            { data: 'uploader', name: 'uploader' },
            { data: 'gallery', name: 'gallery' },
            { data: 'resolution', name: 'resolution', orderable: true },
            { data: 'devices', name: 'compatible', orderable: false },
            { data: 'tags', name: 'tags', orderable: false },
            { data: 'published', name: 'published', orderable: true },
            { data: 'featured', name: 'featured', orderable: true }
        ],
        "columnDefs": [{
                "targets": [2, 3],
                "data": [
                    "title",
                    "slug"
                ],
                "render": function(data) {
                    return '<div class="table-cell-maxed-width">' + data + '</div>';
                }
            },
            {
                "targets": 1,
                "data": "thumbnail",
                "render": function(data) {
                    return '<span class="thumbnail-wrapper img-rounded b-rad-sm bordered b-white"><img src="' + data['path'] + '_1x.jpg?' + data['timestamp'] + '" data-src-retina="' + data['path'] + '_2x.jpg?' + data['timestamp'] + '"></span>';
                }
            },
            {
                "targets": 7,
                "data": "devices",
                "render": function(data) {
                    var output = '<div class="table-tags table-cell-maxed-width">';
                    var tags = JSON.parse(data);
                    if (tags.length === 0) {
                        output += '<span class="label label-warning">No Devices</span> ';
                    }
                    var i = 0;
                    tags.every(function(e) {
                        var name = e['name'];
                        output += '<span class="label">' + name + '</span> ';
                        if (i > 3) return false;
                        i++;
                    });
                    output += '</div>';
                    return output;
                }
            },
            {
                "targets": 8,
                "data": "tags",
                "render": function(data) {
                    var output = '<div class="table-tags table-cell-maxed-width">';
                    var tags = JSON.parse(data);
                    tags.forEach(function(e) {
                        var name = e['name'];
                        output += '<span class="label">' + name + '</span> ';
                    });
                    output += '</div>';
                    return output;
                }
            }
        ],
        select: {
            style: 'os'
        },
        buttons: [{
                text: 'Clear filters',
                action: function(e, dt, node, config) {
                    clearFilters();
                }
            },
            'selectAll',
            'selectNone',
            {
                extend: 'selectedSingle',
                text: 'Edit',
                action: function(e, dt, node, config) {
                    initWallpaperThumbCropper();
                    $wallpaperEditModal.modal('show');
                }
            },
            {
                extend: 'selected',
                text: 'Delete',
                action: function(e, dt, node, config) {
                    $wallpaperDeleteModal.modal('show');
                }
            },
            {
                extend: 'selected',
                text: 'Move',
                action: function(e, dt, node, config) {
                    $wallpaperMoveModal.modal('show');
                }
            },
            {
                extend: 'selected',
                text: 'Publish',
                action: function(e, dt, node, config) {
                    $wallpaperPublishModal.modal('show');
                }
            },
            {
                text: 'Scan',
                action: function(e, dt, node, config) {
                    $galleryScanModal.modal('show');
                }
            }
        ],
        displayLength: 10
    });

    // Gallery scan
    var pending;
    var inProgress;
    var gallery_id; // For process, move
    var gallery_name; // For process, move

    var $galleryScanModal = $('#scan-gallery-modal');
    var $galleryScanProgress = $galleryScanModal.find('#scan-gallery-progress');
    var $galleryScanSelect = $galleryScanModal.find('#scan-gallery-select2');
    var $galleryScanSubmitButton = $galleryScanModal.find('#scan-gallery-submit-button');
    var $galleryScanCloseButton = $galleryScanModal.find('#scan-gallery-close-button');
    var $galleryScanModalBody = $galleryScanModal.find('#gallery-scan-modal-body');

    $galleryScanModal.on('show.bs.modal', function() {
        $galleryScanProgress.css('visibility', 'hidden');
        $galleryScanSubmitButton.removeAttr('disabled');
        $galleryScanCloseButton.removeAttr('disabled');
    });

    $galleryScanSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $galleryScanCloseButton.attr('disabled', 'disabled');
        $galleryScanProgress.css('visibility', 'visible');
        submitGalleryScan();
        return false;
    });

    var filesToProcess;

    function submitGalleryScan() {
        pending = 0;
        inProgress = false;
        filesToProcess = [];
        gallery_id = $galleryScanSelect.val();
        gallery_name = $galleryScanSelect.select2('data').text;
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/wallpapers/scan',
            data: {
                'gallery_id': gallery_id
            },
            success: function(data) {
                console.log(data);
                showNotification($galleryScanModalBody, 'success', data.success);
                filesToProcess = data['new'];
                pending = filesToProcess.length;
                processFiles();
            },
            error: function(data) {
                console.log(data);
            },
            complete: function() {
                refreshSessionModal();
            }
        }, 'json');
    }

    function processFiles() {
        if (pending)
            sendProcessWallpaperRequest(filesToProcess[pending - 1]);
        else {
            $galleryScanProgress.css('visibility', 'hidden');
            $galleryScanModal.modal('hide');
        }
    }

    function sendProcessWallpaperRequest(filename) {
        if (inProgress) {
            console.log('There is already a pending upload request.');
            return false;
        }
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/wallpapers/process',
            data: {
                'gallery_id': gallery_id,
                'filename': filename
            },
            beforeSend: function() {
                inProgress = true;
            },
            success: function(data) {
                console.log(data);
                $wallpapersTable.draw(false);
            },
            error: function(data) {
                console.log(data);
                handleError(data, $galleryScanModalBody);
            },
            complete: function() {
                pending--;
                inProgress = false;
                processFiles();
                refreshSessionModal();
            }
        }, 'json');
    }

    // Wallpaper publish
    var $wallpaperPublishModal = $('#publish-wallpaper-modal');
    var $wallpaperPublishProgress = $wallpaperPublishModal.find('#publish-wallpaper-progress');
    var $wallpaperPublishSelect = $wallpaperPublishModal.find('#publish-wallpaper-select2');
    var $wallpaperPublishSubmitButton = $wallpaperPublishModal.find('#publish-wallpaper-submit-button');
    var $wallpaperPublishCloseButton = $wallpaperPublishModal.find('#publish-wallpaper-close-button');
    var $wallpaperPublishModalBody = $wallpaperPublishModal.find('#publish-wallpaper-modal-body');


    $wallpaperPublishModal.on('show.bs.modal', function() {
        $wallpaperPublishProgress.css('visibility', 'hidden');
        $wallpaperPublishSubmitButton.removeAttr('disabled');
        $wallpaperPublishCloseButton.removeAttr('disabled');
    });

    $wallpaperPublishSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $wallpaperPublishCloseButton.attr('disabled', 'disabled');
        $wallpaperPublishProgress.css('visibility', 'visible');
        submitWallpapersPublish();
        return false;
    });

    var wallpapersToPublish;

    function submitWallpapersPublish() {
        gallery_id = $wallpaperPublishSelect.val();
        gallery_name = $wallpaperPublishSelect.select2('data').text;
        var data = $wallpapersTable.rows({
            selected: true
        }).data();

        wallpapersToPublish = [];
        for (var i = 0; i < data.count(); i++) {
            wallpapersToPublish[i] = data[i]['id'];
        }

        inProgress = false;
        pending = data.count();
        publishWallpapers();
    }

    function publishWallpapers() {
        if (pending)
            sendPublishWallpaperRequest(wallpapersToPublish[pending - 1]);
        else {
            $wallpaperPublishProgress.css('visibility', 'hidden');
            $wallpaperPublishModal.modal('hide');
            $wallpapersTable.draw(false);
        }
    }

    function sendPublishWallpaperRequest(wallpaperId) {
        if (inProgress) {
            console.log('There is already a pending publish request.');
            return false;
        }
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/wallpapers/publish',
            data: {
                wallpaper_id: wallpaperId,
                gallery_id: gallery_id
            },
            beforeSend: function() {
                inProgress = true;
            },
            success: function(data) {
                console.log(data);
                $wallpapersTable.draw(false);
            },
            error: function(data) {
                console.log(data);
                handleError(data, $wallpaperPublishModalBody);
            },
            complete: function() {
                pending--;
                inProgress = false;
                publishWallpapers();
                refreshSessionModal();
            }
        }, 'json');
    }

    // Wallpaper move
    var $wallpaperMoveModal = $('#move-wallpaper-modal');
    var $wallpaperMoveProgress = $wallpaperMoveModal.find('#move-wallpaper-progress');
    var $wallpaperMoveSelect = $wallpaperMoveModal.find('#move-wallpaper-select2');
    var $wallpaperMoveSubmitButton = $wallpaperMoveModal.find('#move-wallpaper-submit-button');
    var $wallpaperMoveCloseButton = $wallpaperMoveModal.find('#move-wallpaper-close-button');
    var $wallpaperMoveModalBody = $wallpaperMoveModal.find('#move-wallpaper-modal-body');


    $wallpaperMoveModal.on('show.bs.modal', function() {
        $wallpaperMoveProgress.css('visibility', 'hidden');
        $wallpaperMoveSubmitButton.removeAttr('disabled');
        $wallpaperMoveCloseButton.removeAttr('disabled');
    });

    $wallpaperMoveSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $wallpaperMoveCloseButton.attr('disabled', 'disabled');
        $wallpaperMoveProgress.css('visibility', 'visible');
        submitWallpapersMove();
        return false;
    });

    var wallpapersToMove;

    function submitWallpapersMove() {
        gallery_id = $wallpaperMoveSelect.val();
        gallery_name = $wallpaperMoveSelect.select2('data').text;
        var data = $wallpapersTable.rows({
            selected: true
        }).data();

        wallpapersToMove = [];
        for (var i = 0; i < data.count(); i++) {
            wallpapersToMove[i] = data[i]['id'];
        }

        inProgress = false;
        pending = data.count();
        moveWallpapers();
    }

    function moveWallpapers() {
        if (pending)
            sendMoveWallpaperRequest(wallpapersToMove[pending - 1]);
        else {
            $wallpaperMoveProgress.css('visibility', 'hidden');
            $wallpaperMoveModal.modal('hide');
            $wallpapersTable.draw(false);
        }
    }

    function sendMoveWallpaperRequest(wallpaperId) {
        if (inProgress) {
            console.log('There is already a pending move request.');
            return false;
        }
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/wallpapers/move',
            data: {
                wallpaper_id: wallpaperId,
                gallery_id: gallery_id
            },
            beforeSend: function() {
                inProgress = true;
            },
            success: function(data) {
                console.log(data);
                $wallpapersTable.draw(false);
            },
            error: function(data) {
                console.log(data);
                handleError(data, $wallpaperMoveModalBody);
            },
            complete: function() {
                pending--;
                inProgress = false;
                moveWallpapers();
                refreshSessionModal();
            }
        }, 'json');
    }

    // Wallpaper delete
    var $wallpaperDeleteModal = $('#delete-wallpaper-modal');
    var $wallpaperDeleteProgress = $wallpaperDeleteModal.find('#delete-wallpaper-progress');
    var $wallpaperDeleteSubmitButton = $wallpaperDeleteModal.find('#delete-wallpaper-submit-button');
    var $wallpaperDeleteCloseButton = $wallpaperDeleteModal.find('#delete-wallpaper-close-button');
    var $wallpaperDeleteModalBody = $wallpaperDeleteModal.find('#delete-wallpaper-modal-body');

    $wallpaperDeleteModal.on('show.bs.modal', function() {
        $wallpaperDeleteProgress.css('visibility', 'hidden');
        $wallpaperDeleteSubmitButton.removeAttr('disabled');
        $wallpaperDeleteCloseButton.removeAttr('disabled');
    });

    $wallpaperDeleteSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $wallpaperDeleteCloseButton.attr('disabled', 'disabled');
        $wallpaperDeleteProgress.css('visibility', 'visible');
        submitWallpapersDelete();
        return false;
    });


    var wallpapersToDelete;

    function submitWallpapersDelete() {
        var data = $wallpapersTable.rows({
            selected: true
        }).data();

        wallpapersToDelete = [];
        for (var i = 0; i < data.count(); i++) {
            wallpapersToDelete[i] = data[i]['id'];
        }

        inProgress = false;
        pending = data.count();
        deleteWallpapers();
    }

    function deleteWallpapers() {
        if (pending)
            sendDeleteWallpaperRequest(wallpapersToDelete[pending - 1]);
        else {
            $wallpaperDeleteProgress.css('visibility', 'hidden');
            $wallpaperDeleteModal.modal('hide');
            $wallpapersTable.draw(false);
        }
    }

    function sendDeleteWallpaperRequest(wallpaperId) {
        if (inProgress) {
            console.log('There is already a pending delete request.');
            return false;
        }
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/wallpapers/delete',
            data: {
                wallpaper_id: wallpaperId
            },
            beforeSend: function() {
                inProgress = true;
            },
            success: function(data) {
                console.log(data);
                $wallpapersTable.draw(false);
            },
            error: function(data) {
                console.log(data);
                handleError(data, $wallpaperDeleteModalBody);

            },
            complete: function() {
                pending--;
                inProgress = false;
                deleteWallpapers();
                refreshSessionModal();
            }
        }, 'json');
    }

    // Wallpaper edit
    var $wallpaperEditModal = $('#edit-wallpaper-modal');
    var $wallpaperEditProgress = $wallpaperEditModal.find('#edit-wallpaper-progress');
    var $wallpaperEditId = $wallpaperEditModal.find('#wallpaper-id-input');
    var $wallpaperEditTitle = $wallpaperEditModal.find('#edit-wallpaper-title-input');
    var $wallpaperEditSlug = $wallpaperEditModal.find('#edit-wallpaper-slug-input');
    var $wallpaperEditDescription = $wallpaperEditModal.find('#edit-wallpaper-description-input');
    var $wallpaperEditTags = $wallpaperEditModal.find('#edit-wallpaper-tags');
    var $wallpaperEditLicense = $wallpaperEditModal.find('#edit-wallpaper-license-select2');
    var $wallpaperEditAuthor = $wallpaperEditModal.find('#edit-wallpaper-author-input');
    var $wallpaperEditAuthorLink = $wallpaperEditModal.find('#edit-wallpaper-author-link-input');
    var $wallpaperEditPublished = $wallpaperEditModal.find('#edit-wallpaper-published-switch');
    var $wallpaperEditFeatured = $wallpaperEditModal.find('#edit-wallpaper-featured-switch');
    var $wallpaperImageCropper = $wallpaperEditModal.find('#wallpaper-image-cropper');
    var $wallpaperImageCropperPreview = $wallpaperImageCropper.find('#wallpaper-cropit-preview');
    var $wallpaperEditSubmitButton = $wallpaperEditModal.find('#edit-wallpaper-submit-button');
    var $wallpaperEditCloseButton = $wallpaperEditModal.find('#edit-wallpaper-close-button');
    var $wallpaperEditModalBody = $wallpaperEditModal.find('#edit-wallpaper-modal-body');


    $wallpaperEditTags.magicSuggest({
        method: 'get',
        dataUrlParams: { 'suggest': true },
        data: '/dashboard/tags',
        queryParam: 'filter_n',
        displayField: 'name',
        valueField: 'name',
        vregex: /^[A-Za-z0-9-\s]{1,20}$/,
        useCommaKey: true,
        hideTrigger: true,
        selectFirst: true,
        allowDuplicates: false,
        minChars: 2,
        highlight: false,
        maxSelection: 10,
        placeholder: '',
        cls: 'form-group form-group-default',
        selectionCls: 'tag label label-info pull-left',
        dom: '<label for="wallpaper_tags">Tags</label>'
    });

    $wallpaperEditModal.on('show.bs.modal', function() {
        $wallpaperEditProgress.css('visibility', 'hidden');
        $wallpaperEditSubmitButton.removeAttr('disabled');
        $wallpaperEditCloseButton.removeAttr('disabled');
        var data = $wallpapersTable.row({
            selected: true
        }).data();
        $wallpaperEditId.val(data['id']);
        $wallpaperEditTitle.val(data['title']);
        $wallpaperEditSlug.val(data['slug']);
        $wallpaperEditDescription.val(data['description']);
        $wallpaperEditTags.magicSuggest({}).setSelection(JSON.parse(data['tags']));
        $wallpaperEditLicense.select2('val', data['license']);
        $wallpaperEditAuthor.val(data['author']);
        $wallpaperEditAuthorLink.val(data['author_link']);
        initSwitch($wallpaperEditPublished, (data['published'] == 'Yes') ? 1 : 0);
        initSwitch($wallpaperEditFeatured, (data['featured'] == 'Yes') ? 1 : 0);

        //Cropit
        var extra = data['extra'];
        var imagePath = '/storage/' + extra['gallery_root'] + extra['gallery_folder'] + '/' + data['filename'] + '.jpg';
        var thumbnail_xhr = new XMLHttpRequest();
        thumbnail_xhr.open('GET', imagePath, true);
        thumbnail_xhr.responseType = 'arraybuffer';
        thumbnail_xhr.onload = function(e) {
            if (this.status == 200) {
                var uInt8Array = new Uint8Array(this.response);
                var i = uInt8Array.length;
                var binaryString = new Array(i);
                while (i--) {
                    binaryString[i] = String.fromCharCode(uInt8Array[i]);
                }
                var data = binaryString.join('');
                var base64 = 'data:image/jpeg;base64,' + window.btoa(data);
                $wallpaperImageCropper.cropit('imageSrc', base64);
            }
        };
        thumbnail_xhr.send(null);
    });

    $wallpaperEditSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $wallpaperEditCloseButton.attr('disabled', 'disabled');
        $wallpaperEditProgress.css('visibility', 'visible');
        submitWallpaperEdit();
        return false;
    });

    function initWallpaperThumbCropper() {
        var data = $wallpapersTable.row({
            selected: true
        }).data();
        var extra = data['extra'];
        var thumbWidth = extra['thumb_width'];
        var thumbHeight = extra['thumb_height'];
        $wallpaperImageCropper.cropit({
            $preview: $wallpaperImageCropperPreview,
            width: thumbWidth,
            height: thumbHeight,
            maxZoom: 1,
            onZoomChange: function(data) {
                console.log(data);
            },
            onOffsetChange: function(data) {
                console.log(data);
            },
            onImageLoaded: function() {
                var data = $wallpapersTable.row({
                    selected: true
                }).data();
                var params = data['thumbnail_params'];
                $wallpaperImageCropper.cropit('zoom', params['zoom']);
                $wallpaperImageCropper.cropit('offset', { x: parseFloat(params['x']), y: parseFloat(params['y']) });
            }
        });
    }

    function submitWallpaperEdit() {
        var params = $wallpaperImageCropper.cropit('offset');
        params['zoom'] = $wallpaperImageCropper.cropit('zoom');
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/wallpapers/edit',
            data: {
                wallpaper_id: $wallpaperEditId.val(),
                wallpaper_title: $wallpaperEditTitle.val(),
                wallpaper_slug: $wallpaperEditSlug.val(),
                wallpaper_description: $wallpaperEditDescription.val(),
                wallpaper_tags: JSON.stringify($wallpaperEditTags.magicSuggest({}).getSelection()),
                wallpaper_license: $wallpaperEditLicense.select2('val'),
                wallpaper_author: $wallpaperEditAuthor.val(),
                wallpaper_author_link: $wallpaperEditAuthorLink.val(),
                wallpaper_published: getSwitchValue($wallpaperEditPublished),
                wallpaper_featured: getSwitchValue($wallpaperEditFeatured),
                wallpaper_thumbnail_params: params,
                //wallpaper_thumbnail: $wallpaperImageCropper.cropit('export', { type: 'image/jpeg', quality: 1 })
            },
            success: function(data) {
                console.log(data);
                showNotification($notificationsHolder, 'success', data.success);
                $wallpaperEditModal.modal('hide');
                $wallpapersTable.draw(false);
            },
            error: function(data) {
                console.log(data);
                handleError(data, $wallpaperEditModalBody);
            },
            complete: function() {
                refreshSessionModal();
                $wallpaperEditProgress.css('visibility', 'hidden');
                $wallpaperEditSubmitButton.removeAttr('disabled');
                $wallpaperEditCloseButton.removeAttr('disabled');
            }
        }, 'json');
    }

    // Tags datatable
    var $tagsTable = $('#tagsTable').DataTable({
        dom: 'Bfrtip',
        serverSide: true,
        ajax: {
            url: '/dashboard/tags'
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name', searchable: true },
            { data: 'slug', name: 'slug' },
            { data: 'created_by', name: 'created_by' },
            { data: 'count', name: 'count' },
            { data: 'published', name: 'published' }
        ],
        select: {
            style: 'os'
        },
        buttons: [
            'selectAll',
            'selectNone',
            {
                extend: 'selectedSingle',
                text: 'Edit',
                action: function(e, dt, node, config) {
                    $tagEditModal.modal('show');
                }
            },
            {
                extend: 'selected',
                text: 'Delete',
                action: function(e, dt, node, config) {
                    $tagsDeleteModal.modal('show');
                }
            }
        ],
        displayLength: 10
    });

    // Tag edit
    var $tagEditModal = $('#edit-tag-modal');
    var $tagEditId = $tagEditModal.find('#tag-id-input');
    var $tagEditName = $tagEditModal.find('#edit-tag-name-input');
    var $tagEditPublished = $tagEditModal.find('#edit-tag-published-switch');
    var $tagEditProgress = $tagEditModal.find('#edit-tag-progress');
    var $tagEditSubmitButton = $tagEditModal.find('#edit-tag-submit-button');
    var $tagEditCloseButton = $tagEditModal.find('#edit-tag-close-button');
    var $tagEditModalBody = $tagEditModal.find('#edit-tag-modal-body');

    $tagEditSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $tagEditCloseButton.attr('disabled', 'disabled');
        $tagEditProgress.css('visibility', 'visible');
        submitTagEdit();
        return false;
    });

    $tagEditModal.on('show.bs.modal', function() {
        var data = $tagsTable.row({
            selected: true
        }).data();
        $tagEditId.val(data['id']);
        $tagEditName.val(data['name']);
        initSwitch($tagEditPublished, (data['published'] == 'Yes') ? 1 : 0);
        $tagEditProgress.css('visibility', 'hidden');
        $tagEditSubmitButton.removeAttr('disabled');
        $tagEditCloseButton.removeAttr('disabled');
    });

    function submitTagEdit() {
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/tags/edit',
            data: {
                tag_id: $tagEditId.val(),
                tag_name: $tagEditName.val(),
                tag_published: getSwitchValue($tagEditPublished),
            },
            success: function(data) {
                console.log(data);
                showNotification($notificationsHolder, 'success', data.success);
                $tagEditModal.modal('hide');
                $tagsTable.draw(false);
            },
            error: function(data) {
                console.log(data);
                handleError(data, $tagEditModalBody);
            },
            complete: function() {
                $tagEditProgress.css('visibility', 'hidden');
                refreshSessionModal();
            }
        }, 'json');
    }

    // Tag delete
    var $tagsDeleteModal = $('#delete-tag-modal');
    var $tagsDeleteProgress = $tagsDeleteModal.find('#delete-tag-progress');
    var $tagsDeleteSubmitButton = $tagsDeleteModal.find('#delete-tag-submit-button');
    var $tagsDeleteCloseButton = $tagsDeleteModal.find('#delete-tag-close-button');

    $tagsDeleteSubmitButton.on('click touchend', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $tagsDeleteCloseButton.attr('disabled', 'disabled');
        $tagsDeleteProgress.css('visibility', 'visible');
        submitTagDelete();
        return false;
    });

    $tagsDeleteModal.on('show.bs.modal', function() {
        $tagsDeleteProgress.css('visibility', 'hidden');
        $tagsDeleteSubmitButton.removeAttr('disabled');
        $tagsDeleteCloseButton.removeAttr('disabled');
    });

    function submitTagDelete() {
        var data = $tagsTable.rows({
            selected: true
        }).data();
        var tagsCount = data.count();
        var tags = [];
        for (var i = 0; i < tagsCount; i++) {
            tags[i] = data[i]['id'];
        }
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/tags/delete',
            data: {
                tags: tags
            },
            success: function(data) {
                console.log(data);
                showNotification($notificationsHolder, 'success', data.success);
                $tagsTable.draw(false);
            },
            error: function(data) {
                console.log(data);
            },
            complete: function() {
                refreshSessionModal();
                $tagsDeleteProgress.css('visibility', 'hidden');
                $tagsDeleteModal.modal('hide');
            }
        }, 'json');
    }

    // Uploads datatable
    var $uploadsTable = $('#uploadsTable').DataTable({
        dom: 'Brtip',
        serverSide: true,
        bFilter: false,
        ajax: {
            url: '/dashboard/uploads'
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'thumbnail', name: 'thumbnail', orderable: false },
            { data: 'title', name: 'title', orderable: true },
            { data: 'description', name: 'description', orderable: false },
            { data: 'uploader', name: 'uploader' },
            { data: 'gallery', name: 'gallery' },
            { data: 'tags', name: 'tags', orderable: false },
            { data: 'resolution', name: 'resolution', orderable: true },
            { data: 'compat_res', name: 'compat_res', orderable: true }
        ],
        "columnDefs": [{
                "targets": 1,
                "render": function(data) {
                    return '<span class="thumbnail-wrapper img-rounded b-rad-sm bordered b-white"><img src="' + data + '_1x.jpg" data-src-retina="' + data + '_2x.jpg"></span>';
                }
            },
            {
                "targets": 2,
                "render": function(data) {
                    if (!data.duplicate)
                        return data.title;
                    else
                        return '<span class="label label-important m-r-10">Duplicate</span>' + data.title;
                }
            },
            {
                "targets": 5,
                "render": function(data) {
                    return JSON.parse(data).name;
                }
            },
            {
                "targets": 6,
                "data": "tags",
                "render": function(data) {
                    var output = '<div class="table-tags table-cell-maxed-width">';
                    var tags = JSON.parse(data);
                    tags.forEach(function(e) {
                        var name = e['name'];
                        output += '<span class="label">' + name + '</span> ';
                    });
                    output += '</div>';
                    return output;
                }
            },
            {
                "targets": 8,
                "data": "compat_res",
                "render": function(data) {
                    var output = '<div class="table-tags table-cell-maxed-width">';
                    if (data != null) {
                        var parsed = JSON.parse(data);
                        parsed.forEach(function(item) {
                            output += '<span class="label">' + item.width + 'x' + item.height + '</span> ';
                        });
                    } else {
                        output += 'None';
                    }
                    output += '</div>';
                    return output;
                }
            }
        ],
        select: {
            style: 'os'
        },
        buttons: [{
                extend: 'selectedSingle',
                text: 'Edit',
                action: function(e, dt, node, config) {
                    var data = $uploadsTable.row({
                        selected: true
                    }).data();
                    initUploadThumbCropper();
                    $uploadEditModal.modal('show');
                }
            },
            {
                extend: 'selectedSingle',
                text: 'Discard',
                action: function(e, dt, node, config) {
                    $uploadDiscardModal.modal('show');
                }
            },
            {
                extend: 'selectedSingle',
                text: 'View',
                action: function(e, dt, node, config) {
                    viewUpload();
                }
            }
        ],
        displayLength: 10
    });


    function viewUpload() {
        var data = $uploadsTable.row({
            selected: true
        }).data();
        window.open(data['extra']['path'], '_blank');
    }

    var $uploadEditModal = $('#edit-upload-modal');
    var $uploadEditProgress = $uploadEditModal.find('#edit-upload-progress');
    var $uploadEditId = $uploadEditModal.find('#upload-id-input');
    var $uploadEditTitle = $uploadEditModal.find('#edit-upload-title-input');
    var $uploadEditSlug = $uploadEditModal.find('#edit-upload-slug-input');
    var $uploadEditDescription = $uploadEditModal.find('#edit-upload-description-input');
    var $uploadEditGallery = $uploadEditModal.find('#edit-upload-gallery-select2');
    var $uploadEditTags = $uploadEditModal.find('#edit-upload-tags');
    var $uploadEditLicense = $uploadEditModal.find('#edit-upload-license-select2');
    var $uploadEditAuthor = $uploadEditModal.find('#edit-upload-author-input');
    var $uploadEditAuthorLink = $uploadEditModal.find('#edit-upload-author-link-input');
    var $uploadEditFeatured = $uploadEditModal.find('#edit-upload-featured-switch');
    var $uploadImageCropper = $uploadEditModal.find('#upload-image-cropper');
    var $uploadImageCropperPreview = $uploadImageCropper.find('#upload-cropit-preview');
    var $uploadEditSubmitButton = $uploadEditModal.find('#edit-upload-submit-button');
    var $uploadEditCloseButton = $uploadEditModal.find('#edit-upload-close-button');
    var $uploadEditModalBody = $uploadEditModal.find('#edit-upload-modal-body');

    $uploadEditTags.magicSuggest({
        method: 'get',
        dataUrlParams: { 'suggest': true },
        data: '/dashboard/tags',
        queryParam: 'filter_n',
        displayField: 'name',
        valueField: 'name',
        vregex: /^[A-Za-z0-9-\s]{1,15}$/,
        useCommaKey: true,
        hideTrigger: true,
        selectFirst: true,
        allowDuplicates: false,
        minChars: 2,
        highlight: false,
        maxSelection: 10,
        placeholder: '',
        cls: 'form-group form-group-default',
        selectionCls: 'tag label label-info pull-left',
        dom: '<label for="wallpaper_tags">Tags</label>'
    });

    $uploadEditModal.on('show.bs.modal', function() {
        $uploadEditProgress.css('visibility', 'hidden');
        $uploadEditSubmitButton.removeAttr('disabled');
        $uploadEditCloseButton.removeAttr('disabled');
        var data = $uploadsTable.row({
            selected: true
        }).data();
        $uploadEditId.val(data['id']);
        $uploadEditTitle.val(data['title']['title']);
        $uploadEditSlug.val(data['slug']);
        $uploadEditDescription.val(data['description']);
        $uploadEditGallery.select2('val', JSON.parse(data['gallery']).id);
        $uploadEditTags.magicSuggest({}).setSelection(JSON.parse(data['tags']));
        $uploadEditLicense.select2('val', data['license']);
        $uploadEditAuthor.val(data['author']);
        $uploadEditAuthorLink.val(data['author_link']);
        initSwitch($uploadEditFeatured, 0);


        //Cropit
        var extra = data['extra'];
        var imagePath = extra['path'];
        var upload_xhr = new XMLHttpRequest();
        upload_xhr.open('GET', imagePath, true);
        upload_xhr.responseType = 'arraybuffer';
        upload_xhr.onload = function(e) {
            if (this.status == 200) {
                var uInt8Array = new Uint8Array(this.response);
                var i = uInt8Array.length;
                var binaryString = new Array(i);
                while (i--) {
                    binaryString[i] = String.fromCharCode(uInt8Array[i]);
                }
                var data = binaryString.join('');
                var base64 = 'data:image/jpeg;base64,' + window.btoa(data);
                $uploadImageCropper.cropit('imageSrc', base64);
            }
        };
        upload_xhr.send(null);
    });

    function initUploadThumbCropper() {
        var data = $uploadsTable.row({
            selected: true
        }).data();
        var extra = data['extra'];
        var thumbWidth = extra['thumb_width'];
        var thumbHeight = extra['thumb_height'];
        $uploadImageCropper.cropit({
            $preview: $uploadImageCropperPreview,
            width: thumbWidth,
            height: thumbHeight,
            maxZoom: 1
        });
    }

    $uploadEditSubmitButton.on('click touchend', function() {
        $uploadEditProgress.css('visibility', 'visible');
        var params = $uploadImageCropper.cropit('offset');
        params['zoom'] = $uploadImageCropper.cropit('zoom');
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/uploads/edit',
            data: {
                upload_id: $uploadEditId.val(),
                upload_title: $uploadEditTitle.val(),
                upload_slug: $uploadEditSlug.val(),
                upload_description: $uploadEditDescription.val(),
                upload_tags: JSON.stringify($uploadEditTags.magicSuggest({}).getSelection()),
                upload_license: $uploadEditLicense.select2('val'),
                upload_gallery: $uploadEditGallery.select2('val'),
                upload_author: $uploadEditAuthor.val(),
                upload_author_link: $uploadEditAuthorLink.val(),
                upload_featured: getSwitchValue($uploadEditFeatured),
                upload_crop_params: params
            },
            success: function(data) {
                showNotification($notificationsHolder, 'success', data.success);
                $uploadEditModal.modal('hide');
                $uploadsTable.draw(false);
            },
            error: function(data) {
                handleError(data, $uploadEditModalBody);
            },
            complete: function() {
                $uploadEditProgress.css('visibility', 'hidden');
                refreshSessionModal();
            }
        }, 'json');
    });

    var $uploadDiscardModal = $('#discard-upload-modal');
    var $uploadDiscardProgress = $uploadDiscardModal.find('#discard-upload-progress');
    var $uploadDiscardId = $uploadDiscardModal.find('#discard-upload-id-input');
    var $uploadDiscardReason = $uploadDiscardModal.find('#discard-upload-reason-select2');
    var $uploadDiscardSubmitButton = $uploadDiscardModal.find('#discard-upload-submit-button');
    var $uploadDiscardCloseButton = $uploadDiscardModal.find('#discard-upload-close-button');

    $uploadDiscardModal.on('show.bs.modal', function() {
        $uploadDiscardProgress.css('visibility', 'hidden');
        $uploadDiscardSubmitButton.removeAttr('disabled');
        $uploadDiscardCloseButton.removeAttr('disabled');
        var data = $uploadsTable.row({
            selected: true
        }).data();
        $uploadDiscardId.val(data['id']);
    });

    $uploadDiscardSubmitButton.on('click touchend', function() {
        $uploadDiscardProgress.css('visibility', 'visible');
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dashboard/uploads/discard',
            data: {
                upload_id: $uploadDiscardId.val(),
                discard_reason: $uploadDiscardReason.select2('val')
            },
            success: function(data) {
                showNotification($notificationsHolder, 'success', data.success);
                $uploadDiscardModal.modal('hide');
                $uploadsTable.draw(false);
            },
            error: function(data) {
                handleError(data, $notificationsHolder);
            },
            complete: function() {
                $uploadDiscardProgress.css('visibility', 'hidden');
                refreshSessionModal();
            }
        }, 'json');
    });


    // Notifications
    function showNotification(container, type, msg, position, style) {
        container.pgNotification({
            style: style || 'bar',
            position: position || 'top',
            type: type,
            timeout: 1500,
            message: msg
        }).show();
    }

    // Back to top
    $('#scrollToTop').click(function() {
        $('html, body').animate({ scrollTop: 0 }, 800);
        return false;
    });

    function handleError(data, container) {
        var parsedData = JSON.parse(data.responseText);
        Object.keys(parsedData).some(function(e) {
            if (parsedData.hasOwnProperty(e)) {
                showNotification(container, 'error', parsedData[e]);
                return true
            }
        });
    }

});