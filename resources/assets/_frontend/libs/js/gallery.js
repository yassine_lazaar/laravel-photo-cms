/* ============================================================
 * Gallery
 * Showcase your portfolio or even use it for an online store!
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */

$(function() {

    /* GRID
    -------------------------------------------------------------*/
    var visible = function() {
        var rect = this.getBoundingClientRect();
        /*
        var inside = (rect.top >= 0 && rect.top <= window.innerHeight && rect.left >= 0 && rect.left <= window.innerWidth) ||
            (rect.top >= 0 && rect.top <= window.innerHeight && rect.right >= 0 && rect.right <= window.innerWidth) ||
            (rect.bottom >= 0 && rect.bottom <= window.innerHeight && rect.left >= 0 && rect.left <= window.innerWidth) ||
            (rect.bottom >= 0 && rect.bottom <= window.innerHeight && rect.right >= 0 && rect.right <= window.innerWidth);
        */
        var inside = (rect.top >= 0 && rect.top <= window.innerHeight) ||
            (rect.bottom >= 0 && rect.bottom <= window.innerHeight);
        //console.log($(this));
        //console.log(inside);
        return inside;
    }

    loadVisible = function($items) {
        $items.find('img.lazy:not(.loaded)').filter(visible)
            .unveil()
            .addClass('loaded');
    }
    window.loadVisibleItems = loadVisible;

    var $gallery = $('.gallery');
    $(window).load(function() { // overlap fix
        $gallery.isotope('layout');
    });

    $gallery.isotope({
        // disable initial layout
        initLayout: false
    });

    // bind event
    $gallery.isotope('on', 'layoutComplete', function() {
        loadVisible($(".gallery-item"));
    });

    // manually trigger initial layout
    $gallery.isotope({
        itemSelector: '.gallery-item',
        layoutMode: 'masonry',
        masonry: {
            columnWidth: 300,
            gutter: 10,
            isFitWidth: true
        }
    });

    function loadOwlVisible($items) {
        $items.each(function() {
            var source = (this.clientWidth <= 350) ? this.getAttribute('data-owl-src-1x') : this.getAttribute('data-owl-src-2x');
            if (source) {
                this.setAttribute("src", source);
            }
        });
    }

    $(window).on('scroll', function() {
        loadVisible($(".gallery-item:not(.gallery-owl-item), .card-content"));
        $owlImages = $(".gallery-owl-item").find('img.lazy:not(.loaded)').filter(visible);
        loadOwlVisible($owlImages);
    });

    $('.owl-carousel').each(function() {
        var _this = this;
        var $items = $(this).children('.gallery-owl-item');
        $(this).owlCarousel({
            loop: $items.size() > 3 ? true : false,
            dots: true,
            nav: false,
            margin: 20,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                680: {
                    items: 2
                },
                1200: {
                    items: 3
                },
                1800: {
                    items: 4
                },
                2200: {
                    items: 5
                }
            },
            onInitialized: function(event) {
                    $visibleImages = $items.find('img.lazy:not(.loaded)').filter(visible);
                    loadOwlVisible($visibleImages);
                }
                /*,
                onChange: function(event) {
                    loadVisible($items);
                }
                */
        });
    });

});