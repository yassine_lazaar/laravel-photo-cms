/**
 * Created by Yassine on 03/03/2016.
 */
$(document).ready(function() {

    /* Session Control */
    initSessionCheck();

    function initSessionCheck() {
        if (isSessionCheckEnabled()) {
            $sessionModal = $('#session-modal');
            $sessionModal.find('.modal-body span').text(redirectMessage);
            defaultDelay = sessionLifetime * 60 * 1000;
            if (document.hasFocus()) {
                displaySessionModal(defaultDelay);
            }
            $sessionModal.on('show.bs.modal', function() {
                //console.log('displaying session modal at '+$.now());
                window.ajaxEnabled = false;
            });
        }
    }

    function checkSessionRequest() {
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/auth/sessioncheck',
            data: {
                request: 'sessioncheck',
            },
            success: function(data) {
                //console.log('success ' + JSON.stringify(data));
                var received_at = $.now();
                var last_activity = parseInt(data.last_activity);
                var server_timestamp = parseInt(data.server_timestamp);
                var delta_t = received_at - server_timestamp;
                var lf = sessionLifetime * 60 * 1000;
                delay = last_activity + lf - server_timestamp;
                //console.log('received_at ' + received_at);
                //console.log('delta_t ' + delta_t);
                //console.log('logged? ' + data.logged);
                //console.log('session ends at (server) ' + (last_activity + lf));
                //console.log('session ends at (client) ' + (received_at + delay));
                if (received_at > last_activity + lf || (!data.logged && logged))
                    displaySessionModal(0);
                else
                    displaySessionModal(delay);
            },
            error: function(data) {
                //console.log('error ' + JSON.stringify(data));
                if (data.responseText) {
                    var error = JSON.parse(data.responseText);
                    //console.log('error ' + JSON.stringify(error));
                    if (error.code == 1000) {
                        displaySessionModal(0)
                    }
                }
            }
        });
    }

    function sessionCheck() {
        if (isSessionCheckEnabled())
            window.checkSessionRequest = checkSessionRequest();
    }

    $(window).focus(function() {
        //console.log('focus');
        sessionCheck();
    });

    $(window).blur(function() {
        //console.log('blur');
        abortSessionCheck();
        if (typeof sessionModalDepoly !== 'undefined')
            clearTimeout(sessionModalDepoly);
    });

    $(window).on('beforeunload', function() {
        //console.log('exit');
        if (typeof sessionModalDepoly !== 'undefined')
            clearTimeout(sessionModalDepoly);
        abortSessionCheck();

    });

    $('.session-modal-btn').on('click touchend', function() {
        window.location.href = redirect;
    });

});

function isSessionCheckEnabled() {
    return (typeof(window.sessionLifetime) != 'undefined');
}

function displaySessionModal(delay) {
    console.log('delay ' + delay);
    if (isSessionCheckEnabled()) {
        sessionModalDepoly = setTimeout(function myFunction() {
            closeOverlays();
            $('#session-modal').modal({
                backdrop: 'static',
                keyboard: false
            })
        }, delay);
    }
}

function refreshSessionModal() {
    if (isSessionCheckEnabled()) {
        if (typeof sessionModalDepoly !== 'undefined')
            clearTimeout(sessionModalDepoly);
        displaySessionModal(window.defaultDelay);
    }
}

function abortSessionCheck() {
    if (window.checkSessionRequest && window.checkSessionRequest.readyState != 4) {
        window.checkSessionRequest.abort();
        window.checkSessionRequest = null;
        //console.log('aborted');
        //console.log('active: '+$.active);
    }
}

function closeOverlays() {
    // Search
    var $target = $('[data-pages="search"]').data('pg.search');
    if ($target != undefined)
        $target.toggleOverlay('hide');

    // Modals
    var $target = $('.modal:not(#session-modal)');
    if ($target != undefined)
        $target.modal('hide');
    if (typeof $select2 !== 'undefined')
        $select2.find('select').select2('close');
    if (typeof $datepicker !== 'undefined')
        $datepicker.datepicker('hide');
}